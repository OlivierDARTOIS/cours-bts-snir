# Découverte de GNU/Linux embarqué sur plateforme RaspberryPi

!!! tldr "Objectifs de ce document"

    - comprendre ce qu'est une carte **SBC** (single board computer),
    - comprendre la notion de *linux embarqué* utilisé dans l'*Internet des objets* (IOT en anglais),
    - installer un système d'exploitation linux à partir d'une image,
    - configurer un système d'exploitation linux : configuration de base, du réseau (filaire et wifi), installation et configuration de logiciels,
    - réaliser une première compilation d'un programme C++ en ligne de commande,
    - configurer la *compilation à distance* sur votre RaspberryPi avec l'EDI de Microsoft Visual Studio,
    - installer une bibliothèque de fonctions pour piloter des entrées/sorties tout ou rien,
    - réaliser un programme simple de commande d'une diode électroluminescente.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 1
    - Compétences évaluées dans ce TP
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (25%)
        - C11 - Maintenir un réseau informatique (50%)
    - Principales connaissances associées
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10
        - Outils logiciels d'évaluation, de traçabilité de l'information, de tests, d'analyse de traitement et de rapport de l'incident -> Niveau 3 : C11

## Introduction

Le raspberryPi est un **nano-ordinateur** comparé à un PC mais cependant il vous permettra de réaliser de très nombreux projets tant au niveau de la programmation que du dialogue avec des parties opératives externes. De plus son prix très modique (environ 35€) permet à n'importe quelle personne d'accéder à la découverte de GNU/Linux.

Le site officiel en anglais : <http://www.raspberrypi.org/> et le brochage : <http://pinout.xyz/>.
Vous trouverez sur ce site tout ce qu'il faut pour démarrer mais en langue anglaise.
En particulier vous téléchargerez la distribution RaspberryPi OS (distribution debian pour raspberryPi) depuis <https://www.raspberrypi.com/software/operating-systems/>.

- un journal dédié au raspberryPi en anglais : <http://www.themagpi.com/>
- un très bon site en français : <http://www.framboise314.fr/>
- et enfin le site avec lequel vous allez travailler tout au long de la découverte du raspberryPi pendant votre formation en BTS CIEL : <http://innovelectronique.fr/> plus précisément <http://innovelectronique.fr/2013/06/09/raspberrypi-a-tout-faire/>

Lors de la création de la carte SD, vous avez le choix entre deux versions :

- RaspberryPi OS *classique* : archive de ± 4Go, interface graphique, nombreux logiciels préinstallés → version utilisée par les utilisateurs standards
- RaspberryPi OS *lite* : archive de ± 600Mo, mode console, minimum vital → version pour les techniciens qui permet des personnalisations

Pour réaliser un serveur, on prendra donc naturellement la version allégée puis on installera ce dont on a besoin !

!!! note "Exercice"
    Vous allez *graver* sur une carte microSD avec l'adaptateur USB fourni, la distribution raspberryPi OS 64 bits en version *lite* en utilisant le logiciel **RaspberryPi Imager**. Ce logiciel est préinstallé sur les ordinateurs de la section de BTS, par contre il doit être lancé en mode *administrateur* (clic droit sur l'application puis Exécuter en tant que...) avec l'utilisateur **tp_rpi** et le mot de passe **tp_rpi**. L'utilisation de ce logiciel n'est pas décrite dans ce document car il est extrêmement simple. Il est décrit dans cette [vidéo](https://youtu.be/ntaXWS8Lk34) (en anglais, à faire en ESLA).

!!! danger "Depuis Septembre 2022 et uniquement sur la version **Lite**"
    La version Lite n'a plus de login et mot de passe par défaut, il faut créer un utilisateur et un mot de passe lorsque vous gravez votre carte SD avec le logiciel **RaspberryPi Imager**. Pour accéder aux options avancées, cliquez sur la roue dentée si elle est présente sinon la combinaison de touches ++ctrl+shift+x++ pour les versions plus anciennes. Dans la fenêtre qui apparaît, cochez la case *Set username and password* et complétez le champ *Username* (ciel par exemple) et *Password* (ciel). Pensez ensuite à cliquer sur le bouton *Save*.

!!! note "Exercice"
    Pour votre premier démarrage de la RPi, vous allez connecter un clavier USB et le câble HDMI libre sur votre poste. Mettez la carte SD dans le support sur la Pi puis alimentez la carte. Attendre qu'elle ait terminé le premier démarrage et que le *login* apparaisse. Identifiez-vous alors avec le login et le mot de passe que vous avez choisi (si vous avez suivi les consignes ciel/ciel).

Configuration de base du Rpi avec la commande `raspi-config` : lire l'article sur innovelectronique et modifiez les paramètres suivants :

- faire toutes les modifications nécessaires dans le menu « Localisation options » pour passer votre raspberrypi en français (la langue, le clavier, le fuseau horaire (Timezone) Europe/Paris, le wifi),
- puis dans les menus :
    - Network options : Hostname : nom du Rpi, mettre *RPi-votreNom*,
    - Interfacing options : activez le bus I2C , activez le serveur SSH.

!!! tldr "Résumé à faire valider par le professeur"
    ❑ Téléchargement de l'image RaspberryPi OS (réalisé le logiciel RaspberryPi Imager)  
    ❑ Création de la carte SD  
    ❑ Alimentation du RPi  
    ❑ Login sur la carte Rpi  
    ❑ Passage en français  
    ❑ changement du nom de votre Rpi (hostname)  
    ❑ activation du bus i2c et du serveur SSH  

<span style="color: #ff0000">**Validation professeur :**</span>

## Configuration réseau

Par défaut le RPi est en mode DHCP. Il cherche donc à obtenir son adresse IP depuis un serveur DHCP sur le réseau. Dans le lycée nous ne souhaitons pas utiliser cette configuration mais imposer une adresse IP fixe au raspberryPi.

### Schéma réseau du TP au lycée / à la maison

Vous trouverez ci-dessous les deux schémas réseaux possibles pour l'utilisation d'une carte RPi.

![Schéma réseau Introduction RPi](../img/SchemaReseauRPiCIEL.svg)

### Configuration de l’ethernet wifi en mode statique (@IP fixe)

La carte ethernet filaire sous GNU/Linux est identifiée par le nom `ethX`, **X** étant le numéro de la carte à partir de zéro. La Rpi disposant à la base d’un port ethernet, vous aurez donc une carte `eth0`. La carte ethernet sans fil est identifiée par le nom `wlanX` donc la première carte wifi aura comme nom `wlan0`.

La fondation RaspberryPi utilise comme logiciel de gestion du réseau **network manager**. Comme nous sommes sur un système sans interface graphique, vous allez utiliser la commande `nmtui` (network manager text user interface) pour configurer le réseau pour les cartes ethernet filaire et wifi :

!!! note "Exercice : modifier la configuration IP de la carte wifi"
    Lancez la commande nmtui en tapant `nmtui`. Pour se déplacer dans l'interface texte utilisez les touches de direction, la touche espace pour sélectionner une case à cocher, la touche entrée pour valider. Sélectionnez *Edit connection* puis *Add*. Sélectionnez l'option *Wifi*. Dans le champ *Profile name*, saisissez **WifiBTSCiel**, champ *Device* mettre **wlan0**, champ *SSID* mettre le nom du point d’accès wifi **BTS-SN-TP**, champ *Security* choisir **WPA & WPA2 Personnal**, champ *Password* : **TUX-BTS-SN-TP**. Passez sur le champ *IPv4 configuration* et changez le champ de **Automatic** à **Manual**. Passez ensuite sur *Show* puis sur le champ *Add* sélectionnez *Show*. Saisir l'@IP `192.168.___.___`, la *Gateway* (passerelle) `192.168.___.___` et enfin le *DNS* (Domain Name Server) `10.187.52.5`. Descendez jusqu'à *OK* pour valider votre configuration.  
    Une nouvelle connexion wifi est apparue. Revenez au menu principal avec *Back* et *Quit* pour sortir du logiciel. Vérifiez votre configuration wifi avec la commande `ip addr`. Si la configuration n'est pas appliquée, relancez la procédure en vérifiant chaque champs !  
    Pour vérifier votre connexion Internet, tapez une commande simple vers un site Internet:  `______________________________________________________________`  
    *Remarque* : cette méthode de configuration est spécifique au distribution utilisant Network Manager, la distribution Debian utilise un autre système de configuration du réseau en mode serveur (fichier `/etc/network/interfaces`).

!!! tip "Remarque"
    Vous utiliserez le même utilitaire **nmtui** pour configurer votre carte **ethernet filaire** en mode automatique (utilisant le DHCP) ou en mode statique.

<span style="color: #ff0000">**Validation professeur :**</span>

### Connexion distante par SSH

Si les tests précédents sont concluants, vous pouvez éteindre la Rpi avec la commande: `sudo poweroff`. Vous pouvez alors déconnecter le clavier USB et le câble HDMI. Vous allez maintenant vous connecter à distance depuis votre poste Windows à l’aide du logiciel **ssh**. La connexion est chiffrée ainsi personne ne peut intercepter vos commandes. Les connexions non chiffrées sont à proscrire (par exemple : la commande *telnet*).

!!! note "Exercice"
    Comme vous êtes connecté à un point d’accès wifi qui fait aussi routeur, vous n'êtes pas sur le même réseau que vos postes Microsoft Windows (voir schéma réseau plus haut dans ce document). Il y aura donc pour l'instant un peu de *magie* pour que vous remontiez vers votre carte RPi (cette *magie* sera étudiée plus tard avec un TP sur les routeurs wifi). En attendant vous allez vous connecter par ssh en précisant un *port de connexion* (port: `_______`) donné par le professeur présent. La commande à saisir dans une fenêtre de commande Windows (lancez un **cmd**) sera de la forme `ssh  -p port nom_utilisateur_rpi@ip_routeur_sans_fil`. Votre commande de connexion : `______________________________________________________________` Testez cette connexion à distance, vous ne devrez plus utiliser une autre méthode pour vous connecter sur votre RPi.

### Mise à l'heure automatique

!!! note "Exercice"
    Pour éditez des fichiers textes, vous allez utiliser la commande **nano**. Par exemple la commande `nano test.txt` permet d'éditer le fichier *test.txt*. Pour sauvegarder le fichier en cours d'édition et quittez le logiciel vous ferez le raccourci clavier ++ctrl+x++.
    Cette partie concerne uniquement la configuration d'une RPi au lycée Turgot. C'est inutile de faire cette modification chez vous ! Il faut éditez le fichier `/etc/systemd/timesyncd.conf` avec la commande **nano* et rajoutez sous la section `[Time]` la ligne `NTP=ntp.unice.fr`. Sauvez le fichier puis redémarrez. Vérifiez ensuite que votre RPi est bien à l'heure avec la commande `date`.

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Résumé de cette partie"
    ❑ Comprendre le schéma réseau du TP et de l’installation personnelle  
    ❑ Réaliser la configuration IP de l’ethernet wifi et/ou filaire  
    ❑ Tester la configuration réseau (commandes ip addr, ping)  
    ❑ Redémarrer le Rpi (reboot), l’éteindre de manière correcte (poweroff)  
    ❑ Se connecter à distance en mode sécurisée (connexion par SSH avec le logiciel ssh sous Microsoft Windows)  
    ❑ Spécifique au lycée : mise à l’heure en modifiant un fichier de configuration, utilisation de l’éditeur nano  

## Commandes de gestion des logiciels et mise à jour

*Remarque* : vous pouvez avoir déjà traité cette partie si vous avez fait le TP sur l'introduction de Proxmox.

Vous travaillerez en ligne de commande donc vous ne pouvez pas utiliser un logiciel comme *synaptics* qui permet de gérer les paquets en mode graphique. La commande de gestion des paquets est `apt`.

Les commandes de gestion des logiciels doivent être exécuté en tant qu'utilisateur `root` (=administrateur) ou à l'aide de la commande `sudo`.

- Pour mettre à jour la liste des logiciels présents sur les dépôts : `apt update`
- Pour mettre à jour tous les logiciels installés sur votre RPi : `apt upgrade`
- Pour chercher un logiciel dans la liste des dépôts : `apt search` *`mot_clé`*
- Pour installer un logiciel depuis les dépôts d'internet : `apt install` *`nom_du_paquet`*
- Pour dé-installer un logiciel de la carte SD : `apt remove --purge` *`nom_du_paquet`*
- Pour libérer de la place sur la carte SD après une mise à jour : `apt clean` et `apt autoremove`

!!! note "Exercice commandes d'installation et test d'un serveur web"
    Actualisez la liste des paquets puis lancez une mise à jour: `________________`  
    Installez le paquet `build-essential` qui vous fournira entre autre le compilateur g++ (normalement il doit déjà être installé): `________________`  
    Testez celui-ci en lançant : `g++ -v` puis donnez la version installée : `_____________`  
    Installez le serveur web `apache2`: `________________`  
    Testez que celui-ci est fonctionnel depuis un navigateur sous Microsoft Windows. ATTENTION le port de connexion vers ce serveur web sera celui donné par le professeur : `________________`  
    La page d'accueil par défaut affiche: `_____________`  
    Modifiez la page d'accueil de votre serveur Apache pour qu'il affiche votre nom et prénom : éditez le fichier `index.html` situé dans `/var/www/html`

!!! note "Exercice installation du langage PHP et tests"
    Installez le langage PHP dans sa version la plus récente: `_____________`  
    Créez à l'aide de **nano** dans `/var/www/html` un fichier `index2.php` avec comme contenu `<?php phpinfo(); ?>` : commande `_________________________________________`. Ouvrez ce fichier depuis votre navigateur web sous Windows. Que permet de vérifier ce test: `_____________`  

!!! note "Exercice : transférer un fichier html existant avec le logiciel WinSCP"
    Avant de transférer un fichier avec le logiciel winSCP, il faut modifier les droits du répertoire `/var/www/html' sinon vous n'avez pas les droits d'écriture (la gestion des droits sera abordée dans le TP Proxmox 2). Pour cela, tapez la commande suivante :
    
    ```console
    sudo chmod 777 /var/www/html
    ```
    
    Transférez un fichier (par exemple un de ceux que vous avez produit lors des Tps HTML/Javascript/PHP) vers `/var/www/html` avec le logiciel **WinSCP** sous Windows, il faudra juste faire attention au port de connexion.

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Résumé de cette partie"
    ❑ Pour gérer l'installation/désinstallation des logiciels : en mode graphique utilisez le logiciel synaptics, en mode texte la commande `apt`  
    ❑ Pour maintenir à jour votre distribution Debian : `sudo apt update` suivi de `sudo apt upgrade`  
    ❑ Pour installer un compilateur C/C++, il suffit d'installer le pseudo paquet `build-essential`  
    ❑ Si vous créez des pages web, il suffit de les rajouter dans le répertoire `/var/www/html` après avoir installé Apache

## Mise en place de l'environnement de développement

### Test de la chaîne de compilation : premier programme C/C++

Éditez avec le logiciel *nano* le fichier *hello.cpp* avec le contenu suivant :

```cpp linenums="1"
#include <iostream>
using namespace std;

int main()
{
   cout << "Compilation ARM sur Rpi OK" << endl;
   return 0;
}
```

Sauvegardez le fichier puis compilez-le :
```console
g++ -o hello hello.cpp -Wall
```

Exécution en tapant la commande :
```console
./hello
```

Résultat de l'exécution du programme, on appelle aussi cela une trace d'exécution :
`____________________`.

!!! tldr "Exercice : explications des paramètres de la ligne de commande"
    - `-o hello` :
    - `-Wall` :
    - `hello.cpp` :

!!! tldr "Exercice : explications du code ligne à ligne"
    |ligne|commentaire (une seule ligne, précis et concis)|
    |:---:|:---|
    |1||
    |2||
    |4||
    |5,8||
    |6||
    |7||

Modifiez le programme pour qu'il affiche votre nom.  
Donnez le résultat de la commande : `file hello`  
`_____________________`  
Donnez la taille du fichier hello en octets (commande `ls -l`) :  
Tapez la commande suivante : `strip hello` puis donnez à nouveau la taille du fichier : `_________`  
Rôle de la commande strip : `______________________`

<span style="color: #ff0000">**Validation professeur :**</span>

### Configuration de Microsoft Visual Studio 2019+ pour une compilation sur cible distante

Développer avec nano en ligne de commande sous GNU/Linux reste possible mais relativement lourd. Nous allons donc utiliser un EDI (Environnement de Développement Intégré) sur le poste Microsoft Windows. Ici ce sera Microsoft Visual 2019+ mais il existe d'autres EDI (IDE en anglais) très répandu comme Eclipse, QtCreator, Code::Blocks, etc...

!!! tip "Compte pour l'utilisation de Microsoft Visual Studio Community Edition"
    Lorsque vous lancez Visual Studio, il vous demande de vous connecter avec une adresse de messagerie. Si vous ne voulez pas utiliser votre compte, vous utiliserez le compte suivant :  
    login : btssnir87@outlook.fr    mdp : TUX-BTS-SN-TP

Téléchargez depuis le site de Microsoft la version Visual Studio Community Edition 2019/2022 puis l'installez sur votre ordinateur personnel. Dans la section de BTS c’est inutile car il est déjà installé.
Nous allons mettre en place le développement sur cible distante (remote development en anglais). Coté Rpi, installez les logiciels : gdb, gdbserver, zip, rsync.

Lancez Visual Studio 2019+, connectez-vous s’il vous le demande avec le compte ci-dessus.
Cliquez sur *Créer un projet* puis dans la barre de recherche tapez *console c++ linux*. Cliquez alors sur le modèle *Application console pour LINUX* puis bouton Suivant. Vous pouvez lire les articles suivants <https://docs.microsoft.com/fr-fr/cpp/linux/download-install-and-setup-the-linux-development-workload?view=vs-2019> et <https://docs.microsoft.com/fr-fr/cpp/linux/create-a-new-linux-project?view=vs-2019> pour avoir plus de détails.

Donnez un nom à votre projet puis bouton Suivant. L’interface de Visual Studio se lance.
Double-cliquez sur le fichier *main.cpp* dans l’explorateur de solution, le code apparaît.
Modifiez votre code source, choisissez la **compilation sur architecture ARM** à coté de *Debug* (barre d’icône en haut de la fenêtre) puis lancez la génération de la solution : menu *Générer* puis *Générer la solution*.

Une fenêtre s’ouvre, vous devez saisir les paramètres de connexion à votre Rpi :

- nom d’hôte : l’adresse ip de votre Rpi pour ce TP,
- port : le port sur lequel écoute le serveur SSH : 22 normalement mais **ici le port donné par le professeur en début de TP**,
- nom d’utilisateur : à priori *ciel* sauf si vous avez créé un autre utilisateur,
- Type d’authentification : mot de passe,
- Mot de passe : le mot de passe par défaut (raspberry) ou votre mot de passe si vous l’avez changé.

Connectez-vous sur la cible par ssh et exécutez le programme que vous venez de compiler. La commande `cd` permet de changer de répertoire. La commande `ls` affiche les fichiers contenus dans le répertoire.
L'exécutable se trouve dans le répertoire `home` de l’utilisateur *pi* généralement dans un répertoire `projects` puis un sous répertoire qui porte le nom de la *solution* (le nom de votre projet dans Visual Studio). L'exécutable se trouve dans le sous répertoire `bin`, `ARM` puis `Debug` ou `Release` en fonction du mode de compilation. Votre exécutable se termine par l’extension `.out` et s'exécute par `./nom_projet.out`.

Si le projet utilise des bibliothèques externes : par exemple pthread, serial, wiringPi, etc., il faut lire la page qui explique comment rajouter cela dans les options de compilation du projet:
<https://docs.microsoft.com/fr-fr/cpp/linux/configure-a-linux-project?view=vs-2019>

Solution (exemple pour la bibliothèque **pigpio**) : Il faut rajouter dans les paramètres du projet (clic droit sur le nom du projet dans l’explorateur de solution puis *propriétés*), cliquez sur l’item *Editeur de liens* puis *Entrées*. Rajouter dans le champ *Dépendances de bibliothèque* le mot **pigpio** puis valider ces changements en cliquant sur le bouton *OK*.

Si l’autocomplétion dans VisualStudio ne fonctionne pas sur votre projet Linux (touches ++ctrl+space++ pour compléter un mot clé lorsque vous tapez votre code), il faut suivre la procédure suivante tirée du site de Microsoft ([lien](https://docs.microsoft.com/fr-fr/cpp/linux/configure-a-linux-project?view=msvc-170)) :

> Pour gérer votre cache d’en-tête, accédez aux options Outils, Gestionnaire des connexions à distance d’en-têtes puis IntelliSense Manager. Pour mettre à jour le cache d’en-têtes après avoir effectué des changements sur votre ordinateur Linux, sélectionnez la connexion à distance, puis sélectionnez Mettre à jour. Sélectionnez Supprimer pour supprimer les en-têtes tout en conservant la connexion. Sélectionnez Explorer pour ouvrir le répertoire local dans l’Explorateur de fichiers.

<span style="color: #ff0000">**Validation professeur :**</span>

## Présentation, compilation et mise en œuvre de la bibliothèque piGpio

La bibliothèque [piGpio](http://abyz.co.uk/rpi/pigpio/) permet de piloter les broches d’entrées/sorties (General Purpose Input Output : GPIO) de la carte raspberryPi. Elle permet aussi d’utiliser les bus **i2c** et **spi** ainsi que la **voie série**. Le problème avec cette bibliothèque est que vos exécutables devront être lancés en mode *root* donc préfixé avec la commande sudo !

!!! tldr "Exercice"
    Connectez-vous avec putty sur votre RPi3/4 puis installez le paquet *pigpio*.

!!! faq "Que permet de faire la commande `raspi-gpio get` ?"
    Réponse:

La numérotation des GPIOs est celle du processeur Broadcom. C’est donc la même numérotation que sur le site <http://pinout.xyz>.

Saisissez le code suivant dans un nouveau projet Visual Studio, puis testez-le

```cpp linenums="1"
#include <iostream>
#include <pigpio.h>

using namespace std;

const int DEL = 18;

int main()
{
    cout << "Clignotement DEL" << endl;
    gpioInitialise();

    while (true) {
            gpioWrite(DEL, 1);
            gpioSleep(PI_TIME_RELATIVE, 1, 0);
            gpioWrite(DEL, 0);
            gpioSleep(PI_TIME_RELATIVE, 1, 0);
        }

    gpioTerminate();
    return 0;
}
```

!!! faq "Comment rajouter une bibliothèque dans un projet Visual Studio"
    Une erreur de compilation (en fait à l’étape de *liens*) survient. Que faut-il rajouter au niveau des options de compilation du projet Visual Studio (observez la ligne de compilation sur <https://abyz.me.uk/rpi/pigpio/cif.html>) et revenez en arrière dans ce support pour gérer la bibliothèque **pigpio** dans un projet Linux créé dans VisualStudio 2019+.  
    Option de compilation à rajouter : `________________`

!!! tldr "Exercice"
    Câblez la DEL sur votre carte (fil noir : masse, fil rouge : gpio18). Exécutez le code précédent, qu’obtenez-vous : `_______________________________________`. A l’aide de la commande `raspi-gpio get`, déterminez si la GPIO18 est en entrée ou en sortie : `__________________`

    Corrigez le code ci-dessus en rajoutant la fonction qui permet de mettre la broche concernée en sortie. Pour cela, lisez <https://abyz.me.uk/rpi/pigpio/cif.html#gpioSetMode>. Recompilez puis relancez le programme, à nouveau qu’obtenez-vous ?: `________________`

!!! faq "Analyse de code C/C++"
    Pour répondre aux questions ci-dessous, reportez-vous à la documentation en ligne de la bibliothèque **pigpio** disponible sur <https://abyz.me.uk/rpi/pigpio/cif.html>.

    |Questions|Réponses : sur une ligne ou en fin de document|
    |:---|:---|
    |Quel est le rôle de la fonction `gpioInitialise()` ?||
    |Quel est le rôle de la fonction `gpioWrite()` ? Quels sont ces arguments ?||
    |Quelle est le prototype de la fonction `gpioSleep()` ? Précisez le type et le nom de ces arguments.||
    |A quelle fréquence clignote la DEL ? Justifiez votre réponse par rapport au code.||
    |Quel est le rôle de la fonction `gpioTerminate()` ?||
    |Pourquoi y a-t-il un `return 0` à la fin du programme ?||
    |Arrive-t-on à la ligne `return 0` ? si non pourquoi ?||
    |Comment quitte-t-on ce programme ?||

!!! tldr "Exercice"
    - Modifiez le code précédent pour qu'il fasse clignoter la del connecté sur la GPIO4
    - Modifiez votre code pour faire clignoter la del à une fréquence de 5Hz

<span style="color: #ff0000">**Validation professeur :**</span>

## Mise en œuvre de la MLI à l’aide de la bibliothèque pigpio : Application sur la variation lumineuse d’une DEL

La Modulation de Largeur d’Impulsion (MLI ou Pulse Width Modulation : PWM en anglais) est une technique couramment utilisé pour faire varier la vitesse d’un moteur à courant continu (moteur CC). Nous allons la mettre en œuvre sur une DEL afin de faire varier son intensité lumineuse. Vous constatez sur le chronogramme ci-dessous qu’il s’agit de signaux carrés.

![Chronogramme signaux MLI](../img/chronogrammeMLI.png)

!!! tldr "Exercice"

    === "Questions sur la MLI"

        Dessinez puis calculez la période et la fréquence du signal. 
        Calculez la valeur moyenne du signal dans chacun des cas ainsi que le rapport cyclique puis représentez la valeur moyenne en rouge dans chacun des cas.
        N'hésitez pas à lire cet [article](https://www.sonelec-musique.com/electronique_bases_modulation_largeur_impulsion.html).

    === "Vos réponses"
    
        - Période et Fréquence du signal :
        - Formule pour le calcul de la valeur moyenne :
        - Formule de calcul du rapport cyclique exprimé en % :

Dans la documentation technique de la bibliothèque **pigpio**, relevez les fonctions, dans le tableau ci-dessous, qui sont utilisées pour réaliser la MLI de manière matériel (ce n’est pas votre programme qui génère les signaux ci-dessus mais un module matériel à l’intérieur du circuit intégré). Vous consulterez ce [lien](https://abyz.me.uk/rpi/pigpio/cif.html#gpioPWM) de la documentation officielle.

!!! tldr "Exercice"
    |Partie *PWM*, Nom de la fonction|Rôle de la fonction|
    |---|---|
    |||
    |||
    |||
    |||

!!! tldr "Exercice"
    Proposez un programme qui fait varier l’intensité lumineuse de la DEL qui suit le *pseudo algorithme*  suivant :
    ```console
    del ← 18
    Initialise bibliothèque pigpio
    i ← 0
    pas ← 1

    tant que (vrai) faire
        gpioPWM(del, i)
        attendre 10ms
        i ← i + pas
        si (i = 255) alors pas ← -1 finsi
        si (i = 0) alors pas ← 1 finsi
    fin tant que

    ferme bibliothèque pigpio
    ```

    !!! faq "Pourquoi le test sur la variable i est égale à 255 et pas une autre valeur ?"
        Réponse:

!!! tldr "Exercice"
    Tracez ci-dessous l’évolution de la variable i dans le temps. Précisez les valeurs maximum et minimum de la variable i. Donnez la période et la fréquence de cette variable i.

    ![Evolution temporelle de la variable i](../img/mliVarITPRPi.svg)

<span style="color: #ff0000">**Validation professeur :**</span>

## Production d'une onde sinusoïdale en MLI

Industriellement on ne commande pas une del de manière linéaire mais avec une sinusoide.
Proposez un programme qui fait varier l’intensité lumineuse de la del en parcourant un tableau de 1024 valeurs d’un sinus compris entre 0 et 254.

Un tableau se déclare en faisant : `type nom_du_tableau[nb de cases]` → `int sinus[1024]`
Les cases du tableau sont alors numérotées de 0 à 1023.

!!! info "Signal sinusoïdale à produire"
    ![Signal sinusoidale](../img/signalSinusoidale.png)

Pour initialiser une case du tableau : `sinus[15] = 34;`. Lorsque vous arrivez à la fin du tableau vous repartez au début.

!!! tldr "Exercice"
    Proposez un code qui répond au cahier des charges précédent.

<span style="color: #ff0000">**Validation professeur :**</span>
