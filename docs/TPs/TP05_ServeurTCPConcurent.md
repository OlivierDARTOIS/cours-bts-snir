# Conception d'un serveur TCP concurent utilisant les sockets POSIX

!!! tldr "Objectifs de l'exercice"

    - définir ce qu'est un serveur TCP *concurrent*,
    - découvrir la programmation multitâche à l'aide de la classe `thread`,
    - savoir expliquer la différence entre processus lourd (appel système `fork`) et
    processus léger (les `threads`),
    - passer des paramètres par référence à un `thread` pour échanger des informations
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Cahier des charges

Vous souhaitez développer un serveur qui renvoie la valeur de la température et de l'hygrométrie en mode console (cf. TPs précédents) qui possède la fonctionnalité suivante : **être capable de servir simultanément plusieurs clients**.

Ce serveur **concurrent** sera plus précisément un serveur **multithreads**, c'est à dire qu'un thread principal (le *main()* en général) gère la connexion des clients puis ensuite chaque client sera confié et géré par un thread différent (un thread par client).

Vous avez déjà développer le code du client. Vous êtes chargé de développer ce serveur concurrent basé sur la bibliothèque socket et la classe thread disponible à partir de la version C++11.

## Découverte de la programmation multitâches

Lorsqu'une application est amenée à effectuer deux tâches simultanément, elle peut créer soit un processus lourd (appel `fork()` sous Unix/Linux ou `CreateProcess()` sous Microsoft Windows), soit un `thread` à qui elle confie alors la gestion d'une des deux tâches.

Un thread (appelé aussi fil d'exécution ou processus léger) représente l'exécution autonome d'un ensemble d'instructions en langage machine. Chaque processus possède au moins un thread (thread principal ou initial, généralement la fonction `main()`).

Les deux figures ci-dessous comparent l'appel d'une fonction au sein d'un programme *monothread* et au sein d'un programme *multithreads*.

![Comparaison appel de fonction monothread et multithread](../img/schemaThread.png)

Les traits pleins indiquent l'exécution des threads et le trait pointillé indique la création d'un thread. La fonction appelante est mise en attente durant l'exécution de la fonction appelée dans un programme monothread. Tandis que dans la version multithreads, la fonction appelante et la fonction appelée, qui se trouve au sein du nouveau thread, s'exécutent **simultanément**.

Quelques avantages du thread par rapport à un processus lourd :

- la création d'un thread peut être 10 à 100 fois plus rapide que la création d'un processus lourd (appel `fork()`).
- tous les threads d'une même application partagent une zone mémoire commune. Ceci facilite l'échange des données entre les threads.

A partir de la norme C++11, le langage C++ possède des nouvelles extensions. En particulier, cette norme possède une classe `thread` pour pourvoir créer des programmes multitâches. Dans la suite du TP, vous utiliserez cette classe `thread` pour la mise en œuvre des applications multithreads. La [documentation](https://en.cppreference.com/w/cpp/thread/thread) de cette classe est disponible par exemple sur le site *cppreference*.

## Exemples de mise en oeuvre des threads

Avant de concevoir le serveur TempHygro multithreads, vous allez vous familiariser avec quelques exemples qui utilisent la classe thread citées ci-dessus. **Testez** chaque programme, **observez** le résultat produit et **utilisez l'aide** pour comprendre chaque instruction.

### Création de 2 threads exécutant 2 fonctions distinctes

```cpp
#include <iostream>
#include <thread>
#include <chrono>

// Fonction exécutée par le thread 1
void traiterTache1();

// Fonction exécutée par le thread 2
void traiterTache2();

int main()
{
    // Déclaration et Création du premier thread 
    // qui appelle la fonction traiterTache1
    std::thread th1(traiterTache1);
    // Déclaration et Création du deuxième thread 
    // qui appelle la fonction traiterTache2
    std::thread th2(traiterTache2);
        
    // Le thread principal attend la fin des 2 threads créés ci-dessus
    th1.join(); 
    th2.join(); 

    std::cout << std::endl << "-------------FIN-------------" << std::endl;
    std::cin.get();
    return 0;
}

// Fonction exécutée par le thread 1
void traiterTache1()
{
    std::chrono::milliseconds dt1(14); // Création d'une pause de 14ms
    for(int i = 1; i<100; i++)
    {
        std::cout << "J'incremente : " << " --- " ; 
        std::cout << i << " ; " ;
        std::this_thread::sleep_for(dt1);
    }
}

// Fonction exécutée par le thread 2
void traiterTache2()
{
    std::chrono::milliseconds dt2(9); // Création d'une pause de 9ms
    for(int c = 1000; c > 900; c--)
    {
        std::cout << "Je decremente : " << " --- " ; 
        std::cout << c << " ; " << std::endl;
        std::this_thread::sleep_for(dt2);
    }       
}
```

Le nom du fichier précédent peut être `deuxThreadsDeuxFonctions.cpp`. Ce programme peut se compiler en ligne de commande sous Unix/Linux par :

```console
g++ -o deuxThreadsDeuxFonctions -pthread deuxThreadsDeuxFonctions.cpp
```

Dans un EDI comme VisualStudio, le fait d'inclure la classe `thread` rajoutera automatiquement la bibliothèque de gestion des threads.

### Création de 2 threads exécutant une même fonction

```cpp
#include <iostream>
#include <thread>
#include <chrono>

// Fonction exécutée par les 2 threads
void traiterTache();

int main()
{
    // Déclaration et Création du premier thread qui appelle la fonction traiterTache1
    std::thread th1(traiterTache);
    // Déclaration et Création du deuxième thread qui appelle la fonction traiterTache2
    std::thread th2(traiterTache);
        
    // Le thread principal attend la fin des 2 threads créés ci-dessus
    th1.join(); 
    th2.join();
    std::cout << std::endl << "-------------FIN-------------" << std::endl;
    std::cin.get();
    return 0;
}

// Fonction exécutée par les 2 threads
void traiterTache()
{
    std::chrono::milliseconds dt(14);
    for(int i = 1; i<100; i++)
    {
        std::cout << "ID du Thread : " << std::this_thread::get_id() << " --- " ; 
        std::cout << i << " ; " ;
        std::cout << std::endl;
        std::this_thread::sleep_for(dt);
    }
}
```

N'oubliez pas de commentez la sortie console de ce programme !

### Création d'un thread exécutant une méthode (≠ fonction)

Vous supposerez que la méthode `traiter()` de la classe `Tache` doit être exécutée en arrière plan à l'aide d'un thread. Ci-dessous vous trouverez le fichier de déclaration (.h) et de définition (.cpp) de la classe `Tache`.

```cpp
#include <iostream>
#include <thread>
#include <chrono>

class Tache
{
private:
// nombre d'itérations à effectuer
       short nbIter;
// pause entre 2 itérations en ms 
       int deltat; 
public :
       Tache(short n_nbIter, int n_deltat);
       void traiter();
};
```

```cpp
#include "tache.h"

Tache::Tache(short n_nbIter, int n_deltat)
{
    this->nbIter = n_nbIter;
    this->deltat = n_deltat;
}

void Tache::traiter()
{
    std::chrono::milliseconds dt(deltat);
    for(int i = 1; i < nbIter; i++)
    {
        std::cout << std::this_thread::get_id()
            << ' ' << i << std::endl ;
        std::this_thread::sleep_for(dt);
    }
}
```

et enfin le `main()` qui exploite cette classe `Tache`.

```cpp
#include "tache.h"

int main()
{
    // Création de 2 objets contenant la tâche à exécuter en arrière plan 
    Tache tache1(24, 55);
    Tache tache2(14, 65);

    // Création de 2 threads qui appelle la méthode 
    // traiter() de 2 objets différents
    std::thread th1(&Tache::traiter, &tache1);
    std::thread th2(&Tache::traiter, &tache2);
        
    // Le thread principal attend la fin des threads créés ci-dessus
    th1.join();
    th2.join();
    std::cout << std::endl << "-------------FIN-------------" << std::endl;

    std::cin.get();
    return 0;
}
```

Veuillez noter la syntaxe particulière de création des threads avec une méthode d'une classe. Exécutez ce code et commentez sa sortie console.

### Création d'un thread avec passage de paramètre(s)

Le code ci-dessous illustre la solution qui permet, au thread initial, de transmettre des données au thread secondaire. Par défaut les paramètres sont transmis par **valeur** à la fonction qui est exécutée par le thread secondaire.  Le mot clé **ref** est utilisé lorsqu'une donnée doit être transmise par **référence** (dans ce cas, il n'y a pas de copie et l'original est directement modifié). Le nombre de paramètres n'est pas limité. Néanmoins si vous devez plus de trois données, il serait judicieux de les encapsuler au sein d'une classe.

```cpp
#include <iostream>
#include <string>
#include <thread>
#include <chrono>

using namespace std;

// Fonction exécutée par le thread 1
void traiterTache1(int nbIter);

// Fonction exécutée par le thread 2
void traiterTache2(int nbIter, string msg);

// Fonction exécutée par le thread 3
void traiterTache3(int nbIter, string& msg);

int main()
{
    // Déclaration/Création du premier thread qui appelle la fonction traiterTache1
    // Fonction avec 1 paramètre ; le paramètre est de type primitif (int)
    thread th1(traiterTache1, 5);
        
    // Déclaration/Création du deuxième thread qui appelle la fonction traiterTache2
    // Fonction avec 2 paramètres ; le 2ème paramètre est un objet (de type string)
    string nom("BTS SNIR");
    thread th2(traiterTache2, 5, nom);

    // Déclaration/Création du troisième thread qui appelle la fonction traiterTache3
    // Fonction avec 2 paramètres ; le 2ème paramètre est transmis par référence 
    // (grâce au mot clé ref) ; la variable message sera modifiée dans la fonction
    // exécutée par le thread
    string message = "C++11";
    thread th3(traiterTache3, 5, ref(message));
        
    // Le thread principal attend la fin des 3 threads créés ci-dessus
    th1.join(); th2.join(); th3.join();

    cout << endl;
    cout << "Contenu variable nom : " << nom << endl;
    cout << "Contenu variable message : " << message << endl;
    
    cout << endl << "-------------FIN-------------" << endl;
    return 0;
}

// Fonction exécutée par le thread 1
void traiterTache1(int nbIter)
{
    chrono::milliseconds dt1(33); 
    for(int i = 1; i < nbIter; i++)
    {
        cout << "J'incremente : " << " --- " ; 
        cout << i << " ; " ;
        this_thread::sleep_for(dt1);
    }
}

// Fonction exécutée par le thread 2
void traiterTache2(int nbIter, string msg)
{
    chrono::milliseconds dt2(33); 
    for(int i = 1; i < nbIter; i++)
    {
        cout << "J'affiche : " << " --- " ; 
        cout << msg << " ; " << endl;
        this_thread::sleep_for(dt2);
    }       
}

// Fonction exécutée par le thread 3
void traiterTache3(int nbIter, string& msg)
{
    chrono::milliseconds dt2(33); 
    for(int i = 1; i < nbIter; i++)
    {
        cout << "Je concatene : " << " --- " ; 
        msg += msg + " "; 
        cout << msg << endl;    
        this_thread::sleep_for(dt2);
    }
}
```

Une nouvelle fois compilez et exécutez ce code. Commentez sa sortie.

## Développement du serveur multithreads *TempHygro*

Vous devez rendre multitâches le serveur TCP que vous avez codé au TP précédent. mais ne vous précipitez pas sur l'écriture du code serveur si vous n'avez pas compris parfaitement la création d'un thread et la technique de transmission des paramètres à la fonction exécuté par un thread.

Pour chacune des 2 premières questions, répondez sur papier et faîtes valider votre réponse (ou votre proposition) par le professeur.

1. En reprenant le séquencement du serveur itératif (monothread), identifiez les tâches qu'on doit confier à un thread, de tel sorte que le serveur (ou plus précisément le thread initial) puisse continuer à recevoir des connexions (et éventuellement à les accepter) provenant des clients.

2. Proposez l'algorithme (ou pseudo-code) de la fonction ( Ex : … gererClient(...) ) qui doit être exécutée par le thread qui gère un client après que le serveur ait accepté la connexion. Précisez les fonctions socket intervenant dans cette fonction gererClient.

3. Codez le serveur multithreads et validez son bon fonctionnement à l'aide plusieurs tests :
    - Serveur et 1 client ; tous les 2 fonctionnent sur le même poste.
    - Serveur et 2 clients ; tous les 3 fonctionnement sur le même poste. Vérifiez que le serveur répond correctement aux 2 clients indépendamment de l'ordre des connexions et vérifiez que le serveur continue à servir un client après que l'autre ait fermé sa connexion. 
    - Idem que ci-dessus, mais des clients fonctionnant sur des postes distants.
