# TP d'initiation Administration système - Partie 2

!!! tldr "Objectifs de ce document"

    - découvrir une solution de virtualisation et conteneurisation basée sur le logiciel Proxmox.
    - réaliser l'administration de base d'un serveur fonctionnant sous GNU/Linux.
    - réaliser un serveur de fichiers avec gestion des droits basé sur le logiciel Samba et des clients Microsoft Windows.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 2
    - Compétences évaluées dans ce TP
        - C06 - Valider un système informatique (25%)
        - C08 - Coder (25%)
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (25%)
    - Principales connaissances associées
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Scripts d’automatisation et d’industrialisation -> Niveau 4 : C08
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10

## Introduction

Ce TP est la suite du TP sur l'initiation à l'administration système **partie 1**.

**Diagramme d'exigences du TP** :
![Diagramme d'exigences du TP](../img/diagExigencesTPAdminSys.svg)

Dans ce TP nous allons :

- présenter l'arborescence standard d'un système GNU/Linux,
- les commandes essentiels pour utiliser au quotidien ce système ainsi que les commandes minimales d'administration,
- ajouter des groupes d'utilisateurs et des utilisateurs,
- gérer les droits standard UNIX/Linux,
- installer et configurer le logiciel SAMBA pour réaliser un serveur de fichiers d'entreprise,
- installer la commande `sudo`, se connecter à distance sans taper de mot de passe, présenter/réaliser quelques scripts d'automatisation et l'utilisation de la *crontab*.

Le serveur GNU/Linux virtuel qui fera office de serveur de fichiers sera celui que vous avez installé dans la partie 1. La machine cliente de ce serveur sera la machine virtuelle Windows que vous avez créer lors de la partie 1.

## Cahier des charges : groupes, utilisateurs et droits associés

Après analyse de la structure hiérarchique de la section de BTS CIEL, vous proposez les groupes et les utilisateurs suivants :

1. Administrateurs du domaine CIEL : les *demi-dieux* de la section. Ils ont les mêmes droits sur les machines Microsoft que les administrateurs locaux. On les appelle aussi *Administrateur du domaine*. Ils peuvent accéder à tout en particulier aux travaux des enseignants et des élèves.  
Nom du groupe : **admin**,   Groupes sec. : prof,ciel1,ciel2,    Utilisateur : **zeus**
1. Les enseignants : les *paladins* de la section CIEL. Ils ont de nombreux droits mais pas tous. Ils ont des secrets à ne divulguer qu'à d'autres paladins. Enfin ils peuvent avoir les mêmes droits que les étudiants.  
Nom du groupe : **prof**,    Groupes sec. : ciel1,ciel2,         Utilisateurs : **dartois**, **dauriac**
1. Les étudiants de deuxième année : les *padawans* de niveau 2. Ils n'ont pas de droits spéciaux sauf celui de coder et travailler...  
Nom du groupe : **ciel2**,   Groupes sec. : aucun,               Utilisateurs : **barbara**, **bertrand**
1. Les étudiants de première année : les *padawans* de niveau 1. Ils n'ont pas de droits spéciaux sauf celui d'essayer de coder et de travailler très dur...  
Nom du groupe : **ciel1**,   Groupes sec. : aucun,               Utilisateurs : **ava**, **axel**

L'arborescence que vous proposez pour partager les documents sur le réseau est la suivante :

- l'arborescence complète sera sous le répertoire `/home/samba`,
- un répertoire **public** sera accessible en lecture/écriture pour tout le monde,
- un répertoire **utilitaires** sera accessible en lecture/écriture uniquement aux membres du groupe **admin**,
- un répertoire **sujets** sera accessible en lecture/écriture aux membres du groupe **prof**, un répertoire **secret** sera accessible en lecture/écriture uniquement à **dartois**,
- un répertoire **Doc_CIEL1** sera accessible en lecture seule au groupe **ciel1** mais pas au groupe **ciel2**, en lecture/écriture pour le groupe **professeur**,
- idem pour le répertoire **Doc_CIEL2** (en adaptant les droits bien sûr).

Vous devez obtenir l'arborescence suivante :

```console
/home
├── admin
│   └── rep_zeus
├── ciel1
│   ├── rep_ava
│   └── rep_axel
├── ciel2
│   ├── rep_barbara
│   └── rep_bertrand
├── prof
│   ├── rep_dartois
│   └── rep_dauriac
└── samba
    ├── Doc_CIEL1
    ├── Doc_CIEL2
    ├── public
    ├── sujets
    └── utilitaires
```

!!! warning "Remarque importante"
    En bon administrateur Linux en devenir que vous êtes, vous décidez de passer du coté *clair* de la force et de réaliser ce cahier des charges en ligne de commande avec des commandes Unix de base. En effet vous vous rappelez l'avantage définitif de la ligne de commande sur l'interface graphique que votre maître jedi vous avez enseigné :  
    **De n'importe quel ordinateur connecté à Internet ou sur un réseau local tu prendras le contrôle à distance de ta machine par SSH**

Avant tout cela vous vous décidez à rafraîchir votre mémoire avec les quelques rappels suivants...

## Arborescence GNU/Linux normalisée

L’arborescence Linux est normalisée. Vous pouvez créer des répertoires ou bon vous semble, cependant il faut avoir une idée de l’utilité de tel ou tel répertoire.
Seul le **super-utilisateur** (**root**) peut créer des répertoires ou il le veut. Un utilisateur classique ne peut pas créer de répertoires en dehors du sien. vous pouvez consulter le [site de M. Goffinet](https://linux.goffinet.org/administration/arborescence-de-fichiers/filesystem-hierachy-standard/) qui présente la structure FHS (File Hierarchy System) utilisé sous GNU/Linux.

| Rep. | Commentaires |
|:--|:--|
|`/`| Le répertoire *racine* (ou **root**). C’est le point de départ de toute l’arborescence, tous les autres répertoires en dépendent. Vous remarquez qu’il n’y a pas de lettre de lecteur comme sous Windows. |
|`/bin`| Il contient les *commandes* (ou exécutables) fondamentaux à la gestion de Linux (en particulier en mode mono utilisateur). |
|`/boot`| Il contient tout ce qui est nécessaire au *chargeur de démarrage* (souvent **GRUB** sur PC). |
|`/dev`| Il contient des fichiers spéciaux qui pointent sur des *périphériques physiques* (*devices* en anglais) (par ex. : /dev/ttyS0 représente la première voie série de la machine). |
|`/etc`| Il contient les fichiers de *configuration* de Linux (par ex. : passwd, smb.conf, ...)
|`/sbin`| Il contient les *commandes supplémentaires* à la gestion du système accessible à tous les utilisateurs. |
|`/lib`| Il contient les *bibliothèques* partagées (équivalent des *dll* de Microsoft Windows). |
|`/tmp`| Il contient les *fichiers temporaires* du système. |
|`/mnt`| Il contient normalement les points de montage des périphériques (lecteur de DVD, clé USB, autre disque dur). Très souvent `/media` dans les distributions Linux récentes. |
|`/proc`| Il contient des informations gérées par le noyau Linux (rappel: `/proc/mdstat` pour surveiller le système RAID). |
|`/home`| Ce répertoire contient tous les répertoires des utilisateurs du système. Tous les fichiers d’un utilisateur sont stockés dans son répertoire (par ex. : `/home/prof/rep_dartois`). Si vous supprimez un utilisateur, vous supprimez du coup tous ces fichiers. |
|`/var`| Il contient des fichiers qui changent souvent comme les *journaux de l’activité système* (fichiers **log**), les files d’attente d’impression (`/var/spool`), les boîtes aux lettres des utilisateurs. |
|`/usr`| Il contient, en gros, tous les binaires utiles à tous les utilisateurs. Par exemple, l'environnement graphique, `/usr/include`, `/usr/src/linux` : contiennent tout ce qui est nécessaire pour la programmation du système et la compilation du noyau, `/usr/man` : contient les pages de manuel. Sur Debian, le répertoire `/usr/share/doc` contient les documentations de tous les paquets installés sur le système. |
|`/opt`| Il contient tous les programmes *supplémentaires*. Souvent ceux qui ne sont pas gérés par la distribution. | 
|`/root`| Il contient tous les fichiers du *super-utilisateur*. |

[//]: # (Cette ligne est la syntaxe d'un commentaire portable en markdown)
[//]: # (Une image pour illustrer ce qui précède (source Blackmore Ops) : ![Structure File Hierarchy System (FHS)](../img/fhs.png))

## Commandes GNU/Linux de base

Connectez-vous avec un compte utilisateur (votre compte perso sur votre machine virtuelle Linux par exemple) puis lancez un terminal (invite de commande). Il est courant de trouver comme *prompt* (symbole pour l'invite de commandes) **‘$’** lorsque vous êtes un utilisateur **normal** et **‘#’** lorsque vous êtes connecté en tant qu’utilisateur **privilégié** (très souvent *root*) Pour passer *super-utilisateur*, il suffit de taper la commande `su -` (le signe `-` (moins) est volontaire) et de saisir le mot de passe de l'utilisateur root. Nous verrons plus tard comme mettre en place la commande `sudo` qui permet d'éviter de passer root. Une fonction très utile est la **complétion**. Vous tapez le début d’une commande ou d’un chemin puis vous tapez sur la touche ++tab++ pour compléter automatiquement votre commande.

Attention, certaines commandes sont dangereuses pour le système si elles sont exécutées par le super utilisateur root. De plus, sur tous les systèmes Unix (donc Linux) les minuscules et les majuscules ne sont pas équivalentes (ex. : *Toto* différent de *TOTO* différent de *toto*).

### Les commandes fondamentales

Dans les commandes qui suivent, le caractère `⸱` indique un espace, il ne faut donc pas le saisir !

| Commande | Commentaires |
|:--|:--|
|`cd`| *change directory* : se déplacer dans l’arborescence. GNU/Linux utilise le *slash*: `/` pour les chemins (contrairement à Microsoft Windows qui utilise l'*antislah* : `\`). Ex.: `cd⸱/` : aller à la racine du système; `cd⸱~` : retourne directement chez vous (répertoire home). |
|`pwd`| *print working directory* :savoir où l’on se trouve dans l'arborescence du système. |
|`ls`| *list* : lister les fichiers d’un répertoire. Ex.: `ls` : liste en colonne; `ls⸱-l` : liste détaillée; `ls⸱-a` : liste les fichiers cachés, ceux commençant par un point `.`; `ls⸱m*` : liste tous les fichiers commençant par la lettre *m*. |
|`cat` ou `more` ou `tail`| visualiser un fichier. Ex.: `cat⸱.bashrc` : affiche le contenu du fichier caché *.bashrc*. `more⸱/etc/passwd` : affiche page par page le fichier des utilisateurs du système. |
|`cp`| *copy* : copier un fichier ou un répertoire. Ex.: `cp⸱test⸱/home/odartois` : copie le fichier *test* dans le répertoire */home/odartois*. `cp⸱-r⸱/home/mygale⸱/home/odartois` : copie de manière **récursive** le contenu du répertoire *mygale* dans le répertoire *odartois*. |
|`rm`| *remove* : supprimer un fichier ou un répertoire. Ex.: `rm⸱test` : efface le fichier test; `rm⸱-rf⸱/tmp` : efface **récursivement** les fichiers et répertoires contenus dans le répertoire */tmp*. Attention très dangereux, aucune possibilité de revenir en arrière. |
|`mkdir`| *make directory* : créer un répertoire. Ex.: `mkdir⸱/tmp/test` : créer le répertoire *test* dans le répertoire *tmp*. |
|`mv`| *move* : déplacer ou renommer un fichier. Ex.: `mv⸱test⸱test1` : renomme le fichier *test* en *test1*. `mv⸱perso⸱/home/odartois` : déplace le fichier *perso* dans le répertoire */home/odartois*. |
|`find`| rechercher un fichier. Ex.: `find⸱/⸱-name⸱test⸱-print` : cherche à partir de la *racine* (`/`) le fichier *test* et affiche le résultat. La commande find est relativement complexe et accepte de nombreux arguments. |
|`ln`| *link* : sert à établir des *raccourcis* vers des fichiers (liens symboliques). Ex.: `ln⸱-s⸱/etc/passwd⸱/home/odartois/motdepasse` puis faire un `ls⸱-l`. |
|`tar`| archiver et comprimer des données en préservant les droits. Ex.: `tar⸱cvzf⸱mesdocs.tgz⸱/home/odartois/archive` : réunie et comprime tout le répertoire */home/odartois/archive* pour en faire une sauvegarde sur une clé USB ou un disque dur externe. `tar⸱xzvf⸱mesdocs.tgz` : décomprime et restaure le répertoire /home. |
|`df` et `du`| connaître l’espace disque utilisé. `df` (disk free) : indique l’espace libre sur chaque périphérique du système. Ex.: `df⸱-h`. `du` (disk usage) : indique l’espace utilisé par le répertoire spécifié (et sous-répertoires). Ex.: `du⸱-h⸱/home`. |
|`exit`|Permet de se déconnecter (raccourci : ++ctrl+d++). |
|`man`| *manual* : consulter une page de manuel sur une commande. Pour lire la page de manuel de la commande *ls*, tapez `man⸱ls`. |

!!! note "Exercice"
    Déplacez-vous dans l'arborescence pour vous placez dans `/home` puis créez l'arborescence de votre futur serveur SAMBA en étant connecté en tant qu'utilisateur *root*. Vous devez recréer l'arborescence présentée dans le cahier des charges. Pour visualiser cette arborescence, il est conseillé d'installer le paquet *tree* (commande: `________________________`) puis de tapez cette commande : `tree /home`

<span style="color: #ff0000">**Validation professeur :**</span>

### les commandes d'administration fondamentales

Pour pouvoir accéder à ces commandes, il faut avoir des droits  privilégiés (généralement être root). Les commandes de gestion des paquets ne sont pas présentées ici, reportez à la première partie de ce document.

| Commande(s) | Commentaires |
|:--|:--|
|`passwd`| *password* : changer son mot de passe. Pour un utilisateur : `passwd` puis tapez l’ancien mot de passe et deux fois le nouveau. Pour *root* : il peut changer le mot de passe de n’importe quel utilisateur du système sans connaître l’ancien. Ex. : `passwd⸱olivier` : fixe le nouveau mot de passe de l’utilisateur *olivier*. |
|`shutdown` ou `poweroff`| arrête ou redémarre la machine. Ex. : `shutdown⸱–r⸱now` : redémarre la machine maintenant (now). `shutdown⸱–h⸱now` : arrête la machine maintenant. |
|`su` ou `su -`| *super user* : passer de manière temporaire *super-utilisateur* depuis n’importe quel utilisateur ou prendre l’identité de n’importe quel utilisateur si vous êtes connecté root. Ex. : `su -`: demande le mot de passe pour passer root (si vous ne l’étiez pas avant). `su⸱olivier` : prend l’identité de l’utilisateur *olivier*. |
|`groupadd` et `groupdel`| permet d'ajouter des groupes d'utilisateurs au système (Ex : `groupadd⸱ciel1`) ou de supprimer un groupe (*group delete*) (Ex : `groupdel ciel`). |
|`useradd` et `userdel`| Permet d'ajouter ou de supprimer un utilisateur dans le système. Les paramètres qui nous intéressent sont :<ul><li>`-m` : crée le répertoire de l'utilisateur spécifié par le paramètre `-d`</li><li>`-d` : indique le chemin du répertoire personnel de l'utilisateur</li><li>`-g` : liste le groupe primaire (ou principal) de l'utilisateur</li><li>`-G` : liste les groupes secondaires de l'utilisateur</li><ul>Ex : pour créer le compte de l'administrateur du réseau SAMBA (la commande est à saisir sur une ligne): `useradd⸱-m⸱-d⸱/home/administrateur/rep_admin1⸱-g⸱admin⸱-G⸱prof,snir1,snir2⸱admin1`|
|`ps` et `kill`| *process*. Ces deux commandes permettent de repérer un programme (processus) qui ne répond plus et de le *tuer* (*kill*) sans perturber le reste du système. `ps` liste les processus qui tourne sur la machine (ex. : `ps⸱ax`). Repérer le PID (Processus IDentifiant, c'est un nombre) du programme qui est bloqué. `kill` envoie un signal de fin au processus spécifié (Ex. : `kill⸱`*`n°_pid`*). Si le processus ne s’arrête pas, vous pouvez le tuer avec la commande : `kill⸱–9⸱`*`n°_pid`* |

!!! note "Exercice"
    Créez tous les groupes d'utilisateurs du cahier des charges. Exemple de commande: `________________________________`. Pour vérifier que vos groupes sont correctement créés, visualisez avec la commande `cat` le fichier `/etc/group`.  
    Créez tous les utilisateurs du cahier des charges en vous inspirant de la commande donnée ci-dessus. Pour vérifier que vos utilisateurs sont correctement créés, visualisez avec la commande `cat` le fichier `/etc/passwd` et à nouveau le fichier `/etc/group` pour l'affectation des groupes secondaires. Si vous vous trompez sur la création d'un utilisateur, effacez-le avec la commande `userdel`.  
    Fixez le mot de passe de tous vos utilisateurs avec la commande `passwd` (mot de passe identique au login).  
    Testez que vos utilisateurs peuvent s'identifier sur votre système.

<span style="color: #ff0000">**Validation professeur :**</span>

### La gestion des droits sous GNU/Linux

Comme vous êtes sur un système multi-utilisateurs, il faut attribuer des **droits** sur chaque fichier et répertoire. Lorsque le message **Permission denied** apparaît, il est fort possible que cela indique que vous n’avez pas les droits suffisants sur le fichier pour l’opération que vous demandez. Dans ce document, seul les droits Unix/Linux standards seront présentés. Si vous voulez découvrir les droits étendus (qui permettent une gestion plus fine des droits utilisateurs) reportez-vous à cet article [Documentation Ubuntu facl](https://doc.ubuntu-fr.org/acl).

Vous pouvez modifier trois caractéristiques d’un **fichier** ou d’un **répertoire** :

Pour un fichier :

- Lecture (Read = **r**) : la permission de lecture autorise à consulter le contenu d’un fichier,
- Écriture (Write = **w**) : la permission d’écriture autorise à modifier un fichier (donc aussi l'effacer),
- Exécution (Execute = **x**) : la permission d’exécution autorise à exécuter un fichier en tant que programme.

Pour un répertoire :

- Lecture : la permission de lecture autorise à consulter le contenu d’un répertoire,
- Écriture : la permission d’écriture autorise à ajouter ou à supprimer des fichiers dans le répertoire concerné,
- Exécution : la permission d’exécuter autorise à se déplacer dans le répertoire (avec la commande `cd`).

!!! warning "Remarque importante"
    En pratique les droits de lecture et exécution pour les répertoires vont de pair (modifier ensemble). Cela veut dire qu'un répertoire peut avoir comme droit : **---** : aucun, **r-x** : possibilité de rentrer dans le répertoire (x) et droit de lecture uniquement (r), **rwx** : droit d'écriture en plus (w) et **pas autre chose** !

Pour permettre le partage de la machine, Unix/Linux *range* les utilisateurs en trois catégories :

- le propriétaire du fichier ou du répertoire (user = **u**),
- le groupe d'appartenance de l'utilisateur (group = **g**)
- les autres (others = **o**).

Vous pouvez fixer les droits de lecture, écriture et exécution pour chaque catégorie d’utilisateurs. Pour illustrer ce qui vient d’être dit, tapez `ls⸱–l /usr/bin`, vous obtenez alors l’affichage suivant (extrait) :

```console
olivier@debian12:~$ ls -l /usr/bin
...
-rwxr-xr-x 1 root root        2206 10 avril  2022  zless
-rwxr-xr-x 1 root root        1842 10 avril  2022  zmore
-rwxr-xr-x 1 root root        4577 10 avril  2022  znew

```

Prenons comme exemple d'analyse la dernière ligne, la commande `znew` :

| Colonne | Commentaire |
|:---:|:---|
| `-rwxr-xr-x` | premier caractère : - : un fichier, d : un répertoire (directory), l: un lien (link) puis rwx : droits du propriétaire puis r-x : droits du groupe et enfin r-x : droits des autres |
| `1` | nombre de liens vers le fichier (pas important dans notre cas) |
| `root` | nom du propriétaire du fichier (ici l'administrateur), celui-ci a les droits rwx donc il peut tout faire sur ce fichier |
| `root` | nom du groupe (ici root). Attention à ne pas confondre nom d'utilisateur et nom du groupe même s'ils portent le même nom. Le groupe (ici r-x) a seulement le droit de lecture (r) et exécution (x), pas le droit d'écriture (w) donc impossible d'effacer le fichier par exemple. |
| `4577` | la taille en octets du fichier. Si vous voulez quelque chose de plus lisible, vous pouvez utiliser l'argument `h` (comme *human*) dans la commande `ls` : `ls -lh /usr/bin` |
| `10 avril 2022` | Date de création ou modification du fichier |
| `znew` | nom du fichier. Comme le droit d'exécution est positionné, il s'agit d'un exécutable. Pour savoir ce que fait cette commande, rappelez-vous que vous avez une commande manuel : `man znew` |

Trois commandes permettent de gérer les droits :

- `chown` : *change owner* : changement de **propriétaire** sur le répertoire ou le fichier indiqué dans la commande,
- `chgrp` : *change group* : changement de **groupe**  sur le répertoire ou le fichier indiqué dans la commande,
- `chmod` : *change mode* : changement des **droits** sur le répertoire ou le fichier indiqué dans la commande.

Exemple d'utilisation des commandes `chown` et `chgrp` (rappel : seul l'utilisateur **root** peut changer tous les droits de tous les fichiers)

- `chown olivier unFichier.txt` : le fichier `unFichier.txt` appartient désormais à l'utilisateur `olivier`,
- `chgrp prof unFichier.txt` : le fichier `unFichier.txt` appartient désormais au groupe `prof`.

Pour la commande `chmod`, nous allons uniquement présenter dans ce document la notation *octale* (pas la la représentation symbolique ugo (user, group, other)) :

| droits du propriétaire (u) | droits du groupe (g) | droits des autres (o) |
|:--:|:--:|:--:|
|r:4 w:2 x:1|r:4 w:2 x:1|r:4 w:2 x:1|

Pour fixer les droits d'un fichier ou d'un répertoire, il suffit d'additionner les valeurs pour les droits r,w et x pour chaque groupe d'utilisateurs.

Exemple d'utilisation de la commande `chmod` avec le cahier des charges suivant: un fichier `unFichier.txt` doit avoir les droits de lecture et d'écriture pour le propriétaire, le droit de lecture pour le groupe et aucun droit pour les autres :

- droits de lecture et écriture pour le **propriétaire** : r = 4, w = 2 donc 4+2 = 6,
- droit de lecture pour le **groupe** : r = 4 donc 4,
- aucun droit pour les **autres** : rien donc 0

La commande pour le fichier sera donc : `chmod 640 unFichier.txt`

!!! warning "Remarque importante"
    Rappelez-vous que le droit d'exécution (x) sur un répertoire permet de *rentrer* dans celui-ci, s'il n'est pas positionné vos utilisateurs **NE PEUVENT PAS** voir (read) ou créer de fichier (write)...

!!! note "Exercice"
    Sur un serveur de partages de fichiers, vous allez raisonner le plus souvent en groupes d'appartenance mais il faut rester cohérent sur le propriétaire du répertoire. Il y a toujours plusieurs solutions sur les droits en fonction du cahier des charges, il est donc tout à fait possible que vous n'ayez pas les mêmes droits que votre voisin. Complétez le tableau ci-dessous avec les droits et commandes associées.

Vous partez d'une situation qui peut être celle-ci pour les répertoires `/home` et `/home/samba` :

```console
olivier@debian12:~$ ls -lh /home
total 24K
drwxr-xr-x  3 root    root    4,0K  9 août  17:20 admin
drwxr-xr-x  4 root    root    4,0K  9 août  17:22 ciel1
drwxr-xr-x  4 root    root    4,0K  9 août  17:22 ciel2
drwxr-xr-x  4 root    root    4,0K  9 août  17:25 prof
drwxr-xr-x  7 root    root    4,0K  9 août  17:24 samba

olivier@debian12:~$ ls -lh /home/samba/
total 20K
drwxr-xr-x 2 root root 4,0K  9 août  17:23 Doc_CIEL1
drwxr-xr-x 2 root root 4,0K  9 août  17:23 Doc_CIEL2
drwxr-xr-x 2 root root 4,0K  9 août  17:23 public
drwxr-xr-x 3 root root 4,0K  9 août  17:24 sujets
drwxr-xr-x 2 root root 4,0K  9 août  17:23 utilitaires

```

| Répertoire | Calcul des droits et commandes associées (`chmod`, `chown` et `chgrp`) |
|:--:|:--|
|`public`| tous les droits pour tout le monde |
|`utilitaires`||
|`sujets` et `secret`||
|`Doc_CIEL1`||
|`Doc_CIEL2`||
|rep. perso| réfléchir aux droits de `/home/ciel1` et `/home/ciel1/ava` par exemple...|

<span style="color: #ff0000">**Validation professeur :**</span>

## Le serveur **SAMBA**

**SAMBA** est un logiciel libre sous licence GPL supportant le protocole SMB (Server Message Block)/CIFS (Common Internet File System). Ce protocole est employé par Microsoft pour le partage de diverses ressources (fichiers, imprimantes, etc.) entre ordinateurs équipés de Windows. SAMBA permet aux systèmes Unix/Linux d'accéder aux ressources de ces systèmes et vice-versa.

Un serveur SAMBA sur une machine Unix/Linux offre les services suivants :

- partage de fichiers,
- partage d’imprimantes aussi bien sur le serveur que sur les clients,
- assiste les clients pour parcourir le *voisinage réseau*,
- authentifie les utilisateurs qui se connecte à un *domaine Windows*,
- permet la résolution de noms par un serveur WINS et/ou par consultation de l'annuaire *Avanced Directory*,
- gère les *profils utilisateurs* sur le serveur.

Le paquetage SAMBA contient les programmes suivants :

- `smbd` : le démon smbd est responsable des ressources partagées entre le serveur et les clients. Il fournit les services de partage de fichiers, d’imprimante et le parcours du voisinage réseau. Il permet aussi l’authentification des utilisateurs.
- `nmbd` : le démon nmbd est un serveur de noms qui imite les fonctionnalités d’un serveur WINS. C’est lui qui fournit les informations sur le voisinage réseau au démon smbd,
- `smbpasswd` ou `pdbedit` : ces programmes permettent de changer le mot de passe SAMBA pour un utilisateur ou une machine. `pdbedit` permet de modifier beaucoup plus finement nombre de paramètres que `smbpasswd`. Ce programme remplacera à terme `smbpasswd`,
- `smbstatus` : ce programme permet de savoir *qui est connecté à quoi* sur le serveur SAMBA. Cette commande est très utile, pensez à l'employer,
- `testparm` : ce programme vérifie la syntaxe du fichier de configuration de SAMBA (qui se trouve dans `/etc/samba/smb.conf`),
- `net` : cette commande a sensiblement la même syntaxe que celle existant sous Windows et permet d'effectuer de nombreuses taches d'administration.

### Installation de SAMBA

Installez le logiciel SAMBA. Lors de l'installation, Debian a configuré pour vous un SAMBA fonctionnel mais qui ne correspond pas à vos attentes. Cependant vous pouvez visualiser le fichier de configuration en root avec la commande : `more⸱/etc/samba/smb.conf`. Renommez ce fichier en `smb.conf.old` à l'aide de la commande mv : `________________________________________`

Trois commandes permettent de piloter le service SAMBA (il faut être root ou utiliser la commande `sudo`):

- pour arrêter le serveur Samba : `systemctl⸱stop⸱smbd.service`
- pour relancer le serveur Samba : `systemctl⸱restart⸱smbd.service`
- pour relire le fichier de configuration sans déconnecter les clients : `systemctl⸱reload⸱smbd.service`

### Première configuration de SAMBA

Vous allez créer votre propre fichier de configuration SAMBA.  Un exemple de premier fichier de configuration est donné ci-dessous :

```console
# Fichier de configuration de SAMBA
# La section qui suit concerne les paramètres qui sont appliqués globalement
[global]
# Notre serveur SAMBA est un serveur simple
server role = standalone server
# Le nom de notre groupe de travail : remplacez les XXX par votre nom
workgroup = TP_SAMBA_XXX

# Le partage ’public’ sera accessible depuis Windows à tout le monde en lecture/écriture
[public]
# Le chemin ou se trouve notre partage du coté Linux
path = /home/samba/public
# Est-il visible dans le voisinage réseau
browseable = Yes
# Tout le monde peut lire et écrire dans ce répertoire
writeable = Yes
# Un commentaire qui apparaîtra du coté Windows
comment = Partage public
# Les droits pour ce partage : tout le monde peut créer et effacer des fichiers donc rwxrwxrwx => 0777
force create mode = 0777
force directory mode = 0777
```

!!! note "Exercice"
    Arrêtez le serveur Samba. Éditez le fichier de configuration précédent (par exemple avec nano) sous le nom `/etc/samba/smb.conf`. Les lignes qui commencent par le caractère '#' sont des commentaires, il n'est pas nécessaire de les saisir...  
    Sauvegardez le fichier. Testez la syntaxe du fichier avec la commande `testparm`. S'il y a des erreurs, corrigez-les. Relancez le serveur Samba.  
    Lancez votre machine virtuel Microsoft Windows. Si ce n'est pas déjà fait, assurez-vous que le *ping* fonctionne de la machine Windows vers la machine Linux. Vous allez parcourir le voisinage réseau sur la machine Windows pour trouver votre serveur. Si vous ne trouvez pas le voisinage réseau, lancez un explorateur et tapez *`\\addr_ip_serveur_linux`*. Essayez alors de vous connecter à un partage (à priori un seul partage est disponible : *public*).  
    Que se passe-t-il ? `_________________________________________`

Cela vient du fait que vous créez les utilisateurs Unix/Linux mais pas les utilisateurs Samba. Pour cela il va falloir avoir recours à la commande `pdbedit`. Cette commande permet de gérer finement les comptes Samba, vous utiliserez essentiellement les commandes suivantes en étant *root* :

- `pdbedit⸱-a⸱-u`*`⸱nom_utilisateur`* : permet de fixer le mot de passe d'un utilisateur,
- `pdbedit⸱-x⸱-u`*`⸱nom_utilisateur`* : permet d'effacer l'utilisateur de Samba,
- `pdbedit⸱-L⸱-u`*`⸱nom_utilisateur`* : permet de visualiser les paramètres utilisateur,
- `pdbedit⸱-L` : permet de lister tous les utilisateurs

!!! note "Exercice"
    Fixez les mots de passe Samba (les mêmes que sous Linux) de vos utilisateurs avec les commandes précédentes. Vérifiez que tous les utilisateurs ont bien un mot de passe Samba : `pdbedit -L`. Testez alors la connexion de vos utilisateurs sur le partage `public`.  
    Pour vous déconnectez de manière sûr il faut fermer toutes les fenêtres de l'explorateur Windows ouvertes, lancez un interpréteur de commande (`cmd` sous Microsoft Windows ou powershell) et tapez la commande suivante plusieurs fois : `net⸱use⸱*⸱/DELETE`. Cette commande détruit toutes les connexions résiduelles par le réseau. Vous pouvez vérifier qu'il n'y a plus de connexion coté Linux avec la commande `smbstatus`.

!!! failure "Problème de déconnexion d'un utilisateur"
    Si la connexion d'un utilisateur reste *permanente* même après une commande `net use * /DELETE`, il faut se rendre dans le panneau de configuration, compte et protection des utilisateurs, gestionnaire d'identification et supprimer Informations des identifications Windows.

### Mise en place des différents partages et du répertoire personnel de l'utilisateur

Vous allez modifier le fichier de configuration de Samba pour rajouter les partages comme défini dans le cahier des charges précédent. Vous pouvez vous servir des pages de manuel de samba disponible sur internet pour faire la recherche des différents paramètres.

!!! note "Exercice"
    Rajoutez les partages demandés :  
    **utilitaires** et **sujets** : lire la description du paramètre `valid users`,  
    **Doc_CIEL1** et **Doc_CIEL2** : lire la description des paramètres `read only` et `write list`.  
    Testez avec différents utilisateurs les différents partages pour voir si la gestion des droits d'accès est bien effective. S'il y a un problème sur un accès, très souvent il s'agit d'un problème de droits Unix/Linux donc pensez à les vérifier.

Vous allez maintenant rajouter un partage qui permet à chaque utilisateur d'avoir accès à **son répertoire personnel**. Pour cela il suffit de rajouter les lignes suivantes au fichier `/etc/samba/smb.conf` :

```console
#Ajout du partage [homes], connexion au répertoire personnel de l’utilisateur
[homes]
    browseable = No
    writeable = Yes
    comment = Répertoire personnel de %U
    valid users = %S
    create mode = 0750
    directory mode = 0750
    # Rajoutons deux lignes pour savoir qui se connecte et se déconnecte du serveur
    # Cela donne par ex.: ’[17:54:17] dartoiso déconnecté(e) depuis poste14 (192.168.0.14)’
    root preexec = /bin/sh -c ’echo "[%T] %U connecté(e) depuis %m (%I)">>/home/samba/messages’
    root postexec = /bin/sh -c ’echo "[%T] %U déconnecté(e) depuis %m (%I)">>/home/samba/messages’
```

!!! note "Exercice"
    Rajoutez le partage `homes` puis testez la connexion vers ce partage depuis votre machine virtuelle Windows. Remarquez que le nom de ce partage change en fonction de la personne connectée. Créez des dossiers et des documents sur le serveur dans le répertoire personnel de l'utilisateur. Visualisez les droits sur ces fichiers du coté Linux. Éventuellement affectez une lettre de lecteur en connectant un lecteur réseau (généralement U: comme utilisateur). Après plusieurs connexions/déconnexions, visualisez le fichier de *log* des connexions : fichier `/home/samba/messages`.

<span style="color: #ff0000">**Validation professeur :**</span>

## Administration système

### Gérer son système Debian en mettant en place la commande `sudo`

Par défaut la commande `sudo` n'est pas disponible sur Debian après l'installation si vous avez mis un mot de passe à l'utilisateur *root*. C'est un choix de Debian qui est expliqué sur ce [site](https://wiki.debian.org/fr/sudo). Vous allez rajouter la possibilité à votre utilisateur créé pendant l'installation d'utiliser la commande `sudo`.

!!! note "Exercice"
    Passez **root** puis installez le paquet *sudo*. Rajoutez votre utilisateur au groupe **sudo** avec la commande suivante : `groupmod -a -U ` *`nom_utilisateur`* `sudo`. Que fait la commande précédente : `________________________________________________`.
    Déconnectez-vous puis reconnectez-vous. Testez que la commande `sudo` est disponible : par exemple en essayant d'éditer un fichier que seul *root* peut normalement éditer : `sudo nano /etc/passwd`  
    Attention la commande `sudo` car vous pouvez faire ce que *root* peut faire et en particulier n'importe quoi !

### Comment se connecter sur un serveur sans taper de mot de passe

Les mots de passe ne sont pas, par définition, sécurisés. Les utilisateurs peuvent choisir un mot de passe qui se trouve facilement avec un dictionnaire. La solution c'est de ne plus avoir un mot de passe pour se connecter à distance mais une **signature** que le serveur authentifiera. Parmi les solutions possibles il y a les échanges de clés privés/clés publiques. Le protocole SSH peut utiliser cette technique.

Les fichiers mis en jeu sont:

- la clé privé : situé de manière générale dans ~/.ssh/id_xxx sur votre ordinateur (le signe `~` désigne le répertoire *home* de l'utilisateur),
- la clé publique : situé de manière générale dans ~/.ssh/id_xxx.pub sur votre ordinateur,
- le fichier ~/.ssh/authorized_keys : liste des clés publiques autorisées sur la machine distante pour se connecter à l’utilisateur désiré.

Il faut donc commencer par générer la paire de clés privé/publique puis copiez la clé publique vers le fichier *authorized_keys* de la machine sur laquelle vous souhaitez vous connecter. Dans cet exemple, nous allons prendre comme machine cliente la machine virtuelle sous Microsoft Windows 10 et comme machine de destination votre machine virtuelle GNU/Linux. Une présentation détaillée sur la génération des clés est disponible sur le [site de Microsoft](https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement).

!!! note "Exercice"
    Lancez votre machine virtuelle Linux si ce n'est pas déjà fait.  
    Connectez-vous sur votre machine virtuelle Windows 10 puis lancez *Windows PowerShell* (dans la barre de recherche, tapez powershell). Tapez la commande suivante: `ssh-keygen.exe -t ed25519` (l'algorithme de chiffrement *ed25519* est conseillé actuellement). Ne changez pas le fichier de destination mais notez où il se trouve (`C:\Users\Votre_Compte\.ssh\id_ed25519.pub`), tapez ++enter++. Ne mettez pas de mot de passe sur la paire de clés donc tapez deux fois la touche ++enter++.  
    Il faut maintenant transférer la clé publique vers la machine sur laquelle vous souhaitez vous connecter. Pour cela tapez la commande suivante en adaptant le *chemin de la clé publique*, le *nom d'utilisateur* et l'*adresse ip*: `scp C:\Users\Votre_Compte\.ssh\id_ed25519.pub nom_utilisateur_linux@addr_ip_machine_linux:`  
    Connectez-vous en tant qu'utilisateur sur la machine Linux. Créez le répertoire `.ssh` avec la commande `mkdir`. Vous allez copier la clé publique dans le fichier `authorized_keys` avec la commande suivante : `cat id_ed25519.pub >> .ssh/authorized_keys`. Effacez la clé publique avec une commande `rm`. Déconnectez-vous de votre machine virtuelle Linux.  
    Testez maintenant la connexion vers la machine Linux depuis votre machine Windows. Toujours sous PowerShell, tapez la commande suivante en adaptant le nom d'utilisateur et l'adresse ip : `ssh nom_utilisateur_linux@addr_ip_machine_linux`. Si tout ce passe bien vous devez vous connecter sur la machine distante sans que le système vous demande un mot de passe.  

<span style="color: #ff0000">**Validation professeur deux exercices précédents:**</span>

### Création d'un script de sauvegarde du répertoire **home**

Les données de vos utilisateurs sont primordiales, vous devez donc les sauvegarder pour éventuellement les restaurer si un problème survient (défaillance de disques, du serveur, chiffrement après un hack, etc...). Cette sauvegarde devrait être faite sur un site distant et chiffré. Le script qui suit n'est donc pas optimal mais il permet de découvrir une méthode simple de sauvegarde. Ce script est inspiré de [celui-ci](https://guide.ubuntu-fr.org/server/backup-shellscripts.html).

```console
#!/bin/bash
# Script sauvegarde du répertoire des utilisateurs

# Quels sont les répertoires à sauvegarder
sauv_reps="/home"

# Ou sont stockés les sauvegardes
dest="/sauvegarde"

# Construction du nom de fichier de la sauvegarde
jour=$(date +%A)
nom_hote=$(hostname -s)
nom_fic_sauv="$nom_hote-$jour.tgz"

# Message de début 
echo "Sauvegarde de $sauv_reps vers $dest/$nom_fic_sauv"
date
echo

# Sauvegarde des répertoires avec la commande tar
tar czf $dest/$nom_fic_sauv $sauv_reps

# Message de fin
echo
echo "Sauvegarde finie"
date

# Affichage des sauvegardes disponibles
ls -lh $dest
```

Quelques remarques sur ce script:

- déclarer une variable c'est lui donner un nom (ex: `sauv_reps`), affecter une valeur à une variable ce fait à l'aide du signe `=`. La valeur affectée peut être une chaîne ou un nombre (ex: `sauv_reps="/home"`),
- pour récupérer le contenu d'une variable on utilise le signe `$` en préfixe (ex: `$sauv_reps` renvoie la chaîne `/home`),
- pour initialiser une variable avec le contenu d'une commande on utilise les parenthèses (ex: `jour=$(date +%A)` la commande `date` renvoie le *nom du jour* courant grâce à l'option `+%A` et cette chaîne est affectée à la variable `jour`),
- la commande `tar` permet de créer des archives en les comprimant (un peu comme la commande `zip`).

!!! note "Exercice : compréhension et amélioration du script"
    Saisissez ce script (par exemple avec nano) et le sauvegarder sous le nom **sauv_home.sh**. Rendre ce script exécutable. A l'aide de la commande `sudo`, créez un répertoire à la racine du disque avec le nom **sauvegarde**. Lancez l'exécution de ce script à l'aide de la commande `sudo`, suivez son exécution, puis donnez la taille en Kilo octets de la taille de l'archive (taille de l'archive: `__________________`).  
    Modifiez le script pour qu'il sauve le répertoire `/etc` en plus du répertoire `/home` (modification: `__________________`).  
    Modifiez le script pour que le nom de fichier de la sauvegarde ne contiennent pas le jour de la semaine mais la date au format AAMMJJ (Modification de la commande `date`: `__________________`). Relancez le script et vérifiez que le nom du fichier a bien changé.  
    La compression **gzip** est tout à fait correct, cependant il existe une compression plus efficace : **bzip2**. Modifiez la commande `tar` pour utiliser la compression bzip (Commande `tar`: `__________________`). Relancez le script. Comparez la taille par rapport à la compression gzip.  
    Taille gzip: `__________________` Taille bzip: `__________________`.  
    Extraire l'archive précédemment créée vers le dossier `/tmp`. Commande: `________________________________` et vérifiez que vous retrouvez l'arborescence original à l'aide de la commande `tree`.

### Automatisation du script précédent (**crontab**)

Le script précédent permet de sauver les répertoires utilisateurs (voire plus...). Mais vous devez le lancer à la main, ce qui nécessite que vous vous connectiez au serveur. Il serait préférable que cette tâche se réalise automatiquement et de préférence la nuit lorsque le serveur est peu sollicité. Pour cela vous disposez d'un système déjà présent : la *crontab* et son démon *cron* (un démon ou *daemon* en anglais est un programme qui s'exécute en arrière plan sur votre machine).  

La syntaxe de la crontab est présentée sur le [site de Ubuntu](https://doc.ubuntu-fr.org/cron) mais il en existe de nombreux autres.

!!! note "Exercice : automatisation d'une tâche"
    Copiez votre script dans le répertoire `/usr/local/bin` à l'aide de la commande `cp` en étant *root* (commande: `__________________`).  
    En ayant lu la présentation de la crontab dans l'article internet précédent, proposer la ligne que vous allez rajouter dans la crontab pour lancer votre script tous les jours sauf samedi et dimanche à 1:53 du matin. Le chemin de la commande à exécuter est donc `/usr/local/bin/sauv_home.sh`.  
    ligne crontab: `___________________________________________`  
    Rajoutez votre ligne crontab à la crontab de l'utilisateur *root* (en effet votre script requière des droits élevés pour s'exécuter): `sudo crontab -e` (choisissez **nano** comme éditeur de texte, le paramètre `-e` signifie *éditer*), pensez à sauvegarder vos modifications. Tapez la commande : `sudo crontab -l` pour visualiser la liste des tâches pour l'utilisateur *root* (`-l` pour *lister*).  
    Le problème est que vous n'allez pas attendre que cette tâche s'effectue. Modifiez votre crontab pour qu'elle s'effectue toutes les 3 minutes.  
    nouvelle ligne crontab: `___________________________________________`  
    Vous pouvez vérifier que votre commande s'exécute toutes les trois minutes en demandant l'état du démon cron: `sudo systemctl status cron`. Si tout se passe correctement vous allez avoir une nouvelle archive toutes les 3 minutes dans votre répertoire `/sauvegarde`.

<span style="color: #ff0000">**Validation professeur deux exercices précédents:**</span>
