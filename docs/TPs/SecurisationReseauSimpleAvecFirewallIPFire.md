# Sécurisation d'un réseau simple à l'aide d'un parefeu IPFire

!!! tldr "Objectifs de ce document"

    - connaître une architecture simple de réseau mettant en jeu un parefeu pour une TPE,
    - installer un parefeu opensource IPFire et identifier les différentes zones gérées par celui-ci,
    - mettre en oeuvre différents services au niveau du parefeu,
    - s'auto évaluer avec des QCMs.
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - TitrePro AIS - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A / Titre Professionnel AIS - Tous les semestres
    - Compétences évaluées dans ce TP
        - C04 - Analyser un système informatique (20%)
        - C05 - Concevoir un système informatique (20%)
        - C06 - Valider un système informatique (50%)
        - C09 - Installer un réseau informatique (10%)
    - Principales connaissances associées
        - Acteurs de l’écosystème réglementaire, référence des bonnes pratiques : ANSSI -> Niveau 2 : C04
        - Niveaux de sécurité attendus par la solution logicielle (chiffrage, protection des communication, politique de mot de passe) -> Niveau 2 : C05
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Sécurisation des réseaux (ACL, mots de passe, pare-feu) -> Niveau 3 : C06
        - Protocoles usuels IPv4, HTTP, HTTPS,TCP/IP, Ethernet, ipV6, DNS, DHCP, SSH -> Niveau 4 : C09
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09

## Introduction
Une T.P.E. (Très Petite Entreprise) fait appel à votre Entreprise de Service du Numérique ([E.S.N.](https://fr.wikipedia.org/wiki/Entreprise_de_services_du_num%C3%A9rique)) pour revoir l'architecture de son réseau informatique suite à une intrusion due à une faille non corrigée sur le serveur de fichiers. Le schéma actuel que vous présente le directeur de la T.P.E. est donnée ci-dessous:

![Schema réseau d'une petite entreprise (T.P.E)](../img/SchemaReseauEntrepriseAPlat_TP_IPFire.svg)

Pour l'instant l'adressage des machines est réalisé manuellement et quelque fois des conflits d'adresses IP apparaissent. Le point d’accès Wifi permet aux visiteurs d'accéder à Internet mais aussi au réseau de l'entreprise. Les utilisateurs enregistrent leurs travails sur le serveur de fichiers, publient des articles sur le site web de l'entreprise, vont sur internet et envoient/reçoivent des mails par le biais du serveur de messagerie. Les clients viennent consulter le site web de l'entreprise et échangent des mails avec le personnel de l'entreprise. Enfin le routeur d’accès à Internet du F.A.I. est vieillissant et ne possède que des fonctions basiques de filtrage.

Vous devez faire évoluer l'architecture réseau pour la rendre plus sécurisée et mettre en place les services nécessaires pour faciliter la maintenance du réseau.

!!! note "Exercice"
    Sur le schéma réseau de l'entreprise et en relisant le texte de présentation de celle-ci, dessinez les flux de données **entrants** en *rouge*, les flux de données **sortants** en *bleu* et les flux de données **internes** en *vert*. On suppose que tous les matériels sont correctement connectés entre eux par des commutateurs.

## Identifier les différentes zones de sécurité du Système d'Informations (S.I.)

Vous allez découper des zones de sécurité sur le schéma réseau ci-dessous à l'aide des phrases ci-dessous:

- une zone appelée **zone démilitarisée** (*D.M.Z.*: Demilitarized Zone en anglais) doit contenir les serveurs joignables à la fois d'internet et de l'intérieur de l'entreprise. Cette zone a un niveau de risque élevé puisqu'elle est très exposée aux attaques,
- une **zone de confiance** : les utilisateurs locaux (considérés comme sûrs) enregistrent leurs documents sur le serveur de fichier. Aucun autre utilisateur ne doit accéder à cette zone d'où qu'il vienne,
- une **zone isolée du réseau** de l'entreprise qui propose un accès à internet mais étanche par rapport au reste du réseau (y compris à la DMZ),
- une **zone non fiable** : cette zone n'est pas gérée par l'entreprise.

!!! warning "Attention"
    Ce qui précède est une analyse simplifiée des différentes zones d'un S.I. et la proposition d'architecture réseau qui en découle est elle aussi simplifiée par rapport aux recommandations de l'ANSSI. Cependant elle permet une première approche d'une architecture sécurisée à moindre coût.

![Schema réseau d'une petite entreprise (T.P.E)](../img/SchemaReseauEntrepriseAPlat_TP_IPFire.svg)

<span style="color: #ff0000">**Validation professeur :**</span>

## Insérer un parefeu dans votre infrastructure réseau

Un des principes de base de la sécurité réseau est le cloisonnement des différentes parties d'un réseau qui n'ont pas lieu de communiquer entre-elles. Dans notre cas, vous avez identifié 4 zones à séparer.

### Définition d'un parefeu

Selon le journal officiel: La traduction officielle du terme anglais **firewall** est **barrière de sécurité** ou **pare-feu** ([JO99](https://www.cert.ssi.gouv.fr/information/CERTA-2006-INF-001/#jo1)). La définition qui est associée est la suivante :

*Dispositif informatique qui filtre les flux d’informations entre un réseau interne à un organisme et un réseau externe en vue de neutraliser les tentatives de pénétration en provenance de l’extérieur et de maîtriser les accès vers l’extérieur.*

Vous pourrez retenir de façon assez large qu’il s’agit d’un dispositif informatique de filtrage de protocoles réseaux (routables) et, par extension, d’un système permettant d’imposer une politique de sécurité entre plusieurs périmètres réseaux.

!!! note "Exercice"
    Complétez le tableau ci-dessous en indiquant si la phrase proposée est un avantage ou un inconvénient d'un pare-feu.

|Avantage|Proposition|Inconvénient|
|:---:|:---:|:---:|
|[ ]|Bloque les accès non autorisés et les menaces externes comme les hackers, les logiciels malveillants et les attaques DDoS|[ ]|
|[ ]|Nécessite des connaissances techniques avancées pour être correctement configuré et géré|[ ]|
|[ ]|Peut ralentir les performances du réseau en raison de la vérification et du filtrage du trafic|[ ]|
|[ ]|Nécessite des mises à jour régulières pour rester efficace contre les nouvelles menaces|[ ]|
|[ ]|Permet de définir des règles pour autoriser ou interdire certains types de trafic en fonction des adresses IP, des ports ou des protocoles|[ ]|
|[ ]|Peut intégrer des systèmes de détection et de prévention des intrusions (IDS/IPS)|[ ]|
|[ ]|Peut bloquer des communications légitimes (faux positifs), perturbant ainsi les activités normales|[ ]|
|[ ]|Un pare-feu est principalement conçu pour protéger contre les menaces externes et peut être moins efficace contre les menaces internes (comme les employés malveillants)|[ ]|
|[ ]|Offre des fonctionnalités de journalisation pour enregistrer les tentatives d'accès, ce qui est utile pour l'audit et l'analyse de sécurité|[ ]|
|[ ]|Peut améliorer les performances du réseau en bloquant le trafic indésirable et en réduisant la congestion|[ ]|

### Ajouter votre parefeu dans votre infrastructure réseau

!!! note "Exercice"
    Rajoutez dans le schéma ci-dessous le parefeu. Vous préciserez combien de cartes réseaux ce parefeu utilise. Rajoutez des commutateurs s'il y a lieu.

![Schema réseau d'une petite entreprise (T.P.E)](../img/SchemaReseauEntrepriseAjoutPareFeu_TP_IPFire.svg)

### Choix du parefeu

Le parefeu peut être *matériel* : un boîtier matériel à insérer dans une baie. Généralement cette solution est onéreuse mais performante.

Le parefeu peut être *logiciel* : dans ce cas il est installé avec un système d'exploitation et il peut fonctionner en même temps que d'autres logiciels. Cette solution est généralement moins performante qu'une solution matérielle mais nettement moins onéreuse (voire gratuite/open source).

Enfin le parefeu peut être virtualisé pour diminuer les coûts mais cela peut dégrader les performances en introduisant encore plus de latence.

!!! warning "Recommandations de l'ANSSI"
    L'ANSSI déconseille fortement l'installation d'un parefeu virtualisé ou mutualisé. Elle conseille l'achat d'une machine dédiée pour l'installation du logiciel parefeu ou un parefeu matériel !

Dans notre cas d'étude et pour que chacun puisse tester l'infrastructure proposée nous allons virtualiser l'infrastructure réseau et les différentes machines dont le parefeu à l'aide de l'hyperviseur Proxmox. Cependant si vous terminez en avance ce TP, vous pourrez mettre en oeuvre ce TP sur du matériel physique (un PC avec 5 cartes réseaux).

Le parefeu vous est imposé : **IPFire**. C'est une solution *tout-en-un* open source, performante et entièrement gérable à travers une interface web. Cette solution est donc bien adapté en BTS. Un autre parefeu logiciel bien connu **pfSense** (ou **opnSense**) sera étudié en bac+3 titre professionnel.

<span style="color: #ff0000">**Validation professeur :**</span>

## Découvrir le parefeu IPFire

Le parefeu IPFire est un parefeu open source disponible sur le site [https://www.ipfire.org/](https://www.ipfire.org/). Il permet de réaliser la plupart des opérations des parefeux commerciaux mais à un coût bien moindre. Il couvre les besoins des particuliers jusqu'aux P.M.E. Parmi [les caractéristiques](https://www.ipfire.org/about) de ce parefeu, vous pouvez lister celles que nous allons mettre en oeuvre:

- segmentation du réseau en 4 zones possibles, 2 au minimum,
- proxy web (cache, liste blanche/noire, authentifiant, ...),
- portail captif avec gestion de *coupons*,
- serveur DHCP, NTP, DNS,
- Serveur VPN : IPSec et OpenVPN.

Si vous utilisez le serveur Proxmox de la section de BTS pour installer IPFire, l'image ISO sera déjà sur le serveur. Sinon téléchargez l'image ISO sur [https://www.ipfire.org/downloads/ipfire-2.29-core190](https://www.ipfire.org/downloads/ipfire-2.29-core190) (le numéro de version est susceptible de changer, consultez le site officiel). Puis avec le logiciel [Rufus](https://rufus.ie/fr/) sous Microsoft Windows, créez une clé USB amorçable puis lancez l'installation sur le matériel **physique** (PC par exemple avec plusieurs cartes réseaux) en démarrant depuis la clé.

Avant de débuter l'installation il faut connaître la segmentation réseau choisie par les concepteurs du logiciel IPFire. Cette segmentation est faite en 4 zones auxquelles est associée une couleur.

!!! note "Comprendre la segmentation réseau du logiciel IPFire"
    Complétez le tableau ci-dessous avec les noms de zones définis précédemment puis complétez le schéma réseau en entourant, avec la bonne couleur, les zones rouge, verte, orange et bleue.

|Couleur|Définition (en anglais)|Nom de zone|
|:---:|:---||
|Rouge|this is the so-called **Untrusted segment**, i.e., the **WAN**: It encompasses all the networks outside the IPFire Appliance or, broadly speaking, the Internet, and is the source of incoming connections. This is the only zone that can not be managed: but only access to and from it can be granted or limited||
|Verte|the **internal network**, i.e., the **LAN**. This zone is the most protected one and is dedicated to the workstations and should never be directly accessed from the RED zone. It is also the only zone that by default can access the management interface||
|Orange|The **DMZ**. This zone should host the servers that need to access the Internet to provide services. It is a good practice that the ORANGE zone be the only zone directly accessible from the RED zone. Indeed, if an attacker manages to break into one of the servers, she will be trapped within the DMZ and will not be able reach the GREEN zone, making impossible for her to gain sensitive information from local machines in the GREEN zone||
|Bleue|the **WiFi zone**, i.e., the zone that should be used by wireless clients to access the Internet. Wireless networks are often not secure, so the idea is to trap by default all the wireless connected clients into their own zone without access to any other zone except RED||

![Schema réseau d'une petite entreprise avec découpage en zone de couleur pour IPFire](../img/SchemaReseauEntreprisePareFeuDecoupageZone_TP_IPFire.svg)

!!! tip "Remarque"
    Vous n'êtes pas obligé d'avoir les quatres zones ! Seulement 2 zones, la rouge, le WAN et la verte, le LAN sont absolument nécessaires. C'est ce que vous retrouvez dans la plupart des parefeux grands publics (les *BOX* internet).

<span style="color: #ff0000">**Validation professeur :**</span>

## Installer le parefeu IPFire

Vous allez réaliser l'installation avec Proxmox, il faut s'assurer que vous pouvez avoir 4 cartes réseaux (une réelle: celle qui est relié au réseau d'établissement et trois virtuelles). Pour cela cliquez sur le noeud *Proxmox* puis cliquez sur *Systèmes* puis *Réseau*. Vous devez avoir une première carte réseau virtuelle notée *vmbr0* (ou *vmbr1*) qui est rattaché à une carte réseau réelle puis trois cartes réseaux virtuelles notées *vmbrX*, *vmbrX+1* et *vmbrX+2*. Toutes ces cartes doivent être dans l'état *actif=OUI* sinon c'est probablement que la création de ces nouvelles cartes virtuelles n'a pas été validé (bouton *Appliquer la configuration*).

!!! note "Exercice"
    L'adresse réseau de départ de l'entreprise est 172.16.0.0/16. Vous avez trois sous-réseaux à réaliser au sein de l'entreprise. Calculez le nouveau masque de ces sous-réseaux puis complétez la colonne **@IP réseau** et **1ere @machine** du tableau ci-dessous.
    Calcul du nouveau masque: `________________________________________________________`

!!! note "Exercice"
    Créez une nouvelle VM (**CIEL-VosInitiales-IPFire**) avec 2 processeurs/2Go de RAM/32Go de disque dur. Choisir l'image ISO de IPFire lorsque c'est nécessaire. Prendre **vmbr1** comme première carte réseau. Une fois la machine virtuelle créée, rajoutez les trois cartes réseaux virtuelles définies précédemment (Item *Matériels* puis bouton *Ajouter* puis *Carte réseau*, choisir vmbrX, vmbrX+1, ...).
    Complétez alors les colonnes **Nom** et **@MAC** du tableau ci-dessous.

|@MAC|Nom|Zone (R,V,B,O)|@IP réseau|1ere @machine|
|:---:|:---:|:---:|:---:|:---:|
|`____:____:____:____:____:____`|net`__`/vmbr`__`|WAN : Rouge|10.187.52.xxx/24|ne pas remplir|
|`____:____:____:____:____:____`|net`__`/vmbr`__`|LAN : Verte|172.16.`___`.`___`/`___`|172.16.`___`.`___`|
|`____:____:____:____:____:____`|net`__`/vmbr`__`|DMZ : Orange|172.16.`___`.`___`/`___`|172.16.`___`.`___`|
|`____:____:____:____:____:____`|net`__`/vmbr`__`|WIFI: Bleu|172.16.`___`.`___`/`___`|172.16.`___`.`___`|

Vous allez mettre en oeuvre le schéma réseau suivant avec les MVs indiquées:

![Schema réseau du TP IPFire](../img/SchemaReseauEntreprisePareFeuAvecVMIP_IPFire.svg)

!!! note "Exercice"
    Vous pouvez alors démarrer votre VM IPFire. Suivez le [tutoriel en anglais](https://www.ipfire.org/docs/installation) d'installation (en particulier les étapes 3,4 et 5). Soyez particulièrement attentif lorsque vous arriverez à l'étape **Menu de configuration réseau** de bien réaliser, **dans l'ordre**, les trois items *Type de configuration réseau*, *Affectation des pilotes et des cartes* et *Configuration d'adresses* avec le tableau précédent.  
    Passez la configuration du serveur **DHCP**. L'installation se termine alors rapidement.
    Connectez-vous alors en *root* avec le mot de passe que vous avez saisi pendant l'installation puis faites la commande `ip a`. Comment s'appelle les interfaces réseaux : `_____________________________`. Vérifiez que les @IP des différentes cartes réseaux sont correctes.

<span style="color: #ff0000">**Validation professeur :**</span>

## Connexion au parefeu IPFire et configuration de base

Vous allez maintenant découvrir l'interface web du parefeu IPFire. Commencez par tester la commande ping depuis votre machine réelle vers la VM IPFire. Que se passe-t-il ? `______________`. Essayez alors de vous connectez à son interface web en saisissant quel adresse dans votre navigateur ? (relire la documentation en ligne) `http__________________________________`. La connexion est-elle possible ? `________`.

!!! note "Exercice"
    En relisant la documentation de IPFire expliquez pourquoi la connexion à l'interface web depuis la zone rouge est impossible ? `_________________________________________________________________________________________________________________________________________________`  
    Proposez alors une solution pour se connecter à l'interface web ? `______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________`

!!! note "Exercice"
    Clonez une VM Windows. Éditez les paramètres de la carte réseau pour qu'elle est comme interface la carte réseau virtuelle qui est associé avec la zone verte (vmbr`__`). Démarrez cette VM puis configurez son @IP pour qu'elle appartienne au réseau LAN (zone verte) en prenant la deuxième @IP de ce réseau (@IP/Masque: `_____________________________________________`). Vous rajouterez comme @IP de passerelle et @IP DNS celle de votre machine IPFire : `_____________________________` Connectez-vous alors à l'interface web avec un navigateur. Les identifiants sont ceux que vous avez saisi pendant l'installation.

<span style="color: #ff0000">**Validation professeur :**</span>

La page d'accueil de l'interface web de IPFire doit se présenter comme celle-ci:

![Page accueil IPFire](../img/PageAccueilIPFire.png)

Vous trouvez la zone des menus en haut, la description des différentes interfaces réseaux au centre : vous visualisez l'état des quatre zones définies précédemment. L'@IP de la zone rouge (ici INTERNET) n'est pas celle que vous devez obtenir, c'est un exemple.

Ci-dessous vous trouvez le détail des menus:

![Les menus de IPFire](../img/menusIPFire.png)

!!! tip "Se servir de l'aide en ligne"
    Lorsque vous cliquez sur une rubrique dans un des menus, vous arrivez sur la page concernée (par exemple *Système* puis *Accueil*). Le titre de la page est rappelée en haut de la page mais surtout il y a un **point d'interrogation** à coté de ce titre. Ce point d'interrogation vous renvoie sur la documentation en ligne qui est souvent très bien faite !

|Menu|Brève description des principales sous-rubriques|
|:---|:---|
|**Système**|*Accueil* : Page par défaut qui présente l'état des cartes réseaux des différentes zones|
||*SSH* : activation/gestion des clés pour la connexion sécurisé à distance|
||*Sauvegarde* : Sauvegarde/Restauration de la configuration de IPFire. Utile pour restaurer rapidement les paramètres dans une nouvelle VM ou une machine réelle|
||*Arrêter* : comme son nom l'indique, arrête le parefeu|
|**Statut**|Ce menu permet de visualiser toutes les métriques du parefeu, d'avoir une vue d'ensemble du fonctionnement du parefeu.|
|**Réseau**|Ce menu permet de configurer de nombreux services du parefeu.|
||*Configuration des zones* : permet le réglage IP des différentes zones|
||*Système de nom de domaine* : configuration du dns principal|
||*Proxy WEB* : gestion du proxy, de la mise cache, du filtrage d'URL|
||*Serveur DHCP* : gestion du serveur DHCP pour les différentes zones|
||*Portail captif* : utile pour la zone bleue (wifi public)|
|**Services**|Gestion des VPN *IPSec* et *OpenVPN*|
||*Heure du serveur* : connexion à un serveur NTP externe et éventuellement serveur NTP local|
||*DNS Dynamique* : si vous **n**'avez **pas** acheté un nom de domaine vous pouvez utiliser les services d'un DNS dynamique pour joindre votre parefeu depuis Internet|
|**Pare-feu**|*règles de pare-feu* : mise en place de la gestion des flux de données entre les différentes zones|
||*Liste de blocage d'@ IP* : listes maintenus par des sites extérieures|
|**Pakfire**|mise à jour du parefeu et installation de modules logiciels complémentaires|
|**Journaux**|tous les fichiers de log du parefeu|

!!! note "Exercice : Configuration de base"
    - [ ] sur la page d'accueil, vérifiez les adresses IP de chacune des zones, vérifiez aussi dans la partie *Réseau* et *Configuration des zones*,
    - [ ] configurez/vérifiez le DNS du parfeu IPFire : menu *Réseau*, *DNS* puis mettre come serveur DNS : 10.187.52.5,
    - [ ] parcourez le menu *Statut* pour visualiser les différents graphiques disponibles,
    - [ ] changez le serveur NTP pour utiliser celui qui fonctionne depuis le lycée: *ntp.unice.fr*, ne pas mettre de serveur ntp secondaire,
    - [ ] vérifiez s'il y a une mise à jour, la réaliser s'il y a lieu,
    - [ ] changer votre mot de passe *admin* de l'interface web pour un plus sécurisé en suivant la procédure ci-dessus:
        1. On the web UI go to: *System / SSH Access* and check **[x] SSH Access**, click *Stop SSH…in 15 min* button. Now the SSH service is active for 15 min.
        1. Open console, e.g. with PuTTY on Windows, login as root, enter password for root,
        1. type **setup**, press enter, a menu appears,
        1. navigate to *admin* password and select [OK],
        1. enter new password twice and select [OK] mdp:`______________________`.
    - [ ] activez la connexion ssh par clé (suivre cet [article](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_02/#comment-se-connecter-sur-un-serveur-sans-taper-de-mot-de-passe)) depuis votre MV Windows située dans la zone verte. Testez alors la connexion depuis votre VM Windows, vous ne devez plus taper de mot de passe. **Désactivez la connexion par mot de passe** : cela évitera les attaques par force brute,
    - [ ] Définissez les zones dans lesquels vous allez activer le serveur DHCP. Dans chacune de ces zones, vous distribuerez des @IP comprises entre 100 et 150. Passez votre VM Windows en mode DHCP, vérifiez alors son adresse IP. Retrouvez cette information dans l'interface de IPFire.
        1. Zones DHCP actif: `_______________________________________`,
        1. **Champs** à compléter pour le DHCP suivant les zones:
            - zone `_________` : @IP début:`________________________` @IP fin:`________________________`,
            - zone `_________` : @IP début:`________________________` @IP fin:`________________________`,
        1. @IP en mode DHCP de votre VM Windows: `______________________________`,
        1. Menu et item dans IPFire pour retrouver les baux DHCP: `________________________________`

!!! tip "Assigner une @IP fixe par DHCP"
    Si vous le souhaitez, vous pouvez par l'intermédiaire du service DHCP, assignez une @IP fixe à une machine. Cela fonctionne en associant @MAC avec l'@IP. Par contre il faut choisir l'@IP en dehors de la plage distribuée par le serveur. Cela se fait dans le menu *Réseau*, *Serveur DHCP*, rubrique *État actuel des baux fixes* : pensez à cochez la case *Activé* puis cliquez sur le bouton *Ajouter*. A titre d'exercice vous pouvez ajouter votre VM Windows pour la mettre en @IP XX.99, renouvelez le bail (commande: `___________________`) puis vérifiez l'@IP obtenu.

<span style="color: #ff0000">**Validation professeur :**</span>

## Analyse des règles du parefeu par défaut

Le parefeu possède des règles par défaut mise en place lors de l'installation en fonction du nombre de zones configurées. Ces règles par défaut sont disponibles sur [https://www.ipfire.org/docs/configuration/firewall/default-policy](https://www.ipfire.org/docs/configuration/firewall/default-policy) et sont présentées dans le tableau ci-dessous:

![Tableau des règles par défaut](../img/TableauReglesParDefaut.png)

!!! note "Exercice"
    Complétez le tableau ci-dessous en indiquant si la phrase proposée est vrai ou fausse. Justifiez votre réponse.

|Proposition|Vrai/Faux|
|:---|:---:|
|Toutes les connexions venant de la zone rouge (internet) sont refusées par défaut||
|Toutes les connexions venant de la zone orange (dmz) vers la zone rouge (internet) sont refusées||
|Les connexions de la zone bleue (wifi public) vers les zones orange (dmz) et verte (lan) sont autorisées||
|La zone verte peut accéder à toutes les zones||
|L'accés Internet est disponible depuis la zone verte et la zone orange||
|Un PC de la zone verte peut *pinguer* une machine dans la zone bleue||
|Un PC de la zone bleue peut interroger un serveur de la zone orange||

## Configuration du réseau BLEU (wifi public) : proxy web, portail captif, filtrage URL

**Commencez** par lire la documentation en ligne (Menu *Pare-feu* -> *Accès réseau BLEU*). Comme indiqué dans la documentation, ce réseau se base sur un filtrage par @MAC. Dans notre cas d'étude (point d’accès wifi disponible pour des visiteurs sans mot de passe wifi), nous ne souhaitons pas ce comportement.

!!! note "Exercice : modifier la configuration du réseau BLEU"
    - [ ] désactivez le filtrage par @MAC sur le réseau BLEU pour **toutes** les machines qui se connecterons sur ce réseau (lire la doc !!),
    - [ ] créez une MV Linux graphique et la connecter au réseau BLEU (attention au vmbr), cette machine sera en DHCP. Une fois connecté, vérifiez que son @IP appartient bien à la plage DHCP configuré sur le logiciel : @IP/Masque: `_____________________________`
    - [ ] testez l’accès à Internet depuis cette machine : *possible/impossible*, quel est le comportement par défaut par rapport à l'accès internet pour cette zone ? `_____________________________________________________________`,
    - [ ] testez l’accès à l'interface d'administration depuis cette machine : *possible/impossible*, quel est le comportement par défaut par rapport à l'accès à l'interface d'administration pour cette zone ? `_____________________________________________________________`,
    - [ ] rajoutez une règle de parefeu (Menu *Pare-feu* -> *Règles de pare-feu*) pour que l’accès à l'interface web d'administration depuis le réseau BLEU sur le port tcp 444 ne soit plus possible (si vous ne trouvez pas tout seul, une fois de plus regarder la documentation en début de paragraphe),
    - [ ] attendez 30s puis vérifiez que l’accès à l'interface d'administration n'est plus possible : *possible/impossible*.

Votre direction change d'avis sur la politique d'accès de la zone wifi public suite à des abus. Il faut maintenant que l’accès wifi soit accessible avec un "coupon" donné à chaque visiteur pour une durée précise.

!!! note "Exercice : mise en place d'un portail captif sur le réseau BLEU"
    - [ ] commencez par lire la documentation sur la mise en place d'un portail captif (Menu *Réseau* -> *Page d'accueil (portail captif)*),
    - [ ] cochez la case pour l'activer sur le réseau BLEU et choisissez le type d’accès à **Coupon** (Voucher en anglais),
    - [ ] personnalisez la page de connexion : titre, couleur de fond, logo, texte... puis sauvez le tout,
    - [ ] les coupons auront par défaut une durée de 1H, créez un coupon de test, cliquez sur le bouton *Export de coupons* pour le visualiser,
    - [ ] depuis votre MV linux grahique, essayez de vous connectez à un site web, utilisez votre coupon, vous pouvez naviguez pendant une heure,
    - [ ] révoquez le coupon d’accès précédent depuis votre interface d'administration, attendez 30s puis réessayez de naviguer sur internet.

<span style="color: #ff0000">**Validation professeur :**</span>

Votre DSI souhaite que vous mettiez en place un filtre d'accès au web (liste de sites interdits) dans un premier temps pour le réseau BLEU **en plus** de l'accès par coupon.

!!! note "Exercice : mise en place du proxy web"
    - [ ] commencez par lire la documentation sur la mise en place du proxy web (Menu *Réseau* -> *Proxy web*). Le logiciel utilisé dans IPFire est [squid](http://www.squid-cache.org/).
    - [ ] pour que l'on puisse filtrer les *urls* de manière efficace (y compris en https), il faut configurer le proxy web en mode *actif* (*conventional mode* en anglais) et **pas** en mode *transparent*. Cochez la case appropriée : `______________________` et donnez le port sur lequel écoutera le serveur proxy : `____________`,
    - [ ] cochez la case *filtre URL* et la case *Journaux activés*,
    - [ ] descendez au bas de la page web et cliquez sur le bouton *Sauvegarder et redémarrer*,
    - [ ] dans la MV linux graphique, il faut changer le paramètre **proxy** de votre navigateur web/système d'exploitation. Ce paramètre dépend du navigateur (pour firefox : menu *Paramètres*, en bas de page *Paramètres réseau*, cliquez sur *Paramètres...*, choisir *Configuration manuelle du proxy*, *Proxy HTTP* mettre l'@IP du IPFire sur le réseau BLEU et le port, cochez la case *Utilisez également pour HTTPS*).

!!! note "Exercice : mise en place du filtre de contenu URL"
    - [ ] commencez par lire la documentation sur la mise en place d'un filtrage de contenu (Menu *Réseau* -> *Filtre de contenu URL*),
    - [ ] vous suivrez la démarche suivante : dans la partie *Maintenance de filtre URL* mettre à jour la blacklist de l'université de Toulouse (cette liste est utilisée par les établissements scolaires et publics pour filtrer l'accès à internet), patientez cela peut être long (plusieurs minutes),
    - [ ] cherchez le bouton *Sauvegarder et redémarrer* pour relancer le logiciel squid. Vérifiez ensuite, que dans la partie *Maintenance de filtre URL* puis *Mise à jour automatique de la liste noire*, la phrase *La dernière mise à jour...il y a 0 jours" est apparue,
    - [ ] En haut de page la liste de *Blocage de catégories* doit être nettement plus importante,
    - [ ] dans votre MV Linux graphique, connectez-vous sur le site de *discord*, la page d’accueil doit s'afficher sinon vérifiez vos paramètres de proxy dans le navigateur,
    - [ ] revenez sur la liste de *Blocage de catégories*, cochez la case **chat** (cette catégorie contient les sites de discussion comme discord) puis cliquez sur le bouton *Sauvegarder et redémarrer*,
    - [ ] revenez dans le navigateur dans la MV linux graphique, connectez-vous à discord. Résultat: `___________________________________`,
    - [ ] rajoutez un filtre pour que le réseau BLEU ne puisse plus accéder aux sites de google et testez le bon fonctionnement du filtre : `____________________________________________`.

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice en autonomie : configurer le proxy automatiquement par DHCP"
    - [ ] Passer sur tous les navigateurs quelque soit le système d'exploitation est long et pénible. Il serait intéressant de distribuer de manière automatique la configuration du proxy. Pour cela cloner une nouvelle machine linux graphique, la positionner encore une fois dans le réseau BLEU,
    - [ ] Lisez cet article sur le **WPAD** (Web Proxy Auto-Discovery Protocol) : [https://www.ipfire.org/docs/configuration/network/proxy/extend/wpad](https://www.ipfire.org/docs/configuration/network/proxy/extend/wpad) et testez sa mise en oeuvre sur la MV linux graphique créée à la ligne précédente.

## Configuration du réseau ORANGE (dmz) : serveur web
 **Commencez** par lire la documentation en ligne sur la mise en place des règles de filtrage ([https://www.ipfire.org/docs/configuration/firewall/rules](https://www.ipfire.org/docs/configuration/firewall/rules)) puis complétez le tableau ci-dessous :

|Etape|Commentaire|
|:---:|:---|
|++1++|La source:|
|++2++|La destination:|
|++3++|NAT (SNAT et DNAT):|
|++4++|Le protocole:|
|++5++|L'action (ACCEPT, DROP, REJECT):|
|++6++|Réglages complémentaires (remplir la remarque avec le rôle de la règle):|
|++7++|Appliquer la règle en cliquant sur le bouton|

!!! note "Exercice : ajout d'un serveur web dans la DMZ et des règles de parefeu associées"
    - [ ] Créez une nouvelle MV Console Linux, changez sa configuration réseau pour que cette machine soit la deuxième @IP du réseau (@IP/Masque: `______________________`), le DNS sera celui de [QuadNine](https://www.quad9.net/fr/) (9.9.9.9), la passerelle sera la machine IPFire sur cette zone.
    - [ ] Testez votre accés à Internet : OK / KO, mettez à jour la liste des paquets puis installez le serveur web Apache2. Vérifiez que ce serveur web est actif en installant le logiciel `lynx` (navigateur en mode texte) en faisant une commande du style `lynx localhost`. Serveur WEB : OK / KO
    - [ ] Mettre en place, si nécessaire, une règle du parefeu pour que le serveur web soit accéssible depuis la zone verte : OK / KO.
    - [ ] Complétez la **première** ligne du tableau ci-dessous puis mettre en place une règle du parefeu pour que le serveur web (juste ce service) soit accéssible depuis la zone bleue ([https://www.ipfire.org/docs/configuration/firewall/rules/bg-holes](https://www.ipfire.org/docs/configuration/firewall/rules/bg-holes)). Testez l'accés depuis une machine de la zone bleue (attention au fait que vous ayez un proxy web, un portail captif) : OK / KO.
    - [ ] Complétez la **deuxième** ligne du tableau ci-dessous puis mettre en place une règle du parefeu pour que le serveur web (juste ce service) soit accéssible depuis la zone rouge ([https://www.ipfire.org/docs/configuration/firewall/rules/dmz-setup](https://www.ipfire.org/docs/configuration/firewall/rules/dmz-setup)). Testez depuis une machine de la zone rouge que vous avez accés au serveur web : OK / KO.

||Source|NAT|Destination|Protocole&Port|Action|
|:---|:---|:---|:---|:---|:---|
|Accés Web depuis zone bleu||||||
|Accés Web depuis zone rouge||||||

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice en autonomie"
    - [ ] Activez le service **https** du serveur Apache2 : `a2enmod ssl`, `a2ensite default-ssl.conf`, `systemctl restart apache2` et vérifiez que tout va bien avec un `systemctl status apache2`. Pour en savoir plus sur la mise en oeuvre d'un site https, lire l'article suivant : [https://wiki.debian.org/Self-Signed_Certificate](https://wiki.debian.org/Self-Signed_Certificate).
    - [ ] Complétez la **première** ligne du tableau ci-dessous puis mettre en place les règles de parefeu pour accéder à ce serveur.
    - [ ] Installez un service FTP anonyme. Complétez la **deuxième** ligne du tableau ci-dessous puis mettre en place les règles de parefeu pour un accés uniquement de la zone rouge.

||Source|NAT|Destination|Protocole&Port|Action|
|:---|:---|:---|:---|:---|:---|
|Accés https depuis zone rouge||||||
|Accés ftp depuis zone rouge||||||

## Mise en oeuvre d'un VPN : OpenVPN

OpenVPN ([https://openvpn.net/](https://openvpn.net/))est un service VPN (Virtual Private Network: [Réseau Privé Virtuel](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel)) qui permet aux réseaux distants ou aux clients sans fil, tels que les ordinateurs portables, de se connecter à IPFire. Cette fonctionnalité est également disponible avec la mise en œuvre d'IPsec, mais OpenVPN adopte une approche différente, basée sur des tunnels SSL. L'utilisation d'OpenVPN au lieu d'IPSec est une question de préférence, bien qu'il y ait quelques très bonnes raisons de choisir l'un plutôt que l'autre.

Le tableau ci-dessous vous présente rapidement des caractéristiques de OpenVPN et IPSec :

|OpenVPN|IPSec|
|:---:|:---:|
|Plus facile à mettre en place et à configurer|Disponible avec les routeurs propriétaires (la plupart des routeurs propriétaires ne supportent pas OpenVPN)|
|Moins susceptible d'être bloqué par des routeurs intermédiaires|Généralement plus sûr, car les utilisateurs d'OpenVPN peuvent (et le font parfois) définir des mots de passe vides, ce qui permet une connexion sans phrase d'authentification|
|Bien meilleur pour les connexions de site à site (lorsqu'un réseau entier est connecté à un autre réseau)|Formellement normalisé via la RFC 3193 de l'IETF|
|Possibilité de créer des tunnels sur la couche Ethernet (ce qui n'est pas possible avec IPSec)|Norme de facto pour les produits Microsoft|
|Plus stable, et le dépannage est généralement plus simple||
|Standard pour les projets OpenSource||

Le logiciel IPFire permet son utilisation/configuration dans le menu *Services* puis *OpenVPN*.

!!! note "Exercice en autonomie"
    - [ ] **Commencez** par lire l'article de mise en oeuvre de OpenVPN ([https://www.ipfire.org/docs/configuration/services/openvpn](https://www.ipfire.org/docs/configuration/services/openvpn)),
    - [ ] Poursuivez avec l'article sur la configuration [https://www.ipfire.org/docs/configuration/services/openvpn/config](https://www.ipfire.org/docs/configuration/services/openvpn/config) : la création des certificats et clé, puis la configuration d'un réseau *Client-to-net* (appelé dans la documentation *roadwarrior*),
    - [ ] Installez un client openVPN (version *community*) sous un Windows 10/11 qui appartiendra au réseau rouge, configurez le client puis testez la connexion vers votre serveur OpenVPN.

<span style="color: #ff0000">**Validation professeur :**</span>

??? note "Correction de l'exercice précédent"
    |Etape|Commentaire|
    |:---:|:---|
    |++1++|Génération des clés et certificats du serveur: Menu *Services*, *OpenVPN* puis dans la zone *Autorité de certification* cliquez sur le bouton *Générer des certificats root/hote*. Complétez le champ *Nom d'organisation* (ex: TPAIS), changer le pays puis cliquez sur le bouton *Générer des certificats*|
    |++2++|Réglages globaux du serveur OpenVPN: Menu *Services*, *OpenVPN* puis cocher la case *OpenVPN sur rouge*, le *sous-réseau OpenVPN* **doit être différent** des réseaux que vous allez relier par l'intermédiaire du réseau VPN. Prenez comme algorithme de chiffrement **AES-GCM (256bits)** (certains autres sont dépréciés et plus supportés). Cliquez sur le bouton *Sauvegarder*|
    |++3++|Démarrer le serveur OpenVPN: Menu *Services*, *OpenVPN* puis cliquez le bouton *Démarrer Serveur OpenVPN*. L'en-tête doit passer en vert|
    |++4++|Ajouter un client OpenVPN: Menu *Services*, *OpenVPN* puis dans la zone *Etat et controle de connexion*, cliquez sur le bouton *Ajouter*. Cochez la case *Hôte au réseau* (Host to net : configuration *roadwarrior*) puis cliquez le bouton *Ajouter*. Dans la page qui vient de s'ouvrir complétez le champ *Nom* et *Remarque* (ex: VPNTPAIS, test connexion OpenVPN). Dans la zone *Authentification*, cochez la case *Générer un certificat*, complétez les champs *Nom d'utilisateur*, *Valide jusqu'au* et *Mot de passe PKCS12* : pour mettre un mot de passe sur le certificat. Cliquez sur le bouton *Sauvegarder* en bas de page|
    |++5++|Récupération de la configuration OpenVPN pour votre client: Menu *Services*, *OpenVPN* puis dans la zone *Etat et controle de connexion* vous trouvez votre client que vous venez de créer. Cliquez sur l'icone *Télécharger le paquet client*. Vous téléchargez alors un fichier *zip* contenant toutes les données pour le client OpenVPN. Il faudra trouver un moyen pour récupérer ce fichier en dehors de votre MV (mail, drive ou autres)|
    |++6++|Installation et configuration d'un client OpenVPN sur une machine: téléchargez un client OpenVPN sur le portable Windows 10 depuis [https://openvpn.net/community-downloads/](https://openvpn.net/community-downloads/). Prenez la dernière version et installez là. Quittez *OpenVPN GUI* : clic gauche sur son icone dans la zone de notification. Dézipper *le paquet client* de configuration openVPN récupéré à l'étape précédente et copier les deux fichiers dans `C:\Users\`*votreNomUtilisateur*`\OpenVPN\config`. Relancez *OpenVPN GUI* par le menu *Démarrer*. Clic gauche sur son icone dans la barre des taches puis choisissez *Connecter*. Le client OpenVPN tente de se connecter, si tout est ok, son icone devient *verte*|
    |++7++|vérification du bon fonctionnement du VPN: grace au VPN nous sommes dans la zone verte de IPFire. Connectez-vous à son interface web d'administration: https://@IP_IPFire_ZoneVerte:444|

!!! note "Augmenter la sécurité de votre VPN OpenVPN"
    En plus de la saisie d'un mot de passe pour lire votre certificat lors de la connection avec le client OpenVPN, vous pouvez rajouter un code TOTP à saisir lors de la connexion. Pour cela vous pouvez modifier la configuration d'un utilisateur en cochant le case *Activer OTP* dans la zone *Options avancés du client*. Une nouvelle icone apparait alors sur la ligne de l'utilisateur (un QRCode). Cliquez sur celui-ci puis scannez le QRCode avec une application du type google authenticator. **ATTENTION** le seul client openVPN compatible avec le TOTP est la version **2.5.7**. Il faudra donc désintaller le client openVPN pour installer celui-ci. N'oubliez pas de récupérer la nouvelle configuration de votre utilisateur. Lors de la connexion, vous devrez saisir le mot de passe du certificat puis un peu plus tard il vous demandera le code TOTP.

