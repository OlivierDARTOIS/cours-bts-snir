# Cryptage selon le "Code de César"

!!! tldr "Objectifs de l'exercice"
    Cet exercice est inspiré d'un exercice pour la spécialité NSI du baccalauréat.
    Dans cet exercice, on étudie une méthode de chiffrement de chaînes de caractères alphabétiques.
    Pour des raisons historiques, cette méthode de chiffrement est appelée **code de César**
    (consultez l'article sur [wikipédia](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage)).
    On considère que les messages ne contiennent que les lettres capitales de l’alphabet "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    et la méthode de chiffrement utilise un nombre entier fixe appelé *la clé de chiffrement*.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Classe *CodeCesar*

Le code partiel de la classe CodeCesar est donné ci-dessous (partie **déclaration** de la classe = fichier d'en-têtes = fichier ".h" et la partie **définition de la classe** = fichier d'implémentation de la classe = fichier ".cpp" = code source) :

```Cpp title="Déclaration de la classe *CodeCesar*" linenums="1"
#include <iostream>
#include <string>

class CodeCesar {
private:
    static const std::string alphabet;
    int cle;

public:
    CodeCesar(int cle);
    char decale(char lettre);
};

```

```Cpp title="Définition de la classe *CodeCesar*"  linenums="1"
const std::string CodeCesar::alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
CodeCesar::CodeCesar(int cle):cle(cle) {}

char CodeCesar::decale(char lettre)
{
    // méthode find à appliquer sur une chaine de type string
    // doc: http://www.cplusplus.com/reference/string/string/find/
    size_t pos = alphabet.find(lettre);
    int nvPos = pos + this->cle;
    if (nvPos >= 26) nvPos -= 26;
    if (nvPos < 0) nvPos += 26;
    return alphabet.at(nvPos);
}
```

La méthode `find` renvoie un entier qui correspond à l'indice de la lettre recherché dans la chaine alphabet. Attention l'indice commence à 0.

!!! note "Exercice"
    === "Analyser un code"
        **Commentez** les trois lignes suivantes et **donnez** la sortie console.

        ```Cpp  linenums="1"
        CodeCesar code(3);
        std::cout << "A decale de 3 donne : " << code.decale('A') << std::endl;
        std::cout << "X decale de 3 donne : " << code.decale('X') << std::endl;
        ```
    === "Correction analyse code"
        - ligne 1 : instantion d'un objet de type `CodeCesar` avec le nom de variable `code`. La clé de cryptage sera initialisé à la valeur 3 car on passe cette valeur au contructeur de la classe.
        - ligne 2 : appelle de la méthode `decale` de l'objet `code` avec comme paramètre le caractère A. Cette méthode renvoie un caractère qui s'affichera avec `std::cout`.
        - ligne 3 : idem ligne 2 sauf que le paramètre de la méthode vaut cette fois-ci le caractère X.

    === "Correction sortie console"
        ```console
        $ ./CodeCesar
        A decale de 3 donne : D
        X decale de 3 donne : A
        ```

## Ajouter une méthode `cryptage`

La méthode de chiffrement du « code César » consiste à décaler les lettres du message dans l’alphabet d'un nombre de rangs fixé par la clé. Par exemple, avec la clé 3, toutes les lettres sont décalées de 3 rangs vers la droite : le A 
devient le D, le B devient le E, etc...  

Exemple de code et trace d'exécution de ce que doit donner l'appel de cette méthode avec la chaîne *SNIR*.

```Cpp  linenums="1"
CodeCesar code(3);
std::cout << "SNIR crypte donne : " << code.cryptage("SNIR") << std::endl;
```

> SNIR crypte donne : VQLU

!!! note "Exercice"
    === "Produire un code"
        **Ajoutez** une méthode `cryptage` dans la classe `CodeCesar` définie à la question précédente, qui reçoit en paramètre une chaîne de caractères (le message à crypter) et qui retourne une chaîne de caractères (le message crypté).
        Cette méthode `cryptage` doit crypter la chaîne texte avec la clé de l'objet de la classe CodeCesar qui a été instancié. 

    === "une solution possible"
        ```Cpp linenums="1"
        std::string CodeCesar::cryptage(const std::string& messageACrypter)
        {
            // Il faut appeler la méthode decale sur chacune des lettres du
            // message à crypter et concatener le résultat dans une
            // nouvelle chaine messageCrypte
            std::string messageCrypte;

            for (auto& carac : messageACrypter)
                messageCrypte.push_back(decale(carac));
            
            return messageCrypte;
        }
        ```

## Ecrire un court programme de test

!!! note "Exercice"
    === "Produire un code"
        Écrivez un programme qui :

        - demande de saisir la clé de chiffrement,
        - crée un objet de la classe `CodeCesar`,
        - demande de saisir le texte à chiffrer,
        - affiche le texte chiffré en appelant la méthode `cryptage`.
        
        Vous supposerez que les saisies utilisateur sont conformes à ce que vous attendez (fonctionnement nominal du programme).

    === "Une solution possible"
        ```cpp
        int cleChiffrement;
    
        std::cout << "Cle de chiffrement (un entier) : ";
        std::cin >> cleChiffrement;
    
        CodeCesar monCodeCesar(cleChiffrement);
        std::string messageACrypter;
    
        std::cout << "Votre message (uniquement des majuscules) : ";
        std::cin >> messageACrypter;
    
        std::cout << "Message crypte : " << monCodeCesar.cryptage(messageACrypter) << std::endl;
        ```

## Analyser un code

On ajoute la méthode `transforme` à la classe `CodeCesar`. Le code de cette méthode est donné ci-dessous :

``` Cpp  linenums="1"
std::string CodeCesar::transforme(const std::string& messageACrypter)
{
    this->cle = -(this->cle);
    std::string messageCrypte = cryptage(messageACrypter);
    this->cle = -(this->cle);
    return messageCrypte;
}
```

puis on réalise le programme de test suivant:

``` Cpp  linenums="1"
CodeCesar monCodeCesar(10);
std::cout << "Message transforme : " << monCodeCesar.transforme("PSX") << std::endl;
```

!!! note "Exercice"
    === "Analyser un code"
        **Donnez** la sortie console de ce programme. **Justifiez** votre réponse.

    === "Correction sortie console"
        ```console
        $ ./CodeCesar
        Message transforme : FIN
        ```

    === "Correction analyse de code"
        - ligne 1 : instantiation d'un objet de type `CodeCesar` avec le nom de variable `monCodeCesar`. La clé de cryptage sera initialisé à la valeur 10 car on passe cette valeur au constructeur de la classe.
        - ligne 2 : appelle de la méthode `transforme` de l'objet `monCodeCesar` avec comme paramètre la chaîne *PSX*. Cette méthode renvoie la chaîne transformée qui s'affichera avec `std::cout`.
        - Pour l'affichage sur la console, il faut analyser le code de la méthode `transforme` : celui-ci inverse la valeur de la clé de cryptage ce qui fait que l'on recule dans l'alphabet, ici de 10. Donc si vous partez du message *PSX* et que vous reculez de 10 lettres, vous obtenez la chaîne *FIN* !

## Produire un diagramme UML/SysML

!!! note "Exercice"
    === "Produire un diagramme UML"
        **Dessinez** le diagramme de classe de la classe `CodeCesar`. Vous devez faire apparaître les méthodes `cryptage` et `transforme`.

    === "Une solution possible"
        ``` mermaid
        classDiagram
        class CodeCesar{
            - alphabet : string const
            - cle : int
            +CodeCesar( cle : int)
            +decale( c : char) char
            +cryptage( message : string const & ) string
            +transforme(messageACrypter : string const & ) string
        }
        ```

## Code complet de l'exercice

```cpp linenums="1"
#include <iostream>
#include <string>

class CodeCesar {
    private:
        static const std::string alphabet;
        int cle;
    public:
        CodeCesar(int cle);
        char decale(char lettre);
        std::string cryptage(const std::string& messageACrypter);
        std::string transforme(const std::string& messageACrypter);
};

const std::string CodeCesar::alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

CodeCesar::CodeCesar(int cle):cle(cle) {}

char CodeCesar::decale(char lettre)
{
    // méthode find à appliquer sur une chaine de type string
    // doc: http://www.cplusplus.com/reference/string/string/find/
    size_t pos = alphabet.find(lettre);
    int nvPos = pos + this->cle;
    if (nvPos >= 26) nvPos -= 26;
    if (nvPos < 0) nvPos += 26;
    return alphabet.at(nvPos);
}

//**** Ce que doit rédiger le candidat ****
std::string CodeCesar::cryptage(const std::string& messageACrypter)
{
    // Il faut appeler la méthode decale sur chacune des lettres du
    // message à crypter et concatener le résultat dans une
    // nouvelle chaine messageCrypte
    std::string messageCrypte;
    for (auto& carac : messageACrypter)
        messageCrypte.push_back(decale(carac));
    return messageCrypte;
}
//********************************************

std::string CodeCesar::transforme(const std::string& messageACrypter)
{
    this->cle = -(this->cle);
    std::string messageCrypte = cryptage(messageACrypter);
    this->cle = -(this->cle);
    return messageCrypte;
}

int main()
{
    CodeCesar code(3);
    std::cout << "A decale de 3 donne : " << code.decale('A') << std::endl;
    std::cout << "X decale de 3 donne : " << code.decale('X') << std::endl;
    std::cout << "SNIR crypte donne : " << code.cryptage("SNIR") << std::endl;
    
    //**** Ce que doit rédiger le candidat ****
    int cleChiffrement;
    std::cout << "Cle de chiffrement (un entier) : ";
    std::cin >> cleChiffrement;
    CodeCesar monCodeCesar(cleChiffrement);
    std::string messageACrypter;
    std::cout << "Votre message (uniquement des majuscules) : ";
    std::cin >> messageACrypter;
    std::cout << "Message crypte : " << monCodeCesar.cryptage(messageACrypter) <<
    std::endl;
    //******************************************

    std::cout << "Message transforme : " << monCodeCesar.transforme(messageACrypter) << std::endl;
    return 0;
}
```
