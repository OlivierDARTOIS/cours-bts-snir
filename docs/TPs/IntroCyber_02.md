# TP de découverte Cybersécurité - Partie 2

!!! tldr "Objectifs de ce document"

    - préciser la méthodologie d'un test d'intrusion y compris l'aspect légal de celui-ci,
    - utiliser une distribution Linux spécialisée pour la cybersécurité : Kali Linux, 
    - approfondir la connaissance de l'outil nmap et de ces méthodes de balayage du réseau,
    - réaliser l'analyse détaillée d'une machine ou d'un en ensemble de machines sur un réseau,
    - réaliser la cartographie d'un réseau.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 2
    - Compétences évaluées dans ce TP
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (25%)
        - C11 - Maintenir un réseau informatique (50%)
    - Principales connaissances associées
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10
        - Outils logiciels d'évaluation, de traçabilité de l'information, de tests, d'analyse de traitement et de rapport de l'incident -> Niveau 3 : C11

## Introduction

## Méthodologie d'un **pentest**

Nous nous plaçons dans le cadre d'un audit de sécurité du système d'informations d'une entreprise. Nous avons donc l'autorisation écrite du directeur de la DSI pour mener à bien nos investigations et nous devrons rendre compte à celui-ci une fois les investigations menées. 

Notre équipe de **white hat** dans un premier temps devra analyser une **black box**. Puis dans un second temps, vous aurez accès au réseau interne de l'entreprise, nous serons alors dans le cas d'analyse d'une **white box**.

Donnez dans le tableau ci-dessous des définitions simples pour les termes suivants:

|Terme en anglais|Définition en français|
|:---|:---:|
|White Hat|Ce sont des hackers éthiques. Ils respectent les textes de loi et utilisent leurs aptitudes au bénéfice des autres|
|Grey Hat||
|Black Hat||
|Black-Box testint||
|Grey-Box testing||
|White-Box testing|Connaissance totale de l'architecture réseau ou du code source afin de l'auditer|

Répondez aux questions suivantes:

|Question|Type *Hat*|Type *Box*|
|:---|:---|:---|
|Vous décidez d'enlever la protection anti-copie d'une application mais vous n'avez pas accès à son code source.|||
|Un client vous demande d'auditer son site web et vous fournie une partie du code PHP de celui-ci|||

Un test d'intrusions déroule généralement en six étapes :

1. **Préparation de l'audit de sécurité**
    1. définir avec le client les objectifs de l'audit,
    1. définir les systèmes à auditer (matériels et/ou logiciels),
    1. définir la stratégie à mener pour répondre aux objectifs avec les membres de votre équipe,
    1. établir les règles de confidentialité et les autorisations avec le client.
1. **Recherche d'informations**
    1. rechercher des informations sur la cible du test. Dans le cas du réseau l'adresse ip public, le fournisseur d'accès internet, la localisation de l'adresse ip, les enregistrements du nom de domaine et de messagerie, le plan d'adressage, etc...
    1. recherche d'informations sur la structure sociale de l'entreprise afin d'avoir une idée de l'organisation globale de l'entreprise et plus précisément de la DSI (Direction des Services d'Informations). Pour cela les sites sociaux fournissent de nombreux renseignements (twitter, facebook, linked'in, prestataires de services en informatiques et bureautiques (photocopieurs), entreprises du BTP intervenant lors d'un chantier, etc...).
1. **Scan des systèmes**
    1. des outils du type *NMap* permettent de rechercher des vulnérabilités potentiels sur des ports TCP/UDP exposés sur Internet,
    1. si vous êtes sur le réseau interne de l'entreprise, identifiez le maximum de machines et services afin d'établir une cartographie la plus précise du réseau.
1. **Exploitation et post-exploitation des vulnérabilités**
    1. A l'aide des vulnérabilités identifiés à l'étape précédente, mettre en œuvre des **exploits** documentés sur des sites tel metasploit et les sites qui références les failles (CVE) pour accéder au système,
    1. Une fois la vulnérabilité exploitée, réalisez une élévation de privilèges (devenir administrateur) afin de maintenir un accès au système distant en dissimulant ces traces y compris après une mise à jour du système.
1. **Documentation des résultats de l'audit**
    1. Faire un compte rendu exhaustif des tests menés sur le système en précisant les sources utilisées à toutes les étapes précédentes,
    1. Identifier les vulnérabilités et les mesures à prendre pour les corriger,
    1. Proposer une planification des mises à jour de sécurité.
1. **Correction des vulnérabilités suite à l'audit**
    1. Mettre en œuvre les mesures de correction recommandées à l'étape précédente,
    1. Vérifier que les corrections apportées ont corrigé la vulnérabilité.

Vous trouverez ci-dessous une liste de liens (en anglais) qui présente différentes méthodologie:

1. [The Open Source Security Testing Methodology Manual](https://www.isecom.org/OSSTMM.3.pdf) : document pdf,
1. [Open Web Application Security Project](https://owasp.org/),
1. [The NIST Cybersecurity Framework](https://www.nist.gov/cyberframework),
1. [The Cyber Assessment Framework](https://www.ncsc.gov.uk/collection/caf/caf-principles-and-guidance).

## Recherche d'informations

Nous allons nous intéresser au point 2 de notre méthodologie : la **recherche d'informations** qui peut mener ensuite à une vulnérabilité. Nous allons nous concentrer sur le site web du lycée dont l'adresse est : [https://www.lyc-turgot.ac-limoges.fr/](https://www.lyc-turgot.ac-limoges.fr/). Répondez aux questions suivantes avec les sites indiqués.

- Donnez l'adresse ip public du lycée à l'aide du site [https://trouver-ip.info/](https://trouver-ip.info/)  
Adresse ip publique du lycée: `____________________`
- Vérifiez la géolocalisation de cette ip à l'aide du site [https://mon-adresse-ip.fr/localiser-une-adresse-ip/](https://mon-adresse-ip.fr/localiser-une-adresse-ip/)  
Adresse postale trouvée : `______________________________________`
- A partir de l'ip publique du lycée donnez le Fournisseur d'Accès Internet (FAI) à l'aide du site [https://ipinfo.io/](https://ipinfo.io/)  
Nom du FAI : `__________________________`  
Vous pouvez avantageusement remplacer les trois étapes précédentes en vous connectant sur le site [ipaddress.my](https://www.ipaddress.my/).
- Connectez-vous sur le site du lycée (adresse donnée précédemment), cliquez sur le lien *Mentions légales* puis affichez le code source de la page HTML. Trouvez quel logiciel CMS (Content Management System) très connu est utilisé pour réaliser le site ainsi que sa version.  
Nom du logiciel: `_____________________` et Version: `____________`.
- Toujours dans le code HTML, trouvez le lien qui permet d'accéder à la partie gestion du site.  
Lien: `__________________________________`
- A l'aide du site [https://lookup.icann.org/fr/lookup](https://lookup.icann.org/fr/lookup), donnez le nom des deux responsables techniques qui ont enregistrés le nom de domaine **ac-limoges.fr**.  
Nom1: `_________________` Nom2: `________________________`
- Toujours dans les mentions légales, donnez le nom de l'hébergeur du site web.  
Nom de l'hébergeur : `________________________________`
- Donnez l'adresse du lycée sur twitter : `__________________________`
- Donnez l'adresse du lycée sur facebook: `__________________________`
- Quelle est l'adresse ip du **site web** du lycée (à ne pas confondre avec l'adresse ip relevée quelques lignes plus haut) en utilisant la commande *ping*.  
Adresse IP du serveur : `_______________`
- A l'aide d'une requête sur le site de l'ICANN, trouvez le nom de la société qui héberge réellement le site du lycée.  
Nom de la société : `___________________`

Si vous désirez bloquer l'accès à internet du lycée par une attaque par déni de service, vous devez attaquer quelle IP: `_____________________`.

Si vous désirez que les utilisateurs ne puissent plus visualiser le site web par une attaque par déni de service, vous devez attaquer quelle IP: `_________________`.

En cherchant les réponses aux questions précédentes vous avez fait de la collecte passive d'informations, aussi connue sous le nom d’**Open Source Intelligence, ou OSINT**, c'est le procédé de collecter des informations librement disponibles à propos de votre cible.

Cette collecte se fait généralement sans interaction directe avec la cible. La collecte passive d’informations peut se faire via des moteurs de recherche. Google permet notamment de le faire, en utilisant sa syntaxe de filtres, pour rechercher données et documents sensibles, indexés dans sa base.

Ce type de recherche Google s’appelle le **Google Dorking**, et il existe d’ailleurs un site spécialisé qui liste les requêtes communes, la [Google Hacking Database](https://www.exploit-db.com/google-hacking-database). Ce potentiel de collecte est énorme, car les sources d’information accessibles publiquement sont innombrables.

Un autre vecteur important est les réseaux sociaux. Le partage d’informations que l’on fait innocemment sur ces réseaux peut s’avérer inestimable pour un pirate, car il peut en déduire des données sensibles. Un réseau comme *LinkedIn* peut être exploité, et permettre de récupérer les noms des employés, leur rôle, et en déduire leurs e-mails, leurs identifiants utilisateurs de connexion, voire leurs mots de passe.

Pour organiser votre recherche, il est conseillé de suivre cette [carte mentale](https://osintframework.com/). Bien sur vous n'êtes pas obligé de suivre toutes les étapes !

!!! tldr "Conclusion partielle"
    L'étape de recherche d'informations est indispensable à la préparation d'un test d'intrusion réussi. Cette étape peut être longue mais permettra de hiérarchiser les cibles lors de la troisième étape. Une autre source d'informations qui peut fournir des renseignements précieux et souvent précis est [**l'ingénierie sociale**](https://fr.wikipedia.org/wiki/Ing%C3%A9nierie_sociale_(s%C3%A9curit%C3%A9_de_l%27information)).

## Scan des systèmes

L'étape 3 **analyse des systèmes cibles** permet d'identifier des failles potentiels dans le système d'informations. A partir des informations récupérées à l'étape 2 vous allez cibler avant tout le réseau et les services fonctionnant sur des machines. Pour réaliser cette étape, vous allez utiliser un seul outil dans le cadre de ce TP : [Nmap](https://nmap.org/).

Vous allez approfondir la connaissance de cette commande dans ce TP. Il est possible que la commande `nmap` ai besoin à certains moment de droits privilégiés (droits administrateur). Dans ce cas, il faudra préfixé votre commande `nmap` par la commande `sudo` (super user do).

### Quelques révisions et approfondissements...

Rappelez la commande `nmap` qui permet de scanner simplement un réseau: `_______________________________`.  
Rappelez la commande `nmap` qui permet de scanner simplement une machine: `___________________________________`.  
Il est conseiller de rendre la commande `nmap` plus *verbeuse* afin de mieux comprendre ce qu'elle fait. Donnez les deux options possibles à rajouter à la ligne de commande de `nmap` (attention une à la fois): `_______` ou `_______`. Dans les commandes qui suivent vous pouvez donc les utiliser.

!!! note "Exercice : comparaison scan TCP et UDP"
    En fait l'option `-sP` dans la première commande est *dépréciée (deprecated)*, il faut maintenant utiliser l'option `-sn`. A l'aide de la documentation (`nmap -h` pour une doc rapide), précisez le type de scan employé : `___________________`.  
    Donnez le nom de la commande qui réalise ce type de demande sur une machine à la fois et qui est très employée pour savoir si une machine est joignable sur un réseau: `__________________________`.  
    Ce type de scan est très rapide mais malheureusement pas toujours efficace pour détecter toutes les machines d'un réseau. Une autre technique un peu plus lente mais plus efficace est la technique du scan par drapeaux de synchronisation. Cherchez dans la documentation cette option: `__________` puis précisez quel protocole de transport est utilisé : `_____________`. Testez votre commande et dites combien de machines ont répondu: `______`.  
    Quel autre protocole de transport connaissez-vous ? `_________________`. Relancez un scan en utilisant ce protocole (attention c'est très long, vous pouvez interrompre le scan avec les touches ++ctrl+c++) et en précisant ci-après votre commande: `__________________________`. Si vous êtes aller au bout du scan : combien de machines ont été découvertes: `_______`. Comparez à votre scan précédent !  
    Le résultat d'un scan TCP et UDP est fourni dans ce [fichier texte](ResultatScanTCPetUDP.txt). Téléchargez-le, analysez-le. Comparez le temps du scan TCP (`_________ sec`) et le temps du scan UDP (`___________ sec soit ________ min`) puis donnez **au moins** deux machines (@IP, nom) qui sont apparus au scan UDP par rapport au scan TCP:  
    machine1: nom: `___________________` @IP:`__________________`  
    machine2: nom: `___________________` @IP:`__________________`  
    Combien y-a-t-il au total de machines dans ce réseau ? `________`

!!! note "Exercice : amélioration du scan TCP et identification des services"
    Dans l'exercice précédent, le scan UDP a donné les @IPs mais aussi les ports qu'il a testé. Il serait bien de faire maintenant un scan des ports en mode TCP qui permette d'obtenir le vrai service qui fonctionne derrière le port testé ainsi que le numéro de version du logiciel qui fournit ce service. Proposez une commande qui va scanner la machine 10.187.52.6: `____________________________________________`. Complétez le tableau ci-dessous.

|Numéro du port|Nom du service|Version|
|:--:|:--:|:--:|
||||
||||
||||
||||
||||
||||
||||

!!! note "Exercice : détermination du système d'exploitation (OS : Operating System)"
    Il nous reste un élément important à déterminer pour terminer notre cartographie de la machine cible. Quel peut être le système d'exploitation qui fonctionne sur celle-ci. Le logiciel `nmap` va nous aider une fois de plus. Donnez la commande qui permet de déterminer le SE de la machine de manière agressive : `_____________________________________________________`.  
    Donnez alors l'adresse MAC de la machine scannée, le type de SE supposé (ligne *running* et *OS details*), son temps depuis la dernière extinction *uptime*:  
    Adresse MAC: `_______________________________________________` et son fabricant `__________________________`  
    Système d'exploitation: `_______________________________________________`  
    UpTime: `_______________________________________________`.  
    Pourquoi cette dernière information est-elle importante ? `_________________________________________________________________________`

!!! danger "Exercice final"
    Réalisez la cartographie des machines de la section de BTS CIEL pour les adresses IP comprises entre 10.187.52.1 et 10.187.52.11. Vous dessinerez un schéma réseau des machines trouvées avec des icônes qui les représentent le mieux et vous donnerez tous les renseignements possibles sur chacune d'elle à l'aide des commandes que vous avez testé précédemment. Vous pouvez utiliser un logiciel en ligne type [diagrams.net](https://diagrams.net) pour faire votre compte rendu que vous donnerez au professeur présent.

## Exploitation et post-exploitation des vulnérabilités

Vous venez de réaliser la cartographie la plus précise possible du réseau afin de détecter d'éventuellement services vulnérables. Pour exploiter ces failles il faut se référer à des sites qui les répertorient. C'est à ce moment que le nom du logiciel, sa version, le système d'exploitation utilisé sont absolument indispensables. Quelques exemples de sites qui présentent les vulnérabilités :

- le site du CERT-France : [https://www.cert.ssi.gouv.fr/alerte/](https://www.cert.ssi.gouv.fr/alerte/)
- le site qui répertorie les CVE : [https://www.cve.org/](https://www.cve.org/) et un exemple de recherche sur les [failles enregistrés du programme OpenSSH](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=openssh)

La recherche des failles peut être aussi réalisée avec le logiciel [MetaSploit](https://www.metasploit.com/). Mais cette étape fera partie d'un autre TP.
