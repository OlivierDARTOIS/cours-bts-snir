# TP Utilisation du logiciel GLPI

!!! tldr "Objectifs de ce document"

    - configurer une solution de gestion de parc informatique et de *helpdesk* : GLPI,
    - configurer un *agent* pour réaliser l'inventaire de manière automatique sous Microsoft Windows et GNU/Linux, 
    - cycle de vie d'un *ticket* : création, assignation, traitement, retour d'expérience
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - TP AIS - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 2
    - Compétences évaluées dans ce TP
        - C06 - Valider un système informatique (25%)
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (50%)
    - Principales connaissances associées
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10

## Introduction

Vous avez installer et réaliser la configuration de base d'une solution **GLPI** dans le précédent TP. Vous devez maintenant personnaliser la configuration de GLPI pour l'adapter à votre entreprise. Vous devez connaître l'organisation de la page d'accueil, créer des *entités* représentatives de la structure de votre entreprise. Cela vous conduira à créer des *utilisateurs*, affecter les entités à ces mêmes utilisateurs ainsi que leurs niveaux de droits sur celles-ci gérés par des *profils*. Nous allons développer tout cela dans la suite du TP.

**Diagramme d'exigences du TP** :

Ce diagramme vient *compléter* le diagramme d’exigences du premier TP.

![Diagramme d'exigences du TP](../img/diagExigencesTPGLPI2.svg)

## Présentation de la page d'accueil de GLPI

Ci-dessous vous avez la capture écran de la page d'accueil par défaut du logiciel GLPI avec l'utilisateur `glpi` créé lors de l'installation. Cet utilisateur est en fait un **super-administrateur**, il a absolument tous les droits sur le logiciel et donc un maximum de possibilités dans les menus.

![Écran d'accueil GLPI pour le super administrateur GLPI](../img/ecranAccueilGLPI.png)

|Numéro|Fonction|
|:---:|:---|
|1|Le menu **Utilisateur** vous permet de changer vos préférences : votre langue, votre mot de passe par l'item *Mes préférences*, l'aide en ligne mais surtout sur quelle(s) entité(s) vous allez travailler. Vous pouvez aussi vous déconnecter.|
|2|Le menu **Principal** vous permet d'accéder aux différents modules de GLPI parmi : Parc, Assistance, Gestion, Outils, Administration, Configuration.|
|3|Cette zone vous présente le **Fil d'ariane**, c'est à dire le contexte d'utilisation de la zone principale (zone 4)|
|4|La **Zone principale** de travail est la zone privilégiée pour les interactions avec l'application GLPI.|
|5|La **Boîte de recherche** vous permet d'effectuer des recherches globales à n'importe quel moment.|

Description rapide des modules présents dans la zone 2:

|Nom|Description|
|:---:|:---|
|**Parc**| cet item vous permet d'accéder à la gestion de votre parc informatique déjà inventorié (ordinateurs, périphériques, logiciels, etc...)|
|**Assistance**|cet item vous permet de créer et suivre des *tickets*, des problèmes et de suivre les statistiques des demandes|
|**Gestion**|cet item vous permet de gérer des contacts, des fournisseurs, votre budget, des contrats et divers documents (non traité dans ce TP)|
|**Outils**|cet item vous permet de gérer des projets, des notes, la base de connaissance, les réservations de matériels, les comptes rendus|
|**Administration**|cet item vous permet, si vous en avez les droits, de gérer les utilisateurs, les groupes, les entités, les profils, les règles, la file de messages des demandes|
|**Configuration**|cet item vous permet d'accéder aux options de configurations générales : notifications, tâches automatiques, authentification, *plugins*|

!!! warning "Attention !"
    Les modules disponibles dans le menu principal dépendent de votre *profil* (donc de vos droits). Vous aurez le menu complet si vous êtes un **super-admin** sinon vous aurez un menu simplifié.

Ci-dessous vous avez une présentation des étapes à réaliser pour obtenir un fonctionnement correct pour réaliser l'inventaire :

```dot
digraph BPGLPI {
  fontname="Helvetica,Arial,sans-serif"
  node [fontname="Helvetica,Arial,sans-serif"]
  edge [fontname="Helvetica,Arial,sans-serif"]
  node [shape=box];
  rankdir="LR";
  
  subgraph cluster_0 {
	style=filled;
	color=lightgrey;
  fontcolor=black;
	node [style=filled,color=white,fontcolor=black];
  "Traiter les alertes\nde sécurité" -> "Créer les entités\nde l'entreprise"
	"Créer les entités\nde l'entreprise" -> "Créer/Supprimer\ndes profils"
	"Créer/Supprimer\ndes profils" -> "Créer vos\nutilisateurs"
	"Créer vos\nutilisateurs" -> "Déployer vos agents\navec un TAG"
	label = "Bonnes pratiques Post installation GLPI";
  }
}
```

Si vous avez suivi la premiere partie sur l'installation de GLPI, vous avez du traiter les alertes de sécurité. Vous allez donc passer à l'étape 2 : la création des **entités** dans GLPI.

## Création des **entités**

Le concept d'entité est un concept clé dans GLPI. Les entités s'organisent en une hiérarchie qui décrit le fonctionnement d'une entreprise. Cela permet à une seule instance de GLPI d'isoler les différentes composantes de l'entreprise afin de mieux gérer les droits. Ces entités peuvent reproduire l'organisation de votre annuaire (LDAP ou Active Directory). Une fois le système des entités mis en oeuvre un ordinateur peut être affecté à une entité, un ticket peut être affecté à une entité, les profils et autorisations peuvent être spécifique à une entité. Ci-dessous vous avez sur la gauche une proposition de hiérarchie pour une entreprise (par exemple l'éducation nationale) et sur la droite sa mise en oeuvre dans un cas réel.

```dot
digraph ExEntites {
  fontname="Helvetica,Arial,sans-serif"
  node [fontname="Helvetica,Arial,sans-serif"]
  edge [fontname="Helvetica,Arial,sans-serif"]
  node [shape=box];
 
  subgraph cluster_0 {
    style=filled;
    color=lightgrey;
    fontcolor=black;
    node [style=filled,color=white,fontcolor=black];
    Racine -> Pays
    Pays -> Région
    Région -> Département
    Département -> Ville
    Ville -> Lycée
    Ville -> Collège
    Ville -> Ecole
    Lycée -> Batiment
    Batiment -> Etage
    Etage -> Salle
    Collège -> Batiment
    Ecole -> Batiment
    label = "Ex. Arborescence Entités";
    Batiment [color="#BFE8E4"]
    Etage [color="#BFE8E4"]
    Salle [color="#BFE8E4"]
  }
 
  subgraph cluster_1 {
    style=filled;
    color=lightgrey;
    fontcolor=black;
    node [style=filled,color=white,fontcolor=black];
    "Entité Racine" -> France
    "Entité Racine" -> Chine
    "Entité Racine" -> Italie
    France -> Aquitaine
    France -> "Grand Est"
    France -> "Ile de France"
    Aquitaine -> Creuse
    Aquitaine -> Corrèze
    Aquitaine -> "Haute-Vienne"
    Creuse -> Guéret
    Corrèze -> Brive
    "Haute-Vienne" -> Limoges
    Limoges -> "Lycée Valadon"
    Limoges -> "Lycée Turgot"
    Limoges -> "Collège Maurois"
    "Lycée Turgot" -> "Batiment A"
    "Lycée Turgot" -> "Batiment B"
    "Lycée Turgot" -> "Batiment C"
    "Batiment C" -> "Etage 1"
    "Batiment C" -> "Etage 2"
    "Batiment C" -> "Etage 3"
    "Etage 2" -> "Salle C201"
    "Etage 2" -> "Salle C202"
    "Etage 2" -> "Salle C203"
    label = "Entités GLPI Education Nationale";
  }
}
```

Comme vous le constatez une entité **racine** existe dans tous les cas. Si vous donnez des droits d’accès à cette entité racine à un profil ou une personne alors ce profil ou cette personne pourra effectuer toute ces actions sur l'intégralité de l'arbre (cas du super-admin **glpi**). Au contraire vous pouvez limiter les droits à une entité plus basse hiérarchiquement dans l'arbre, la personne ou le profil visualisera alors que cette entité (et ces sous-entités) ainsi que ce qui s'y rattache (cas d'un enseignant du lycée Turgot par exemple).

Les entités sont gérés par le super-admin **glpi** et s'ajoute par le menu **Administration** puis **Entités**.

!!! note "Exercice: Créer les entités de l'arbre **Education Nationale**"
    Connectez-vous en tant qu'utilisateur **glpi** puis construisez l'arbre des entités **Education nationale**.

<span style="color: #ff0000">**Validation professeur :**</span>

## Gestion des profils

Un profil est une liste de droits, définissant pour chaque fonction de l'application l'autorisation d'utiliser ou non cette fonction. Les profils sont ensuite appliqués à chaque utilisateurs (qui a donc accès à un ensemble restreint ou non de fonctions) sur une ou plusieurs entités. Les profils peuvent être répartis en deux catégories :

- ceux orientés vers les clients du service *helpdesk*,
- ceux en charge de la gestion du parc et de l'assistance.

La gestion des profils se fait en accédant au menu **Administration** puis **Profils** et nécessite d'avoir des droits *super-admin*. Différents profils (8 par défaut) sont disponibles dans GLPI après l'installation.

!!! note "Exercice : Complétez le tableau ci-dessous avec le nom des profils en fonction de la description donnée. Complétez aussi s'il s'agit d'un profil orienté *client* ou *gestion*"
    |Nom du profil|Description|Client/Gestion|
    |:---:|:---||
    ||Ce profil possède tous les droits et donc celui de configurer et de paramétrer l’application. Le nombre d’utilisateurs ayant ce profil doit rester restreint. Ce profil permet de créer des utilisateurs **Admin** qui auront en charge la gestion d'une partie des entités définies dans le logiciel.||
    ||Ce profil administre les droits sur l’intégralité du logiciel GLPI. Cependant, il n’a pas accès à toutes les fonctionnalités sur la configuration des règles, des entités et autres points sensibles pouvant dégrader les actions de GLPI.||
    ||Ce profil possède des droits similaires à ceux du technicien ainsi que les droits pour gérer l’organisation d’une équipe (attribution des tickets), en quelque sorte il s'agit d'un superviseur des techniciens.||
    ||Ce profil correspond à la fonction de technicien de service informatique. Il permet d’accéder aux fonctions de gestion de parc en lecture et d’intervenir dans le traitement des tickets du *helpdesk*.||
    ||Ce profil permet de saisir les tickets et d’avoir accès à leur suivi. En revanche, ce profil ne permet pas de les prendre en charge. C'est ce profil qui est souvent utilisé dans les centres d'appels qui assurent le support niveau 1.||
    ||Ce profil est dédié au dépôt de tickets d’assistance. Il est défini à l’installation de GLPI comme étant le profil par défaut. Ainsi, GLPI attribue ce profil à tout nouvel utilisateur qui se connecte. Ce profil est typiquement celui de vos utilisateurs.||
    ||Ce profil n’accède qu’en lecture aux informations liées à l’inventaire et à sa gestion. S’agissant des tickets, ce profil peut les déclarer ou s’en voir attribuer mais il ne peut pas en attribuer. Ce profil ne sera pas utiliser dans ce TP.||
    ||Ce profil définit l’accès en lecture seule. Il est utilisé lorsque les objets sont verrouillés. Il peut également être utilisé pour donner aux utilisateurs le droit de déverrouiller des objets. Ce profil ne sera pas utiliser dans ce TP.||

!!! note "Exercice : Activer l'association de matériel avec un ticket"
    Rendez-vous dans le menu **Administration** puis **Profils**. Sélectionnez alors le profil **Self-service**. Dans l'onglet **Assistance** partie **Association**, vérifiez sur la ligne *Liaison avec les matériels pour la création de tickets* que la case **Tous les éléments** est cochée. Sauvegardez vos modifications avec le bouton **Sauvegarder** en bas de page. Cette manipulation sera utile lors de la création de ticket (voir la section *Gérer le support aux utilisateurs*).

<span style="color: #ff0000">**Validation professeur :**</span>

## Gestion des utilisateurs

Maintenant que vous avez créé vos entités et que vous connaissez les différents types de profils, vous pouvez créer vos utilisateurs en sachant quels **droits** ils auront sur quelle(s) **entité(s)**. La création des utilisateurs est accessible par le menu **Administration** puis **Utilisateurs**. Vous ajoutez un utilisateur en cliquant sur le bouton **Ajouter utilisateur...** en haut de l'écran. Remplissez les différents champs: son *identifiant*, le *mot de passe*, la période de *validité* du compte (par exemple, pour un stagiaire, sa durée de stage). Le plus important est la partie sous le bandeau **Habilitation** : il faut choisir son **profil** et les **entités** auxquels son profil s'appliquera. Il faut aussi activer l'option **Récursif** pour que l'utilisateur est accès aux sous-entités de son entité principal.

!!! note "Exercice : Ajouter des utilisateurs"
    Vous allez commencer par rajouter des profils **admin** sur des entités différentes pour déléguer la gestion des sous-entités à ces administrateurs.
      
      - utilisateur/mdp: adminAquitaine/aa, profil: admin, entité: France->Aquitaine
      - utilisateur/mdp: adminGrandEst/ag, profil: admin, entité: France->Grand Est
      - utilisateur/mdp: superviseurAquitaine/sa, profil: superviseur, entité: France->Aquitaine
      - utilisateur/mdp: tech1Aquitaine/t1, profil: technicien, entité: Lycée Valadon
      - utilisateur/mdp: tech2Aquitaine/t2, profil: technicien, entité: Lycée Turgot
      - utilisateur/mdp: utilisateurTurgot/ut, profil: self-service, entité: lycée Turgot
      - utilisateur/mdp: utilisateurValadon/uv, profil: self-service, entité: Lycée Valadon

    Connectez-vous alors avec ces différents utilisateurs, vérifiez que vous avez bien affecté le bon profil et qu'ils ne peuvent interagir qu'avec la (ou les) entité(s) que vous leur avez affecté.

<span style="color: #ff0000">**Validation professeur :**</span>

## Déploiement des agents avec affectation automatique de l'entité

Pour qu'un inventaire soit directement mis dans la bonne entité il faut créer une **règle**. Admettons que vous installiez les agents GLPI sur les PCs de la salle C203 du Lycée Turgot à Limoges. Vous pouvez choisir que l'inventaire arrive au niveau de l'entité *Limoges* ou de l'entité *Lycée Turgot* ou encore de l'entité *Salle C203*. C'est donc vous qui choisissez, lors de la déclaration de la règle, à quelle entité vous allez rattacher l'inventaire.

!!! note "Exercice : Ajouter une règle à une entité"
    Vous allez rattacher l'inventaire d'une machine virtuelle Windows à la salle C203. Pour cela nous allons créer un **tag** (une marque) qui permettra de rediriger l'inventaire vers la bonne entité. Ce tag peut être **TurgotC203**. Il faut tout d'abord sélectionner la bonne entité, ici la salle **Salle C203** (menu **Administration** puis **Entités** puis sélectionnez l'entité désirée).  
    Choisissez le sous menu **Règles** de l'entité **Salle C203**. Dans la zone **Règles d'affectation d'un élément à une entité**, complétez le champ **Nom** avec *Salle C203* et le champ **Description** avec *Affectation inventaire Salle C203* puis cliquez sur le bouton **Ajouter** à la fin de la ligne. Une nouvelle règle apparaît, cliquez sur le nom de celle-ci (ici *Salle C203*). Vos arrivez alors sur une nouvelle page : la définition d'une **règle**.  
    Le sous menu **Actions** est prérempli car nous venons d'une entité (ici action d'assigner à une entité). Cliquez sur le sous menu **Critères** puis sur le bouton **Ajouter un nouveau critère**. Sélectionnez dans la liste déroulante à coté de *Critère* le champ **Tag d'inventaire** puis, dans le champ qui vient d’apparaître, tapez le nom du tag souhaité (ici **TurgotC203**). Cliquez sur le bouton **+Ajouter**. Vous pouvez visualiser la totalité de votre règle en cliquant sur le sous menu **Tous** et ainsi la vérifier en cliquant sur le bouton **Tester**.

Vous venez de rédiger votre première règle. Maintenant il faut configurer les agents GLPI avec l'envoie d'un tag qui sera reconnu par la règle précédente. C'est ce que vous allez réaliser dans l'exercice suivant.

!!! note "Exercice : Installer un agent GLPI sous Microsoft Windows avec envoi d'un TAG"
    Il faut effacer le premier inventaire que vous avez réalisé lors du TP d'installation : Menu **Parc** puis **Ordinateurs**, sélectionnez l'ordinateur souhaité en cliquant sur son nom. Dans la page de résumé qui s'affiche, cliquez sur le bouton **Mettre à la corbeille**.  
    Lors du TP d'installation de GLPI, vous avez installé l'agent GLPI pour Microsoft Windows. Désinstallez l'agent GLPI sous Windows puis relancez son installation. Au moment du choix de la cible (target), remplissez les champs comme dans le TP1 mais décochez la case **Quick installation**. Ne rien mettre dans la page **Options SSL**. Ne rien mettre dans la page **Proxy Options**. A la page **Execution Mode**, laissez le mode d'exécution à *Windows Service* mais cochez les deux cases *Run inventory* et *Install GLPI-Agent-Monitor*. Ne rien changer dans la page **HTTP server Options**.  
    Sur la page **Miscelaneous Options**, il faut rajouter un TAG qui va permettre le classement automatique dans l'arbre des entités. Dans notre cas, on va rajouter le TAG : **TurgotC203**. Validez en cliquant sur le bouton *Next*. Ne rien changer sur la page **Advanced Options**. Ne rien changez sur la page **Debug Options** mais notez cependant le chemin du fichier de log qui peut nous servir en cas de problème d'inventaire: `_____________________________________________`. Terminez ensuite l'installation.  
    Connectez-vous sur l'interface web de l'agent puis déclenchez un inventaire. Patientez quelques minutes puis vérifiez que l'inventaire a été fait et affecté à la bonne entité dans le tableau de bord du logiciel GLPI.

!!! note "Exercice : Installer un agent GLPI sous GNU/Linux avec envoi d'un TAG"
    Effacer l'inventaire de votre machine virtuelle Linux. Vous créez une nouvelle règle vers l'entité *Lycée Valadon* pour y affecter cette machine. Le tag sera **Valadon**. Pour désinstaller l'agent GLPI (en remplaçant *num_version* par le numéro de version réel):
    ```console
    perl glpi-agent-num_version-linux-installer.pl --uninstall
    ```
    Puis proposer la commande d'installation de l'agent qui intègre le nom du tag (reportez-vous à la commande d'installation de l'agent dans le premier TP GLPI):
    `___________________________________________________________`.  
    Vérifiez que l'inventaire s'effectue et qu'il est affecté à la bonne entité (ici *Lycée Valadon*).

<span style="color: #ff0000">**Validation professeur :**</span> 

## Gérer le support aux utilisateurs

Vous venez de réaliser la gestion de l'inventaire des ordinateurs de votre parc. Vous pouvez désormais activer le service de support à vos utilisateurs (**helpdesk** en anglais). Pour augmenter la qualité de services de votre SI (Système d'Informations), il faut pouvoir collecter les demandes de vos utilisateurs sous la forme de **tickets**. Le cycle de vie de ces tickets est présenté ci-dessous, il suit la norme **ITIL** (Information Technology Infrastructure Library) :

```dot
digraph CycleDeVieTicketsGLPI {
  fontname="Helvetica,Arial,sans-serif"
  node [fontname="Helvetica,Arial,sans-serif"]
  edge [fontname="Helvetica,Arial,sans-serif"]
  node [shape=box];
  rankdir="LR";
  
  subgraph cluster_0 {
	style=filled;
	color=lightgrey;
  fontcolor=black;
	node [style=filled,color=white,fontcolor=black];
  "Saisi du ticket\npar l'utilisateur" -> "Traitement du ticket\n(affecté à un technicien)"
	"Traitement du ticket\n(affecté à un technicien)" -> "Traitement du ticket\n(planifié par un technicien)"
	"Traitement du ticket\n(planifié par un technicien)" -> "Ticket en cours\nde traitement"
	"Ticket en cours\nde traitement" -> "Ticket résolu"
  "Ticket résolu" -> "Ticket fermé"
	label = "Cycle de vie des Tickets - GLPI";
  }
}
```

A la création d'un ticket par un utilisateur, ce ticket à comme status **Nouveau**. Quand un superviseur affecte le ticket à un technicien, le ticket passe au status **Traitement du ticket (affecté à un technicien)**. Quand le technicien acquitte le ticket et qu'une date de traitement de la demande est fixé, la ticket passe au status **Traitement du ticket (planifié par un technicien)**. Quand une solution est trouvée, le ticket passe à **résolu**. Enfin **l'utilisateur**, qui a édité le ticket, valide la solution et seulement à ce moment le status du ticket passera à **fermé**.

Un ticket est généralement ouvert par un utilisateur avec le profil **Self-service** (qui est le profil par défaut pour les nouveaux utilisateurs). Ce profil fixe l'interface web à **simplifiée** ce qui restreint les possibilités de l'utilisateur. Un ticket peut aussi être ouvert par un technicien (par exemple dans un *call-center*).

!!! note "Exercice : Valider le cycle de vie d'un ticket"
    Vous allez déclarer un ticket sur un des *ordinateurs* de la *salle C203* en vous connectant avec l'utilisateur **utilisateurTurgot**. Après votre connexion, il faut choisir l'entité avec laquelle vous allez associer votre ticket. Cliquez sur votre identifiant en haut à droite, dans le menu déroulant cliquez sur le choix de l'entité (le deuxième choix). Dans la fenêtre qui s'ouvre, déroulez l'arborescence (triangle noir vers la droite) pour sélectionner l'entité ou vous vous trouvez: dans notre cas nous allons prendre la **salle C203**.  
    Changez à votre convenance les premiers champs de votre demande. Pour associer un matériel, cliquez sur le **+** à coté de **Éléments associés**, cliquez sur **Général**, cherchez l'item **Ordinateurs**. Cliquez sur le nouveau champ de sélection qui vient d'apparaître puis choisissez votre ordinateur. Enfin cliquez sur le bouton **+Ajouter** pour associer ce matériel à votre demande. Vous pouvez recommencer si vous avez plusieurs matériels concernés par votre demande. Complétez le champ *Titre* et le champ *Description* puis soumettez votre demande. Revenez à l'accueil et visualisez votre ticket. Déconnectez-vous.  
    Connectez-vous en tant que **superviseurAquitaine**, Cliquez sur **Assistance** puis **Tickets**. Cliquez sur le ticket que vous avez créé précédemment. Vérifiez que le matériel associé à cette demande apparaît correctement (chercher dans la colonne de droite). Attribuez ce ticket au technicien **tech2Aquitaine** (colonne de droite, rubrique **Statut**). Pourquoi **tech1Aquitaine** n’apparaît pas ? `________________________________________________________`.  Cliquez sur le bouton **Sauvegarder** en bas de page. Visualisez l'onglet *Statistiques* pour suivre les étapes de vie du ticket.  
    Connectez-vous en tant qu'**utilisateurTurgot** pour vérifier la progression de votre ticket.  
    Connectez-vous en tant que **tech2Aquitaine** passer le ticket en **planifié**. Mettre un message pour votre utilisateur dans le *chat*. Sauvegardez. Vérifiez avec **utilisateurTurgot**.  
    Connectez-vous en tant que **tech2Aquitaine** passer le ticket en **résolu**. Mettre un message pour votre utilisateur dans le *chat*. Sauvegardez. Vérifiez avec **utilisateurTurgot** et approuvez (ou non) la solution dans la zone de *chat*. Sauvegardez. Visualisez alors les statistiques.  
    Dans quel état un ticket non approuvé est-il mis (créez un nouveau ticket pour répondre) ? `_____________________________________________________`.

<span style="color: #ff0000">**Validation professeur :**</span>

## Conclusion

Vous avez découvert l'utilisation de GLPI. Beaucoup de fonctionnalités de GLPI n'ont pas été présentées. Si vous devez mettre en place un tel système dans une entreprise la lecture de la documentation officielle ainsi que des tests approfondis sont nécessaires. Vous pouvez aussi vous procurez un exemplaire du [livre GLPI](https://www.editions-eni.fr/livre/glpi-gestion-libre-de-parc-informatique-installation-et-configuration-d-une-solution-de-gestion-de-parc-et-de-helpdesk-4e-edition-9782409037689) aux éditions ENI pour continuer votre apprentissage. Toujours chez ENI, une [formation sur GLPI existe](https://www.eni-service.fr/formation/glpi-gestion-dun-parc-informatique-et-helpdesk/) ! Ce qui est intéressant c'est le programme détaillé de cette formation qui vous indique les étapes d'apprentissages pour maîtriser ce logiciel.
