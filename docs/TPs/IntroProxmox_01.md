# TP d'initiation Administration système - Partie 1

!!! tldr "Objectifs de ce document"

    - découvrir une solution de virtualisation et containérisation basée sur le logiciel Proxmox.
    - réaliser l'administration de base d'un serveur fonctionnant sous GNU/Linux.
    - réaliser un serveur de fichiers avec gestion des droits basé sur le logiciel Samba et des clients Microsoft Windows.
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 2
    - Compétences évaluées dans ce TP
        - C06 - Valider un système informatique (25%)
        - C08 - Coder (25%)
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (25%)
    - Principales connaissances associées
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Scripts d’automatisation et d’industrialisation -> Niveau 4 : C08
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10

## Introduction

Les TPs d'administration système vous permettent d'aborder le travail quotidien des **admins sys** (administrateurs systèmes). Nous avons choisi de n'aborder
dans ce TP que l'administration système sous [Debian GNU/Linux](https://www.debian.org/index.fr.html). Une installation d'un serveur Microsoft sera abordé dans un autre TP. Vous trouverez dans le diagramme d'exigences ci-dessous les différents thèmes abordés dans ce document. En particulier et en premier lieu nous allons utiliser des machines virtuelles. Celles-ci seront gérées par le logiciel [Proxmox](https://www.proxmox.com/en/proxmox-ve).

**Diagramme d'exigences du TP** :
![Diagramme d'exigences du TP](../img/diagExigencesTPAdminSys.svg)

## Découverte de Proxmox

[Proxmox VE](https://www.proxmox.com/en/proxmox-ve) (Virtualization Environment) est une solution professionnelle de virtualisation, alternative aux solutions propriétaires existantes type [VMWare](https://www.vmware.com/fr.html). Elle est basée sur Debian 12. Elle utilise la technologie de virtualisation KVM (Kernel-based Virtual Machine) afin de servir d’hyperviseur Linux pour des machines virtuelles. Et elle utilise LXC (LinuX Containers) pour gérer des conteneurs Linux.

!!! info "Oracle VirtualBox"
    Oracle VirtualBox est une solution de virtualisation personnelle très performante et gratuite pour une utilisation non commerciale. Généralement vous utiliserez cette solution pour faire fonctionner des machines virtuelles sur votre ordinateur personnel. C'était cette solution qui, avant le BTS CIEL, était utilisée sur les machines de la section de BTS. Vous pouvez télécharger cette solution sur [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads) et lire [l'ancienne documentation](VirtualBox_Jan2022.pdf) au format PDF !

Vous pourrez ainsi avec Proxmox VE créer et gérer des VM et conteneurs, des ressources de stockage, réseaux virtuels, clusters, etc. Vous pourrez le faire depuis une interface web (présenté ici), ou en ligne de commandes (non traité ici).

Veuillez noter que vous pouvez [télécharger Proxmox VE](https://www.proxmox.com/en/downloads/category/proxmox-virtual-environment) librement et l'installer sur une machine pour le tester chez vous. Dans le cadre du BTS, un serveur (Serveur HP DL380, 256 Go de mémoire, 20 cœurs, 16To de disques) fournira le service Promox et hébergera vos machines virtuelles.

Le schéma réseau associé à ce TP est le suivant:
![Schéma réseau pour le TP Proxmox](../img/SchemaReseauProxmox.png)

Le but de ce premier exercice est de **créer** une machine virtuelle GNU/Linux Debian et de **cloner** une machine virtuelle Microsoft Windows 10.

## Découverte de l'environnement Proxmox

!!! note "Exercice"
    Démarrez votre ordinateur sous Microsoft Windows puis connectez-vous à l'adresse `https://10.187.52.243:8006` depuis un navigateur web. Acceptez le certificat de sécurité auto signé pour poursuivre. Saisissez votre login et mot de passe fourni par l'enseignant présent. Vérifiez que vous êtes bien dans le *royaume* (realm en anglais) **Proxmox VE authentication server** avant de valider. Une fois que vous êtes connecté, cliquez sur votre nom en haut à droite puis choisissez l'option *mot de passe* pour changer votre mot de passe pour plus de sécurité.

Vous arrivez sur l'interface suivante:
![Page accueil Proxmox](../img/PageAccueilProxmox.png)

|Numéro|Fonction|
|:--:|:--|
| 1 | Cette vue arborescente présente toutes les ressources auxquelles vous pouvez accéder. Dans la capture ci-dessus la vue **Vue dossier** est utilisée et c'est celle que nous vous conseillons par défaut. Lorsque vous cliquez sur un des noeuds de la liste déroulante (Centre de données, Noeuds, Pool de ressources, Machine virtuelle, Stockage) la colonne de la **zone 2** change de contenu en fonction de ce que vous avez sélectionné. |
| 2 | Cette vue présente toutes les options disponibles en fonction de ce qui est sélectionné dans la **zone 1**. Par exemple si vous sélectionnez une machine virtuelle dans la zone 1 puis l'item *Matériel* dans la zone 2 vous verrez alors apparaître dans la **zone 3** les caractéristiques techniques de la machine virtuelle. |
| 3 | Cette vue permet de visualiser voire de modifier les paramètres de l'item sélectionné dans la zone 2. Dans la continuité de l'exemple précédent, si vous avez l'item *Matériel* sélectionné dans la zone 2, vous pouvez à partir de la zone 3 et des boutons en haut de celle-ci, ajouter ou supprimer des éléments de votre machine virtuelle. |
| 4 | Cette vue présente l'historique des différentes actions demandées et si elles ont réussi ou échoué. C'est donc le fichier de **log** qui permet d'avoir une traçabilité de vos actions. |
| 5 | Ce bouton permet de créer des machines virtuelles (VM) et le bouton voisin des conteneurs LXC. |
| 6 | Ce bouton permet de vous déconnecter et de gérer vos paramètres. |

!!! note "Exercice"
    === "Complétez les questions suivantes"
        Familiarisez-vous avec l'interface de Proxmox en naviguant dans les différents items. Suivez la logique : je sélectionne un élément dans la colonne de gauche (**zone 1**) puis je choisis un item dans la colonne centrale (**zone 2**) puis je peux modifier des éléments de l'item sélectionné dans la **zone 3**.  
        Donnez le nombre de processeurs, la taille du disque dur et la taille de la mémoire vive (RAM en anglais) des machines virtuelles modèles **CIEL-Win10** et **CIEL-Debian12Base**.  
        Donnez le nom de l'image ISO (image d'un CDRom ou DVDRom) ainsi que la taille de la plus petite image ISO hébergée sur le serveur.
    === "VM CIEL-Win10"  
        - Nb de processeurs :
        - Taille du disque dur en Go :
        - Taille de la mémoire vive en Go :
    === "VM CIEL-Debian12Base"
        - Nb de processeurs :
        - Taille du disque dur en Go :
        - Taille de la mémoire vive en Go :
    === "Analyse des images ISO disponibles sur le serveur"
        - Nom de l'image ISO :
        - Taille de l'image ISO (précisez l'unité) :

<span style="color: #ff0000">**Validation professeur :**</span>

## Clonage d'une MV modèle

Des machines virtuelles *modèles* sont mises à disposition des étudiants pour personnaliser rapidement un environnement. Vous évitez ainsi l'installation de base du système d'exploitation (Microsoft Windows ou GNU/Linux) qui peut être relativement longue. Les MVs modèles sont disponibles dans le menu **Machine virtuelle** dans la colonne de gauche (zone 1). Vous pouvez les reconnaître grâce à l'écran qui est rajouté à coté de leur nom. Dans la suite de ce TP (partie administration système avancée), vous aurez besoin d'une machine Microsoft Windows 10. Vous n'allez pas l'installer à partir d'une image ISO mais plutôt la cloner à partir du modèle.

!!! note "Exercice"
    === "Réalisez les actions demandées puis répondez aux questions"
        Cloner la MV modèle de Windows 10 en faisant un clic droit sur celle-ci puis sélectionnez l'item **Cloner**.  
        Dans la fenêtre qui s'ouvre ne changez pas les items *Noeud cible* et *VM ID*. Complétez le champ *Nom* avec un nom facilement compréhensible (pas d'accent, d'espace, de caractères spéciaux). Le champ *Pool de ressources* doit vous proposez une seule option : le pool portant votre nom. Enfin positionner le champ *Mode* à *Clone intégral*, passez le champ *Stockage cible* à votre pool de stockage. Le champ *Format* reste sur *QEMU*.  
        Cliquez alors sur le bouton **Cloner** et patienter car le clonage intégral duplique la MV d'origine.  
        Une fois la machine dupliquée, sélectionnez-la puis cliquez sur le bouton **Démarrer** (en haut à gauche de la zone 3). Cliquez ensuite sur l'item **Console** (dans la zone 2) pour visualiser votre machine virtuelle Windows.  
        Identifiez-vous (login: ciel, mdp: ciel) puis saisir les paramètres IP donnés par le professeur présent en suivant la démarche proposée:  
        Tapez *paramètres* dans la barre de recherche puis cliquez l'application *Paramètres*, sélectionnez *Réseau et Internet*, dans la partie droite de la fenêtre qui vient de s'ouvrir, sous l'icône *Ethernet* cliquez sur le bouton *Propriétés*. Faites défiler jusqu'à *Paramètres IP* puis cliquez sur le bouton *Modifier*. Saisissez les paramètres IPv4 fournis et désactivez les paramètres IPv6.  
        A l'aide de commandes *ping*, testez la connectivité entre votre machine Windows réelle et la machine virtuelle.  
        Pour arrêter votre machine virtuelle, faites comme une machine réelle sous Microsoft Windows.
    === "Notez les paramètres IP ci-dessous:"
        - Adresse IP de la MV et masque (notation pointée et CIDR) :
        - Adresse IP de la passerelle :
        - Adresse IP du serveur DNS (Domain Name Service : service de résolution de nom de domaine) :
        - Commandes *ping* :
        - Résultats et explications :

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice optionnel"
    L'image diffusée dans l'onglet proxmox de votre machine virtuelle Windows est généralement de mauvaise qualité. Vous allez mettre en place le **Bureau à distance** sur la machine virtuelle et vous vous connecterez sur celui-ci depuis votre machine réelle. Vous constaterez alors que la qualité est nettement supérieure. Pour cela suivez la procédure suivante:  
    Sur la **machine virtuelle** tapez dans la zone de recherche *paramètres* puis cliquez sur l'application *Paramètres*. Dans la zone de recherche des paramètres, tapez *Bureau*  puis choisissez l'item *Paramètres du bureau à distance*. Déplacez alors le bouton sur *Activé* pour le paramètre *Activer le Bureau à distance*.  
    Sur la **machine réelle**, dans la zone de recherche tapez *bureau à distance* puis cliquez sur l'application *Connexion Bureau à distance*. Dans la fenêtre qui apparaît, tapez l'adresse IP de votre machine virtuelle puis validez. Tapez alors votre login et mot de passe de la machine virtuelle. Passez outre l'avertissement de sécurité en cliquant sur le bouton *Oui*. Vous avez alors la main sur la machine virtuelle en mode graphique en haute qualité !

## Création d'une MV

Pour réaliser la partie **Administration système de base**, vous allez installer une machine virtuelle sous Debian GNU/Linux (en version 12 actuellement) à partir d'une image ISO disponible sur le serveur Proxmox. Cette installation se fera pour un environnement professionnel: pas d'interface graphique, mode console uniquement, installation la plus simple possible pour ensuite installer uniquement les logiciels nécessaires, mise en place du RAID (niveau 1 et 5) pour le système et les données utilisateurs, découverte de base de l'administration système.

### Qu'est ce que le **RAID** et son utilité ?

Le mot **RAID** est un acronyme qui signifie *Redundant Array of Independant Disks*.  C'est le regroupement de plusieurs disques indépendants pour en former un autre avec différents niveau RAID. Nous allons décrire trois niveaux de RAID (0,1 et 5), sachant qu'il en existe d'autres, à l'aide de l'illustration suivante:

![Niveau RAID 0,1 et 5](../img/NiveauRAID_Poster_HP.png)

!!! warning "Remarque importante"
    Un système RAID ne vous affranchit pas de faire de la sauvegarde de données chiffrée (par exemple sur un disque externe ou sur internet). Dans la machine virtuelle vous ne verrez pas d'effet du RAID car les disques sont simulés sur un disque physique, par contre, en projet de deuxième année il est possible que vous mettiez en oeuvre cette solution sur un serveur physique avec cinq disques ou plus SATA et/ou SSD.

### Cahier des charges pour le serveur GNU/Linux Debian

Vous allez installer votre serveur virtuel avec les contraintes matérielles suivantes:

- Le système (noté **/** ou racine) sera installé sur **deux disques virtuels de 6 Go en mode RAID1** afin d'assurer une fiabilité importante et un transfert de données rapide en lecture. Le disque RAID résultant sera appelé **md0** (multi-disque numéro 0), le système de fichiers utilisé sur ce disque sera **EXT4**,
- Les répertoires des utilisateurs et les partages SAMBA (noté **/home**) seront installés sur **trois disques virtuels de 2Go en mode RAID5** afin d'assurer une fiabilité importante et de bonnes performances en lecture/écriture. Le disque RAID résultant sera appelé **md1** (multi-disque numéro 1), le système de fichiers utilisé sur ce disque sera **EXT4**,
- la mémoire RAM sera fixée à 2 Go, le nombre de processeurs à 2.

!!! note "Exercice - Création de la MV Serveur Debian GNU/Linux"
    Les étapes ci-dessous décrivent la création de la machine virtuelle qui va accueillir le serveur Debian GNU/Linux. Il n'y a pas de captures d'écrans, juste de la description textuelle et rien ne vous interdit de **lire ce qui apparaît à l'écran !!**.

    1. Cliquez sur le bouton **Créer une VM** (en haut à droite de la zone 3),
    1. Une fenêtre s'ouvre sur le premier onglet de création d'une MV, l'onglet **Général**. Laissez les items *Noeud* et *VM ID* à leur valeur par défaut. Donnez un nom à votre MV dans l'item *Nom* préfixé par **CIEL-**. Choisissez votre pool dans l'item *Pool de ressources*. Ne rien changeR d'autre. Validez avec le bouton **Suivant**,
    1. Vous êtes maintenant sur l'onglet **Système d'exploitation**. Vous allez installer votre serveur à partir d'une image ISO. Il faut donc cocher *Utiliser une image de média*, vérifiez que l'item *Stockage* correspond au stockage qui héberge les images ISO et enfin choisir son ISO dans l'item *Image ISO*. Dans notre cas l'image ISO sera quelque chose comme *Debian-12.X.X-AMD64-DVD*. Le *Système d'exploitation de l'invité* sera réglé pour le *Type* à **Linux** et la *Version* à **6.x Kernel**. Validez avec le bouton **Suivant**,
    1. Dans l'onglet **Système**, vous ne devez rien modifier donc vous cliquez sur le bouton **Suivant**,
    1. Vous êtes dans l'onglet **Disques**, un des plus important pour l'installation de notre serveur. Laissez les items *Bus/périphérique*,*Cache* et *Format* à leurs valeurs par défaut. Vérifiez que l'item *Stockage* est bien positionné sur votre espace de stockage. Modifiez la taille du disque virtuel avec l'item *Taille du disque (GB)*. Vous pouvez ensuite rajouter un nouveau disque virtuel en cliquant sur le bouton **Ajouter** en bas à gauche de la fenêtre. Rajoutez donc vos cinq disques virtuels pour respecter le cahier des charges (**ATTENTION** à la taille des disques !). Si vous vous trompez, vous pouvez effacer un disque dur virtuel en cliquant sur l'icône *Poubelle* à coté de celui-ci. Quand vous avez fini cette étape, cliquez sur le bouton **Suivant**,
    1. Dans l'onglet **Processeur**, vous modifiez juste l'item *Coeurs* que vous passez à 2 pour respecter le cahier des charges. Le reste n'est pas modifié donc vous pouvez passer au prochain onglet,
    1. L'onglet **Mémoire** permet de configurer la mémoire vive affecté à la MV. Attention elle s'exprime en MiB (en Mo) donc pour respecter le cahier des charges (2 Go), il faut mettre 2048 dans le champ *Mémoire*. Validez,
    1. L'onglet **Réseau** permet de configurer une carte réseau. Vous devez juste vous assurer que **vmbr1** est inscrit dans l'item *Pont (bridge)*. Les autres paramètres ne doivent pas être modifiés. Validez cet onglet.
    1. L'onglet **Confirmation** affiche un résumé des caractéristiques de votre MV. C'est le moment de vérifier vos paramètres avant de cliquez sur le bouton **Terminer**.

Quelques secondes plus tard, une nouvelle MV doit apparaître sous l'item **Machine virtuelle** de la zone 1. Vous venez de créer votre première VM ! Maintenant il faut installer un système d'exploitation dessus...

<span style="color: #ff0000">**Validation professeur :**</span>

Nous allons lister les contraintes que vous allez rencontrer lors de l'installation du système d'exploitation Debian GNU/Linux. Vous lancerez l'installation **APRES** les avoir lues toutes :

- installation en mode graphique (choix graphical install),
- choisir la langue française, un clavier français,
- au moment d'indiquer le nom du système, cliquez sur le bouton *Revenir en arrière* et sélectionnez *Configurez vous même le réseau*. Répondez aux questions sur la configuration IP avec les paramètres donnés par l'enseignant,
- le nom de votre machine sera : **CIEL-** suivi de vos initiales (prénom et nom),
- le domaine sera laissé vide, le mot de passe de l'utilisateur **root** (l'administrateur de la machine) sera positionné à **root**,
- le nom complet du nouvel utilisateur sera le votre, vérifiez ensuite ce que vous propose l'installeur et fixez votre mot de passe,
- à l'étape de partitionnement des disques durs, vous devez choisir l'option **Manuel**, vous devez retrouver vos cinq disques.
    - Cliquez sur le premier puis cliquez sur le bouton **Continuer** pour créer une table des partitions sur le disque. Recommencez sur tous les disques.
    - Vous allez ensuite configurer le RAID en choisissant l'option **Configurer le RAID avec gestion logicielle**. Vous devez maintenant vous débrouiller pour respecter le cahier des charges :
        1. constituez les deux disques RAID,
        2. formatez les disques RAID en ext4,
        3. affectez leur point de montage (/ : ligne sous périphérique RAID1 et /home : ligne sous périphérique RAID5).

Avant de passer à la suite vous devez faire valider cette étape. Une capture d'écran disponible ci-dessous vous montre ce que vous devez obtenir. Aucune partition d'échange ne sera configurer, vous pouvez continuer sans.  

<span style="color: #ff0000">**Validation professeur**</span> mise place RAID :

- L'installeur va mettre en place un système minimum, soyez patient. Vous n'avez pas d'autres support à analyser. Il faudra utiliser un miroir sur le réseau (sur internet plus précisément, cela permet d'aller télécharger les logiciels manquants directement ainsi que les mises à jour). Choisissez un miroir en France, il n'y a pas de *mandataire*.
- Vous ne souhaitez pas participer à l'étude statistique. Vous arrivez à une étape importante de l'installation, le choix des principaux logiciels. Vous souhaitez travailler en environnement minimal donc il faut décocher *environnement de bureau Debian* et *GNOME*. Cochez *Serveur SSH* et *utilitaires usuels du système*. Le système va finir de s'installer.
- Vous arrivez à l'étape d'installation du chargeur GRUB. Il faut installer celui-ci sur le disque principal. Vous choisirez le disque qui s'appelle **/dev/sda** dans la liste. L'installation se termine et vous demande de vérifier qu'il n'y a pas de support d'installation dans un lecteur : dans notre cas le CDRom virtuel contient toujours l'image ISO d'installation mais lorsque la machine virtuelle démarre elle utilise en priorité le disque dur. Vous pouvez donc cliquez sur le bouton **Continuer**.

Ci-dessous, une capture écran pour vous aider à l'étape de partitionnement des disques durs et création des RAID logiciel:

![Partitionnement Debian RAID](../img/DebianInstall_Partitionnement.png){ width=90%; : .center }

!!! note "Exercice"
    Démarrez la machine virtuelle que vous venez de créer.  
    Installez maintenant la distribution Debian GNU/Linux avec les consignes précédentes !  
    Connectez-vous ensuite avec votre compte puis tapez la commande `cat /proc/mdstat`. Cette commande permet de visualiser l'état de vos systèmes RAID md0 et md1. Si tout va bien vous devriez avoir pour md0 : `[UU]` (les deux disques durs du RAID1 sont *Up*) et pour md1 : `[UUU]` (les trois disques du RAID5 sont *Up*).  
    Déconnectez-vous en tapant la commande `exit`. Vous êtes prêts pour passer à la suite...

<span style="color: #ff0000">**Validation professeur :**</span> 

## Administration système de base

Dans les lignes suivantes vous allez découvrir quelques commandes simples d'usage générale et d'administration. Le but de cette partie sera de mettre en oeuvre et de tester un serveur **LAMP** (Linux, Apache : serveur web, MariaDB : base de données, PHP : langage de dev. web) et le logiciel d'administration de base de données **PHPMyAdmin**.

Un serveur est généralement installé dans une pièce climatisée et/ou inaccessible. Aussi vous vous connecterez rarement directement sur le serveur. Nous allons simuler cette situation en vous connectant depuis votre machine Windows virtuelle vers votre machine Debian virtuelle avec un protocole chiffré : le protocole **SSH** (Secure SHell).

!!! note "Exercice"
    Relevez l'@IP de votre machine réelle Microsoft Windows : `____________________`.  
    Démarrez votre machine virtuelle Debian, connectez-vous en tant qu'utilisateur **root** puis donnez son adresse ip à l'aide de la commande `ip addr` : `______________________`.  
    Faites un *ping* de la machine Windows vers la machine Linux : **OK ou KO**.  
    Faites un *ping* de la machine Linux vers la machine Windows : **OK ou KO**.  
    Quel est le problème avec le *ping* dans ce sens: `_________________________`  
    et comment le résoudre: `________________________`  
    Démarrez le logiciel **Putty** dans votre machine Windows. Ce logiciel va vous permettre de vous connecter à distance en sécurisé vers votre machine Linux. Lancez le logiciel Putty puis compléter les champs nécessaires pour vous connecter. Vous vous connecterez avec votre login et mot de passe puis pour passer **root** vous taperez la commande `su -`. Il faudra alors saisir le mot de passe root.

### Commandes de gestion des logiciels et mise à jour

La commande de gestion des logiciels (on les appelle les *paquets* sous GNU/Linux) est `apt`. Les commandes de gestion des logiciels doivent être exécutées en tant qu'utilisateur **root** ou à l'aide de la commande `sudo` si la distribution Linux est configurée de cette manière.

- Pour mettre à jour la liste des logiciels présents sur les dépôts: `apt update` ou `sudo apt update`
- Pour mettre à jour tous les logiciels en une seule fois: `apt upgrade`
- Pour chercher un logiciel dans la liste des dépôts: `apt search` *`mot_clé`*
- Pour installer un logiciel depuis les dépôts d'Internet: `apt install` *`nom_du_paquet`*
- Pour retirer un logiciel du système (et ces dépendances): `apt remove --purge` *`nom_du_paquet`*`
- Pour nettoyer votre disque dur: `apt clean` et `apt autoremove`

!!! note "Exercice commandes d'installation et test d'un serveur web"
    Actualisez la liste des paquets puis lancez une mise à jour: `__________________________________`
    Installez le paquet `build-essential` qui vous fournira entre autre le compilateur g++ : `________________`  
    Testez celui-ci en lançant : `g++ -v` puis donnez la version installée : `_____________`  
    Installez le serveur web `apache2`: `________________`  
    Testez que celui-ci est fonctionnel depuis un navigateur de votre machine réelle. Adresse http à taper dans le navigateur web : `_____________`  
    La page d'accueil par défaut affiche : `_____________`  
    Modifiez la page d'accueil de votre serveur Apache pour qu'il affiche votre nom et prénom : éditez le fichier `index.html` situé dans `/var/www/html`. Pour cela vous allez utiliser deux nouvelles commandes : `cd` (change directory) et l'éditeur de texte `nano`.  
    `/var/www/html` est un chemin pour accéder à un répertoire, il commencera toujours par `/` qui indique la racine du disque dur (par exemple la lettre *C:* sous Microsoft Windows). `index.html` est le nom du fichier à éditer avec nano pour le modifier. Pour sauvegarder votre modification sous nano faire ++ctrl+x++. Donnez les deux commandes à taper:  
    Commande **cd**: `________________`  
    Commande **nano**: `________________`  
    Après modification réactualisez la page web pour visualiser vos changements.

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice installation du langage PHP et tests"
    Installez le langage PHP : `_______________________________`  
    Créez dans `/var/www/html` un fichier `index2.php` avec comme contenu `<?php phpinfo(); ?>` à l'aide de la commande `nano`.  
    Commande à saisir: `__________________________________________`  
    Ouvrez ce fichier depuis votre navigateur web sous Windows. Que permet de vérifier ce test: `_____________`  

!!! note "Exercice : transférer un fichier html existant avec le logiciel WinSCP"
    Avant de transférer un fichier avec le logiciel winSCP, il faut modifier les droits du répertoire `/var/www/html` sinon vous n'avez pas les droits d'écriture. Pour cela, tapez la commande suivante :
    ```console
    chmod 777 /var/www/html
    ```
    Vous devrez très certainement installer WinSCP dans votre machine virtuelle Windows. Démarrez le logiciel WinSCP et connectez-vous sur la machine Linux avec votre compte *ciel*. Transférez un fichier (par exemple un de ceux que vous avez produit lors des Tps HTML/Javascript/PHP) vers `/var/www/html`. Visualisez le (ou les) fichier(s) que vous avez transféré à l'aide d'un navigateur web. Que tapez-vous comme adresse : `__________________________________`.

<span style="color: #ff0000">**Validation professeur :**</span>

Vous avez quasiment fini l'installation d'un serveur LAMP. Il vous manque donc un logiciel de SGBDR (Système de Gestion de Base de Données Relationnel). Nous choisissons la base MariaDB car elle remplace avantageusement MySQL (Opensource, compatible au niveau programmation). Vous allez aussi installer un logiciel web de gestion de cette base de données : phpMyAdmin.

!!! note "Exercice : installation d'un SGBDR"
    Pour réaliser le cahier des charges précédent il faut installer les paquets suivants : mariadb-server, mariadb-client, php-mysql, phpmyadmin.
    Lors de l’installation choisir le serveur web apache2 (touche espace : **une étoile doit apparaître dans la case**) et le mode de configuration *dbconfig-common*. Le mot de passe qui vous sera demandé lors de l’installation ne sert que pour l’utilisateur phpmyadmin qui n’a aucun droit de création de base.

!!! note "Exercice : création d'un nouvel utilisateur pour le SGBDR"
    Pour des raisons de sécurité, l’utilisateur `root` n’a pas le droit de se connecter sur l’interface de phpMyAdmin depuis un poste distant. Nous allons donc créer un utilisateur qui aura les mêmes droits que `root` mais depuis n’importe quel ordinateur. **Veuillez noter qu’il s’agit là d’une très mauvaise pratique qui ne devra en aucun cas être utilisée sur un serveur connecté à Internet**.

    Tapez la ligne suivante dans une console en mode administrateur :
    ```console
    mysql -e "GRANT ALL ON *.* TO 'ciel'@'%' IDENTIFIED BY 'ciel' WITH GRANT OPTION"
    ```

Vous venez de créer un utilisateur nommé `ciel` avec comme mot de passe `ciel` qui aura tous les droits sur toutes les bases (`GRANT ALL ON *.*`) depuis n’importe quel ordinateur (`%`).

!!! note "Exercice : test de connexion à l'interface de gestion de la SGBDR avec phpMyAdmin"
    Pour vous connecter à phpMyAdmin : *http://adresse_ip_serveur_linux/phpmyadmin* , l’utilisateur sera `ciel` avec le mot de passe `ciel`. Vous pouvez alors créer de nouvelles bases, de nouveaux utilisateurs, etc...

**Conclusion** : vous avez un serveur LAMP (Linux, Apache, MariaDB/MySQL, PHP) complet pour la maison afin de réaliser les Tps de première année HTML/PHP/MySQL ! Vous pouvez refaire toute cette installation avec le logiciel **Oracle VirtualBox** sur votre pc personnel.

<span style="color: #ff0000">**Validation professeur :**</span>

### Comment automatiser les tâches d'administrations ?

Vous avez installé votre premier serveur LAMP et vous vous êtes rendu compte qu'il y a de nombreuses commandes à taper. Plutôt que de recommencer à taper toutes ces lignes il est plus judicieux de créer un **script** pour enchaîner toute ces actions. Depuis le début vous tapez vos commandes dans un *interpréteur de commandes*, celui choisi par Debian s'appelle **BASH** ([Bourne Again SHell](https://fr.wikipedia.org/wiki/Bourne-Again_shell)). Le *shell BASH* fournit tout un ensemble de mots clés pour réaliser des scripts. En fait il s'agit d'un langage de programmation à part entière. Notez que sous Microsoft Windows, les scripts d'automatisation sont réalisés en [PowerShell](https://fr.wikipedia.org/wiki/Windows_PowerShell).

Une ressource possible pour la création de scripts Bash est disponible en ligne gratuitement : [Guide avancé d'écriture des scripts Bash](https://abs.traduc.org/abs-fr/index.html). Pour l'instant vous allez vous contenter d'écrire un script simple.

!!! note "Exercice : premier script BASH"
    Vous allez commencer par saisir à l'aide de nano en tant qu'utilisateur root le script suivant:
    ```console
    #!/bin/bash

    # Premier script BASH
    # automatisation de la MAJ
    echo -e "Lancement MAJ...\n"
    apt -y update
    apt -y upgrade
    echo -e "\nFin MAJ...\n"
    ```

    Sauvez ce script sous le nom **MAJ.sh** (l'extension n'est pas nécessaire mais cela indique qu'il s'agit d'un script shell). Il faut rendre ce fichier exécutable : `chmod +x MAJ.sh`. Vous pouvez alors l'exécuter: `./MAJ.sh`

Quelques explications sur ce script:

- les scripts doivent commencer par cette ligne pour indiquer que vous allez utiliser le shell BASH : `#!/bin/bash`
- un commentaire commence par le caractère `#`
- la commande `echo` permet d'afficher un message à l'écran
- les deux commandes `apt` permettent de réaliser la mise à jour

!!! note "Exercice"
    Quel est le rôle du paramètre `-e` dans les commandes `echo` : `_______________________________________`  
    Quel est le rôle du paramètre `-y` dans les commandes `apt` : `_______________________________________`  
    Quel est le rôle du caractère `\n` dans la commande `echo` (consultez ce [site](https://www.ionos.fr/digitalguide/serveur/configuration/commande-echo-sous-linux/) pour répondre) : `_______________________________________`

<span style="color: #ff0000">**Validation professeur :**</span>

## Créer un conteneur

La conteneurisation est principalement faite sous GNU/Linux avec le logiciel [Docker](https://www.docker.com/) mais il existe d'autres solutions. Proxmox a choisi d'utiliser les conteneurs [LXC](https://www.redhat.com/fr/topics/containers/whats-a-linux-container).

!!! warning "Quelle est la différence entre un conteneur et une MV ?"
    Les machines virtuelles virtualisent toute une machine (jusqu'aux couches matérielles) tandis que les conteneurs ne virtualisent que les couches logicielles au-dessus du niveau du système d'exploitation. En général les conteneurs sont plus légers en terme d'occupation mémoire, de temps de démarrage. Ils sont utilisés pour réaliser des *micro-services* (un conteneur = une tâche = un serveur web ou une BDD par exemple). De ce fait la gestion de nombreux conteneurs est généralement plus complexe que celle d'une seule machine virtuelle.

Vous allez créer un conteneur qui lancera une distribution Linux très légère (10.5 Mo) qui sert de base à de nombreux autres conteneurs : Alpine Linux. Pour cela suivez la procédure ci-dessous.

!!! note "Exercice : premier conteneur Proxmox"
    1. Cliquez sur le bouton **Créer un conteneur** en zone 5,
    1. Une fenêtre s'ouvre sur l'onglet **Général**. Donnez un nom à votre conteneur en le préfixant par **CIEL-** (par ex. CIEL-Alpine). Choisissez votre pool de ressources puis fixer le mot de passe de l'utilisateur **root** dans ce conteneur (attention minimum 5 caractères). Cliquez sur le bouton **Suivant**,
    1. Sur l'onglet **Modèles**. Choisissez **ImagesISO** pour l'item *Stockage* puis choisissez le modèle de conteneur AlpineLinux pour l'item *Modèle*. Cliquez sur le bouton **Suivant**,
    1. L'onglet **Disques**, vous permet de choisir votre pool de stockage dans l'item *Stockage*. Pour l'item *Taille du disque*, vous mettrez 1 GiB ce qui est largement suffisant pour Alpine Linux. Cliquez sur le bouton **Suivant**,
    1. Dans l'onglet **Processeur**, vous ne changez que l'item *Coeurs* que vous passerez à 2. Cliquez sur le bouton **Suivant**,
    1. Dans l'onglet **Mémoire**, laissez l'item *Mémoire* à 512Mo pour Alpine Linux. Dans le cas de conteneur hébergeant des services plus exigeants (serveur LAMP par ex.), passez cette mémoire à 4Go et l'*Espace d'échange (swap)* a 1Go. Cliquez sur le bouton **Suivant**,
    1. Sur l'onglet **Réseau** configurez la partie **IPv4** : sélectionnez une configuration **Statique** (vous fixez vos paramètres IP) selon les indications données pendant le TP. Vous devez également sélectionner le pont **Vmbr1** (lien vers le réseau BTS). Cliquez sur le bouton **Suivant**,
    1. Sur l'onglet **DNS** (choix du serveur de résolution de noms de domaines), laissez les paramètres par défaut. Bouton **Suivant**,
    1. Onglet **Confirmation**. Cette onglet affiche un résumé du conteneur que vous venez de configurer. Cliquez sur le bouton **Terminer** si la configuration vous convient.

    Un nouvel item apparaîtra dans la zone 1 avec le nom **Conteneur LXC**. Cliquez sur celui-ci, sélectionnez votre conteneur puis démarrez-le. Pouvez-vous estimer le temps de démarrage ? `______________`. Connectez-vous avec l'utilisateur **root** et le mot de passe choisi lors de la création du conteneur. Tapez la commande `df -h` qui permet d'obtenir l'occupation des disques. Notez la taille occupée par Alpine Linux (point de montage de `/`) : `____________`. Est-ce en accord avec ce qui était attendu ? `__________`.

!!! note "Exercice"
    Créez maintenant un conteneur pour un serveur *LAMP*. Le modèle de conteneur est proposé parmi ceux disponibles et tester son fonctionnement depuis un navigateur web dans la machine virtuelle Windows.

<span style="color: #ff0000">**Validation professeur :**</span>

## Conclusion

Vous venez de terminer la première partie de ce document. Vous aurez donc très prochainement un examen de TP les concepts présentés ici. **Vous aurez droit à ce document pendant l'examen donc il est de votre responsabilité qu'il soit complété et annoté avec soin**.
