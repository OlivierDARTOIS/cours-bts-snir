# Structures, énumérations, pointeurs, opérateurs de transtypage

<span style="color: #ff0000">**TP de révision de première année**</span> 

!!! tldr "Objectifs de l'exercice"

    - comprendre et mettre en oeuvre des **structures**,
    - comprendre la notion de **cast** (conversion forcée des types),
    - rôle des mots clés `sizeof` et `enum`,
    - rappel sur les **pointeurs**.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - S4. Développement logiciel
        - S4.1 : Principes de base
            - Gestion mémoire : adresse/valeur, pointeurs, variables statiques, allocations automatique et dynamique (pile/tas), etc : niveau 4 (maitrise méthodologique)
            - Variables ; durée de vie, visibilité : niveau 4 (maitrise méthodologique)
        - S4.3 : Structure et gestion des données
            - Types dérivés : tableaux, énumérations, enregistrements : niveau 3
            - Structures de données et méthodes d’accès directe et/ou indirecte : liste, file, pile, tableau, etc. : niveau 3

## Définition des constantes en C++
Le mot clé `enum` permet de définir des **constantes** de type entier et de les nommer. Les énumérations permettent de rendre le code plus lisibles et d'éviter les *nombres magiques*. Supposons qu'une application utilise plusieurs type de capteurs : *Accéléromètre*, *Anémomètre*, *Capteur de déplacement*, *Capteur de température*, *Extensomètre*, *Hygromètre*, *Inclinomètre*. Voici comment nous pouvons les implémenter et les utiliser  dans un programme :

```cpp
#include <iostream>

// Définition d'un nouveau type (TypeCapteur) 
// L'énumération commence à 0 si non précisé
enum TypeCapteur {ACC,ANE,DEP,TEM,EXT,HYG,INC};

// Déclaration 
TypeCapteur tCapteur_1; // valide en C++ mais pas en C
enum TypeCapteur tCapteur_2; // valide en C et C++

// Initialisation
tCapteur_1 = TEM; 
tCapteur_2 = ANE; 

// Utilisation ou affichage
std::cout << "Type du capteur N°1 : " << tCapteur_1 << " ; " 
          << "Type du capteur N°2 : " << tCapteur_2 << std::endl;

// On ne peut pas affecter n'importe quelle valeur à une variable de type TypeCapteur
// tCapteur_1 = 10 ; // !! ERREUR !!
```

Sortie console du code précédent:

```console
Type du capteur N°1 : 3
Type du capteur N°2 : 1
```

Il ne faut pas s'étonner de voir un entier à la place du nom que vous avez déclaré dans l'énumération, ce comportement est normal. En C++ moderne, il faut déclarer une **classe énumération fortement typée**. La documentation est disponible sur le site de C++ reference : [enumeration class](https://en.cppreference.com/w/cpp/language/enum).

!!! note "Exercice"
    === "Produire un code"
        Vous allez déclarer une énumération fortement typée (entier non signé) pour stocker les jours de la semaine et un élément `INCONNU` avec comme valeur `99`. Puis vous proposerez un test de votre énumération. Utilisez l'EDI de votre choix sous Microsoft Windows ou GNU/Linux.

        *Remarque* : souvenez-vous que la méthode `cout` ne peut afficher que des entiers (pas des `unsigned`). Il faut donc faire un transtypage simple (à la manière du C) en préfixant la variable que vous voulez afficher par `(int)`. Nous verrons dans ce TP, les opérateurs de transtypages a utiliser en C++.

    === "Une correction possible"
        ```c++
        enum class mesJoursSemaine:unsigned int { DIM=0, LUN, MAR, MER, JEU, VEN, SAM, INCONNU=99 };
        mesJoursSemaine leJourOuOnDort = mesJoursSemaine::DIM;
        mesJoursSemaine leJourOuJeTravaille = mesJoursSemaine::INCONNU;

        std::cout << "Le jour ou on dort : " << (int)leJourOuOnDort << std::endl;
        std::cout << "Le jour ou je travaille : " << (int)leJourOuJeTravaille << std::endl;
        ```

## Les structures

Les **structures** permettent des types personnalisés complexes. Les structures sont *équivalentes* à des classes qui ne contiennent **pas de méthodes** et où tous les attributs sont **par défaut public**. Dans le code ci-dessous, on définit et utilise une structure `Etudiant` qui contient deux champs : un entier et un flottant.

```cpp
#include <iostream>

// Définition
struct Etudiant
{
  int numID;
  float moyenneGenerale;
};

// Déclaration 
Etudiant etd1; // valide en C++ mais pas en C
struct Etudiant etd2; // valide en C et C++

// Initialisation : opérateur '.'
etd1.numID = 12432;
etd1.moyenneGenerale = 12.43;

// Initialisation + Déclaration : avec des accolades !
Etudiant etd3 {12458 , 15.13};

// Utilisation et Affichage
std::cout << "Numero d'ID : " << etd3.numID << " ; " << "Moyenne : " << etd3.moyenneGenerale << std::endl;
```

Sortie console du code précédent:

```console
Numero d'ID : 12458 ; Moyenne : 15.13
```

Sachant que l'instruction `sizeof` permet d'indiquer la taille en octets d'une variable, précisez la sortie produite par les instructions suivantes :

```cpp
cout << sizeof (etd1) << " ; " << sizeof(Etudiant) << endl;
Etudiant liste[20];
cout << sizeof (liste) << endl;
```

!!! warning "Attention à la taille des variables suivant l'architecture"
    Suivant si le système d'exploitation fonctionne sur 32 bits ou 64 bits, la taille des variables est différentes:

        - `char` ou `unsigned char` : 1 octet en 32 et 64 bits,
        - `short` ou `unsigned short` : 2 octets en 32 et 64 bits,
        - `int` ou `unsigned int` : 4 octets en 32 bits mais 8 octets en 64 bits,
        - `long` ou `unsigned long` : 8 octets en 32 bits et 16 octets en 64 bits
        - `float` : 4 octets en 32 bits,
        - `double` : 8 octets en 32 bits,
        - `booleen` : 4 octets en 32 bits,
        - `pointeur` : taille suivant architecture : 4 octets en 32 bits, 8 octets en 64 bits.

!!! note "Exercice"
    === "Analyse de code"
        Complétez les propositions ci-dessous en justifiant votre calcul :

        - taille de la variable `etd1` en octets :
        - taille de la structure `Etudiant` en octets :
        - taille de la variable `liste` en octets :

        Vérifiez vos réponses en compilant le code précédent et en l'exécutant.

    === "Une solution possible"
        ATTENTION le résultat donné ci-dessous est compilé sur une architecture 32 bits. Votre résultat peut être différent sur une architecture 64 bits !

        ```cpp
        cout << sizeof (etd1) << " ; " << sizeof(Etudiant) << endl;
        Etudiant liste[20];
        cout << sizeof (liste) << endl;
        ```

        ```console
        8 ; 8
        160
        ```

        Vous devez être capable d'expliquer la sortie console sinon c'est que vous n'avez pas compris !

Vous allez maintenant combiner une énumération et une structure.
Vous devez créer un type structuré de données nommé **Capteur** dont les caractéristiques sont :

- un premier champ qui indique le type du capteur (`TypeCapteur`) (cf. 1. Définitions des constantes),
- le second champ est la valeur du capteur de type `double`.

!!! note "Exercice"
    === "Produire un code"
        Proposez une définition du type Capteur dans la zone grisée ci-dessous:
        ```cpp
        struct 
        .
        .
        .
        .
        ```

    === "Déclarer une variable"
        Créez une nouvelle variable `monCapteur` de type `Capteur` dont vous initialiserez le champ `TypeCapteur` à `TEM` et sa valeur à 22.65°C.
        ```cpp

        ```

    === "Produire le code d'une fonction"
        Codez la fonction dont le prototype est donnée ci-après dans la zone grisée ci-dessous. Cette fonction permet de mettre le champ valeur du capteur passé en paramètre à 0 :
        ```cpp
        void initMesureCapteur(Capteur* pCapteurAInitialiser)
        {





        }
        ```

## Rappel sur les pointeurs

Vous allez travailler sur le code suivant (cellule ci-dessous).

```cpp
#include <iostream>

int a = 7, b = 0;                 
int* pa = nullptr;  // Notez que la variable s'appelle pa, et non *pa
int* pb = nullptr;       
pa = &a;                          
b = *pa + 2;                      
pb = &b;  
std::cout << std::hex;
std::cout <<   a << " " <<   b << std::endl;
std::cout <<  &a << " " <<  &b << std::endl;
std::cout << *pa << " " << *pb << std::endl;
std::cout <<  pa << " " <<  pb << std::endl;
std::cout << &pa << " " << &pb << std::endl;
std::cout << std::dec;
```

!!! note "Exercice"
    
    Répondez aux questions suivantes:

    1. A quoi sert `std::hex` et `std::dec` dans le flux d'affichage ?
    2. Que réalise l'opérateur `&` lorsqu'il est placé devant une variable ?
    3. A quoi sert le mot clé `nullptr` (lire cet [article](https://h-deb.clg.qc.ca/Sujets/Divers--cplusplus/CPP--NULL.html)) ?
    4. Comment s'appelle l'opérateur `*` dans le contexte ci-dessus (il ne s'agit pas de la multiplication) ?
    5. Que réalise l'opérateur `*` lorsqu'il est placé avant une variable de type **pointeur sur** ?

!!! note "Exercice"
    
    Complétez le tableau suivant en supposant que toutes les lignes avant le premier `std::cout` ont été exécuté et que la machine fonctionne avec un processeur et un système d'exploitation 64 bits:

    1. Quelle est la taille en octet d'une variable de type int sur ce système ?
    2. Quelle est la taille en octet d'un pointeur quelconque sur ce système ?

    Les adresses sont données à titre d'exemple et ne correspondent pas à l'exécution du code ci-dessus.

    | Variable    | Adresse (hexa) | Contenu     |
    | :---------: | :----------    | :---------- |
    | a           | 0x12ffd0       |             |
    | b           | 0x...          |             |
    | pa          | 0x...          |             |
    | pb          | 0x...          |             |

## Opérateurs de transtypage ou opérateurs de *cast*

Soit le code suivant dans la cellule ci-dessous. Exécutez-le et lisez attentivement le message d'erreur renvoyé par le compilateur.

```cpp
#include <iostream>

void fonction_1(short* v)
{
  std::cout << *v << " ; " << std::hex << *v << std::dec << std::endl;
}

int main()
{
    int i = 6666;
    std::cout << i << std::endl;
    fonction_1(&i);
    return 0;
}
```

!!! note "Exercice"

    Corrigez l'erreur en utilisant une conversion de type, en anglais une opération de **Cast**. Rendez-vous sur le site [Zeste de savoir](https://zestedesavoir.com/tutoriels/553/les-conversions-de-types-en-c/). La conversion doit s'effectuer au niveau de l'appel de la fonction `fonction_1`. Expliquez votre choix.

!!! note "Exercice"

    Testez le programme précédent en fixant la valeur de i à 66666. Justifiez l'affichage.

## Extrait BTS 2009 - Mise en oeuvre des opérateurs de transtypage

Un conditionneur d'entrées/sorties transmet des données analogiques (float) à un calculateur sous forme d'un tableau de 4 octets. On vous demande de coder cette fonction. Par exemple, le réel 2.5 est représenté dans la mémoire par la valeur hexadécimal 0x40200000 au format IEEE754. Par conséquent votre fonction doit retourner un tableau contenant d'abord le poids faible, ici 0 (0x0), 0 (0x0), 32 (0x20), et enfin le poids forts 64 (0x40).

Consultez les liens suivants :

- [Calculatrice IEEE754](https://www.h-schmidt.net/FloatConverter/IEEE754.html) : testez la valeur réel 2.5
- [présentation du format IEEE754 sur Wikipédia](https://fr.wikipedia.org/wiki/IEEE_754)

Le code peut se présenter de la manière suivante (recopiez-le dans la cellule de code ci-dessous et complétez-le) :

```cpp
void extraireOctet(float donnee, unsigned char* octets)
{
    // A Compléter
}

float donneeMesuree = 2.5;
unsigned char octetsATransmettre[4];
extraireOctet(donneeMesuree, octetsATransmettre);
//Transmission des 4 octets 
//...... 
```
