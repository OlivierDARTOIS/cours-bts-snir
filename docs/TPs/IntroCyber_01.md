# TP de découverte Cybersécurité - Partie 1

!!! tldr "Objectifs de ce document"

    - découvrir une distribution Linux spécialisée pour la cybersécurité : Kali Linux, 
    - utiliser des outils pour réaliser un "scan d'adresses IP" et "scan de ports"
    - utiliser un outil d'analyse de trames réseaux en mode graphique.
    - découvrir quelques termes associés au domaine de la cybersécurité.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 1
    - Compétences évaluées dans ce TP
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (25%)
        - C11 - Maintenir un réseau informatique (50%)
    - Principales connaissances associées
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10
        - Outils logiciels d'évaluation, de traçabilité de l'information, de tests, d'analyse de traitement et de rapport de l'incident -> Niveau 3 : C11

## Introduction

La **cybersécurité** est un domaine vaste et très spécialisé. Dans ce TP nous allons nous contenter de découvrir des outils, d'en mettre certains en œuvre et d'analyser simplement les résultats obtenus. Chaque test simple mériterait un TP complet ! Vous pourrez ainsi vous rendre compte des domaines de compétences très larges qu'un expert en cybersécurité se doit de maîtriser (ici il s'agira plutôt du réseau).

La *trousse à outils* pour réaliser des tests est la base d'un travail efficace. Ici nous ne présenterons qu'un système d'exploitation préinstallé avec de nombreux outils : la distribution **Kali Linux**.

## La distribution **Kali Linux**

C'est une distribution Linux (basée sur Debian GNU/Linux) optimisée pour les tests de sécurité d'un système d'information comme les tests d'intrusion, le piratage éthique et l'évaluation de la sécurité réseau.

Le site officiel en anglais : <https://www.kali.org/> et la partie téléchargement : <https://www.kali.org/get-kali/>.

Veuillez noter que vous pouvez télécharger et faire fonctionner Kali Linux de différentes façons. Nous allons détailler deux solutions :

- la [version autonome](https://www.kali.org/get-kali/#kali-live) sur clé USB : La présentation de l'installation de cette clé est faites sur cette [page](https://www.kali.org/docs/usb/). Téléchargez l'image puis transférez-la sur la clé USB avec l'utilitaire [Balena Etcher](https://www.balena.io/etcher/) sous Microsoft Windows. Cette solution n'est pas celle qui sera utilisée dans ce TP.
- la [distribution Kali Linux native](https://www.kali.org/get-kali/#kali-installer-images) à installer sur un disque dur : c'est cette solution qui a été choisie dans la section de BTS. Au démarrage du PC, dans le menu qui s'affiche, sélectionnez **Kali Linux**. L'installation de cette distribution n'est pas détaillée ici et fera l'objet d'une installation dans une machine virtuelle plus tard dans l'année.

!!! note "Exercice"
    Vérifiez que le PC est relié au réseau ethernet filaire. Démarrez votre PC, lancez Kali Linux. Identifiez-vous (login: **ciel**, mdp: **ciel**) puis lancez un **terminal** (icône noire en haut de l'écran à gauche avec un $): il s'agit d'un interpréteur de commandes. Notez que la plupart des utilitaires seront en ligne de commande (en mode textuel), il faut donc s'habituer tout se suite à travailler de cette façon.  
    Pour obtenir de l'aide sur une commande passez le paramètre `-h` (ex: `ip -h`) et/ou consultez son manuel (ex: `man ip`).  
    Vous allez utiliser la commande `ip` et la commande `cat` pour relever des données qui nous seront utiles dans la suite du TP. Complétez le tableau ci-dessus.

|Commande|Fonction|Aide|Résultat de la commande|
|:--:|:--:|:--:|:--:|
|ip -c addr|permet de lister les cartes réseaux du système|Combien de cartes réseaux trouvez-vous ?|nb: `____`|
|ip -c addr|permet d'obtenir l'adresse ip (groupe de 4 nombres séparés par des points)|début de la ligne `inet` et nombre commençant par 10.xx||
|ip -c addr|permet d'obtenir le masque de réseau en notation CIDR|début de la ligne `inet` (nombre juste après l'adresse ip séparé par /)||
|ip -c addr|permet d'obtenir l'adresse mac|début de la ligne `link/ether` (groupe de 6 nombres hexadécimal séparés par :)||
|ip -c route|permet d'obtenir l'adresse ip de la passerelle du réseau|adresse ip de la ligne `default`||
|cat /etc/resolv.conf|permet d'obtenir l'adresse ip du serveur DNS|adresse ip après le mot `nameserver`||

!!! info "Notions abordées dans les commandes précédentes"
    - Le couple adresse ip/masque de réseau permet d'identifier de manière unique une machine dans un réseau, l'adresse ip peut être changé facilement,
    - L'adresse MAC identifie une carte réseau (wifi ou ethernet) de manière unique, l'adresse MAC est fixe et déterminé par le constructeur de la carte,
    - Une passerelle est une machine qui permet de changer de réseau (par exemple votre box chez vous permet de passer de votre réseau local au réseau Internet),
    - Un serveur DNS permet d'associer une adresse compréhensible par l'homme en une adresse ip compréhensible par la machine et vice-versa. Par exemple l'adresse du site **www.kali.org** est associée avec l'adresse ip **104.18.4.159**.

!!! tldr "Conclusion partielle"
    - il faut savoir utiliser la console Linux pour taper des commandes de base,
    - il faut connaître les adresses IPv4 (IPv6 est un plus),
    - il faut connaître les protocoles de base du fonctionnement du réseau (adresse ip, masque, route par défaut, dns, ...).

<span style="color: #ff0000">**Validation professeur :**</span>

## Votre premier *pentest*

Le terme **pentest** vient de l'anglais *penetration testing* ou en français **test d'intrusion**. Nous allons tout d'abord faire un scan des adresses ip d'un réseau pour trouver des machines puis un scan de ports sur une des machines pour déterminer quels sont les *services* disponibles sur celle-ci.

!!! danger "Aspects légaux des tests d’intrusion"
    Avant d’effectuer des tests d’intrusion, l’organisation effectuant le test doit avoir le consentement de l’organisation testée. Sans un tel accord, les *pentests* sont illégaux et peuvent constituer une infraction pénale. Pour cette raison, les tests qui suivent seront réalisés sur un réseau privé et sur une machine préparée à l'avance pour ce TP.

Vous allez utiliser la commande **nmap** pour réaliser le scan ip et le scan de ports. Un scan ip consiste à balayer une plage d'adresses ip, généralement de la première à la dernière. Chaque machine peut avoir un ou plusieurs services fonctionnant sur celle-ci (c'est pour cette raison que l'on parle de *serveur*). Un service peut être par exemple un serveur web. Chaque service fonctionne sur ce qu'on appelle un port. Par exemple pour un serveur web il s'agit du port 80.

!!! note "Exercice"
    Le numéro du port est codé sur 16 bits en non signé (pas de nombre négatif). Combien de ports sont disponibles sur une machine ?  `_____________`. Les ports sont répartis suivant trois catégories, consultez le site [frameIP](https://www.frameip.com/liste-des-ports-tcp-udp/) pour compléter le tableau ci-dessous.

|Plage de ports|Description de la plage concernée|
|:--:|:--:|
|N° `____` à `____`||
|N° `____` à `____`||
|N° `____` à `____`||

!!! note "Exercice"
    Dans un terminal, tapez la commande suivante en adaptant l'adresse du réseau avec celle relevé précédemment: `nmap -sP adresse_réseau/masque_réseau`. L'adresse réseau s'obtient en faisant un ET logique bit à bit entre l'adresse ip relevée et le masque. Le masque est donné en comptant le nombre de bits à 1 en partant de la gauche. Complétez le tableau ci-dessous puis testez la commande `nmap`.

||Réponse en décimal pointée|octet1 en binaire|octet2 en binaire|octet3 en binaire|octet4 en binaire|
|:--:|:--:|:--:|:--:|:--:|:--:|
|Adresse ip en notation pointée||||||
|Masque (nb de bits à 1)||||||
|ET|||||
|Adresse réseau en notation pointée||||||

La commande `nmap` est donc la suivante: `__________________________`. Combien de machines ont répondu ? `______`

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Conclusion partielle"
    - il faut connaître les protocoles de transport les plus utilisés : **UDP** et **TCP** ainsi que la notion de ports associés à ceux-ci,
    - il faut savoir réaliser un *scan* du réseau pour repérer les différentes machines fonctionnant sur celui-ci,
    - il faut connaître les notions d'adresse de réseau, de diffusion, de masques et les opérateurs logiques de base.

!!! note "Exercice"
    Parmi toutes ces machines, une nous intéresse plus particulièrement, vous allez la *scanner* pour savoir si elle héberge des services. Tapez la commande suivante: `nmap 10.187.52.6` puis complétez le tableau ci-dessous.

|Numéro du port|Protocole (TCP ou UDP)|Etat|Nom du service|
|:--:|:--:|:--:|:--:|
|||||
|||||
|||||
|||||
|||||
|||||
|||||

Complétez les services précédents suivant s'ils sont sécurisés ou non (s'ils emploient ou non un chiffrement des données) dans le tableau ci-dessous :

|Nom du service|Rôle du service|Service sécurisé|
|:--:|:--:|:--:|
|ftp||OUI/NON|
|ssh||OUI/NON|
|http||OUI/NON|

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Conclusion partielle"
    - il faut savoir identifier les services fonctionnant sur une machine en scannant les ports,
    - il faut connaître les services les plus répandus et leur fonctionnement,
    - il faut savoir identifier les protocoles chiffrés.

!!! note "Exercice"
    Parmi les services disponibles, un serveur web est disponible. Connectez-vous à l'aide d'un navigateur (firefox est disponible dans kali linux) sur l'adresse ip précédente. Dans la page web qui s'affiche, donnez ci-dessous une définition du mot en rouge dans le contexte informatique.

|Mot en rouge|Définition du mot|Source (wikipédia ou autres)|
|:--:|:--:|:--:|
||||
||||

!!! note "Exercice"
    A l'aide de la commande `nc` (netcat, [documentation](https://www.kali.org/tools/netcat/) sur le site de kali linux), connectez-vous sur l'adresse ip 10.187.52.6 et le port 13. `netcat` est une commande très utile sous linux pour réaliser un client ou un serveur TCP sous Linux. Donnez la commande que vous avez tapé : `______________________` ainsi que son résultat `______________________`. Le résultat retourné est-il en accord avec le rôle du service attendu : OUI / NON.

Un service pour transférer des fichiers à distance est disponible sur la machine, il s'agit du service `ftp` (file transfert protocol). Vous souhaitez vous connecter mais vous ne vous rappelez que du login de l'utilisateur: `happy`. Parmi les utilitaires disponibles sur la distribution kali linux, **hydra** permet de tester des couples login/mot de passe sur un service.

!!! note "Exercice"
    A l'aide de la [documentation](https://www.kali.org/tools/hydra/) de la commande hydra sur le site de kali linux, adaptez la commande proposée pour tester le serveur ftp à l'adresse 10.187.52.6 avec l'utilisateur happy. Vous rajouterez les paramètres `-V` et `-f` à cette ligne en précisant quels sont leurs rôles. Vous en déduirez le mot de passe de l'utilisateur happy.

|Questions|Vos réponses|
|:--|:--:|
|Commande hydra proposée :||
|rôle du paramètre `-V`:||
|rôle du paramètre `-f` :||
|mot de passe trouvé  :||

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Conclusion partielle"
    - il faut savoir ce qu'est un *pentest* et les outils que l'on peut utiliser pour réaliser celui-ci,
    - il faut connaître les attaques par **force brute** (non traitée dans ce TP), les attaques par **dictionnaires** (vous venez d'en faire une avec le logiciel hydra),
    - il faut savoir lire et interpréter les documentations des logiciels.

Vous avez de la chance que le mot de passe de l'utilisateur `happy` soit dans un dictionnaire, ce qui n'est bien sur pas toujours le cas. Si vous avez accès au réseau filaire de l'entreprise, vous pouvez essayer de récupérer des mots de passe en analysant les trames qui circulent sur le réseau. Dans notre cas nous allons nous connecter vers un serveur **telnet** et voir si nous arrivons à récupérer le mot de passe. Pour cela nous allons utiliser un utilitaire très connu : **WireShark** en mode graphique.

!!! note "Exercice"
    Lancez l'utilitaire **WireShark** en cliquant sur l’icône kali linux en haut à gauche de l'écran puis en tapant dans la zone de recherche *wireshark*. Dans la fenêtre qui s'ouvre, saisissez dans le champ **filtre** : `tcp port 23` car le protocole telnet se sert de TCP comme protocole de transport et il dialogue sur le port 23. Double cliquez ensuite sur la carte **eth0** pour lancer la capture.  
    Dans un terminal, tapez la commande `telnet 10.187.52.6` puis saisissez comme login **happy** et comme mot de passe (non renvoyé à l'écran) celui trouvé précédemment. Vous êtes alors connecté sur la machine distante. Tapez alors ++ctrl+d++ pour vous déconnecter.  
    Basculez sur la fenêtre de wireshark puis arrêtez la capture avec le bouton **stop** (icône carré rouge en haut de la fenêtre). Faites un clic droit sur la première ligne de la capture puis faites **Suivre** et **Flux TCP**. Dans la fenêtre qui s'ouvre repérez le mot de passe !

!!! note "Exercice"
    Refaites la démarche précédente en vous connectant avec ssh (secure shell). Le filtre sera `tcp port 22` car le serveur ssh fonctionne sur le port 22 TCP. Connectez-vous en tapant la commande suivante dans un terminal : `ssh happy@10.187.52.6`. Essayez d'identifier à nouveau le mot de passe en suivant le flux TCP. Que constatez-vous et pourquoi ? `______________________________________`.

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Conclusion partielle"
    - **wireshark** est un outil puissant d'analyse de trames réseaux. Il est important de savoir l'utiliser,
    - le modèle en couches OSI ou TCP/IP doit être maîtrisé car wireshark fonctionne en présentant ces analyses de cette manière,
    - il ne faut plus jamais utiliser un protocole non chiffré dans un réseau local ou sur internet car les mots de passe circulent en clair. Le protocole telnet est donc à bannir, la connexion par ssh est obligatoire. Le protocole http doit être évité, le https doit être favorisé.

## Réaliser une attaque vers une machine

Les attaques de types **D.O.S** ou **D.D.O.S** sont très souvent employées. Complétez le tableau ci-dessous en vous aidant de sites Internet tel Wikipédia.

|Type d'attaque|Soit en anglais|Définition simple en français|
|:---:|:---:|:---|
|D.O.S.|||
|D.D.O.S.|||

!!! note "Exercice"
    Réalisez une attaque de type **Ping of death** conduisant à un D.O.S. avec la commande hping3 vers la machine 10.187.52.6 avec l'utilisateur ciel. Pendant que cette attaque est menée, essayez de vous connecter de manière légitime en ssh.  
    Veuillez noter qu'il faut exécuter cette commande en mode **root** (administrateur), donc la commande hping3 sera précédée de **sudo**. Pour arrêter cette commande, tapez ++ctrl+c++. La commande `hping3 -h` vous aide pour répondre aux questions.  
    Pour quittez la connexion ssh, tapez ++ctrl+d++.  
    La commande sera de la forme : `sudo hping3 --icmp --flood addr_ip_a_tester`

|Questions|Vos réponses|
|:---|:---|
|Commande hping3 :||
|rôle paramètre `--icmp`||
|rôle paramètre `--flood`||
|Connexion ssh :|possible/impossible. Pourquoi ?|

!!! note "Exercice"
    Lancez une attaque de type **SYN flood** avec hping3 vers le port 80 (donc le serveur web). Essayez de vous connecter de manière légitime sur le serveur web. Il est conseiller de lire [l'article](https://www.ionos.fr/digitalguide/serveur/securite/syn-flood/) sur le site de l'hébergeur IONOS sur l'attaque *SYN Flood*.

|Questions|Vos réponses|
|:---|:---|
|Commande hping3 :||
|Connexion vers le serveur web :|possible/impossible. Pourquoi ?|

Quasiment toutes les commandes disponibles dans la distribution kali linux avec leur documentation (attention souvent incomplète) sont disponibles sur cette [page](https://www.kali.org/tools/).

<span style="color: #ff0000">**Validation professeur :**</span>

## Quelques sites à consulter

- le site de l'**ANSSI** : <https://www.ssi.gouv.fr/> et des articles à consulter <https://www.ssi.gouv.fr/agence/rayonnement-scientifique/publications-scientifiques/articles-ouvrages-actes/>. Une lecture technique mais indispensable : <https://www.ssi.gouv.fr/entreprise/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/>
- le site de **CloudFlare** : <https://www.cloudflare.com/learning/ddos/what-is-a-ddos-attack/>
- le site de **CERT France** : <https://www.cert.ssi.gouv.fr/actualite/>
- un site d’entraînement **root-me** : <https://www.root-me.org/>
- un autre site d’entraînement **tryhackme** : <https://tryhackme.com/>
- le site **Yes we hack edu** : <https://www.yeswehack.com/fr/edu/>

## QCM de fin de TP

Cliquez sur le lien ci-après et répondez aux questions.
[QCM en ligne](http://135.125.103.133/digiquiz/q/63ec06184bb13/)

## Conclusion

Au travers des différents exercices précédents, vous vous êtes rendu compte qu'il faut des connaissances exhaustives dans différents domaines. Vous devez retenir deux grands principes :

- la maîtrise des bases du réseau (pile OSI, rôle des différentes couches), des protocoles (ARP,  TCP, UDP, IP, DNS, DHCP, etc...), de l'analyse de trames à l'aide d'outils type Wireshark sont très importantes,
- quasiment tous les outils spécialisés sont en anglais et réclament une lecture attentive de leur documentation afin de comprendre comment les utiliser.

Il ne faut donc pas se décourager et persévérer dans les apprentissages. Nous avons évoqué ici quelques types d'attaques mais nous n'avons jamais évoqué comment s'en protéger...
