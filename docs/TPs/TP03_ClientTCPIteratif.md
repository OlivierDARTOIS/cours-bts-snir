# Conception d'un client TCP itératif utilisant les sockets POSIX

!!! tldr "Objectifs de l'exercice"

    - définir ce qu'est un client TCP *itératif*,
    - améliorer le code du TP précédent afin de dialoguer avec le serveur TCP
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Cahier des charges

Vous avez développé dans le TP précédent un client simple qui se connecte à un serveur, réceptionne la réponse et la traite. Vous allez améliorer votre client en permettant un dialogue entre un utilisateur et le serveur en utilisant votre client.

Le dialogue entre le client et le serveur sera très simple et comportera un nombre limité de commandes :

|Commande|Résultat attendu|
|:---:|:---|
|**TMP**|Le serveur vous renvoie une chaîne de caractères contenant l'heure et la date de la mesure ainsi que la valeur de la température en degré Celsius|
|**HUM**|Le serveur vous renvoie une chaîne de caractères contenant l'heure et la date de la mesure ainsi que la valeur de l'hygrométrie exprimée en %RH|
|**ALL**|Le serveur vous renvoie une chaîne de caractères contenant l'heure et la date de la mesure ainsi que la valeur de la température suivi de l'hygrométrie|
|**BYE**|Demande la fin du dialogue avec le serveur. Le serveur interrompt la connexion et ferme la socket de dialogue|

Le programme prend ces arguments sur la ligne de commande (adresse IP, numéro de port) puis propose à l'utilisateur de saisir sa commande. Ci-dessous un exemple d'échange entre le client et le serveur:

>Saisissez votre commande parmi TMP, HUM, ALL, BYE : TMP  
>Réponse du serveur : Wed Sep  9 15:33:05 2015   T: 25.823262°C  
>Saisissez votre commande parmi TMP, HUM, ALL, BYE : HUM  
>Réponse du serveur : Wed Sep  9 15:37:09 2015   H: 32.711548%  
>Saisissez votre commande parmi TMP, HUM, ALL, BYE : AAA  
>Réponse du serveur : Je ne comprends pas votre demande…  
>Saisissez votre commande parmi TMP, HUM, ALL, BYE : BYE  
>Réponse du serveur : Au revoir...

## Conseils pour la réalisation du TP

Complétez le nom des étapes du clients dans le schéma ci-après et vous indiquerez clairement ou se situe la boucle de saisie de l'utilisateur. Cela vous permettra de bien repérer ou vous devez mettre la boucle dans votre code.

Il n'est pas souhaitable que le serveur ai à vérifier les commandes que vous lui envoyez. Aussi il est de votre responsabilité de vérifier ce qu'à saisie l'utilisateur !

Proposez une solution de test du serveur sans écrire de code (utilisation de l'utilitaire netcat sous GNU/Linux : commande nc ou le logiciel herkule sous Microsoft Windows).

Il n'est pas demandé de compte-rendu pour ce TP car il est très simple par rapport au précédent.

![Schéma client-serveur à compléter](../img/schemaClientServeurACompleter.png)
