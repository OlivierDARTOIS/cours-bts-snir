# Créer et tester une classe simple

!!! tldr "Objectifs de l'exercice"
    - Analyser un cahier des charges
    - Compléter un diagramme de classe UML (Unified Modelling Language)
    - Créer le fichier de déclaration de la classe (fichier « header » : .h) et le fichier de définition de la classe (fichier .cpp)
    - Réaliser des tests unitaires sur cette classe
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Cahier des charges

Il s'agit de réaliser une classe **Personne** qui aura les caractéristiques suivantes :

- attributs de la classe (*nom de variable*):
    - année de naissance (`adn`),
    - age (`age`),
    - nombre d'enfants (`nbe`),
    - adresse (`adresse`),
    - numéro de sécurité sociale sous la forme d'une chaîne de caractères (`numSecu`),
    - genre : HOMME ou FEMME ou INCONNU : faire une énumération (`enum GENRE ...`).

- méthodes de la classe :
    - un constructeur par défaut (sans passage de paramètres),
    - un constructeur qui prend en argument le numéro de sécurité sociale,
    - pas de destructeur,
    - un accesseur (*getter* en anglais) et un mutateur (*setter* en anglais) sur les attributs : adresse, nombre d'enfants,
    - des méthodes :
        - `evalAttributs` qui analyse le numéro de sécurité sociale et renseigne les attributs de la classe (année de naissance, age, genre),
        - `verifCleNumSecu` qui calcule la clé du numéro de sécu et la compare avec celle fournie. Le code source de cette méthode est donné ci-dessous,
        - `naissanceEnfant` qui rajoute un enfant,
        - `toString` renvoie une chaîne qui reprend l'intégralité des attributs.

## Code de la méthode `verifNumSecu`

Le numéro de sécurité sociale est composé de 13 chiffres suivi d'une clé de contrôle de 2 chiffres. Reportez-vous à l'article sur [le numéro de sécurité sociale](https://fr.wikipedia.org/wiki/Num%C3%A9ro_de_s%C3%A9curit%C3%A9_sociale_en_France) sur wikipédia qui explique comment calculer cette clé.

```Cpp title="Définition de la méthode *verifNumSecu* de la classe *Personne*" linenums="1"
bool Personne::verifCleNumSecu()
{
    string numSecuSansCle = this->numSecu.substr(0,13); 
    // Calcul d'un modulo sur un nombre tres grand 
    // https://www.geeksforgeeks.org/how-to-compute-mod-of-a-big-number/ 
    int cleCalc = 0;

    for (int i = 0; i < numSecuSansCle.length(); i++) 
         cleCalc = (cleCalc*10 + (int)numSecuSansCle[i] - '0') % 97; 
    cleCalc = 97 - cleCalc; 
     
    int cle = stoi(this->numSecu.substr(13,2)); 

    return (cleCalc == cle); 
}
```

## Présentation du diagramme UML de classe

Le langage **UML** (Unified Modelling Language) permet à l'aide de diagrammes de présenter des projets informatiques. Il est compléter par le langage SysML pour décrire n'importe quel type de projets.

Ci-dessous vous avez une présentation succincte du diagramme UML d'une classe.

![Diagramme de classe UML](../img/DiagrammeClasse.png)

!!! note "Dessinez le diagramme UML de la classe *Personne*"
    A partir du cahier des charges et du code de la méthode `verifNumSecu`, dessinez le diagramme UML de classe de la classe `Personne`. Il vous appartient de choisir les différents types des attributs et de mettre en concordance les types utilisés par les méthodes.
    Notez qu'il n'existe pas *UNE* solution unique mais des solutions, chacune avec leurs avantages et leurs inconvénients. Après concertation nous nous mettrons d'accord sur un modèle pour cette classe `Personne`.

## Produire le code la classe `Personne`

Une classe est généralement constituée de deux fichiers :

- un fichier de déclaration de la classe : c'est le fichier d'en-tête ou fichier *.h* (*header* en anglais),
- un fichier de définition de la classe : c'est le fichier qui contient le code de chacune des méthodes ou fichier *.cpp* (c plus plus).

Le fichier de déclaration est construit à partir du diagramme de classe UML. Un modèle standard du fichier de déclaration est présenté ci-dessous:

!!! example "Fichier de déclaration d'une classe"

    ``` Cpp
    #ifndef _NomClasse_ // Ces lignes empèche l'inclusion multiple
    #define _NomClasse_

    #include <....>     // les includes nécessaires à la classe

    class NomClasse {   // déclaration de la classe
        private:        // zone privé, mettre les attributs 
        ...             //et les méthodes privés de la classe

        public:
        NomClasse();    // Constructeur par défaut de la classe
        NomClasse(...); // Constructeur de la classe avec passage de paramètres
        ~NomClasse();   // Destructeur de la classe
        ...             // mettre maintenant les getters, setters et méthodes
    };                  // Attention au point virgule

    #endif
    ```

Le fichier de définition contient le code C++ de chacune des méthodes de la classe. Un modèle standard du fichier de définition est présenté ci-dessous:

!!! example "Fichier de définition d'une classe"

    ``` Cpp
    #include "NomClasse.h"  // inclusion du fichier de déclaration de la classe

    NomClasse::NomClasse()  // Code du constructeur par défaut
    {
        // A compléter...
        // Un constructeur est principalement utilisé pour initialiser 
        // les attributs de la classe à des valeurs connues
    }

    type_retour NomClasse::NomMéthode(type var1, type var2)  // Code d'une méthode
    {
        // A compléter...
    }

    ...
    ```

!!! note "Codez la classe *Personne*"
    A l'aide de votre EDI (Environnement de Développement Intégré) préféré (VSCode + ligne de commande ou Visual Studio), codez la classe `Personne`.

    - Pensez à regarder l'article wikipédia [le numéro de sécurité sociale](https://fr.wikipedia.org/wiki/Num%C3%A9ro_de_s%C3%A9curit%C3%A9_sociale_en_France) ou le tableau ci-dessous pour extraire les informations du numéro fourni,
    - Pensez à lancer la compilation du fichier de définition de la classe régulièrement pour corriger les fautes de frappes et de syntaxe.

![Tableau analyse numéro de sécurité sociale](../img/numSecu.png)

## Réalisation de tests unitaire

Les tests unitaires permettent de tester **individuellement** chaque méthode d'une classe et d'analyser le résultat de celle-ci pour vérifier qu'elle remplie bien la fonction prévue. Ces tests peuvent être:

- manuel : vous créez un fichier *main* qui testera chacune des méthodes de la classe et vous analyserez à l'écran le retour de chacune des méthodes pour vérifier le comportement attendu,
- automatique : par exemple avec le framework [**catch2**](https://github.com/catchorg/Catch2/tree/v2.x) qui permet de vérifier le retour des méthodes de manières automatique avec différents cas en entrée.

!!! note "Réalisation des tests unitaires"
    Vous proposerez un *main* qui teste chacune des méthodes avec différents numéros de sécurité sociale erronés ou non. Réalisez des captures écran de vos tests.

    Si vous êtes en avance, proposez des tests avec le framework catch2.
