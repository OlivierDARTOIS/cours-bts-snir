# Conception d'un client TCP simple utilisant les sockets POSIX

!!! tldr "Objectifs de l'exercice"

    - mettre en oeuvre les concepts vu en cours sur les *client/serveur*,
    - coder un client TCP simple sous Microsoft Windows et/ou GNU/Linux,
    - repérer les étapes d'un client TCP,
    - gérer les erreurs de compilation.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"
    - Domaine C3 Concevoir
        - Compétence C3.1 : analyser un cahier des charges
        - Compétence C3.6 : recenser les solutions existantes répondant au cahier des charges
    - Domaine C4 Réaliser :
        - Compétence C4.3 : installer et configurer une chaîne de développement
        - Compétence C4.4 : développer un module logiciel
        - Compétence C4.5 : tester et valider un module logiciel
        - Compétence C4.7 : documenter une réalisation matérielle et/ou logicielle

    - Analyser le cahier des charges :
        - T3.1 : S’approprier le cahier des charges
    - Proposer des solutions pour répondre aux besoins du cahier des charges dans un contexte technico-économique contraint :
        - T5.1 : Identifier les solutions existantes de l’entreprise
        - T5.2 : Identifier des solutions issues de l’innovation technologique
    - Réaliser ou mettre en œuvre et valider une solution :
        - T7.1 : Réaliser la conception détaillée du matériel et/ou du logiciel
        - T7.2 : Produire un prototype logiciel et/ou matériel
        - T7.3 : Valider le prototype

## Cahier des charges

Une entreprise dispose dans chacune de ces pièces d'un RaspberryPi équipé d'un capteur de température et d'humidité sur bus I2C. Chacun des RaspberryPi possède une adresse IP dans le réseau 10.187.52.0/24. Sur chacun des RaspberryPi, un serveur logiciel est installé et répond sur le port TCP 12345. Lors d'une connexion, le serveur renvoie directement une chaîne de caractères contenant la date de la mesure, la valeur de la température en °C et la valeur de l'hygrométrie en %.

![Schéma réseau du TP Client/serveur TCP](../img/DiagReseauTPClientTCP.png)

Vous devez donc programmer un client TCP qui se connecte sur ces serveurs et affiche la chaîne renvoyée par le serveur sur une console (donc en mode texte). Dans un premier temps le port et l'adresse IP du serveur seront codés *en dur* dans le code. Le client peut être réalisé sous GNU/Linux avec comme IDE VSCode et/ou sous Microsoft Windows avec comme IDE VisualStudio.

Afin d'améliorer votre programme, vous coderez un client qui prend en argument sur sa ligne de commande l'adresse IP du serveur auquel vous souhaitez vous connecter ainsi que son port.

Enfin une correction légère doit être appliqué sur l'hygrométrie en fonction de la température. Le capteur utilisé est un HTU21D. Recherchez dans sa documentation la formule correctrice et codez-là dans votre programme. 

Le compte rendu de votre travail doit faire apparaître clairement dans le code les différentes étapes d'un client utilisant les sockets POSIX. Vous indiquerez comment vous avez traité les arguments de la ligne de commande et le dialogue avec l'utilisateur en cas de passage de mauvais paramètres.  

Vous préciserez la formule correctrice de l'hygrométrie. Enfin vous expliquerez comment vous avez fait pour passer d'une chaîne de caractère à des nombres pour pouvoir les traiter numériquement. Vous indiquerez aussi les réponses aux questions posées dans ce document.

## Ressources en ligne à consulter pour réaliser ce TP

Nous vous conseillons d'utiliser les sites suivants (liste non exhaustive) :

- Programmation socket POSIX: 
    - support de cours sur les sockets POSIX du BTS CIEL – Lycée Turgot,
    - cours sur les **socket** sur [developpez.com](http://broux.developpez.com/articles/c/sockets/),
- Traiter les arguments de la ligne de commande:
    - un exemple dans les *snippets* mis à disposition sur ce même dans la partie *Ressources*,
- Conversion string vers nombre ou nombre vers string : vous devez maintenant utiliser les méthodes de la STL (Standard Template Library) mise à disposition dans C++11:
    - conversion d'un nombre quelconque vers une chaîne (string) : fonction [to_string](http://www.cplusplus.com/reference/string/to_string/),
    - conversion d'une chaîne vers un nombre : fonctions stoXX : exemple avec la fonction [stod](http://www.cplusplus.com/reference/string/stod/) (string to double).

## Conseils pour la réalisation du TP

Avant de vous lancer dans l'écriture du code de votre client, il faut **TOUJOURS** essayer de trouver une solution qui ne fait pas intervenir votre code. Par exemple dans notre cas, comment tester la connexion au serveur de température et d'hygrométrie sans écrire une ligne de code.

Les réponses aux questions suivantes devront être consignées dans votre compte rendu.

Tout d'abord proposer une commande qui permet de savoir si le serveur (adresse IP : 10.187.52.16) est bien accessible depuis votre machine par le réseau.

Sous GNU/Linux, il existe une commande appelée *netcat* (`nc` en ligne de commande). Cet utilitaire peut vous permettre de tester le serveur et de voir ce que celui ci renvoie sans écrire de code. Recherchez sur Internet un tutoriel d'utilisation de la commande `nc` et proposer une commande de test (faire une capture écran du résultat).

Si vous êtes sous Microsoft Windows, utilisez l'utilitaire *Putty* pour vous connecter au serveur puis faites une capture d'écran qui justifie son utilisation.

L'affichage du serveur sera disponible sur le video projecteur. Lorsque vous vous connectez depuis votre poste, relevez le numéro du port sur lequel vous dialoguez avec le serveur. Pourquoi ce numéro de port est différent du port de connexion ?

Si vous regardez l'heure de la mesure que vous renvoie le serveur il est possible qu'elle soit différente de l'heure de votre PC (en supposant que le serveur et votre PC soit à l'heure!). Essayez d'expliquer ce décalage en regardant l'affichage du serveur et le fait que vous soyez plusieurs à vous connecter sur ce serveur ?

## Grille de notation

|Critère de notation|Note|
|:---|:---:|
|Test réseau avec la commande ping, capture écran, explications|/1|
|Utilisation d’un logiciel pour tester la connexion avec le serveur (hercule, putty ou autre), captures écrans, intérêt de réaliser ce test, commentez le résultat|/2|
|Modification d’un client simple pour l’adapter au TP, bien indiqué les changements, inclusion de la bibliothèque socket, repérer les étapes d’un client, captures écrans|/4|
|Analyse de la chaîne reçue pour en extraire la température et l’hygrométrie, explications des différentes méthodes utilisées, captures écrans + explications|/4|
|Recherche de la formule de correction de l’hygrométrie dans la documentation du capteur, justification des constantes, mise en œuvre dans le code, captures écrans|/3|
|Passage de paramètres avec les arguments de la ligne de commande, traitement et vérification de ceux-ci (adresse ip et port), explications sur la méthode employée pour analyser l’adresse ip, mise en œuvre dans le code, captures écrans des tests unitaires|/5|
|Explications sur le décalage horaire entre le serveur et votre client, capture écran|/1|
|Note finale|/20|
