# TP Installation du logiciel GLPI

!!! tldr "Objectifs de ce document"

    - installer une solution de gestion de parc informatique (matériels et logiciels, ticketing),
    - installer un *agent* pour réaliser l'inventaire de manière automatique sous Microsoft Windows et GNU/Linux, 
    - sécuriser votre installation.
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - TP AIS - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A - Semestre 2
    - Compétences évaluées dans ce TP
        - C06 - Valider un système informatique (25%)
        - C09 - Installer un réseau informatique (25%)
        - C10 - Exploiter un réseau informatique (50%)
    - Principales connaissances associées
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09
        - Langages de Scripts, Interface ligne de commande d’équipements et de système d’exploitation, Outils de mise à jour système et sécurité système (gestion des paquets logiciels, mise à jour de sécurité, script mise à jour automatique, ...) -> Niveau 3 : C10

## Introduction

**GLPI** est l'acronyme de **G**estionnaire **L**ibre de **P**arc **I**nformatique. C'est une solution logicielle qui permet de réaliser la gestion d'un système d'informations (SI) et de fournir une assistance aux utilisateurs sous la forme de *tickets*. L’accès à cette solution se fait à travers un navigateur web ce qui lui assure une diffusion sur toutes les plateformes matériels (PC, tablette, smartphone) et sur tous les systèmes d'exploitations (Microsoft Windows, IOS/MacOS, Linux).

GLPI a été créé en 2003 et est maintenant développé par la société Teclib sous licence GPLv3. Le site officiel est [https://glpi-project.org](https://glpi-project.org). Pour plus de détail je vous invite à lire l'article GLPI sur [wikipédia](https://fr.wikipedia.org/wiki/Gestionnaire_Libre_de_Parc_Informatique).

!!! info
    GLPI fait partie des logiciels de [gestion des services informatiques](https://fr.wikipedia.org/wiki/Gestion_des_services_informatiques), plus connue sous le sigle anglais de **ITSM** (Information Technology Service Management). C'est une brique de base de l'[**ITIL**](https://fr.wikipedia.org/wiki/Information_Technology_Infrastructure_Library) (Information Technology Infrastructure Library) ou en français : bibliothèque pour l'infrastructure des technologies de l'information : ce sont les *bonnes pratiques* à mettre en oeuvre pour assurer un management efficace du système d'information.

**Diagramme d'exigences du TP** :
![Diagramme d'exigences du TP](../img/diagExigencesTPGLPI.svg)

## Installation simplifiée de GLPI

Dans un premier temps, vous allez installer le logiciel GLPI de façon à ce qu'il soit rapidement utilisable. L'installation sécurisé sera présenté à la fin de cette article et vous la réaliserez s'il vous reste du temps. Pour réaliser cette installation, trois sites ont été utilisés :

- le site officiel de GLPI : [documentation GLPI Installation](https://glpi-install.readthedocs.io/en/latest/),
- un article du site neptunet.fr : [Installer GLPI 10 sous Debian 12](https://neptunet.fr/install-glpi10/),
- un article du site it-connect.fr : [Installation pas à pas de GLPI](https://www.it-connect.fr/installation-pas-a-pas-de-glpi-10-sur-debian-12/).

Ce qui est décrit ci-dessous est une simplification de ces tutoriels pour aller à l'essentiel.

!!! note "Exercice: Clonage VM sous Proxmox"
    Vous allez clonez une VM Debian 12 Base depuis votre espace Proxmox. La VM clonée sera nommée **CIEL-vosInitiales-DebianGLPI**. Lancez la VM, connectez-vous en tant qu'utilisateur *root* (Rappel login/mdp: root/root). Fixez ensuite l'adresse IP/masque (donnée par le professeur), la passerelle et le serveur DNS en modifiant le fichier `/etc/network/interfaces` (remplacez les *XXX* par l'adresse ip) :
    
    ```console
    iface eth0 inet static
      address 10.187.52.XXX/24
      gateway 10.187.52.245
      dns-nameservers 10.187.52.5
    ```
    
    Redémarrez (ou plus simplement: `systemctl restart networking.service`) puis vérifiez vos paramètres IP à l'aide des trois commandes suivantes:

    - `ip addr` : pour vérifier l'adresse IP et le masque de votre interface ethernet (10.187.52.XXX/24),
    - `ip route` : pour vérifier que l'adresse ip de la passerelle est bien prise en compte (10.187.52.245),
    - `cat /etc/resolv.conf` : pour vérifier l'adresse IP du (ou des) serveur(s) DNS (10.187.52.5).
    
    Vérifiez que vous avez un accès à Internet: `__________________________`. Effectuez alors une mise à jour du système: `________________________________`.  
    Clonez une VM Debian 12 Base avec le nom **CIEL-vosInitiales-DebianAgentGLPI**. Si vous disposez d'une VM Microsoft Windows 10, conservez-là sinon clonez le modèle avec le nom **CIEL-vosInitiales-Win10AgentGLPI**. Nous installerons sur ces deux VMs les agents de collecte pour enrichir la base de GLPI.

!!! note "Exercice: Installation d'une solution LAMP, des paquets PHP et d'une base de données pour GLPI"
    Connectez-vous à votre machine virtuelle GLPI par ssh en mode console depuis votre machine réelle (d'abord avec l'utilisateur **ciel** puis ensuite passez **root** avec la commande `su -`). GLPI nécessite un serveur web, une base de données, le langage PHP et de nombreux modules PHP. Vous allez commencer par installer la solution LAMP: 
    
    ```console
    apt install apache2 mariadb-server php
    ```
    
    puis vous installez tous les modules php:

    ```console
    apt install php-mysql php-mbstring php-curl php-gd php-xml php-intl php-ldap php-apcu php-xmlrpc php-zip php-bz2 php-imap
    ```

    Vous allez créer une base de données pour GLPI (db_glpi) ainsi qu'un utilisateur (adminGLPI/mdpAdminGLPI) qui aura tous les droits sur celle-ci. Veuillez taper sur la touche **Entrée** après la ligne `mysql -u root -p` (pas de mot de passe à saisir) :

    ```console
    mysql -u root -p
    create database db_glpi;
    grant all privileges on db_glpi.* to adminGLPI@localhost identified by "mdpAdminGLPI";
    flush privileges;
    exit
    ```

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Téléchargement, installation et configuration de base de GLPI"
    Le logiciel GLPI se présente sous la forme d'une archive [**tgz**](https://fr.wikipedia.org/wiki/Tgz) (archive tar compressée). Pour trouver toujours la dernière version de GLPI rendez-vous sur son site [GitHub](https://github.com/glpi-project/glpi/releases). Sur cette page repérez la dernière version et en particulier le lien de téléchargement : ce sera une url du style `https://github.com/glpi-project/glpi/releases/download/num_version/glpi-num_version.tgz` avec **num_version** le dernier numéro de version. Faites un clic droit sur le lien de téléchargement puis **Copier l'adresse du lien**. Vous allez télécharger l'archive avec la commande `wget`([documentation](https://man.cx/wget)) puis la désarchiver avec une commande `tar` ([documentation](https://man.cx/tar(1)/fr)):

    ```console
    wget https://github.com/glpi-project/glpi/releases/download/num_version/glpi-num_version.tgz
    tar xzvf glpi-num_version.tgz -C /var/www/
    ```

    Complétez le tableau ci-dessous en précisant le rôle de chacun des arguments :
    
    |Argument tar|rôle|
    |:---|:---|
    |x||
    |z||
    |v||
    |f||
    |-C||

    Vous allez effacer l'archive tgz de GLPI avec une commande `rm` ([documentation](https://man.cx/rm(1)/fr)) puis proposer une commande `chown` ([documentation](https://man.cx/chown(1)/fr)) qui changera **récursivement** le propriétaire du répertoire `/var/www/glpi` et de ces sous-répertoires à **www-data** afin que le serveur apache puisse lire/écrire dans ceux-ci.

    |Commande|Votre proposition|
    |:---|:---|
    |`rm`||
    |`chown`||

    Pour éviter un avertissement de sécurité lors de l'installation de GLPI, il faut réaliser une modification du fichier `/etc/php/8.2/apache2/php.ini` avec nano. Une fois que le fichier est ouvert dans nano faites la combinaison de touche ++ctrl+w++ pour rechercher la ligne de configuration suivante : `session.cookie_httponly`. Modifiez cette ligne pour avoir `session.cookie_httponly = on`. Sauvegardez le fichier puis quittez nano.

    Il faut ensuite créer un fichier **virtualhost** pour apache qui gérera le site GLPI. Ce fichier portera le nom de `glpi.conf` et vous allez l'éditer avec nano.

    ```console
    nano /etc/apache2/sites-available/glpi.conf
    ```

    Le contenu de ce fichier sera le suivant (**ATTENTION à changer `addr_ip_VM_GLPI` par l'adresse ip de votre VM**):

    ```console
    <VirtualHost *:80>
        # ServerName glpi   <- si vous disposez d'un DNS qui pointe vers @ip de votre VM
        ServerAlias addr_ip_VM_GLPI
        DocumentRoot /var/www
        Alias "/glpi" "/var/www/glpi/public"
        <Directory /var/www/glpi>
            Require all granted
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ index.php [QSA,L]
        </Directory>
    </VirtualHost>
    ```

    Sauvez votre fichier puis quittez nano. Vous activez le module **rewrite** d'apache puis le site GLPI avec les commandes suivantes:

    ```console
    a2enmod rewrite
    a2ensite glpi.conf
    systemctl restart apache2
    ```

    Vérifiez que le serveur web Apache s'est bien lancé avec la commande suivante sinon cela veut très certainement dire qu'il y a une erreur de syntaxe dans le fichier `glpi.conf` :

    ```console
    systemctl status apache2
    ```

    Vous devez avoir un message en vert *active (running)*. Vous allez maintenant pouvoir finir l'installation à l'aide de l'interface web de GLPI.

!!! note "Exercice: Configuration finale de GLPI via son interface web"
    Vous accédez à votre VM GLPI depuis votre poste avec un navigateur sur une URL qui aura la forme suivante `http://addr_ip_vm_glpi/glpi/`. Vous arrivez sur la page d'accueil *GLPI SETUP*, sélectionnez votre langue puis acceptez la licence. Vous ne faites pas une mise à jour mais bien une première installation donc sélectionnez le bouton *Installer*. Une série de test sera lancé, vous devez tous les passer (coche verte) sinon vous avez certainement oublié quelque chose aux étapes précédentes.  
    Vous devez maintenant configurer l’accès à la base de données MariaDB. Le champ *Serveur SQL* sera positionné sur `localhost`, le champ *Utilisateur SQL* sur `adminGLPI` (l'utilisateur de la BDD que vous avez créé précédemment), enfin le champ *Mot de passe SQL* devra contenir le mot de passe que vous avez défini pour l'utilisateur `adminGLPI`, dans notre cas `mdpAdminGLPI`. Si tous les paramètres sont bons le test de connexion à la base de données sera concluant. Sélectionnez alors la base de données que vous avez créé précédemment `db_glpi` puis cliquez sur le bouton *Continuer*. Vous devrez patientez quelques dizaines de secondes pendant que GLPI crée les différentes tables. Décochez la case *Envoyer "statistiques d'usage"* puis *Continuer*. Lisez le message de l'étape 5 puis *Continuer*. Arrêtez-vous à l'étape 6 pour noter ci-dessous tous les comptes créés par défaut.

    |Type de compte|identifiant|mot de passe|
    |:---:|:---:|:----:|
    |administrateur|||
    |technicien|||
    |normal|||
    |post-only|||

    Vous arrivez ensuite à la page d'accueil du site. Vérifiez que la **Source de connexion** est bien sur **Base interne GLPI** puis testez le compte administrateur. Vous devez arriver sur le tableau de bord. Plusieurs alertes de sécurité seront corrigées plus tard dans ce document. Cette page d'accueil sera décrite dans le TP **Utilisation de GLPI**.

<span style="color: #ff0000">**Validation professeur :**</span>

## Installation des agents de collecte (Windows et Linux)

La documentation sur l'installation des agents GLPI est disponible sur [GitHub](https://glpi-agent.readthedocs.io/en/latest/). L'agent est disponible pour Microsoft Windows, Apple MacOS et GNU/Linux.

!!! warning "Remarque importante"
    Installer un agent sur une machine qui va envoyer des données vers le serveur GLPI peut être dangereux du point de vue de la sécurité ! Dans ce TP, les données ne seront pas chiffrées et circulent en clair sur le réseau. Il est donc préférable que le serveur GLPI et les machines qui ont un agent GLPI soient dans le même réseau privé ou utilisent un tunnel chiffré IPSec si vous comptez envoyer des données à travers l'Internet.

!!! note "Exercice: Activation de l'inventaire sur le serveur GLPI"
    Connectez-vous sur votre serveur GLPI en tant qu'administrateur. Vous traiterez les alertes de sécurité qui apparaissent en haut de l'écran dans la partie sécurisation de ce document. Cliquez sur l'item **Administration** dans le menu latéral gauche puis sur l'item **Inventaire**.  
    En haut de la page cochez la case **Activer l'inventaire** puis cliquez sur le bouton **Sauvegarder** en bas de page. Cliquez sur l'item **Parc** dans le menu latéral gauche puis sur l'item **Tableau de bord**. C'est sur cette page que vous verrez apparaître vos futurs inventaires machines !

!!! note "Exercice: Installation de l'agent GLPI en environnement Microsoft Windows"
    Vous allez installer l'agent GLPI sous Microsoft Windows dans sa configuration de base. Si vous souhaitez décortiquer les options d'installation, rendez-vous sur la page d'[installation pour Windows de l'agent](https://glpi-agent.readthedocs.io/en/latest/installation/index.html#windows).  
    Démarrez votre machine virtuelle Windows. A l'aide d'un navigateur, rendez-vous à l'adresse suivante pour télécharger la dernière version de l'agent Windows: [https://github.com/glpi-project/glpi-agent/releases](https://github.com/glpi-project/glpi-agent/releases). Téléchargez la dernière version **x64** en version **Windows installer** puis lancez l'installation de l'agent en mode administrateur (il se peut que Windows refuse de lancer l'installation, passez outre cet avertissement).  
    Cliquez sur le bouton **Suivant** jusqu'à arriver à la page **Choose Setup Type**, cliquez sur le bouton **Typical**. Dans le champ **Remote targets**, mettre l'url suivante : `http://addr_ip_serveurGLPI/glpi/`, vérifiez que la case **Quick installation** est cochée puis cliquez sur le bouton **Next** puis sur le bouton **Install**. Attendez la fin de l'installation puis quitter le logiciel d'installation.  
    Pour vérifier que l'agent fonctionne correctement lancez un navigateur web puis connectez-vous à **localhost** sur le port 62354. Indiquez ci-après la ligne que vous tapez dans un navigateur : `________________________________`. Vous pouvez voir à quelle heure est prévue le prochain inventaire ou bien **forcer un inventaire** : c'est ce que vous aller faire maintenant. Il faudra alors attendre quelques minutes que l'inventaire soit transmis au serveur GLPI. 
    
!!! note "Exercice: Recherche simple dans un inventaire"
    Retournez sur la VM de votre serveur GLPI et rafraîchissez la page **Parc**/**Tableau de bord** jusqu'à ce que l'inventaire de la VM Windows apparaisse. Trouvez alors les éléments suivants en explorant les différentes rubriques d'un inventaire. Servez-vous aussi des **filtres** !
    
    |Élément|Valeur|
    |:---:|:---|
    |Version de Windows 10||
    |Version de Chrome||
    |version de Putty||
    |nom du BIOS||

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice: Installation de l'agent GLPI sous GNU/Linux"
    Vous allez maintenant installer l'agent Linux dans la VM **DebianAgentGLPI** que vous avez créé au début de ce TP. Lancez cette VM, connectez-vous en root puis relevez son adresse ip: `___________________`. Connectez-vous sur votre VM Debian en ssh depuis votre machine réelle puis identifiez-vous en tant qu'utilisateur **ciel** puis passez **root**.  
    La documentation officiel conseille l'installation de l'agent à partir du script *perl* disponible sur [GitHub](https://github.com/glpi-project/glpi-agent/releases/). Pour télécharger le script, utilisez la commande suivante en adaptant le numéro de version (**num_version**) dans les commandes ci-dessous:

    ```console
    wget https://github.com/glpi-project/glpi-agent/releases/download/num_version/glpi-agent-num_version-linux-installer.pl
    ```

    Pour afficher une aide sur l'installation de l'agent avec ce script, tapez:

    ```console
    perl glpi-agent-num_version-linux-installer.pl --help
    ```

    En lisant attentivement cette aide, proposez ci-dessous la ligne de commande à saisir pour:

    - envoyer l'inventaire vers l'adresse http://addr_ip_serveurGLPI/glpi/,
    - activer le serveur http de l'agent sur le *port 62354*,
    - que l'agent soit lancé en *mode service*,
    - que l'agent soit *verbeux* lors de son installation (affichage des messages d'installation à l'écran),
    - que l'inventaire soit effectué *directement après l'installation*.
  
    Votre proposition de ligne de commande:  
    ```console
    perl glpi-agent-num_version-linux-installer.pl ________________________________________________________________________
    ```

    Exécutez la ligne précédente avec vos arguments, patientez pendant l'installation de l'agent. Pour vérifier que l'agent a correctement effectué son travail, vous pouvez suivre ces derniers messages avec la commande:

    ```console
    systemctl status glpi-agent.service
    ```

    Vérifiez sur le serveur GLPI qu'un nouvel ordinateur a été ajouté. Donnez la version du paquet Debian **xz-utils**: `__________________`.  
    Pour relancer un inventaire à n'importe quel moment, il suffit de taper la commande `glpi-agent`.

<span style="color: #ff0000">**Validation professeur :**</span>

## Sécurisation de votre serveur GLPI

Ci-dessous vous trouverez des conseils pour sécuriser votre serveur GLPI. Ce qui suit est découpé en deux parties:

- la sécurisation simple qui fait suite à ce que vous avez fait jusque là,
- la sécurisation avancée qui nécessitera de reprendre l'installation depuis le début pour passer le serveur web en mode https. Cet aspect n'est pas traité en 1ère année.

### Sécurisation simple

Lorsque vous vous connectez sur le logiciel GLPI, vous avez des avertissements de sécurité qui apparaissent en haut du tableau de bord en orange. Vous allez régler ces problèmes en priorité.

!!! note "Exercice: Enlever les alertes de sécurité de GLPI"
    Il faut changer les mots de passe par défaut des utilisateurs que GLPI a créé lors de l'installation. Pour cela, il faut s'identifier en tant qu'administrateur puis se rendre dans le menu **Administrations/Utilisateurs**.  
    Cliquez par exemple sur l'utilisateur **glpi**, la page change pour afficher les **très** nombreuses rubriques que vous pouvez modifier. Pour l'instant contentez-vous de modifier le mot de passe et de sauvegarder. Recommencez l'opération pour tous les utilisateurs par défaut. Revenez à l'accueil, l'alerte concernant les utilisateurs par défaut à du disparaître.  
    Pour ne pas *rejouer* une installation de GLPI, il est préférable de supprimer le fichier `install/install.php`. Connectez-vous par ssh sur votre VM GLPI puis en tant qu'utilisateur **root**, effacez ce fichier avec une commande `rm` en sachant ou il se trouve !  
    Cherchez dans cette documentation ou vous avez désarchivé GLPI lors de l'installation. Votre commande: `________________________________`

!!! note "Exercice: Sécuriser la base de données MariaDB, désactiver le site par défaut"
    Vous pouvez augmenter un peu le niveau de sécurité de votre BDD en lançant en tant qu'utilisateur **root** le script `mysql_secure_installation`. Je vous laisse lire les questions que va vous poser ce script, la plupart des réponses par défaut (appuyer sur la touche ++enter++) conviennent.  
    Profitez-en aussi pour désactiver le site par défaut qu'apache2 active lors de son installation (la page *It works !*). Pour cela tapez la commande suivante:

    ```console
    a2dissite 000-default.conf
    systemctl reload apache2
    ```

    Testez alors l'accès à l'URL par défaut du site web de votre serveur GLPI : `http://addr_ip_serveur_glpi/`. Vous n'avez plus la page par défaut mais une page qui présente deux répertoires : celui de glpi et celui du site par défaut.

<span style="color: #ff0000">**Validation professeur :**</span>

## Conclusion

Vous venez d'installer et configurer un serveur GLPI. Vous savez installer et configurer des agents sous Microsoft Windows et GNU/Linux. Votre inventaire de parc machines est donc fonctionnel mais par encore totalement optimal. L'utilisation de la partie **helpdesk** n'a pas du tout été évoquer ici, en effet il faut avant cela configurer les *entités* de GLPI. Tout cela fera l'objet d'un nouveau TP : **Utilisation de GLPI**.
