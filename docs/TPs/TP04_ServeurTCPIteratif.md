# Conception d'un serveur TCP simple puis itératif utilisant les sockets POSIX

!!! tldr "Objectifs de l'exercice"

    - définir ce qu'est un serveur TCP *itératif*,
    - mettre en oeuvre les concepts vu en cours sur les serveurs TCP,
    - repérer les étapes d'un serveur TCP simple puis itératif,
    - coder un serveur TCP simple sous Microsoft Windows et/ou GNU/Linux,
    - améliorer le serveur TCP pour le rendre *itératif*,
    - gérer les erreurs de compilation (instruction `bind`).
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Cahier des charges

Vous avez développé dans les TPs précédents des clients qui se connectent à un serveur. Vous allez donc maintenant coder ce serveur en vous basant sur les exemples distribués en cours. Vous pouvez indifféremment travailler sous GNU/Linux ou Microsoft Windows.

Dans un premier temps, vous codez un serveur TCP qui accepte **une** connexion d'un client (celui que vous avez codé auparavant par exemple mais vous pouvez aussi utiliser le logiciel *herkule* par exemple). A la connexion d'un client, ce serveur renvoie une chaîne de caractères qui contient la date et l'heure, la valeur de la température et la valeur de l'hygrométrie. Puis le serveur coupe le dialogue avec le client.

Vous ne disposerez pas d'un capteur de température et d'hygrométrie réel sur bus i2c. Vous simulerez les valeurs de température et d'hygrométrie en tirant un nombre aléatoire (compris entre 15 et 25 pour la température, entre 10 et 90 pour l'hygrométrie). La date et l'heure, la température et l'hygrométrie seront stockés dans une structure. Le code de cette fonction est donné partiellement en annexe.

Le programme prend ces arguments sur la ligne de commande (adresse IP).

Dans un second temps, vous codez un serveur **itératif** (c'est à dire qu'il sert des clients les uns après les autres donc un seul à la fois) qui respectent les commandes vu au TP précédent (TMP, HUM, ALL et BYE).

## Conseils pour la réalisation du TP

Proposer une structure qui permet de stocker la date et l'heure, la valeur de la température et la valeur de l'hygrométrie. Cette structure s’appellera `mesure`.

A l'aide des exemples de code disponibles dans les *snippets* de ce site, en particulier sur le tirage de nombre au hasard et la récupération de la date et de l'heure, codez la fonction `prendreMesure()`.

Complétez le schéma client/serveur ci-après pour la réalisation d'un serveur **itératif** : indiquez le nom de toutes les fonctions C mises en jeu ainsi que les **deux** boucles nécessaires à la réalisation du serveur itératif.

N'oubliez pas de commenter vos codes car ce TP fera l'objet d'un compte-rendu noté dans lequel vous consignerez:

- la structure choisie,
- les fonctions utilisées pour le tirage de nombres aléatoires,
- les fonctions utilisées pour la gestion de la date et l'heure,
- le schéma des échanges client/serveur complété (avec les deux boucles),
- les codes commentés demandés.

## Annexes

```cpp
#include <random>
// à compléter

void prendreMesure(mesure& m) {
	double valTemp = 0.0;
	double valHum = 0.0;
	std::string dateHeureMesure;

	// Tirage aléatoire de nombres
	
	// tirage pour la valeur du capteur de température entre 15°C et 25°C, tirage d'un double
	
    // tirage pour la valeur du capteur d'hygrométrie entre 10%RH et 90%RH, tirage d'un entier
	
    // récupération de la date et l'heure courante
	
	// remplissage de la structure avec les valeurs précédentes

}
```

![Schéma client-serveur à compléter](../img/schemaClientServeurVide.png)