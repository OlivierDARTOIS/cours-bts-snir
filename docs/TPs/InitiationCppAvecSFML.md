# Initiation à la programmation C++ par l'utilisation d'une bibliothèque multimédia

!!! tldr "Objectifs de ce document"

    - Ce document permet de découvrir les principes de la programmation orientée objet (P.O.O) en utilisant une bibliothèque multimédia. Cette bibliothèque s'appelle **SFML** pour *Simple and Fast Multimedia Library*. Elle permet de réaliser très facilement des applications graphiques simples voire des jeux complexes.
    - Instanciation d'objets, appel de méthodes, lecture de documentation, algorithme simple...
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Prérequis

Avant de pouvoir utiliser la bibliothèque SFML, il y a un certain nombre de pré-requis:

- il faut connaître les **bases du C/C++** : savoir déclarer une variable, l'initialiser, faire des tests (`if`), réaliser des boucles (`for`, `while`), appelez des fonctions avec passage de paramètres, faire un programme basique avec une fonction  `main`,
- il faut savoir utiliser les **fonctions de bases d'un EDI** (Environnement de Développement Intégré). Dans notre cas il s'agira de *Microsoft Visual Studio 2017* (ou une version ultérieure) sous environnement Microsoft Windows ou *Microsoft Visual Studio Code* sous environnement GNU/Linux Debian.

!!! info "Téléchargement des logiciels de développement"
    - Microsoft Visual Studio Community Edition est la version gratuite de l'environnement de développement de Microsoft. Vous pouvez le télécharger sur le [site officiel](https://visualstudio.microsoft.com/fr/vs/) et l'installer sur votre ordinateur,
    - Microsoft Visual studio Code est l'éditeur de code open source de Microsoft. Il fonctionne sur une multitude de plateforme (Windows, Linux, Mac). Vous pouvez le télécharger sur le [site officiel](https://code.visualstudio.com/).

## Présentation de la SFML

Le cadriciel (ou bibliothèque) C++ SFML facilite le développement d'applications multimédias ou de jeux vidéos en simplifiant l’accès aux matériels audio, vidéo, souris et clavier voire le réseau. Sur le [site officiel](https://www.sfml-dev.org/index.php) vous trouverez les téléchargements des bibliothèques pré-compilées pour différents systèmes d'exploitations. Ce cadriciel est composé de cinq modules principaux (lire [description des modules](https://www.sfml-dev.org/tutorials/2.5/)) :

- **system** : gestion du temps, programmation multitâches (threads), flux de données utilisateurs, vecteurs 2D et 3D. Ce module est obligatoire et constitue une dépendance pour les autres modules,
- **window** : ouverture et gestion d'une fenêtre SFML, la gestion des évènements souris, clavier et joystick, gestion d'OpenGL,
- **graphics** : gestion 2D, les textures et les [*sprites*](https://fr.wikipedia.org/wiki/Sprite_(jeu_vid%C3%A9o)), les textes et polices, les transformations (rotation, position, mise à l'échelle), les [*shaders*](https://fr.wikipedia.org/wiki/Shader),
- **audio** : gestion de la musique et des sons, 
- **network** : la communication réseau par socket, 

Vous allez tout au long de ce document découvrir les différents modules et utiliser les différentes *méthodes* de ceux-ci.

## Environnement de développement

Pour faciliter l'installation de cette bibliothèque et se concentrer sur l'aspect *découverte de la programmation C++*, deux machines virtuelles VirtualBox sont disponibles en téléchargement. Vous devez donc avoir installé au préalable VirtualBox sur votre ordinateur quelque soit son environnement. Cliquez sur le lien de téléchargement choisi puis double-cliquez sur le fichier *ova* pour importer la machine virtuelle dans VirtualBox.

- Machine virtuelle Microsoft Windows 10 avec l'EDI Microsoft Visual Studio Community Edition 2017 préconfiguré (taille : 9Go) : [Téléchargement de la machine virtuelle Microsoft Windows 10](https://innovelectronique.fr/TP_SFML/Windows10_SFML.ova)
- Machine virtuelle GNU/Linux Debian 11 avec l'éditeur de code Microsoft Visual Studio Code préconfiguré, la compilation s'effectue en ligne de commande (taille : 2Go, utilisateur: snir, mdp: snir) : [Téléchargement de la machine virtuelle Debian GNU/Linux 11](https://innovelectronique.fr/TP_SFML/Debian11_SFML.ova)

## Notions de classes et d'objets

Avant de commencer à programmer avec la SFML, il faut se familiariser avec les notions de classes, d'objets, de méthodes et d'attributs. Nous allons commencer par l'étude d'un objet commun connu : par exemple un stylo encre. Notez que ces notions ne se substituent pas à un cours complet de C++ !

Les **attributs** sont les caractéristiques de l'objet. Dans le cas d'un stylo ça sera par exemple :

- la couleur de l'encre,
- le niveau de l'encre,
- si le capuchon est sur le stylo ou non.

Les **méthodes** ce sont les actions que vous pouvez réaliser avec l'objet. Dans le cas du stylo encre cela pourrait être :

- ecrire,
- mettre le capuchon,
- enlever le capuchon,
- obtenir le niveau d'encre.

Nous allons transformer cette description textuelle en un diagramme de classe [Unified Modelling Language](https://fr.wikipedia.org/wiki/UML_(informatique)). Le modèle d'un diagramme de classe est le suivant :

![Exemple de diagramme de classe](../img/SFML/PresentationDiagClasse_TP_SFML.png)

Ce qui se traduit pour notre exemple du stylo encre par le diagramme suivant:

![Exemple de diagramme de classe pour un stylo encre](../img/SFML/DiagClasseStylo_TP_SFML.png)

A partir du diagramme de classe, vous pouvez coder le **fichier de définition** de la classe (ou fichier d'en-têtes ou fichier .h)

```C++
class StyloPlume
{
    private:
    std::string couleur;
    int niveauEncre;
    bool capuchon;
    
    public:
    StyloPlume();
    ~StyloPlume();
    void mettreLeCapuchon();
    void enleverLeCapuchon();
    void ecrire(std::string phrase);
};
```

Le **fichier de déclaration** (ou fichier d'implémentation de la classe ou fichier .cpp) ne sera pas présenté ici. Par contre nous allons présenter une utilisation de cette classe dans un programme principal.

```C++
int main()
{
    // instanciation d'un objet de la classe StyloEncre
    // Vous passez la couleur "vert" par le constructeur
    StyloPlume monStylo("vert");
    
    // Vous enlevez le capuchon pour pouvoir écrire
    monStylo.enleverLeCapuchon();
    
    // Vous écrivez une phrase
    monStylo.ecrire("on ecrit virtuellement ce message en vert");
    
    // Vous refermez le capuchon pour pas que l'encre virtuelle seche
    monStylo.mettreLeCapuchon();
}
```

Dans le cas de la SFML, vous utiliserez des classes déjà créées. Vous aurez les prototypes des différentes méthodes dans des fichiers d'en-têtes et dans [la documentation de l'API](https://www.sfml-dev.org/documentation/2.5.1/annotated.php) (Application Programming Interface). Vous instanciez un objet et vous appelez les méthodes sur cet objet comme vu ci-dessus.

## Exemple 1 : test de compilation d'une application SFML

Ce premier test permet de vérifier que la bibliothèque SFML est correctement installée. De nombreuses notions sont présentes dans cet exemple mais seront reprises au fur et à mesure des exemples et exercices. Ne cherchez pas à comprendre tous les éléments de ce code, contentez-vous de saisir ce code et de l'exécuter. Si tout se passe bien vous obtiendrez la fenêtre suivante :

![Premiere fenetre SFML](../img/SFML/FenetreExempleSFML.png)

- sous Microsoft Windows, utilisez l'exemple de base avec le code ci-dessous. Lancez la compilation puis l'exécution.
- sous Debian GNU/Linux, vous compilerez l'exemple ci-dessous avec les commandes suivantes :

```console
snir@debian10-SFML:~/dev/sfml$ g++ -o test1_uneFenetre test1_uneFenetre.cpp -lsfml-graphics -lsfml-window -lsfml-system`
snir@debian10-SFML:~/dev/sfml$ ./test1_uneFenetre`
```

Saisissez le code suivant dans votre EDI, compilez-le et exécutez-le :

```cpp
#include <SFML/Graphics.hpp>

int main()
{
    sf::RenderWindow maFenetre(sf::VideoMode(300, 300), "SFML fonctionne !");
    sf::CircleShape cercle(100.f);
    cercle.setFillColor(sf::Color::Green);
    cercle.setPosition(50, 50);
    
    while (maFenetre.isOpen())
    {
        sf::Event evenement;
        while (maFenetre.pollEvent(evenement))
        {
            if (evenement.type == sf::Event::Closed)
                maFenetre.close();
        }

        maFenetre.clear();
        maFenetre.draw(cercle);
        maFenetre.display();
    }

    return 0;
}
```

## Analyse de l'exemple 1

Nous allons maintenant examiner le code précédent. Mais avant tout, il faut comprendre l'algorigramme ci-dessous qui décrit le fonctionnement de toutes les application SFML !

![Algorithme d'une application SFML](../img/SFML/OrganigrammeJeuSFML.png)

Le repère utilisé par la bibliothèque SFML se présente de la manière suivante. Notez que l'axe des ordonnées (axe y) est orienté vers le "bas". Les objets que vous créez ont par défaut leur origine dans le coin supérieur gauche (y compris s'il s'agit d'un cercle). Il faut en tenir compte lorsque vous positionnez vos objets ou alors il faudra changer leur origine.

![Repere utilisé par la SFML et les objets](../img/SFML/RepereSFML.png)

!!! note "Questions d'analyse du code"
    === "Questions"
         1. Repérez dans le programme précédent, les différentes parties de l'algorigramme,
         1. Quel est le rôle de la ligne #include <SFML/Graphics.hpp> ?
         1. Que signifie sf:: dans le code ci-dessus ?
         1. Quelle est la taille en pixel de la fenêtre que vous ouvrez ?
         1. Quelle est le type de la variable cercle ?
         1. Consultez la documentation de la classe [CircleShape](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1CircleShape.php) et expliquez le constructeur de la classe ?
         1. Quel est le rôle de la méthode [setFillColor()](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Shape.php#a3506f9b5d916fec14d583d16f23c2485) et comment changez-vous la couleur de remplissage (consultez la classe [Color](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Color.php)) ?
         1. Quel est le rôle de la méthode [setPosition()](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Transformable.php#af1a42209ce2b5d3f07b00f917bcd8015), pourquoi mettre (50,50) comme coordonnées de position et pas (150,150) qui est le centre de la fenêtre ?
         1. La méthode [isOpen()](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Window.php#ae873503db7d48157bb9cbf6129562bce) de la variable maFenetre renvoie quel type de variable ?
         1. Sur quel type d’évènement la fenêtre sera-t-elle fermée ?
    === "Réponses aux questions"
         1. partie initialisation : les 4 premières lignes, lecture des entrées avec maFenetre.pollEvent(), pas de calculs des nouvelles positions, affichage à l'écran : les 3 dernières lignes, le test de la fenêtre ouverte se faisant dans la boucle while, la libération des ressources est automatique,
         1. vous utilisez des objets 2D dans ce code, il faut donc inclure la gestion 2D de la bibliothèque SFML. Il faut donc inclure le fichier d'en-têtes *graphics.hpp*. Celui-ci incluant à son tour ces dépendances : *System.hpp* et *Window.hpp*,
         1. l'opérateur `::` est l'opérateur de résolution de portée. Vous l'utilisez depuis vos premiers codes C++ avec les méthodes cin et cout. En fait ces méthodes appartiennent à l'espace de nom standard `std`. Vous devriez donc écrire `std::cout`. Dans notre cas lorsque vous rencontrerez `sf::` cela indique que ce qui suit appartient à l'espace de nom de la SFML,
         1. dans le code vous trouvez `sf::VideoMode(300, 300)`. Commencez par lire [la documentation](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1VideoMode.php) de cette classe. Le premier paramètre donne le **nombre de pixel en x**, le deuxième paramètre donne le **nombre de pixel en y**, donc la taille de la fenêtre que vous allez ouvrir,
         1. dans le code vous trouvez : `sf::CircleShape cercle(100.f)` . Le nom de la variable est donc `cercle`, les parenthèses indiquent l'argument que vous passez au constructeur (ici le rayon : 100 pixels) et enfin le type de la variable est indiqué en début de ligne donc `sf::CircleShape`,
         1. dans la documentation de la classe [CircleShape](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1CircleShape.php) vous trouvez le constructeur suivant: `CircleShape (float radius=0, std::size_t pointCount=30)`. Le premier paramètre est le rayon du cercle en pixel, sa valeur par défaut est 0. Le deuxième paramètre est le nombre de points pour dessiner le cercle, sa valeur par défaut est 30. Si vous changez ce paramètre avec un nombre faible vous dessinez d'autres formes. Par exemple la valeur 3 dessine un triangle, la valeur 4 un carré, la valeur 5 un pentagone, etc...
         1. la méthode `setFillColor` permet de remplir l'intérieur du cercle avec une couleur unie. Cette couleur est spécifiée par l'argument de cette méthode. Cet argument est du type `Color`. En lisant la documentation de la classe `Color`, vous remarquez que vous pouvez spécifier une couleur avec un trio de valeurs RVB (par exemple `sf::Color(128, 128, 128)`) ou une couleur prédéfinie (`sf::Color::Black`, `sf::Color::White`, etc...). L'autocomplétion de votre EDI est très utile dans des cas comme celui-ci,
         1. la méthode `setPosition` permet de positionner un objet aux coordonnées x et y spécifiées en argument de cette méthode. La position d'un objet se définit à partir de son coin supérieur gauche et pas à partir de son centre (sauf si vous avez changé l'origine avec la méthode `setOrigin`). Dans notre cas vous avez ouvert une fenêtre de 300x300 donc le centre de cette fenêtre est en (150,150). Le rayon du cercle est de 100. Donc il faut positionner le cercle en (50,50) pour qu'il soit au centre de la fenêtre (reportez-vous au repère utilisé par la SFML vu plus haut, faites un dessin si vous ne comprenez pas...),
         1. le prototype de la méthode `isOpen` est le suivant : `bool sf::Window::isOpen()` donc cette méthode renvoie un booléen. Ce booléen est évalué dans la boucle `while` pour savoir si la fenêtre est ouverte et donc si le programme continue de s'exécuter,
         1. la fenêtre sera fermée (ligne `maFentre.close()`) si l'évènement intercepté est de type `sf::Event::Close`. Cet évènement est réalisé si vous cliquez sur la croix de fermeture de la fenêtre.

!!! note "Exercice : modifications du code d'origine"
    === "Questions"
        1. Modifiez la taille de la fenêtre en 640x480 pixels,
        1. Modifier la couleur du cercle en rouge, son diamètre à 400 et sa position au centre de la fenêtre,
        1. Rajouter un nouvel élément graphique : un rectangle de 300 pixel en x et 150 en y, de couleur blanche, centré sur la fenêtre. Ce rectangle doit apparaître par dessus le cercle rouge,
        1. Rajouter au code précédent la possibilité de fermer la fenêtre en appuyant sur la touche Echap (lire ce [tutoriel](https://www.sfml-dev.org/tutorials/2.5/window-events-fr.php)).
    === "Code C++ avec les modifications demandées"
         Le code avec les modifications demandées est disponible ci-dessous :

        ```C++
        #include <SFML/Graphics.hpp>

        int main()
        {
           // Fenetre en 640x480
           sf::RenderWindow maFenetre(sf::VideoMode(640, 480), "SFML fonctionne !");
    
           // premier element graphique : un cercle rouge de 200 pixels de rayon
           sf::CircleShape cercle(200.f);
           cercle.setFillColor(sf::Color::Red);
           cercle.setPosition(120, 40);

           // deuxieme element graphique
           sf::RectangleShape rectangle(sf::Vector2f(300,150));
           rectangle.setFillColor(sf::Color::White);
           rectangle.setPosition(170,165);

           // L'affichage boucle tant que la fenetre est ouverte
           while (maFenetre.isOpen())
           {
              sf::Event evenement;
              while (maFenetre.pollEvent(evenement))
              {
                  // fenetre se ferme qd vous cliquez sur la croix
                  if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                  // fenetre se ferme qd vous appuyez sur la touche ECHAP
                  if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
              }

              // Effacement de la fenetre
              maFenetre.clear();
              // Dessin dans l'ordre du cercle puis du rectangle
              // Le rectangle apparait au dessus du cercle
              maFenetre.draw(cercle);
              maFenetre.draw(rectangle);
              // Affichage des éléments graphiques dans la fenetre
              maFenetre.display();
           }

        return 0;
        }
        ```

A l'exécution de votre code vous obtenez l'image ci-dessous si vous avez mis dans le bon ordre l'appel à la méthode `draw()` de vos différents objets 2D :

![Panneau interdit](../img/SFML/PanneauInterditSFML.png)

## Exercice 1 : le soleil levant

Vous devez réaliser un programme qui simule le lever et le coucher du soleil sur la mer. Le soleil sera symbolisé par un cercle rempli de jaune avec une bordure jaune foncé. La mer sera symbolisée par un rectangle bleu clair. Le soleil doit apparaître derrière la mer. Le programme s'arrête avec un appui sur la touche ECHAP. L'illustration ci-dessous peut vous aider à réaliser votre programme:

![Illustration Soleil Levant](../img/SFML/SoleilLevantSFML.png)

Pour ralentir l'exécution de votre programme, vous utiliserez une pause dans votre boucle de dessin à l'aide de la méthode [sleep()](https://www.sfml-dev.org/documentation/2.5.1/group__system.php#gab8c0d1f966b4e5110fd370b662d8c11b). Un exemple d'utilisation de cette méthode peut être `sf::sleep(sf::milliseconds(30));`.

**Facultatif** : pour plus de réalisme, changer la couleur de fond du noir au jaune clair en fonction de la position du soleil. Le mouvement du soleil suit une sinusoïde. Un exemple de réalisation est présentée dans la cellule suivante. La qualité du gif animé est faible donc ne pas tenir du rendu dans ce document.

![soleil levant](../img/SFML/SoleilLevantSFML.gif)

Vous trouverez ci-dessous la solution de l'exercice 1. Cette solution contient aussi les modifications facultatives demandées ci-dessus :

??? note "Solution du soleil levant"
    ```cpp
    #include <SFML/Graphics.hpp>
    #include <cmath> // pour les fonctions mathematiques sin, cos, ...

    int main()
    {
        // Fenetre en 400x300
        sf::RenderWindow maFenetre(sf::VideoMode(400, 300), "Soleil levant...");
    
        // premier element graphique : un soleil jaune de 100 pixels de rayon
        sf::CircleShape soleil(100.f);
        // Chgt d'origine pour etre au milieu du soleil
        soleil.setOrigin(100, 100);
        soleil.setFillColor(sf::Color(0xE3, 0xC8, 0x00)); // les couleurs sont données en hexadécimal
        // la couronne autour du soleil
        soleil.setOutlineColor(sf::Color(0xB0, 0x95, 0x00));
        soleil.setOutlineThickness(5);
        soleil.setPosition(200, 300);

        // position en y du soleil
        int posYSoleil = 300;
        // le nombre de pixel de déplacement du soleil à chaque tour de la boucle d'animation
        int pasSoleil = -1;

        // couleur de fond
        int coulFond;

        // deuxieme element graphique : la mer
        sf::RectangleShape mer(sf::Vector2f(400,100));
        mer.setFillColor(sf::Color(0xDA, 0xE8, 0xFC));
        mer.setPosition(0, 200);

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            }

            // Calcul des nouvelles positions des objets
            // ici mvt du soleil en suivant une cosinusoide
            posYSoleil += pasSoleil;
            int posYSoleilCos = 300-300*cos((posYSoleil*1.57)/300); // 1.57 = pi/2
            if (posYSoleil == 0) pasSoleil = 1;
            if (posYSoleil == 300) pasSoleil = -1;
            soleil.setPosition(200, posYSoleilCos);

            // Calcul de la couleur de fond en fonction de la position du soleil
            if (posYSoleil > 255) coulFond = 0;
            else coulFond = (255 - posYSoleilCos);

            // Couleur de fond : blanc
            // maFenetre.clear(sf::Color::White);
            // Couleur de fond : du noir au jaune clair
            maFenetre.clear(sf::Color(coulFond, coulFond, coulFond/2));

            // Dessin dans l'ordre du soleil puis de la mer
            // La mer apparait au dessus du soleil
            maFenetre.draw(soleil);
            maFenetre.draw(mer);
            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 30ms
            sf::sleep(sf::milliseconds(30));
        }

        return 0;
    }
    ```

## Exemple 2 : Affichage d'images, de textes et quelques transformations...

Jusqu'à présent vous avez travaillé avec des objets 2D simples (cercles et rectangles). Nous allons maintenant voir comment charger une image pour en faire une *texture*, puis l'appliquer sur un cercle pour en faire un *sprite*. L'effet appliqué sur ce sprite sera une rotation et éventuellement un zoom mais il en existe d'autres (lire la documentation de la classe [Transformable](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Transformable.php)). Dans un deuxième temps, vous allez afficher du texte à l'écran à l'aide d'une police TrueType et réaliser un scrolling latéral.

Vous pouvez consultez le tutoriel en ligne sur ces deux aspects:

- [Les sprites et textures](https://www.sfml-dev.org/tutorials/2.5/graphics-sprite-fr.php)
- [Les textes et polices](https://www.sfml-dev.org/tutorials/2.5/graphics-text-fr.php)

### Texturer un cercle puis lui appliquer des effets

Lorsque vous lisez la documentation de la classe circleShape vous trouvez les méthodes suivantes:

- [setTexture(const Texture *texture, bool resetRect=false)](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Shape.php#af8fb22bab1956325be5d62282711e3b6) : applique l'image sur la forme,
- [rotate(float angle)](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Transformable.php#af8a5ffddc0d93f238fee3bf8efe1ebda) : effectue une rotation à partir de l'angle actuel,
- [setScale(float factorX, float factorY)](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Transformable.php#aaec50b46b3f41b054763304d1e727471) : effectue une mise à l'échelle en x et/ou en y.

Vous disposez donc de tout ce qu'il faut pour faire notre programme. Vous devez [télécharger l'image](../img/SFML/FondEcranRayonSoleilSFML.png) pour ce code et la sauvegarder au même endroit ou vous exécutez votre code (attention sous Microsoft Windows, ce n'est pas l'endroit ou se situe votre code source !).

!!! note "Exercice"
    Reprenez un de vos codes antérieurs. Créez une fenêtre de 400x400 pixels, puis créez un cercle de 300 pixels de rayon. Positionnez l'origine de ce cercle en son centre et positionnez celui-ci au milieu de la fenêtre. Les instructions suivantes permettent de charger une image en tant que texture puis de l'appliquer sur le cercle :

    ```C++
    ...
    // premier element graphique : un cercle de 300 pixel de rayon
    // Ce cercle s'étend donc en dehors de la zone visible de la fenêtre
    sf::CircleShape cercleTexture(300.f);
    ...
    // Chargement de l'image dans une texture
    sf::Texture texture;
    if (!texture.loadFromFile("FondEcranRayonSoleilSFML.png"))
    {
        // erreur...
        std::cout << "Image non trouvée..." << std::endl;
        exit(-1);
    }

    // Application sur le cercle
    cercleTexture.setTexture(&texture);
    ...
    // Calcul des nouvelles positions des objets
    // on effectue la rotation du cercle, ici par pas de 2°
    cercleTexture.rotate(2);
    ```

    Compilez le code et testez-le. Complétez ce code pour réaliser un zoom sur ce cercle texturée. Le zoom sera compris entre 1.0 et 10.0 par pas de 0.1. Vous devriez obtenir un peu prés l'effet ci-dessous.

![cercle texture en roto-zooming](../img/SFML/CercleTexture.gif)

Oui ca donne le mal de mer et normalement cela trouble votre vue... Si vous regardez quelques secondes la spirale puis vous regardez une autre partie de votre écran, vous verrez celle-ci "s'enrouler" de la même manière que la spirale.

??? "Correction de l'exercice"
    ```cpp
    #include <SFML/Graphics.hpp>
    #include <iostream>
    #include <cmath> // pour les fonctions mathematiques sin, cos, ...

    int main()
    {
        // Fenetre en 400x400
        sf::RenderWindow maFenetre(sf::VideoMode(400, 400), "Mal de mer...");
    
        // premier element graphique : un cercle de 300 pixel de rayon
        sf::CircleShape cercleTexture(300.f);
        // Chgt d'origine pour etre au milieu du cercleTexture
        cercleTexture.setOrigin(300, 300);
        cercleTexture.setPosition(200, 200);

        float zoom = 1;
        float pasZoom = 0.1;

        // Chargement de l'image dans une texture
        sf::Texture texture;
        if (!texture.loadFromFile("FondEcranRayonSoleilSFML.png"))
        {
            // erreur...
            std::cout << "Image non trouvée..." << std::endl;
            exit(-1);
        }

        // Application sur le cercle
        cercleTexture.setTexture(&texture);

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            }

            // Calcul des nouvelles positions des objets
            // on effectue la rotation du cercle, ici par pas de 2°
            cercleTexture.rotate(2);
            // même mise à l'echelle en x et en y mais ce n'est pas obligatoire
            cercleTexture.setScale(zoom, zoom);
            zoom += pasZoom;
            if (zoom > 10.0) pasZoom = -0.1;
            if (zoom < 1.0) pasZoom = 0.1;

            // effacement de la fenetre en noir
            maFenetre.clear();

            // Dessin du cercle texturé
            maFenetre.draw(cercleTexture);

            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 30ms
            sf::sleep(sf::milliseconds(30));
        }

        return 0;
    }
    ```

### Charger une police de caractères et afficher des messages

L'utilisation du texte dans votre fenêtre va vous permettre d'afficher des informations à destination de votre utilisateur (comme le score, le nombre de vie ou le temps restant). La SFML utilise par défaut le format de police TrueType (fichier TTF). Les polices que vous utiliserez ne sont pas celles installées dans le système mais celles que vous mettrez au niveau de votre exécutable. Pour réaliser ce TP, je vous propose de télécharger cette police ([Heroes Legend](../img/SFML/hl.ttf)).

Repartez d'un de vos anciens codes, nettoyez votre code pour ne garder que le strict nécessaire (création fenêtre, boucle d'évènements et d'affichage). Lisez la documentation de la classe [`sf::Font`](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Font.php) et [`sf::Text`](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Text.php), relisez [le tutoriel](https://www.sfml-dev.org/tutorials/2.5/graphics-text-fr.php) sur l'utilisation des textes puis **notez les étapes essentielles** pour afficher un texte à l'écran.

Comment réaliser un scrolling latéral ? Il faut tout simplement déplacer le texte d'un pixel vers la droite ou la gauche à chaque boucle d'affichage. Dans le scroll ci dessous, le texte est centré au milieu de la fenêtre, le texte est positionné en dehors de la zone affichable à droite puis vous commencez à décrémenter la position en x du texte. Cette position en x doit être décrémenté jusqu'à ce que le texte disparaisse complètement à gauche : il faudra donc connaître la largeur de la boite englobante du texte (bounding box en anglais). Vous repositionnez alors le texte en dehors de la zone d'affichage à droite.

Le schéma si-dessous vous aidera à déterminer les positions importantes pour réaliser votre scrolling :

![Explications scrolling](../img/SFML/ExplicationsScrollSFML.png)

Pour obtenir la boite englobante d'un objet vous disposez de la méthode [getGlobalBounds()](https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Sprite.php#aa795483096b90745b2e799532963e271).

Un exemple de réalisation du scrolling d'un texte de droite à gauche. Le ligne présente sur la fenêtre représente le milieu de la fenêtre.

![Scrolling Texte](../img/SFML/ScrollingTexteSFML.gif)

Voici le code de l'effet ci-dessus. Vous trouverez des nouvelles méthodes pour déterminer la taille des objets, de la fenetre. Lisez attentivement les commentaires :

```cpp
#include <SFML/Graphics.hpp>
#include <iostream> // pour afficher sur la console avec cout

int main()
{
    // Fenetre en 600x200
    sf::RenderWindow maFenetre(sf::VideoMode(600, 200), "Scrolling message !");
    
    // Ligne de repère au milieu de la fenetre
    sf::RectangleShape ligne(sf::Vector2f(maFenetre.getSize().x,1));
    ligne.setFillColor(sf::Color::White);
    ligne.setPosition(0, maFenetre.getSize().y / 2);

    // Création d'un objet de type Font
    sf::Font maPolice;
    if (!maPolice.loadFromFile("hl.ttf"))
    {
        // erreur...
        std::cout << "Police non trouvée..." << std::endl;
        exit(-1);
    }

    // Création d'un objet de type Text
    sf::Text texteAAfficher;

    // choix de la police à utiliser
    texteAAfficher.setFont(maPolice);
    // Texte de la chaîne de caractères à afficher
    texteAAfficher.setString("Programmation C++ avec SFML !");
    // choix de la taille des caractères
    texteAAfficher.setCharacterSize(50); // exprimée en pixels, pas en points !
    // choix de la couleur du texte
    texteAAfficher.setFillColor(sf::Color::Red);

    // position de départ du texte en x : largeur de la fenetre
    int positionXTexte = maFenetre.getSize().x;
    // boite englobante du texte (bounding box en anglais)
    sf::FloatRect boiteEnglobanteTexte = texteAAfficher.getGlobalBounds();
    // calcul de la position en y du texte pour qu'il soit centré dans la fenetre
    int positionYTexte = (maFenetre.getSize().y / 2) - (boiteEnglobanteTexte.height / 2);

    // L'affichage boucle tant que la fenetre est ouverte
    while (maFenetre.isOpen())
    {
        sf::Event evenement;
        while (maFenetre.pollEvent(evenement))
        {
            // fenetre se ferme qd vous cliquez sur la croix
            if (evenement.type == sf::Event::Closed)
                maFenetre.close();
            // fenetre se ferme qd vous appuyez sur la touche ECHAP
            if (evenement.key.code == sf::Keyboard::Escape)
                maFenetre.close();
        }

        // Calcul des nouvelles positions des objets
        // vous positionnez votre texte en x et y grace aux variables
        // positionXTexte et positionYTexte
        texteAAfficher.setPosition(positionXTexte, positionYTexte);
        // on décale la position en x du texte vers la gauche
        positionXTexte-=2;
        // si la position en x est négative et inférieur à la longeueur du texte 
        // cela signifie que le texte a complétement disparu à gauche donc on remet
        // la position de départ
        if (positionXTexte < -boiteEnglobanteTexte.width) positionXTexte = maFenetre.getSize().x;;

        // effacement de la fenetre en noir
        maFenetre.clear();

        // Dessin du texte
        maFenetre.draw(ligne);
        maFenetre.draw(texteAAfficher);

        // Affichage des éléments graphiques dans la fenetre
        maFenetre.display();

        // On ralentie la boucle : ici 10ms
        sf::sleep(sf::milliseconds(10));
    }

    return 0;
}
```

## Exercice 2 : Affichage de *GAME OVER*

Réalisez un programme qui montrera l'animation ci-dessous. Il n'existe pas une solution unique et le code de correction peut être critiqué. Par exemple il n'est pas optimisé, la lisibilité et la compréhension ont été privilégié.

![Animation GameOver](../img/SFML/GameOverSFML.gif)

??? note "Un exemple de code pour l'exercice 2"
    ```cpp
    #include <SFML/Graphics.hpp>
    #include <iostream> // pour afficher sur la console avec cout

    int main()
    {
        // Fenetre en 600x200
        sf::RenderWindow maFenetre(sf::VideoMode(800, 300), "Scrolling message !");
    
        // Ligne de repère au milieu de la fenetre
        sf::RectangleShape ligne(sf::Vector2f(maFenetre.getSize().x,8));
        ligne.setFillColor(sf::Color(0,0,0));
        ligne.setPosition(0, (maFenetre.getSize().y / 2) - 4);
        int coulLigne = 0;
        int pasCoulLigne = 5;

        // Création d'un objet de type Font
        sf::Font maPolice;
        if (!maPolice.loadFromFile("hl.ttf"))
        {
            // erreur...
            std::cout << "Police non trouvée..." << std::endl;
            exit(-1);
        }

        // Création d'un objet de type Text
        sf::Text texte1;

        // choix de la police à utiliser
        texte1.setFont(maPolice);
        // Texte de la chaîne de caractères à afficher
        texte1.setString("GAME OVER");
        // choix de la taille des caractères
        texte1.setCharacterSize(60); // exprimée en pixels, pas en points !
        // choix de la couleur du texte
        texte1.setFillColor(sf::Color(40,116,166));

        // boite englobante du texte (bounding box en anglais)
        sf::FloatRect boiteEnglobanteTexte = texte1.getGlobalBounds();
        texte1.setOrigin(boiteEnglobanteTexte.width/2, boiteEnglobanteTexte.height/2);
        texte1.setPosition(maFenetre.getSize().x/2, -boiteEnglobanteTexte.height-15);
        int nbPixels = boiteEnglobanteTexte.height+15+(maFenetre.getSize().y/2);
        int cpt=0;

        // Les autres textes sont crées à partir du premier
        sf::Text texte2(texte1);
        texte2.setFillColor(sf::Color(52,152,219));
        texte2.move(5,5);

        sf::Text texte3(texte1);
        texte3.setFillColor(sf::Color(133,193,243));
        texte3.move(10,10);

        sf::Text texte4(texte1);
        texte4.setFillColor(sf::Color(214,234,248));
        texte4.move(15,15);

        // booléen pour savoir quel texte doit être déplacé
        bool texte1OK = false, texte2OK = false, texte3OK = false, texte4OK = true;

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            }

            // Calcul des nouvelles positions des objets
            // Les 4 lignes suivantes pour gérer la couleur de la ligne centrale (noir <-> rouge)
            coulLigne+=pasCoulLigne;
            if (coulLigne > 255) { pasCoulLigne = -1; coulLigne = 255; }
            if (coulLigne < 0) { pasCoulLigne = 1; coulLigne = 0; }
            ligne.setFillColor(sf::Color(coulLigne, 0, 0));
            // gestion de l'affichage des différents textes les uns aprés les autres
            if (cpt < nbPixels) {
                if (texte1OK) texte1.move(0, 1);
                if (texte2OK) texte2.move(0, 1);
                if (texte3OK) texte3.move(0, 1);
                if (texte4OK) texte4.move(0, 1);
                cpt++;
            } else {
                cpt=0;
                if (texte1OK) { texte1OK = false; }
                if (texte2OK) { texte2OK = false; texte1OK = true; }
                if (texte3OK) { texte3OK = false; texte2OK = true; }
                if (texte4OK) { texte4OK = false; texte3OK = true; }
            }
        
            // effacement de la fenetre en noir
            maFenetre.clear(sf::Color::White);

            // Dessin du texte
            maFenetre.draw(ligne);
            maFenetre.draw(texte4);
            maFenetre.draw(texte3);
            maFenetre.draw(texte2);
            maFenetre.draw(texte1);

            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 2ms
            sf::sleep(sf::milliseconds(2));
        }

        return 0;
    }    
    ```

## Exemple 3 : utilisation de la souris et du clavier

Vous allez commencer par utiliser la souris. Cette classe est la plus simple d'utilisation. Le clavier quand à lui réclame plus d'attention. En particulier comment gérer l'appui long sur les touches et le relâchement de celles-ci.

### La souris

La SFML gère **une souris** et jusqu'à **cinq boutons** sur celle-ci (y compris la roulette). Il n'y a pas d'objet *souris* a instancier. En effet toutes les méthodes de la classe [sf::Mouse](https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Mouse.php) sont statiques donc elles peuvent s'utiliser directement. Parmi les méthodes disponibles, vous utiliserez le plus souvent:

- `static Vector2i getPosition(const Window& relativeTo)` : cette méthode renvoie la position relativement à la coordonnée (0,0) de la fenêtre. Vous pouvez donc avoir des coordonnées négatives !. Si vous voulez les coordonnées par rapport à l'origine de l'écran, utilisez cette méthode sans passer d'argument. Pour récupérer les coordonnées x et y de la souris, il faut accéder aux membres x et y du `sf::Vector2i`,
- `static bool sf::Mouse::isButtonPressed(Button button)` : cette méthode permet de tester si le bouton passé en argument est appuyé (true) ou relâché false). Les boutons les plus utilisés sont `sf::Mouse::Left` et `sf::Mouse::Right`.

Le programme qui suit trace une droite entre l'origine de la fenêtre et la position actuelle de la souris à condition qu'elle soit à l'intérieure de la fenêtre:

```cpp
#include <SFML/Graphics.hpp>

int main()
{
    // Fenetre en 300x300
    sf::RenderWindow maFenetre(sf::VideoMode(300, 300), "Suivi souris");
    
    // Creation d'une ligne 2D à l'aide d'un tableau de vertex
    sf::Vertex ligne[2];
    // point de départ de la ligne en (0,0)
    ligne[0].position = sf::Vector2f(0,0);
    // couleur de début de la ligne
    ligne[0].color = sf::Color::Black;
    // couleur de fin de la ligne
    ligne[1].color = sf::Color::Cyan;
    
    // variables pour stocker la position actuelle et l'ancienne position de la souris
    sf::Vector2i localPosition = sf::Mouse::getPosition(maFenetre);
    sf::Vector2i localPositionLast = sf::Mouse::getPosition(maFenetre);

    // L'affichage boucle tant que la fenetre est ouverte
    while (maFenetre.isOpen())
    {
        sf::Event evenement;
        while (maFenetre.pollEvent(evenement))
        {
            // fenetre se ferme qd vous cliquez sur la croix
            if (evenement.type == sf::Event::Closed)
                maFenetre.close();
            // fenetre se ferme qd vous appuyez sur la touche ECHAP
            if (evenement.key.code == sf::Keyboard::Escape)
                maFenetre.close();
        }

        // Calcul des nouvelles positions des objets
        localPosition = sf::Mouse::getPosition(maFenetre);
        // La position de la souris a-t-elle changé depuis la dernière boucle
        if (localPosition != localPositionLast) {
            localPositionLast = localPosition;
            // la souris est-elle dans la fenêtre
            if (localPositionLast.x>=0 && localPositionLast.x<=maFenetre.getSize().x
                && localPositionLast.y>=0 && localPositionLast.y<=maFenetre.getSize().y) {
                    // on positionne le deuxième point d'une ligne aux coordonnées de la souris
                    ligne[1].position=sf::Vector2f(localPositionLast.x, localPositionLast.y);
            }
        } 

        // effacement de la fenetre en blanc
        maFenetre.clear(sf::Color::White);

        // Dessin de la ligne, elle est composée de 2 vertex
        maFenetre.draw(ligne, 2, sf::Lines);

        // Affichage des éléments graphiques dans la fenetre
        maFenetre.display();

        // On ralentie la boucle : ici 10ms
        sf::sleep(sf::milliseconds(10));
    }

    return 0;
}
```

Saisissez ce code, compilez-le puis exécutez-le. Vous devez obtenir quelque chose comme ce qui suit (la souris n'apparaît pas mais la ligne suit le mouvement de celle-ci):

![Ligne Suivi Souris](../img/SFML/LigneSuiviSourisSFML.gif)

### Mise en oeuvre de la souris avec deux exemples à coder

Pour mettre en application ce que vous venez de découvrir, deux exercices de mises en oeuvre vous sont proposés:

!!! note "Exercice : la varicelle"
    **la varicelle** : ouvrez une fenêtre de 300x300, à chaque clic gauche dans la fenêtre affichez un cercle plein de diamètre aléatoire et de couleur rouge. Si la souris sort de la fenêtre, tous les cercles disparaissent, vous êtes guéris ! Reportez-vous à la démonstration ci-dessous. Le problème de cet exercice est de gérer **UN** clic gauche à la fois et pas plusieurs.

![Varicelle SFML](../img/SFML/VaricelleSFML.gif)

??? note "Une correction possible pour *la varicelle*"
    ```cpp
    #include <SFML/Graphics.hpp>
    #include <random> // pour le tirage au hasard

    int main()
    {
        // Fenetre en 300x300
        sf::RenderWindow maFenetre(sf::VideoMode(300, 300), "Varicelle");
    
        // Initialisation du générateur de nombres aléatoires
        std::random_device rd;
        // tirage aléatoire du rayon entre 10 et 50 pixels
        std::uniform_int_distribution<int> rayonBouton(10, 50);

        // un bouton de varicelle
        sf::CircleShape bouton;
        bouton.setFillColor(sf::Color::Red);
        bouton.setOutlineThickness(5);
        bouton.setOutlineColor(sf::Color(236, 112, 99));

        // variables pour stocker la position actuelle
        sf::Vector2i localPosition;

        // Gestion du clic gauche pour éviter le comptage erroné
        bool boutonGaucheRelache = true;

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            }

            // Calcul des nouvelles positions des objets
            localPosition = sf::Mouse::getPosition(maFenetre);
        
            // la souris est-elle dans la fenêtre avec un clic gauche ?
            if (localPosition.x>=0 && localPosition.x<=maFenetre.getSize().x
                && localPosition.y>=0 && localPosition.y<=maFenetre.getSize().y) 
            {
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (boutonGaucheRelache == true)) 
                {
                    // oui on dessine un cercle avec un rayon tiré au hasard à la 
                    // position actuelle de la souris
                    bouton.setRadius(rayonBouton(rd));
                    bouton.setOrigin(bouton.getRadius(), bouton.getRadius());
                    bouton.setPosition(localPosition.x, localPosition.y);

                    boutonGaucheRelache = false;
                    maFenetre.draw(bouton);
                }
            }
            else
                maFenetre.clear(sf::Color(250, 219, 216));

            // on vérifie que le bouton gauche est relaché pour accepter le comptage
            if (boutonGaucheRelache == false) {
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left) == false)
                    boutonGaucheRelache = true;
            }

            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 10ms
            sf::sleep(sf::milliseconds(10));
        }

        return 0;
    }
    ```

!!! note "exercice : machine compteuse de votes"
    Une **machine compteuse de votes** : ouvrez une fenêtre de 300x300, séparez là en deux avec une ligne verticale, affichez deux compteurs en haut à droite et à gauche. Ces compteurs sont incrémentés à chaque fois que vous réalisez un clic gauche dans la partie droite ou gauche. Reportez-vous à la démonstration ci-dessous.

![Machine à voter](../img/SFML/MachineVoteSFML.gif)

Même chose que dans l'exercice précédent il faut gérer correctement le clic gauche (compter un seul clic et pas une multitude). Pour les textes, vous travaillez avec la classe `string` de la STL. Mais la SFML travaille avec sa propre représentation des chaines de caractères. L'extrait de code ci-dessous indique comment convertir une chaîne string de la STL en une chaîne string de la SFML:

```C++
sf::Text texteGraphiqueSFML; // un élément text graphique de la SFML
std::string uneChaine = "Hello"; // une chaine de type string de la STL 
// on appelle la méthode setString qui permet de fixer le texte d'un texte graphique
// en argument on appelle le constructeur de la classe sf::String qui permet de
// réaliser une conversion std::string vers sf::String
texteGraphiqueSFML.setString(sf::String(uneChaine));
```

Pour convertir un nombre entier en une chaîne : il faut utiliser la  méthode `std::to_string()`. Lire [la documentation](http://www.cplusplus.com/reference/string/to_string/) de cette méthode.

??? note "Exemple de solution pour la machine *compteuse de votes*"
    ```C++
    #include <SFML/Graphics.hpp>
    #include <iostream>
    #include <string>

    int main()
    {
        // Fenetre en 300x300
        sf::RenderWindow maFenetre(sf::VideoMode(300, 300), "Machine Vote");
    
        // Creation d'une ligne 2D à l'aide d'un tableau de vertex
        sf::Vertex ligne[2];
        // point de départ de la ligne en (0,0)
        ligne[0].position = sf::Vector2f(maFenetre.getSize().x/2, 0);
        ligne[1].position = sf::Vector2f(maFenetre.getSize().x/2, maFenetre.getSize().y);
        ligne[0].color = sf::Color::Black;
        ligne[1].color = sf::Color::Cyan;

        // Les textes des compteurs de vote
        sf::Font maPolice;
        if (!maPolice.loadFromFile("hl.ttf"))
        {
            // erreur...
            std::cout << "Police non trouvée..." << std::endl;
            exit(-1);
        }

        // Création d'un objet texte pour le compteur gauche
        sf::Text texteCptGauche;
        texteCptGauche.setFont(maPolice);
        texteCptGauche.setString("Gauche: 0");
        texteCptGauche.setCharacterSize(10); // exprimée en pixels, pas en points !
        texteCptGauche.setFillColor(sf::Color::Black);
        texteCptGauche.setPosition(10,10);

        sf::Text texteCptDroite(texteCptGauche);
        texteCptDroite.setString("Droit: 0");
        texteCptDroite.setPosition(maFenetre.getSize().x/2 + 10, 10);

        // variables pour stocker la position actuelle
        sf::Vector2i localPosition;

        // Compteur de clic pour coté gauche et droit
        int cptGauche = 0, cptDroit = 0;
        // Gestion du clic gauche pour éviter le comptage erroné
        bool boutonGaucheRelache = true;

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            }

            // Calcul des nouvelles positions des objets
            localPosition = sf::Mouse::getPosition(maFenetre);
        
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (boutonGaucheRelache == true)) {
                // la souris est-elle dans la fenêtre partie gauche
                if (localPosition.x>=0 && localPosition.x<=maFenetre.getSize().x/2
                && localPosition.y>=0 && localPosition.y<=maFenetre.getSize().y) {
                    // on ajoute un au compteur de gauche et on met à jour le texte du compteur
                    cptGauche++;
                    std::string cptGaucheString = "Gauche: " + std::to_string(cptGauche);
                    texteCptGauche.setString(sf::String(cptGaucheString));
                    boutonGaucheRelache = false;
                }
                if (localPosition.x>maFenetre.getSize().x/2 && localPosition.x<=maFenetre.getSize().x
                && localPosition.y>=0 && localPosition.y<=maFenetre.getSize().y) {
                    // on ajoute un au compteur de droite et on met à jour le texte du compteur
                    cptDroit++;
                    std::string cptDroitString = "Droit: " + std::to_string(cptDroit);
                    texteCptDroite.setString(sf::String(cptDroitString));
                    boutonGaucheRelache = false;
                }
                // Debug dans la console
                // std::cout << "G:" << cptGauche << "   D:" << cptDroit << std::endl;
            }
            // on vérifie que le bouton gauche est relaché pour accepter le comptage
            if (boutonGaucheRelache == false) {
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left) == false)
                    boutonGaucheRelache = true;
            }

            // effacement de la fenetre en blanc
            maFenetre.clear(sf::Color::White);

            // Dessin de la ligne, elle est composée de 2 vertex
            maFenetre.draw(ligne, 2, sf::Lines);
            // Affichage des deux textes
            maFenetre.draw(texteCptGauche);
            maFenetre.draw(texteCptDroite);

            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 10ms
            sf::sleep(sf::milliseconds(10));
        }

        return 0;
    }
    ```

### Le clavier

Vous avez vu dans les exercices sur la souris que la gestion sans évènement de la souris n'est pas forcement la plus simple. Du coup il est peut être plus facile de gérer les saisies clavier et souris avec des évènements. Nous allons nous concentrer sur la gestion du clavier dans cette partie.

Le programme suivant permet de déplacer un cercle à l'écran à l'aide des touches fléchées. Les seuls changements sur ce code sont la gestion des touches de direction et le déplacement associé du cercle. La seule contrainte est que le cercle ne sorte pas de la zone de visualisation de la fenêtre en vérifiant ces coordonnées.

```cpp
#include <SFML/Graphics.hpp>

int main()
{
    // Fenetre en 300x300
    sf::RenderWindow maFenetre(sf::VideoMode(300, 300), "Deplacement avec touches");
    
    // le sprite que l'on va déplacer à l'écran
    sf::CircleShape vaisseau(30.f);
    vaisseau.setFillColor(sf::Color::Cyan);
    vaisseau.setOutlineThickness(5);
    vaisseau.setOutlineColor(sf::Color::Blue);
    vaisseau.setOrigin(vaisseau.getRadius(), vaisseau.getRadius());
    vaisseau.setPosition(maFenetre.getSize().x/2, maFenetre.getSize().y/2);
   
    // L'affichage boucle tant que la fenetre est ouverte
    while (maFenetre.isOpen())
    {
        sf::Event evenement;
        while (maFenetre.pollEvent(evenement))
        {
            // fenetre se ferme qd vous cliquez sur la croix
            if (evenement.type == sf::Event::Closed)
                maFenetre.close();
            // fenetre se ferme qd vous appuyez sur la touche ECHAP
            if (evenement.key.code == sf::Keyboard::Escape)
                maFenetre.close();
            // gestion des déplacements de la forme en fonction des touches de directions
            if (evenement.key.code == sf::Keyboard::Left)
                if (vaisseau.getPosition().x > 0) vaisseau.move(-1,0);
            if (evenement.key.code == sf::Keyboard::Right)
                if (vaisseau.getPosition().x < maFenetre.getSize().x) vaisseau.move(1,0);
            if (evenement.key.code == sf::Keyboard::Up)
                if (vaisseau.getPosition().y > 0) vaisseau.move(0,-1);
            if (evenement.key.code == sf::Keyboard::Down)
                if (vaisseau.getPosition().y < maFenetre.getSize().y) vaisseau.move(0,1);
        }
        
        // Effacement de la fenêtre
        maFenetre.clear(sf::Color::White);

        // dessin des éléments graphiques dans la fenêtre
        maFenetre.draw(vaisseau);

        // Affichage des éléments graphiques dans la fenetre
        maFenetre.display();

        // On ralentie la boucle : ici 2ms
        sf::sleep(sf::milliseconds(2));
    }

    return 0;
}
```

## Exercice 3 : un jeu complet

Jeu de tir au canon. Reportez-vous à la capture d'écran ci-dessous. Les règles :

- **orientez le canon** de l'angle souhaité avec les flèches haut et bas,
- **réglez la puissance de tir** avec les flèches droite et gauche,
- **tirez votre boulet** de canon avec la barre d'espacement,
- si votre **boulet touche la caisse** vous avez **gagné** sinon vous avez **perdu**,
- **recommencez une nouvelle partie** en appuyant sur la touche espace

![Ecran du jeu de tir](../img/SFML/EcranJeuDeTirSFML_Sprites.png)

Les [objets graphiques](https://redfoc.com/item/cannon-ball-assets/) sont librement utilisables dans votre jeu.

Le boulet suit les équations du mouvement d'un solide tiré avec un certain angle *alpha* exprimé en radians et une vitesse initiale *V0* exprimée en m/s. La lettre *g* représente l'accélération de la pesanteur et sera prise égale à 9,81 m/s-2:

![Trajectoire d'un projectile](../img/SFML/TrajectoireProjectileSFML.jpg)

x(t) = V0 * cos(alpha) * t et y(t) = 0.5 * g x t^2 + V0 * sin(alpha) * t  

Dans l'image ci-dessous vous avez les dimensions des différents éléments graphiques ainsi que leur point d'origine conseillé.

![Sprites du jeu de tir](../img/SFML/FeuilleDeSpritesPF_JeuDeTirSFML.png)

Les étapes de mises en oeuvre du jeu:

1. Partez d'un exemple de code déjà existant avec la gestion des touches de direction par exemple.
1. Listez tous les éléments graphiques du jeu puis créer toutes les variables pour les textures, les sprites, les polices, les textes. Il faut ensuite les placer à l'écran au bon endroit. Compilez puis exécutez votre code pour vérifier que tous les éléments graphiques sont correctement placés. Cette partie représente les deux tiers du code. Vous devez obtenir un écran qui ressemble à la capture d'écran donnée ci-dessus.
1. Animez le canon avec une rotation. La flèche vers le haut augmente l'angle du canon. La flèche vers le bas diminue l'angle du canon. L'angle doit toujours être compris ente 0° et 90°. La mise à jour du texte de l'angle fonctionne.
1. La puissance du canon (assimilée à *V0*) est gérée par les flèches droite et gauche. La flèche droite augmente la puissance, la flèche gauche diminue la puissance. La puissance est toujours comprise entre 60 et 300. La mise à jour du texte de puissance fonctionne. Vous pouvez aussi dessiner la jauge de puissance (un rectangle plein dont la taille évolue avec le niveau de puissance, un rectangle vide avec un bord de taille fixe de longueur égale à la puissance maximum).
1. La partie la plus difficile : la gestion de la trajectoire du boulet. Celui-ci doit suivre les équations données. Cependant il faut éliminer la composante temporelle (le temps t) et faire une équation de la forme **y=f(x)**. Il suffira alors de parcourir l'axe des x pour avoir la position en y et donc la position du boulet. Le tir du boulet sera déclenché par un appui sur la touche espace. Réalisez le déplacement du boulet à l'écran en tenant compte de l'angle du canon et de la puissance. Pour l'instant ne tenez pas compte du fait que le boulet doit sortir de la gueule du canon.
1. Corrigez le point de départ du boulet. Il doit sortir du bout du canon. La coordonnée de l'extrémité du canon change en fonction de l'angle. Réalisez un dessin pour déterminer la coordonnée de ce point (rappelez-vous des relations sinus, cosinus dans un triangle rectangle). Corrigez votre code pour que le boulet parte du bout du canon.
1. Listez les collisions possibles du boulet avec les différents éléments graphiques. Déduisez-en les conditions pour une partie gagnante ou perdante. Mettre en place cette gestion des collisions (intersection des boites englobantes des différents sprites).
1. Mettre en place la fin de jeu (partie gagnée ou perdue) et comment recommencer le jeu.

Une solution possible (sans doute pas la meilleure) est donnée ci-dessous. Elle peut être améliorée par vos soins une fois que vous vous l'aurez appropriée.

??? "Une correction possible du jeu"
    ```cpp
    #include <SFML/Graphics.hpp>
    #include <cmath> // pour les fonctions math. sin, cos, tan
    #include <iostream> // cout pour debug, pas necessaire apres prog ok
    #include <string>  // pour avoir des chaines de type string et méthode to_string
    #include <random> // pour le tirage au hasard

    int main()
    {
        // Fenetre en 1000x600
        sf::RenderWindow maFenetre(sf::VideoMode(1000, 600), "Jeu de tir");

        // Le sol : texture et sprite
        sf::Texture textureSol;
        if (!textureSol.loadFromFile("SolSFML.png"))
            exit(EXIT_FAILURE);
        textureSol.setRepeated(true);

        sf::RectangleShape spriteSol(sf::Vector2f(maFenetre.getSize().x, textureSol.getSize().y));
        spriteSol.setTextureRect(sf::IntRect(0,0,maFenetre.getSize().x, textureSol.getSize().y));
        spriteSol.setTexture(&textureSol);
        spriteSol.setPosition(0,maFenetre.getSize().y-spriteSol.getLocalBounds().height);
    
        // La base du canon fixe : texture et sprite
        sf::Texture textureBaseCanon;
        if (!textureBaseCanon.loadFromFile("BaseCanonSFML.png"))
            exit(EXIT_FAILURE);
    
        sf::Sprite spriteBaseCanon;
        spriteBaseCanon.setTexture(textureBaseCanon);
        spriteBaseCanon.setOrigin(43,17); // Centre de rotation de la base du canon
        // Base du canon posé sur le sol
        spriteBaseCanon.setPosition(spriteBaseCanon.getOrigin().x, 
            maFenetre.getSize().y-spriteBaseCanon.getLocalBounds().height+spriteBaseCanon.getOrigin().y
            -spriteSol.getSize().y);

        // Le canon mobile : texture et sprite
        sf::Texture textureCanon;
        if (!textureCanon.loadFromFile("CanonSFML.png"))
            exit(EXIT_FAILURE);

        textureCanon.setSmooth(true); // permet l'anticrénelage sur le canon
        sf::Sprite spriteCanon;
        spriteCanon.setTexture(textureCanon);
        spriteCanon.setOrigin(41,34); // Centre de rotation du canon
        // On aligne le centre de rotation du canon avec la base du canon
        spriteCanon.setPosition(spriteBaseCanon.getPosition().x, spriteBaseCanon.getPosition().y);

        // Le boulet : texture et sprite
        sf::Texture textureBoulet;
        if (!textureBoulet.loadFromFile("BouletSFML.png"))
            exit(EXIT_FAILURE);

        sf::Sprite spriteBoulet;
        spriteBoulet.setTexture(textureBoulet);
        spriteBoulet.setOrigin(textureBoulet.getSize().x/2, textureBoulet.getSize().y/2);
        // on déplace le boulet en dehors de la zone visible
        spriteBoulet.setPosition(-spriteBoulet.getLocalBounds().width, 0);


        // Initialisation du générateur de nombres aléatoires
        std::random_device rd;
        // tirage aléatoire de la postion en x de la caisse entre 200 et largeur fenetre-50
        std::uniform_int_distribution<int> posXCaisse(200, maFenetre.getSize().x-50);

        // La caisse qui sert de cible : texture et sprite
        sf::Texture textureCaisse;
        if (!textureCaisse.loadFromFile("CaisseSFML.png"))
            exit(EXIT_FAILURE);

        sf::Sprite spriteCaisse;
        spriteCaisse.setTexture(textureCaisse);
        spriteCaisse.setOrigin(0, textureCaisse.getSize().y);
        // on positionne la caisse cible à une abscisse x tiré au hasard
        spriteCaisse.setPosition(posXCaisse(rd), maFenetre.getSize().y-textureSol.getSize().y);

        // Le fond d'écran, pas obligatoire, peut être une couleur unie
        sf::Texture textureFondEcran;
        if (!textureFondEcran.loadFromFile("FondJeuCanonSFML.png"))
            exit(EXIT_FAILURE);
        
        sf::Sprite spriteFondEcran;
        spriteFondEcran.setTexture(textureFondEcran);

        // La jauge de puissance et son cadre, pas obligatoire
        sf::RectangleShape jaugePuissance(sf::Vector2f(100, 10));
        jaugePuissance.setFillColor(sf::Color::White);
        jaugePuissance.setPosition(80, maFenetre.getSize().y - 20);

        sf::RectangleShape cadreJaugePuissance(sf::Vector2f(304, 14));
        cadreJaugePuissance.setFillColor(sf::Color::Transparent);
        cadreJaugePuissance.setOutlineThickness(2);
        cadreJaugePuissance.setOutlineColor(sf::Color::White);
        cadreJaugePuissance.setPosition(78, maFenetre.getSize().y - 22);

        // les textes graphiques utilisés dans ce programme
        sf::Font maPolice;
        if (!maPolice.loadFromFile("hl.ttf"))
            exit(EXIT_FAILURE);

        // Création d'un objet texte pour afficher l'angle
        sf::Text texteAngleRotationCanon;
        texteAngleRotationCanon.setFont(maPolice);
        texteAngleRotationCanon.setString(L"A: 0°");
        texteAngleRotationCanon.setCharacterSize(10); // exprimée en pixels, pas en points !
        texteAngleRotationCanon.setFillColor(sf::Color::White);
        texteAngleRotationCanon.setPosition(10, maFenetre.getSize().y-40);

        // Création d'un objet texte pour afficher la puissance
        sf::Text textePuissance(texteAngleRotationCanon);
        textePuissance.setString("P: 100");
        textePuissance.setPosition(10, maFenetre.getSize().y-20);

        sf::Text textePerdu;
        textePerdu.setFont(maPolice);
        textePerdu.setString("PERDU !");
        textePerdu.setCharacterSize(60);
        textePerdu.setFillColor(sf::Color::Magenta);
        textePerdu.setOrigin(textePerdu.getLocalBounds().width/2, textePerdu.getLocalBounds().height/2);
        textePerdu.setPosition(maFenetre.getSize().x/2, maFenetre.getSize().y/2);

        sf::Text texteGagnee(textePerdu); // on reprend les caractéristiques de textePerdu
        texteGagnee.setString("GAGNE !");
        texteGagnee.setFillColor(sf::Color::Green);

        // Les variables et constantes utilisées dans ce programme
        // variable angle de rotation du canon (comprise entre 0 et 90°)
        int angleRotationCanon = 0;
        // vitesse initiale en sortie de canon en m/s (compris entre 60 et 400)
        // dans ce programme Vo est remplacé par la variable puissance
        int puissance = 100;

        // position du boulet calculé en x et y
        float posXBoulet = 0.0;
        float posYBoulet = 0.0;
    
        // tir du canon en appuyant sur espace
        bool tirCanon = false;

        // partie gangee ou partie
        bool partiePerdue = false;
        bool partieGagnee = false;

        // constantes du programme
        const float g = 9.81;
        const float coeffConvDegRad = 0.0174533;

        // L'affichage boucle tant que la fenetre est ouverte
        while (maFenetre.isOpen())
        {
            sf::Event evenement;
            while (maFenetre.pollEvent(evenement))
            {
                // fenetre se ferme qd vous cliquez sur la croix
                if (evenement.type == sf::Event::Closed)
                    maFenetre.close();
                // fenetre se ferme qd vous appuyez sur la touche ECHAP
                if (evenement.key.code == sf::Keyboard::Escape)
                    maFenetre.close();
            
                // gestion du canon (angle et puissance) en fonction des touches de directions
                // Fleche Gauche : diminution puissance et mise à jour texte et jauge
                if (evenement.key.code == sf::Keyboard::Left) {
                    if (puissance > 60) {
                        puissance--;
                        jaugePuissance.setSize(sf::Vector2f(puissance, 10));
                        std::string t = "P: " + std::to_string(puissance);
                        textePuissance.setString(sf::String(t));
                    }
                }
                // Fleche Droite : augmentation puissance et mise à jour texte et jauge    
                if (evenement.key.code == sf::Keyboard::Right) {
                    if (puissance < 300) {
                        puissance++;
                        jaugePuissance.setSize(sf::Vector2f(puissance, 10));
                        std::string t = "P: " + std::to_string(puissance);
                        textePuissance.setString(sf::String(t));
                    }
                }
                // Fleche Haut : augmentation angle si inférieur à 90°, mise à jour texte et sprite Canon    
                if (evenement.key.code == sf::Keyboard::Up) {
                    if (angleRotationCanon < 90) {
                        spriteCanon.rotate(-1);
                        angleRotationCanon++;
                        std::wstring t = L"A: " + std::to_wstring(angleRotationCanon) + L"°";
                        texteAngleRotationCanon.setString(sf::String(t));
                    }
                }
                // Fleche Bas : diminition angle si supérieur à 0°, mise à jour texte et sprite Canon
                if (evenement.key.code == sf::Keyboard::Down) {
                    if (angleRotationCanon > 0) {
                        spriteCanon.rotate(1);
                        angleRotationCanon--;
                        std::wstring t = L"A: " + std::to_wstring(angleRotationCanon) + L"°";
                        texteAngleRotationCanon.setString(sf::String(t));
                    }
                }
                // touche Espace : tir du canon ou réinitialisation des variables pour nouveau tir
                // cette touche doit être traitée uniquement une fois -> ici au relachement
                if (evenement.key.code == sf::Keyboard::Space && evenement.type == evenement.KeyReleased) {
                    if (partieGagnee==true || partiePerdue==true) {
                        posXBoulet = 0;
                        partiePerdue = partieGagnee = false;
                        spriteBoulet.setPosition(-spriteBoulet.getLocalBounds().width, 0);
                        spriteCaisse.setPosition(posXCaisse(rd), maFenetre.getSize().y-textureSol.getSize().y);
                    }
                    else if (partieGagnee==false && partiePerdue==false)       
                        tirCanon = true;
                }
            }

            // Calcul des positions des sprites et gestion des collisions si le tir est déclenché
            if (tirCanon) {
                // On augmente la position en X du boulet de 10 pixels
                posXBoulet+=10.0;
                // Calcul de la position en Y du boulet en fonction de la position du canon
                // et de l'équation du mouvement d'un solide
                posYBoulet = (-g/(2*puissance*puissance*cos(angleRotationCanon*coeffConvDegRad)
                            *cos(angleRotationCanon*coeffConvDegRad)))*posXBoulet*posXBoulet
                            + tan(angleRotationCanon*coeffConvDegRad)*posXBoulet;
                posYBoulet = -posYBoulet;
                int lgCanon = spriteCanon.getLocalBounds().width-spriteCanon.getOrigin().x;
                spriteBoulet.setPosition(
                posXBoulet+spriteCanon.getPosition().x+lgCanon*cos(angleRotationCanon*coeffConvDegRad), 
                posYBoulet+spriteCanon.getPosition().y-lgCanon*sin(angleRotationCanon*coeffConvDegRad));
                // test de collision entre le boulet et la caisse -> partie gagnée
                if (spriteBoulet.getGlobalBounds().intersects(spriteCaisse.getGlobalBounds())) {
                    tirCanon = false;
                    partieGagnee = true;
                }
                // test de collision entre le boulet et le sol -> partie perdue
                else if (spriteBoulet.getGlobalBounds().intersects(spriteSol.getGlobalBounds())) {
                    tirCanon = false;
                    partiePerdue = true;
                }
                // test pour savoir si le boulet est sorti de la fenetre en abscisse (x) -> partie perdue
                else if (posXBoulet > maFenetre.getSize().x) {
                    tirCanon = false;
                    partiePerdue = true;
                }
            }

            // Effacement de la fenêtre
            maFenetre.clear(sf::Color::White);

            // dessin des éléments graphiques dans la fenêtre
            maFenetre.draw(spriteFondEcran);
            maFenetre.draw(spriteSol);
            maFenetre.draw(spriteCanon);
            maFenetre.draw(spriteBaseCanon);
            maFenetre.draw(spriteCaisse);
            maFenetre.draw(cadreJaugePuissance);
            maFenetre.draw(jaugePuissance);
            maFenetre.draw(spriteBoulet);
            maFenetre.draw(texteAngleRotationCanon);
            maFenetre.draw(textePuissance);
            if (partiePerdue) maFenetre.draw(textePerdu);
            if (partieGagnee) maFenetre.draw(texteGagnee);

            // Affichage des éléments graphiques dans la fenetre
            maFenetre.display();

            // On ralentie la boucle : ici 50ms pour visualiser la trajectoire du boulet
            sf::sleep(sf::milliseconds(50));
        }

        return 0;
    }
    ```

## Pour aller plus loin...

Si vous désirez apprendre à vous servir de cette bibliothèque pour réaliser des jeux, je vous conseille la chaîne Youtube de [Kofibrek](https://www.youtube.com/@Kofybrek) et son [dépôt GitHub](https://github.com/Kofybrek) pour étudier les codes sources.  
Bonne lecture...
