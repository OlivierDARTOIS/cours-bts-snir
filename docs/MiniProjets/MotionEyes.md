# Réaliser un système de vidéo surveillance avec MotionEyes

!!! tldr "Objectifs de ce document"

    - réemployer des cartes Radxa RockPi 4 avec la distribution DietPi
    - mettre en oeuvre un système de vidéo surveillance utilisant motionEyes
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Les cartes matériels : Radxa Rock série 4

Pendant une assez longue période les cartes raspberryPi étaient introuvables. Nous avons alors testé deux cartes du fabricant [Radxa](https://radxa.com/):

- une rock pi 4 plus : [https://radxa.com/products/rock4/4bp](https://radxa.com/products/rock4/4bp),
- une rock pi 4 SE : [https://radxa.com/products/rock4/4se](https://radxa.com/products/rock4/4se).

De plus nous avons acheté l'extension *m2* ([documentation au format pdf](https://dl.radxa.com/accessories/m2-extend/radxa_m2_extension_board_product_brief_Revision_1.0.pdf)) pour tester des ssd nvme. Cette extension est compatible avec les deux cartes. Nous avons aussi acquis des modules de stockage 32Go eMMC ([https://radxa.com/products/accessories/emmc-module](https://radxa.com/products/accessories/emmc-module)) compatible uniquement avec les rock pi 4 SE. La carte rock pi 4 plus possède une puce eMMC soudée sur la carte mère.

Cela afin de se passer de la carte uSD qui reste un support de stockage relativement *fragile*.

Le problème avec ces cartes est que le support logiciel (le [BSP](https://en.wikipedia.org/wiki/Board_support_package) fourni) ne dure généralement que jusqu'à la prochaine gamme de cartes. Aussi je ne m'oriente pas vers les [téléchargements](https://radxa.com/products/rock4/4bp#downloads) de Radxa pour créer une carte uSD mais je vais me tourner vers la distribution [DietPi](https://dietpi.com/). En particulier le téléchargement de Debian Bookworm (debian 12) pour la carte [rock pi 4 plus](https://dietpi.com/downloads/images/DietPi_ROCKPi4-ARMv8-Bookworm.img.xz) et la carte [rock pi 4 SE](https://dietpi.com/downloads/images/DietPi_ROCK4SE-ARMv8-Bookworm.img.xz).

Après avoir téléchargé les images, vous pouvez les transférer avec l'utilitaire de la fondation raspberrypi : [raspberry pi imager](https://www.raspberrypi.com/software/). Choisissez *Custom* dans le choix de l'OS.

## Copie du SE sur la mémoire embarquée eMMC

Démarrez la carte rock pi avec la uSD créée auparavant. Le login/mdp par défaut est root/dietpi. Pour avoir les étapes détaillées de l'installation d'une distribution DietPi il faut suivre la [documentation](https://dietpi.com/docs/install/). Je vais juste mettre ici quelques points de l'installation de la rock pi 4 SE, la version de DietPi sera la v9.10.0 :

- carte reliée à un réseau ethernet filaire disposant d'un serveur DHCP (typiquement une box internet),
- la carte démarre et se connectera automatiquement au réseau ethernet, si vous avez mis un écran sur la sortie HDMI, vous obtiendrez l'@IP de la carte,
- connectez-vous par ssh sur l'@IP précédente (un serveur ssh est fonctionnel directement apres le premier boot). Rappel: root/dietpi,
- la distribution se mettra alors automatiquement à jour. A la question *Change global software password...*, choisissez **OK** puis tapez un mot de passe. Je ne change pas les mots de passe par défaut de root (machine de test). Désactivez les consoles sur la voie série,
- vous devez arrivé sur le menu principal **DietPi-Software** : changez le serveur SSH (Dropbear par défaut) en OpenSSH Server,
- choisissez ensuite l'item *DietPi-Config* : changez le *langage/options régionales* pour passer en français (fr_FR.UTF-8, Europe/Paris), si besoin changez le serveur NTP avec l'option *Network Options: Misc*. Vous pouvez revenir plus tard configurer d'autres options. Revenez au menu principal,
- appliquez vos modifications en choisissant l'item *Install*. Répondez *Opt OUT and purge uploaded data* si vous ne voulez pas partager vos statistiques d'installation,
- vous arrivez au prompt ! vous avez tous les rappels sur les différents logiciels à utiliser (dietpi-launcher, dietpi-config, dietpi-software).

Lancez *htop* pour obtenir la quantité de ram, le nombre de processeurs (ex: 4G, 6 coeurs). Apres l'installation seulement 103Mo sont utilisés !

Pour transférer le SE vers la mémoire eMMC de la carte vous pouvez lire l'article suivant: [https://wiki.radxa.com/Rockpi4/install/eMMC#Option_2:_With_ROCK_4](https://wiki.radxa.com/Rockpi4/install/eMMC#Option_2:_With_ROCK_4) et en particulier l'option 2. Voici les étapes que j'ai réalisé:

- carte rock pi éteinte, mettre en place la mémoire eMMC puis démarrez la carte avec la carte uSD,
- transfert de l'image dietpi téléchargée à l'étape précédente vers la carte rock pi en cours de fonctionnement, utilisation de winscp sous windows ou d'une commande scp sous linux : par ex. `olivier@Casimir:~/Téléchargements$ scp DietPi_ROCK4SE-ARMv8-Bookworm.img.xz  root@192.168.10.168:`,
- installation de xz-utils sur la carte rockpi (`root@DietPi:~# apt install xz-utils`) puis décompression de l'image (`root@DietPi:~# xz -d DietPi_ROCK4SE-ARMv8-Bookworm.img.xz`),
- choix de la mémoire de stockage (`root@DietPi:~# lsblk`) : vous devez trouvez des disques mmcblk1, mmcblk0. Repérez celui qui représente la carte uSD et celui de la eMMC (par exemple avec les tailles),
- transférez l'image sur la eMMC, dans mon cas sur /dev/mmcblk0 : `root@DietPi:~# dd if=DietPi_ROCK4SE-ARMv8-Bookworm.img of=/dev/mmcblk0 bs=1M`,
- arrêtez le SE (`poweroff`), débranchez électriquement la carte, retirez la carte uSD, redémarrez : le système doit démarrer depuis la mémoire eMMC, vous aurez alors à faire la même configuration qu'au paragraphe précédent.
  
!!! warning "Pas de démarrage depuis la eMMC"
    Il est possible que la carte rockpi 4 SE ne démarre pas depuis la mémoire eMMC. J'ai alors téléchargé l'image *rock-4se_debian_bullseye_kde_b33.img.xz* depuis le site de radxa, flashé cette image sur une carte uSD, démarré depuis cette carte pour transféré cette même image sur la eMMC, retiré la uSD, démarré depuis la eMMC. Apres démarrage de kde, j'ai tout mis à jour et utilisé la commande rsetup. Redémarré à nouveau, puis flashé l'image de la dietPi sur la eMMC. Je ne saurais dire pourquoi cela a fonctionné sur la carte rock pi 4 se.
    Sur la carte rock pi 4 plus, je n'ai pas eu le moindre problème avec la procédure décrite ci-dessus !

Si vous êtes arrivé jusque là, votre carte rockpi démarre sur la mémoire embarquée donc plus besoin de carte uSD !

## Installation de MotionEye

Lancez l'utilitaire `dietpi-software` puis l'item *Browse Software*. Dans la liste cherchez *Camera & Surveillance* et cochez la case *motionEye* et confirmez. Lancez alors l'installation avec l'option *Install*.
La [documentation](https://dietpi.com/docs/software/camera/#motioneye) sur le logiciel motionEye est disponible sur le site de ditPi. Dans cette documentation il est précisé que vous pouvez accéder à l'interface web de gestion de motionEye en vous connectant sur l'adresse IP de votre carte rockpi sur le port 8765. Le login est alors *admin* **sans mot de passe**. Le site de dietPi renvoie aussi vers quelques sites qui présentent le logiciel MotionEye.

## Ajout d'une caméra dans MotionEye

Je dispose d'une caméra USB de marque inconnue. Je la connecte sur un port USB de la carte. Les messages du noyau donnent les indications suivantes:

```console
[ 1945.615271] usb 7-1: New USB device found, idVendor=0458, idProduct=708c, bcdDevice= 4.27
[ 1945.615328] usb 7-1: New USB device strings: Mfr=3, Product=1, SerialNumber=2
[ 1945.615354] usb 7-1: Product: USB_Camera
[ 1945.615374] usb 7-1: Manufacturer: KYE Systems Corp.
[ 1945.615393] usb 7-1: SerialNumber: 200901010001
[ 1945.651421] usb 7-1: Found UVC 1.00 device USB_Camera (0458:708c)
[ 1945.670028] usbcore: registered new interface driver uvcvideo
[ 1953.384240] usbcore: registered new interface driver snd-usb-audio
```

Après une courte recherche sur Internet avec les identifiants idVendor et idProduct, on trouve que c'est une caméra de marque *KYE Systems Corp* et son nom est *Genius WideCam F100*. Rendez-vous sur le site de motionEye et rajoutez cette caméra (comme ce sera la premiere, son répertoire de stockage sera Camera1 et ceci même si vous changez apres coup son nom dans l'interface web).

Au passage dans le menu accessible par l’icône en haut à gauche, dans l'item *General Settings* changez la langue pour le français puis bouton *Apply* qui apparaît en haut de l'interface. L'image sera sans doute très saccadée, c'est normal car la configuration de base dans l'item *Périphérique vidéo* renvoie 2 images par seconde. Pour changer la resolution et la cadence de rafraîchissement, cherchez les items *Résolution vidéo* et *Fréquence d'images*. Le flux réseau sera bien sur plus élevé.

Changez le nom de la caméra : item *Nom de la caméra* dans *Périphérique vidéo*. La suite dépend de ce que vous souhaitez faire avec motionEye : de la détection de mouvement ou de l'enregistrement continu de flux vidéo. Dans mon cas je donne une configuration pour faire de la surveillance continue (attention à l'espace occupé sur le système de stockage). Dans l'item *Vidéos*, je choisis le format vidéo à **H.264** (moins gourmand en processeur, taille raisonnable des vidéos), le *Mode d'enregistrement* à *Enregistrement continu*, la durée maximale du film à *600* secondes (donc 10 minutes) et l'item *Conserver les films* sur *Pendant une semaine*. J'applique mes changements. La vidéo apparaît avec un cadre rouge signifiant que cette caméra enregistre.

Les vidéos sont enregistrées dans `/mnt/dietpi_userdata/motioneye/Camera1/` puis ensuite dans un répertoire au format AAAA-MM-JJ puis ensuite dans des fichiers qui représente 10 minutes de vidéo au format HH-MM-SS.

Dans cette configuration (video en 320x200, 20 images/s, format H.264, qualité du film sur 75%, compression par le CPU), je passe de 1% d'utilisation apres l'installation de base à environ 17%. La taille du fichier vidéo est alors de 263Mo (de nombreux paramètres peuvent faire changer la taille du fichier). Il faudra donc chercher le meilleur compromis entre la resolution de la video, la qualité de compression, le choix de l'algorithme de compression, le taux d'occupation du processeur, le nombre de flux camera à enregistrer...

## Quelques tests avec d'autres caméras

J'ai une pi0 avec une caméra officiel (connecteur CSI). J'ai compilé le logiciel [MJPEG-Streamer](https://github.com/jacksonliam/mjpg-streamer) pour visualiser le flux de cette caméra. Vous pouvez rajouter cette caméra en choisissant comme type *Network Camera* (le type *Simple MJPEG Camera* fonctionne aussi). Il faut juste donner l'url pour se connecter sur le flux vidéo. Dans le cas de mjpeg-streamer ce sera `http://@IP_RPIZero/?action=stream`. Il n'y a pas d’accès avec un login/password sur ce flux.

Nous disposons aussi d'anciennes [caméras IP Pan/Tilt](https://www.velleman.eu/products/view/camera-ip-interieur-ir-pan-tilt-wifi-camip5n1/?id=411776&lang=fr) de marque Velleman. Ces caméras sont certes dépassées maintenant mais elles vont permettre de faire d'autres tests. En particulier les commander à distance (rotation, inclinaison, dels IR). La documentation est disponible dans le lien précédent. Il faudra en particulier télécharger le document sur l'API de cette caméra qui va nous permettre de découvrir tous les *ordres* qu'elle comprend.

Tout d'abord faire un **reset** matériel en appuyant plus de 5s sur le bouton de reset accessible sous la base caméra lorsque celle-ci est alimentée. Après le reset, la caméra est en mode DHCP sur son interface filaire, l'interface wifi est désactivée. Le serveur DHCP de votre réseau (la box par ex.) distribue une @IP à la caméra, vous pouvez alors vous y connecter avec un navigateur web. Le login est *admin* sans mot de passe. La création d'un utilisateur avec mot de passe est alors obligatoire. Le mode d'utilisation conseillé pour MotionEye est alors le mode *Push*. Vous pouvez rajouter cette caméra dans motionEye en choisissant *Network Camera* puis en mettant comme lien URL : `http://@IP_Cam/videostream.cgi?user=olivier&pwd=mot2pass&resolution=32`. Cette ligne vient de la lecture du document sur l'API. Le paramètre *resolution=32* indique que l'on veut une résolution de 640x480. Le flux caméra doit s'afficher dans l'interface de motionEye.

Il serait bien de commander cette caméra en rotation et en inclinaison. Pour cela les ordres à lui envoyer par l'API (encore une fois ce reporter à la documentation) sont du type : `/decoder_control.cgi?command=xx` ou xx est à remplacer par 0 pour aller vers le haut, par 1 pour arrêter le déplacement vers le haut, etc... Vous pouvez tester cela avec une command *curl* : `curl -X GET "http://@IP_Cam/decoder_control.cgi?user=olivier&pwd=mot2pass&command=0"`.

MotionEye propose un certain nombre de *boutons d'actions*. Pour cela il faut lire l'article [https://github.com/motioneye-project/motioneye/wiki/Action-Buttons](https://github.com/motioneye-project/motioneye/wiki/Action-Buttons). Si vous lisez l'article il s'agit de faire un *exécutable* dans le répertoire de configuration de motionEye qui a comme nom *action*_*id_camera*. Par exemple pour la caméra **2** et l'action **up**, le nom du fichier sera **up_2**. Dans notre cas, il y aura 4 scripts bash rendus exécutables avec une commande `chmod +x` et comme utilisateur/groupe `motion/motion`. Par exemple ci-dessous un script pour faire déplacer vers le *haut* pendant 1s:

```console
root@DietPi:/etc/motioneye# ls -l up_2 down_2 left_2 right_2 
-rwxr-xr-x 1 motion motion 294 24 févr. 18:22 down_2
-rwxr-xr-x 1 motion motion 294 24 févr. 18:30 left_2
-rwxr-xr-x 1 motion motion 294 24 févr. 18:30 right_2
-rwxr-xr-x 1 motion motion 294 24 févr. 18:20 up_2

root@DietPi:/etc/motioneye# cat /etc/motioneye/up_2
#! /bin/bash

URL1="http://@IP_Cam/decoder_control.cgi?user=olivier&pwd=mot2pass&command=0"
URL2="http://@IP_Cam/decoder_control.cgi?user=olivier&pwd=mot2pass&command=1"

METHOD="GET"
SLEEP="1"

curl -X $METHOD "$URL1" > /dev/null
sleep $SLEEP
curl -X $METHOD "$URL2" > /dev/null
```
Relancez le démon motionEye : `systemctl restart motionEye`, délogez-vous/relogez-vous sur l'interface web de motion. La caméra pan/tilt concernée doit voir apparaître de nouveaux boutons (flèches de directions) lorsqu'on clique sur celle-ci. Testez ces boutons, la caméra doit se déplacer.

## Conclusion

Une mise en oeuvre rapide de motionEye en utilisant la distribution DietPi sur un matériel Radxa. Ce cours article sera sans doute enrichi au fur et à mesure de mes expériences avec motionEye et peut être aussi un peu avec les cartes Radxa.