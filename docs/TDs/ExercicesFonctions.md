# TD sur les fonctions et les types de passage de paramètres

!!! tldr "Objectifs de l'exercice"

    - lire et analyser du code sans le tester dans un compilateur,
    - repérer les erreurs courantes avec les fonctions en C++,
    - identifier les différents types de passage de paramètres,
    - vérifier ces réponses avec un compilateur en ligne.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

Dans toutes les questions qui suivent, vous préciserez :

- si le code *compile* alors vous donnerez la sortie console,
- si le code *ne compile pas*, la (ou les) erreur(s) qui empêche(nt) la compilation.

Vous vérifierez vos réponses avec un compilateur en ligne : [cpp.sh](https://cpp.sh).

!!! tldr "Enoncé exercice 1"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourne7()
        {
            return 7;
        }

        int retourne9()
        {
            return 9;
        }

        int main()
        {
            std::cout << retourne7() + retourne9() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...


!!! tldr "Enoncé exercice 2"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourne7()
        {
            return 7;
            int retourne9()
            {
                return 9;
            }
        }

        int main()
        {
            std::cout << retourne7() + retourne9() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 3"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourne7()
        {
            return 7;
        }

        int retourne9()
        {
            return 9;
        }

        int main()
        {
            retourne7();
            retourne9();
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 4"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourneDesNombres()
        {
            return 5;
            return 7;
        }

        int main()
        {
            std::cout << retourneDesNombres() << std::endl;
            std::cout << retourneDesNombres() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 5"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourne 5()
        {
            return 5;
        }

        int main()
        {
            std::cout << retourne 5() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 6"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int retourneCinq()
        {
            return 5;
        }

        int main()
        {
            std::cout << retourneCinq << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 7"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void disBonjour()
        {
            std::cout << "Bonjour" << std::endl;
        }

        int main()
        {
            disBonjour();
            std::cout << disBonjour() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 8"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void disBonjour()
        {
            std::cout << "Dans la fonction disBonjour()" << std::endl;
            return "disBonjour";
        }

        int main()
        {
            disBonjour();
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 9"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void affiche()
        {
            std::cout << "A" << std::endl;
            return;
            std::cout << "B" << std::endl;
        }

        int main()
        {
            affiche();
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 10"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int affiche()
        {
            std::cout << "A" << std::endl;
            return 5;
            std::cout << "B" << std::endl;
        }

        int main()
        {
            std::cout << affiche() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 11"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void afficheA()
        {
            std::cout << "A" << std::endl;
        }

        int main()
        {
            std::cout << afficheA() << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 12"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int addition(int x, int y)
        {
            return (x + y);
        }

        int multiplication(int v, int w)
        {
            return (v * w);
        }

        int main()
        {
            std::cout << addition(4, 5) << std::endl;
            std::cout << addition(1+2, 3*4) << std::endl;

            int a {5};
            std::cout << addition(a, 5) << std::endl;
            std::cout << addition(1, multiplication(a, 3)) << std::endl;
            std::cout << addition( addition(4, -1), -6) << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 13"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void multiplication(int v, int w)
        {
            return (v * w);
        }

        int main()
        {
            int a (5);
            std::cout << multiplication(a, 5) << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 14"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void plus1(int x)
        {
            ++x;
        }

        int main()
        {
            int y {5};
            std::cout << "valeur de y: " << y << std::endl;
            plus1(y);
            std::cout << "valeur de y: " << y << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 15"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        void plus1(int& x)
        {
            ++x;
        }

        int main()
        {
            int y {5};
            std::cout << "valeur de y: " << y << std::endl;
            plus1(y);
            std::cout << "valeur de y: " << y << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 16"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {
            int y {5};
            std::cout << y << std::endl;
            std::cout << &y << std::endl;
            std::cout << *( &y ) << y << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 17"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {
            int y {5};
            int* ptr {&y};
            std::cout << *ptr << std::endl;
            
            int x {-1};
            ptr = &x;
            std::cout << *ptr << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 18"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {
            int y {5};
            int* ptr {&y};
            std::cout << *ptr << std::endl;
            
            *ptr = 6;
            std::cout << *ptr << std::endl;
            std::cout << y << std::endl;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 19"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {
            int y {5};
            int& ref {y};
            int* ptr {&y};
            
            std::cout << y << std::endl;
            std::cout << *ptr << std::endl;
            std::cout << ref << std::endl;

            ref = 6;
            std::cout << y << std::endl;
            std::cout << *ptr << std::endl;
            std::cout << ref << std::endl;

            *ptr = 7;
            std::cout << y << std::endl;
            std::cout << *ptr << std::endl;
            std::cout << ref << std::endl;

            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...


!!! tldr "Enoncé exercice 20"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {
            short value{ 7 }; // &value = 0012FF60
            short otherValue{ 3 }; // &otherValue = 0012FF54

            short* ptr{ &value };

            std::cout << &value << '\n';
            std::cout << value << '\n';
            std::cout << ptr << '\n';
            std::cout << *ptr << '\n';
            std::cout << '\n';

            *ptr = 9;

            std::cout << &value << '\n';
            std::cout << value << '\n';
            std::cout << ptr << '\n';
            std::cout << *ptr << '\n';
            std::cout << '\n';

            ptr = &otherValue;

            std::cout << &otherValue << '\n';
            std::cout << otherValue << '\n';
            std::cout << ptr << '\n';
            std::cout << *ptr << '\n';
            std::cout << '\n';

            std::cout << sizeof(ptr) << '\n';
            std::cout << sizeof(*ptr) << '\n';

            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...

!!! tldr "Enoncé exercice 21"
    === "Code"
        ```cpp linenums="1"
        #include <iostream>

        int main()
        {   
            int x{ 5 };
            int y{ 6 };

            int& ref{ x };
            ++ref;
            ref = y;
            ++ref;

            std::cout << x << ' ' << y;
            return 0;
        }
        ```

    === "Votre réponse"
        Le code compile : OUI / NON  
        si OUI, donnez la sortie console. Si NON, donnez l'erreur de compilation :  
        ...
