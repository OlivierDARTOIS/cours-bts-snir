# Exercice sur la mise en oeuvre de l'héritage avec deux classes simples

!!! tldr "Objectifs de ce document"

    - comprendre la notion d'héritage,
    - utiliser le vocabulaire lié à la notion d'héritage en C++ : classe mère, classe dérivée, redéfinition, polymorphisme, méthodes virtuelles et virtuelles pures, classe abstraite,
    - coder en C++ le fichier de déclaration, de définition et un programme principal de tests mettant en oeuvre l'héritage.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Diagramme de classe des classes **Humain** et **Barman**

Le diagramme de classe ci-dessous est incomplet voire faux... On se propose de le corriger en répondant aux questions qui sont ci-dessous.
![Diagramme de classes *Humain Barman*](../img/DiagClasseExHumainBarman.png)

## Questions associées au diagramme de classe

??? note "Q1. Précisez la relation entre les deux classes"
    La classe `Barman` **hérite** de la classe `Humain`. Vous pouvez aussi dire que la classe `Barman` est une **spécialisation** de la classe `Humain`. La flèche qui relie ces deux classes est la flèche qui désigne l'héritage, elle part toujours des **classes dérivées** pour pointer vers la **classe mère**.

??? note "Q2. Qu’indique le caractère **#** dans la classe `Humain`, qu’est ce que cela implique pour la classe `Barman` ?"
    Le caractère **#** devant un attribut indique que cet attribut est *protégé* (`protected`). Cela signifie que cet attribut est considéré comme *privé* (`private`) de l'extérieur de la classe et comme *public* (`public`) pour les classes dérivées. La classe `Barman` peut donc accéder directement à tous les attributs déclarés protégés de la classe `Humain`. Pour y accéder, vous utiliserez l'opérateur de résolution de portée `::`. Par exemple : `Humain::nom`.

??? Note "Q3. Précisez les types des variables `nom` et `boissonFavorite` de la classe `Humain`."
    Rien n'est précisé sur le diagramme UML pour les types de ces deux attributs. C'est donc à vous de les choisir. Vous pouvez prendre :
    
    - attribut `nom` : chaîne de caractères -> `std::string`,
    - attribut `boissonFavorite` : chaîne de caractères -> `std::string`.

??? Note "Q4. Donnez le nom des **accesseurs** dans les deux classes."
    Pour la classe `Humain`, vous avez deux **accesseurs** (en anglais **getter**) :

    - la méthode `getNom()`,
    - la méthode `getBoissonFavorite()`.
    
    Pour la classe `Barman`, vous avez un **accesseur** :
    
    - la méthode `getNomBar()`.

??? Note "Q5. Donnez le nom des **mutateurs** dans les deux classes."
    Pour la classe `Humain`, vous avez un **mutateur** (en anglais **setter**) :

    - la méthode `setBoissonFavorite()`.
    
    Pour la classe `Barman`, il n'y a aaucun **mutateur**.

??? Note "Q6. Corrigez les **getters** et **setters** de la classe `Humain`."
    Un **accesseur** (getter) renvoie **systématiquement** une valeur. De plus, il ne doit modifier en aucun cas un des attributs de la classe, pour cette raison vous rajouterez systématiquement le mot clé `const`. Cela permet au compilateur de faire plus de vérification pour détecter des *bugs*.  
    Un **mutateur** (setter) positionne un attribut à la valeur qui lui est passé en argument. Un mutateur permet de vérifier la cohérence de la valeur que vous passez par rapport à une gamme de valeur *normale*. Un mutateur ne renvoie **rien**.

    Correction des accesseurs de la classe `Humain`:

    - `std::string getNom() const`,
    - `std::string getBoissonFavorite() const`.
    
    Correction du mutateur de la classe `Humain` :

    - `void setBoissonFavorite(std::string boisson)`.

??? Note "Q7. Corrigez les méthodes `parle()`, `sePresente()` et `boit()` de la classe `Humain` dans le diagramme UML car elles renvoient toutes `void`."
    Les prototypes des fonctions ci-dessus sont les suivants :

    - `void parle()`,
    - `void sePresente()`,
    - `void boit()`.

??? Note "Q8. Donnez le nom des **constructeurs** dans les deux classes ? Y-a-t-il un **constructeur par défaut** dans les deux classes ?"
    Dans la classe `Humain`, le **constructeur** est `Humain()` sans paramètre. Ce qui peut sembler bizarre car la plupart du temps le constructeur initialise les attributs. Vous verrez plus tard comment contourner le problème.  
    Dans la classe `Barman`, le **constructeur** est `Barman(const std::string nom = "", const std::string boissonFavorite = "biere", const std::string nomBar = "")`. Puisque la classe `Barman` hérite de la classe `Humain`, il est normal que ce constructeur reçoive les attributs de la classe mère en plus des siens.  
    Un **constructeur par défaut** est un constructeur qui ne prend pas d'argument. Dans notre cas le constructeur de la classe `Humain` est un constructeur par défaut.

??? Note "Q9. Qu’indique le caractère ‘~’ devant `Humain()` dans la classe `Humain` ?"
    Il s'agit du caractère **tilde** qui est mis devant le **destructeur**. Rappel: un destructeur est appelé lorsqu'un objet est supprimé de la mémoire, il s'agit de la dernière méthode appelée.

??? Note "Q10. Vous remarquez la même méthode `sePresente()` dans les deux classes. Comment nomme-t-on cette technique ?"
    Si une méthode est disponible dans la classe mère **et** dans les classes dérivées, il s'agit alors d'une **redéfinition** de la méthode (et pas d'une *surcharge*).

??? Note "Q11. Pourquoi l’attribut `nomBar` de la classe `Barman` est en *privé* et pas en *protégé* ?"
    Une explication possible peut être le fait que la classe `Barman` n'est pas destinée à être dérivée. Dans ce cas les attributs doivent être mis en **privé**. Dans tous les cas il s'agit d'un choix du développeur.

??? Note "Q12. La méthode `terminePhrase()` de la classe `Barman` est en privé, peut-elle accéder à l’attribut `nomBar` ?"
    Une méthode quelconque d'une classe, qu'elle soit publique, protégée ou privée, peut accéder à n'importe quel attribut privé. Il faut bien sur que la méthode et l'attribut appartiennent à la même classe. Vous pouvez considérer qu'un attribut est une **variable globale pour la classe**.

??? Note "Q13. Il y a un problème par rapport au **polymorphisme** dans ce diagramme. Que faut-il rajouter pour corriger l’erreur ?"
    Comme vu à la question 10, il y a une **redéfinition** de la méthode `sePresente()`. Comme vu en cours, si on utilise un pointeur de la classe `Humain` avec un objet de la classe `Barman`, il faut que l'appel de la méthode `sePresente()` soit celle de la classe `Barman` (*résolution dynamique des liens à l'exécution*). Le seul moyen pour le compilateur de gérer cette situation est de rajouter le mot clé `virtual` devant les méthodes redéfinies. Pour mémoire si une méthode est déclarée virtuelle dans la classe mère, les redéfinitions dans les classes filles sont automatiquement virtuelles. Cependant on rajoute souvent le qualificatif `virtual` dans les classes dérivées.  
    En conséquence l'écriture de la méthode `sePresente()` devient `virtual std::string sePresente()` dans la classe mère et les classe dérivées.

??? Note "Q14. La classe `Humain` est-elle abstraite ?"
    Une classe est **abstraite** si et seulement si au moins une de ces méthodes est **virtuelle pure**. Ceci est indiqué sur le diagramme UML ou dans le fichier d'en-tête de la classe en rajoutant `=0` derrière le nom de la méthode. Vous pouvez aussi trouver le mot clé **abstract** sous le nom de la classe dans le diagramme UML.  
    Dans notre exemple il n'y a pas de méthode virtuelle pure dans la classe `Humain` donc ce **n'est pas** une classe abstraite, vous pourrez donc instancier des objets de cette classe.

Pour les questions suivantes vous pouvez cloner le projet de cet exercice pour Microsoft Visual Studio sur [Gitlab](https://gitlab.com/OlivierDARTOIS/classeshumainbarman) ou récupérer les différents fichiers mis à votre disposition.

??? Note "Q15. Donnez le fichier de déclaration de la classe `Humain`"
    ```cpp linenums="1"
    #pragma once
    #include <string>
    
    class Humain
    {
        protected:
            std::string nom;
            std::string boissonFavorite;
    
        public:
            Humain();
            virtual ~Humain();
            std::string getNom() const;
            std::string getBoissonFavorite() const;
            void setBoissonFavorite(std::string boisson);
            void parle();
            virtual void sePresente();
            void boit();
    };
    ```

??? Note "Q16. Donnez le fichier de déclaration de la classe `Barman`"
    ```cpp linenums="1"
    #pragma once
    #include "Humain.h"

    class Barman:public Humain
    {
        private:
            std::string nomBar;
    
        public:
            Barman(const std::string nom = "", const std::string boissonFavorite = "biere", const std::string nomBar = ""):Humain();
            std::string getNomBar() const;
            void parle(const std::string texte);
            virtual void sePresente();
            void sert(const Humain & client);
    
        private:
            std::string terminePhrase();
    };
    ```

??? Note "Q17. Codez les **constructeurs** des deux classes. Codez les méthodes `getBoissonFavorite()`, `setBoissonFavorite()`, `getNom()` et `sePresente()` de la classe `Humain`. Puis codez les méthodes `getNomBar()` et `sePresente()` de la classe `Barman`."
    ```cpp linenums="1"
    // Fichier de déclaration de la classe Humain
    #include <iostream>
    #include <string>

    #include "humain.h"

    Humain::Humain()
    {
	    std::cout << "Dans constructeur Humain..." << std::endl;
	    this->nom = "";
	    this->boissonFavorite = "";
    }

    Humain::~Humain()
    {
	    std::cout << "Dans destructeur Humain..." << std::endl;
    }

    std::string Humain::getNom() const
    {
	    return this->nom;
    }

    std::string Humain::getBoissonFavorite() const
    {
	    return this->boissonFavorite;
    }

    void Humain::setBoissonFavorite(std::string boisson)
    {
	    this->boissonFavorite = boisson;
    }

    void Humain::parle()
    { }

    std::string Humain::sePresente()
    {
	    return "Methode sePresente classe Humain";
    }

    void Humain::boit()
    { }
    ```

    ```cpp linenums="1"
    // Fichier de déclaration de la classe Barman
    #include <iostream>
    #include <string>

    #include "barman.h"

    Barman::Barman(const string nom, const string boissonFavorite, const string nomBar) : Humain()
    {
	    std::cout << "Dans constructeur Barman..." << std::endl;
	    Humain::nom = nom;
	    Humain::setBoissonFavorite(boissonFavorite);
	    this->nomBar = nomBar;
    }

    Barman::~Barman()
    {
	    std::cout << "Dans destructeur Barman..." << std::endl;
    }

    std::string Barman::getNomBar() const
    {
	    return this->nomBar;
    }

    void Barman::parle(const std::string texte)
    { }

    std::string Barman::sePresente()
    {
	    return "Methode sePresente classe Barman";  
    }

    void Barman::sert(const Humain& client)
    { }
    ```

??? Note "Q18. Dans un programme de test, instanciez un `humain` `h1`, instanciez un `barman` `b1` avec comme paramètres : `nom` = Arthur, `boissonFavorite` = Whisky, `nomBar` = chezMoi. Appelez la méthode `getNom()` sur `h1`. Appelez la méthode `getNomBar()` sur `b1`. Commentez le résultat."
    ```cpp linenums="1"
    #include <iostream>
    #include "Barman.h"

    int main()
    {
	    Humain h1;
	    Barman b1("Arthur", "Whisky", "chezMoi");
        std::cout << h1.getNom() << std::endl;
	    std::cout << b1.getNomBar() << std::endl;
	    
	    return 0;
    }
    ```
    Vous pouvez instancier un objet de la classe `Humain` car elle n'est par abstraite. Par contre le constructeur de la classe `Humain` ne prend aucun argument donc les attributs `nom` et `boissonFavorite` seront initialisés avec une chaîne vide.  
    Pour instancier un `Barman` il faut passer 3 arguments au constructeur de cette classe. Vous remarquez que le constructeur de la classe `Humain` a été appelé avant le constructeur de la classe `Barman`. C'est normal car la classe `Barman` hérite de la classe `Humain`.  
    La méthode `getNom()` est appelé sur l'objet `h1`. L'appel à la méthode `getNom()` va vous renvoyer une chaîne vide. C'est pour cette raison que vous avez une ligne vide lors de l'exécution.  
    Vous appelez enfin la méthode `getNomBar()` sur l'objet b1. Si vous avez codé correctement cette méthode, elle renvoie le nom du bar.

??? Note "Q19. Comment affiche-t-on le nom du barman b1 ? Commentez votre solution."
    ```cpp linenums="1"
    ...
	   std::cout << b1.getNom() << std::endl;
    ...
    ```
    Il faut se rappeler la définition de l'héritage. Ici `Barman` hérite de `Humain` donc un *Barman* est un *Humain*. Du coup toutes les méthodes publiques ou protégées de la classe mère sont accessibles directement par les classes dérivées. Donc vous avez le droit de faire un appel du type `b1.getNom()`.

??? Note "Q20. Comment change-t-on la boisson favorite du barman `b1` en *Vodka* ?"
    ```cpp linenums="1"
    ...
	    std::cout << b1.getBoissonFavorite() << std::endl;
        b1.setBoissonFavorite("Vodka");
        std::cout << b1.getBoissonFavorite() << std::endl;
    ...
    ```

??? Note "Q21. Comment change-t-on le nom du barman `b1` en *Gonzagues* ?"
    Vous ne pouvez accéder à l'attribut `nom` de la classe `Humain` car il est déclaré `protected` ce qui, si vous êtes à l'extérieur de la classe, ce comporte comme un attribut privé. Et même si l'objet qui essaye d'y accéder est un héritier de la classe mère. Il faudrait une méthode `setNom()` dans la classe mère. Vous ne pouvez pas changer le nom d'un *barman* ou d'un *humain*.

??? Note "Q22. Instanciez un pointeur sur un humain `ph` et initialisez-le avec le barman `b1`. Vérifier que le polymorphisme fonctionne en appelant la méthode `sePresente()` avec le pointeur `ph`."
    ```cpp linenums="1"
    ...
	    Humain* ph = &b1;
        ph->sePresente();
    ...
    ```
    Le pointeur `ph` pointe vers un objet de type `Barman`. Si le polymoprhisme a été correctement implémenté (mot clé `virtual` sur les fonctions redéfinies), alors la méthode `sePresente()` appelée sera celle de la classe `Barman`.  
    Rappel: *un pointeur doit se comporter comme ce qu'il pointe pas comme ce qu'il est*.

??? Note "Q23. Comment s’appelle le **type de passage de paramètres** de la méthode `sert()` de la classe `Barman` ?"
    Le protype de la méthode est : `void sert(const Humain& client)`. Vous remarquez que derrière le type `Humain` il y a le caractère `&`. Cela signifie que vous avez un passage de paramètre à la méthode par **référence**. C'est à dire que vous passez l'objet en lui-même, il n'y a pas de copie de l'objet de départ. De plus pour être certain que la méthode `sert()` ne modifiera pas l'objet que vous lui passez, cet objet est déclaré constant (mot clé `const`).