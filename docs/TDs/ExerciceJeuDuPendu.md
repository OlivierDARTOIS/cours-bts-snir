# Réalisation du jeu du pendu

!!! tldr "Objectifs de ce document"

    - analyser un cahier des charges,
    - mettre en oeuvre la lecture d'un fichier texte,
    - tirer au hasard un mot parmi n,
    - gestion de la saisie utilisateur et de la logique de jeu.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Cahier des charges

Dans ce jeu, l'ordinateur commence par choisir un mot de manière aléatoire dans une liste de mot. Cette liste de mot est stockée dans un fichier texte (un mot par ligne, ce fichier est fourni, son nom est [listeMotsNonAccentues.txt](listeMotsNonAccentues.txt) ). Ensuite, une suite de tirets '-' est affichée, un tiret par lettre et l'ordinateur invite le joueur à deviner un caractère de ce mot.

Si le joueur a fait un bon choix, toutes les occurrences de cette lettre dans le mot sont affichées, avec toutes les lettres déjà trouvées. Dans le cas contraire, un message indique au joueur son mauvais choix. Le jeu se termine quand le joueur a trouvé le mot choisi par l'ordinateur ou quand toutes les tentatives ont échoué : le joueur est alors pendu. Le nombre de tentatives est égal au nombre de lettres contenues dans le mot.

Vous vous reporterez aux *snippets* disponible sur ce site pour vous aidez à coder cet exercice.

!!! note "Exemple de trace d'exécution"
    === "Jeu du pendu : réussite"
        ```console
        C:\Users\Olivier\Documents\Visual Studio 2015\Projects\JeuDuPendu\Debug>JeuDuPendu.exe
        Nb mots : 1000
        Nb mots : 2000
        Nb mots : 3000
        Nb mots : 4000
        Nb mots : 5000
        Nb mots : 6000
        Nb mots : 7000
        Nb mots : 8000
        Nb mots : 9000
        Nb mots : 10000
        Nb mots : 11000
        Nb mots : 12000
        Nb mots : 13000
        Nb mots : 14000
        Nb mots : 15000
        Nombre de mots dans le fichier : 15424

        Mot a trouver : --------
        Il vous reste 8 essai(s).
        Tapez une lettre : e

        Mot a trouver : -e-----e
        Il vous reste 7 essai(s).
        Tapez une lettre : s

        Mot a trouver : -e---sse
        Il vous reste 6 essai(s).
        Tapez une lettre : a

        Mot a trouver : -e--asse
        Il vous reste 5 essai(s).
        Tapez une lettre : c

        Mot a trouver : -e--asse
        Il vous reste 4 essai(s).
        Tapez une lettre : t

        Mot a trouver : te--asse
        Il vous reste 3 essai(s).
        Tapez une lettre : r

        Bravo, vous avez trouve le mot terrasse en 6 essais.
        ```

    === "Jeu du pendu : échec"
        ```console
        C:\Users\Olivier\Documents\Visual Studio 2015\Projects\JeuDuPendu\Debug>JeuDuPendu.exe
        Nb mots : 1000
        Nb mots : 2000
        Nb mots : 3000
        Nb mots : 4000
        Nb mots : 5000
        Nb mots : 6000
        Nb mots : 7000
        Nb mots : 8000
        Nb mots : 9000
        Nb mots : 10000
        Nb mots : 11000
        Nb mots : 12000
        Nb mots : 13000
        Nb mots : 14000
        Nb mots : 15000
        Nombre de mots dans le fichier : 15424

        Mot a trouver : -------
        Il vous reste 7 essai(s).
        Tapez une lettre : e

        Mot a trouver : ---e-e-
        Il vous reste 6 essai(s).
        Tapez une lettre : s

        Mot a trouver : ---e-e-
        Il vous reste 5 essai(s).
        Tapez une lettre : t

        Mot a trouver : ---ete-
        Il vous reste 4 essai(s).
        Tapez une lettre : a

        Mot a trouver : -a-ete-
        Il vous reste 3 essai(s).
        Tapez une lettre : r

        Mot a trouver : -a-eter
        Il vous reste 2 essai(s).
        Tapez une lettre : p

        Mot a trouver : -a-eter
        Il vous reste 1 essai(s).
        Tapez une lettre : f

        Vous avez perdu, le mot etait cafeter..
        ```

## Questions supplémentaires

1. Afficher les lettres déjà proposées à l'utilisateur,
2. Générer le fichier **listeMotsNonAccentues.txt** à partir du fichier [listeMots.txt](listeMots.txt) qui lui contient des caractères accentués,
3. Utiliser le fichier **listeMots.txt** pour le jeu : c'est beaucoup plus dur !
