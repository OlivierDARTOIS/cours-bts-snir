# Réalisation de classes simples à partir d'un cahier des charges

!!! tldr "Objectifs de ce document"

    - analyser un cahier des charges,
    - identifier les attributs et les méthodes de la classe,
    - dessiner le diagramme de classe UML,
    - coder en C++ le fichier de déclaration, de définition et un programme principal de tests.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Exercice 1 - Cahier des charges : classe **Chien**

Cet exercice est inspiré d'un exercice donné en spécialité NSI en terminale en python.

On souhaite dans cet exercice créer une classe **Chien** ayant deux attributs :

- un nom,
- une masse exprimée en Kg.

Cette classe possède aussi différentes méthodes décrites ci-dessous (*chien* est une instance de la classe Chien) :

- un constructeur qui initialise les attributs ;
- chien.getNom() qui renvoie la valeur de l'attribut nom ;
- chien.getMasse() qui renvoie la valeur de l'attribut masse ;
- chien.machouille(jouet) qui renvoie son argument, la chaîne de caractères jouet, privé de son dernier caractère ;
- chien.aboie(nbFois) qui renvoie la chaîne 'Ouaf' * nbFois, où nbFois est un entier non signé passé en argument ;
- chien.mange(ration) qui modifie l'attribut masse en lui ajoutant la valeur de l'argument ration exprimé en Kg (de type float).

On ajoute les contraintes suivantes concernant la méthode *mange* :

- on vérifiera que la valeur de ration est comprise entre 0 (exclu) et un dixième de la masse du chien (inclus),
- la méthode renverra *True* si ration satisfait ces conditions et que l'attribut masse est bien modifié, *False* dans le cas contraire.

Proposer un diagramme de classe UML de cette classe.  
Proposer un fichier de déclaration (`chien.h`) à partir du schéma UML.  
Proposer un fichier de définition (`chien.cpp`) de cette classe.  
Proposer un *main* qui teste la classe précédente (toutes les méthodes).

??? Example "Correction du diagramme de classe UML"
    ```dot
        digraph Couriers {
        
        rankdir = BT

        node [
            fontname="Helvetica,Arial,sans-serif"
            shape=record
            style=filled
            fillcolor=gray85
        ]

        edge [
            arrowhead = "empty"
        ]

        Personne [
            fontcolor="black"
            label = "{Chien|- nom : string\l- masse : float\l|+ Chien(nom : string, masse : float)\l+ getNom() : string const\l
            + getMasse() : float const\l+ machouille(jouet : string) : string\l+ aboie(nbFois : unsigned int) : string\l+ mange(ration : float) : bool\l}"
            fillcolor=azure3
        ]
        }
        ```

??? Example "Correction du fichier de définition de la classe `Chien`"
    ```cpp linenums="1"
    // Exercice TD classe simple *Chien*
    // Fichier de définition de la classe
    // O. DARTOIS, Nov. 2023

    #ifndef _CHIEN_H_
    #define _CHIEN_H_

    #include <string>

    class Chien
    {
    private:
        std::string nom;
        float masse;

    public:
        Chien(std::string nom, float masse);
        std::string getNom() const;
        float getMasse() const;
        std::string machouille(std::string jouet);
        std::string aboie(unsigned int nbFois);
        bool mange(float ration);
    };

    #endif
    ```

??? Example "Correction du fichier de déclaration de la classe `Chien`"
    ```cpp linenums="1"
    // Exercice TD classe simple *Chien*
    // Fichier de déclaration de la classe
    // O. DARTOIS, Nov. 2023

    #include "chien.h"

    Chien::Chien(std::string nom, float masse)
    {
        this->nom = nom;
        this->masse = masse;
    }

    std::string Chien::getNom() const
    {
        return this->nom;
    }

    float Chien::getMasse() const
    {
        return this->masse;
    }

    std::string Chien::machouille(std::string jouet)
    {
        if (jouet.size() == 1)
            return std::string("Plus rien a machouiller");
        else 
            return jouet.substr(0, jouet.size()-1);
    }

    std::string Chien::aboie(unsigned int nbFois)
    {
        if (nbFois == 0)
            return std::string("...");

        std::string chaineAboie;
        for (unsigned int i=0; i<nbFois; i++)
            chaineAboie += "Ouaf... ";

        return chaineAboie;
    }

    bool Chien::mange(float ration)
    {
        if ((ration > 0) && (ration <= this->masse/10.0)) {
            this->masse += ration;
            return true;
        }
        else
            return false;
    }
    ```

??? Example "Programme de test de la classe `Chien`"
    ```cpp linenums="1"
    // Exercice TD classe simple *Chien*
    // Fichier de test de la classe
    // O. DARTOIS, Nov. 2023

    #include <iostream>
    #include "chien.h"

    int main()
    {
        // on instancie un chien *monChien* avec comme nom medor et comme masse 10Kg
        Chien monChien("Medor", 10);

        // on teste les accesseurs (getters)
        std::cout << "nom du chien : " << monChien.getNom() << std::endl;
        std::cout << "masse du chien : " << monChien.getMasse() << std::endl;

        // on fait aboyer le chien 4 fois puis 0 fois
        std::cout << "le chien aboie (4 fois) : " << monChien.aboie(4) << std::endl;
        std::cout << "le chien aboie (0 fois) : " << monChien.aboie(0) << std::endl;
    
        // on fait machouiller un bidule au chien
        std::cout << "le chien machouille un bidule : " << monChien.machouille("bidule") << std::endl;
        // on fait machouiller un os deux fois à un chien
        std::string osAMachouiller("os");
        osAMachouiller = monChien.machouille(osAMachouiller);
        std::cout << "le chien machouille un os (premiere fois) : " << osAMachouiller << std::endl;
        osAMachouiller = monChien.machouille(osAMachouiller);
        std::cout << "le chien machouille un os (deuxieme fois) : " << osAMachouiller << std::endl;

        // on donne à manger au chien : ration correcte par rapport à sa masse (1/10 eme)
        if (monChien.mange(0.5))
            std::cout << "le chien a mange !\nsa masse est maintenant de " << monChien.getMasse() << std::endl;

        // on donne maintenant une ration trop importante
        if (monChien.mange(5.5))
            std::cout << "Le chien a explose !!!" << std::endl;
        else
            std::cout << "Le chien refuse sa gamelle, il tient a sa ligne..." << std::endl;
    }
    ```
    Trace d'exécution :
    ```console
    olivier@Casimir:~/Dev/cpp/ClasseC++_Chien$ ./testClasseChien 
    nom du chien : Medor
    masse du chien : 10
    le chien aboie (4 fois) : Ouaf... Ouaf... Ouaf... Ouaf... 
    le chien aboie (0 fois) : ...
    le chien machouille un bidule : bidul
    le chien machouille un os (premiere fois) : o
    le chien machouille un os (deuxieme fois) : Plus rien a machouiller
    le chien a mange !
    sa masse est maintenant de 10.5
    Le chien refuse sa gamelle, il tient a sa ligne...
    ```

## Exercice 2 - Cahier des charges : classe **Pingouin**

Il s'agit de réaliser une classe **Pingouin** qui aura les caractéristiques suivantes :

Un pingouin se caractérise par sa **masse de gras** et son **nombre de plumes**. Il a un **nom**, il **marche**, il **mange** et il peut **crier** de temps en temps. S’il n’a pas assez de plumes et de gras il peut attraper froid donc être **malade**.

- Analyser la classe Pingouin : dessinez le diagramme de classe UML,
- La classe Pingouin permet d’instancier un pingouin de quatre façons différentes :
    - par défaut avec *GNULinux* comme nom, une masse et un nombre de plumes moyens (40 Kg et 30000)
    - en précisant le nom, les autres attributs ont les valeurs par défaut,
    - en précisant le nom et la masse de gras, le dernier attribut aura sa valeur par défaut,
    - en précisant le nom, la masse de gras et le nombre de plumes.
    - *Remarque* : vous pouvez écrire un seul constructeur avec des paramètres par défaut (faites une recherche Internet ou demandez à votre professeur)
- Pour les actions marcher et crier, vous retournerez une chaîne qui indique l’action demandée, par exemple, `return "Je boude";`.
- Si la masse de gras du pingouin est inférieure à 30Kg **ET** que le nombre de plumes est inférieur à sa masse * 1000, il peut attraper froid donc devenir malade !
- L’action de manger augmente la masse de gras et le nombre de plumes. La classe propose deux solutions :
    - une augmentation par défaut : par exemple 1Kg et 1500 plumes,
    - une augmentation qui précise la masse de gras supplémentaire et le nombre de plumes en plus.
- La classe propose une fonction d’affichage des valeurs du pingouin. Par exemple : *Je suis Marcel, 50Kg, 45000 plumes, en pleine forme* ou *Je suis Maurice, 20Kg, 10000 plumes, fin imminente*. Vous comprenez donc que si le pingouin est malade vous affichez *fin imminente* sinon *en pleine forme*.

Proposer un fichier de déclaration (`pingouin.h`) à partir du schéma UML.  
Proposer un fichier de définition (`pingouin.cpp`) de cette classe.  

Pour tester cette classe, proposer un *main* :

- qui déclare trois *pingouins* :
    - un s’appelle *Tux*,
    - un autre s’appelle *Maigrichon*, une masse de 25 Kg et 15000 plumes,
    - un autre à 5000 plumes si c’est possible,
- afficher les valeurs de *Tux* et *Maigrichon*,
- *Tux* marche,
- *Tux* mange,
- *Tux* crie,
- si *Maigrichon* peut attraper froid, il doit manger pour faire du gras et produire des plumes : vous utiliserez la méthode `manger()` puis la méthode `manger()` qui prends des arguments,
- afficher les valeurs de *Tux* et *Maigrichon*.


