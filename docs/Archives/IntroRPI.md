# GNU/Linux embarqué sur carte RaspberryPi (Partie 1)

!!! tldr "Objectifs de ce document"

    - comprendre ce qu'est une carte **SBC** (single board computer),
    - comprendre la notion de *linux embarqué* utilisé dans l'*Internet des objets* (IOT en anglais),
    - installer un système d'exploitation linux à partir d'une image,
    - configurer un système d'exploitation linux : configuration de base, du réseau (filaire et wifi),  installation et configuration de logiciels.
    ---
    Enseignant: Olivier DARTOIS - BTS SNIR - Lycée Turgot - Limoges

!!! info "Référentiel du BTS SNIR"

    - S4. Développement logiciel
        - S4.1 : Principes de bases (niveau 4)
        - S4.2 : Algorithmique (niveau 3)
    - S6. Système d'exploitation
        - S6.1 : Notions fondamentales (niveau 3)
        - S6.4 : Systèmes embarqués (niveau 2)
    - S7 :
        - S7.2 : Concepts fondamentaux des réseaux (niveau 3)

## Introduction

Le raspberryPi est un **nano-ordinateur** comparé à un PC mais cependant il vous permettra de réaliser de très nombreux projets tant au niveau de la programmation que du dialogue avec des parties opératives externes. De plus son prix très modique (environ 35€) permet à n'importe quelle personne d'accéder à la découverte de GNU/Linux.

Le site officiel en anglais : <http://www.raspberrypi.org/> et le brochage : <http://pinout.xyz/>.
Vous trouverez sur ce site tout ce qu'il faut pour démarrer mais en langue anglaise.
En particulier vous téléchargerez la distribution RaspberryPi OS (distribution debian pour raspberrypi) depuis <https://www.raspberrypi.com/software/operating-systems/>.

- un journal dédié au raspberrypi en anglais : <http://www.themagpi.com/>
- un très bon site en français : <http://www.framboise314.fr/>
- et enfin le site avec lequel vous allez travailler tout au long de la découverte du raspberrypi pendant votre formation en BTS SNIR : <http://innovelectronique.fr/> plus précisément <http://innovelectronique.fr/2013/06/09/raspberrypi-a-tout-faire/>

!!! warning "Rappel"
    Vous êtes en section de technicien supérieur aussi nous n’allons pas forcement utiliser l’interface graphique pour piloter le raspberrypi. Vous allez saisir l’essentiel de vos commandes en mode console depuis un ordinateur distant (pas d’écran, pas de clavier sur le Rpi : configuration dite *headless*).

Lors de la création de la carte SD, vous avez le choix entre deux versions :

- RaspberryPi OS *classique* : archive de ± 2Go, interface graphique, nombreux logiciels préinstallés → version utilisée par les utilisateurs standards
- RaspberryPi OS *lite* : archive de ± 300Mo, mode console, minimum vital → version pour les techniciens qui permet des personnalisations

Pour réaliser un serveur, on prendra donc naturellement la version allégée puis on installera ce dont on a besoin !

!!! note "Exercice"
    Vous allez *graver* sur une carte microSD avec l'adaptateur USB fourni, la distribution raspberryPi OS 32 bits en version *lite* en utilisant le logiciel **RaspberryPi Imager**. Ce logiciel est préinstallé sur les ordinateurs de la section de BTS, par contre il doit être lancé en mode *administrateur* (clic droit sur l'application puis Exécuter en tant que...) avec l'utilisateur **tp_rpi** et le mot de passe **tp_rpi**. L'utilisation de ce logiciel n'est pas décrite dans ce document car il est extrêmement simple. Il est décrit dans cette [vidéo](https://youtu.be/ntaXWS8Lk34) (en anglais, à faire en ESLA).

!!! danger "Depuis Septembre 2022 et uniquement sur la version **Lite**"
    La version Lite n'a plus de login et mot de passe par défaut, il faut créer un utilisateur et un mot de passe lorsque vous gravez votre carte SD avec le logiciel **RaspberryPi Imager**. Pour accéder aux options avancées, cliquez sur la roue dentée si elle est présente sinon la combinaison de touches ++ctrl+shift+x++ pour les versions plus anciennes. Dans la fenêtre qui apparaît, cochez la case *Set username and password* et complétez le champ *Username* (pi par exemple) et *Password* (raspberry) pour reprendre les mots de passe par défaut. Pensez ensuite à cliquer sur le bouton *Save*.

!!! note "Exercice"
    Pendant que le système est transféré sur la carte SD, cherchez le nom de login et le mot de passe par défaut sur Internet.

    - login :
    - mot de passe :

Vous ne disposez pas d'écran HDMI. Vous allez donc vous connecter par voie série sur le raspberrypi comme décrit sur le site innovelectronique. Pour cela un convertisseur USB-Série vous est fourni. Le chipset de ce dongle est un Prolific PL2303 ou un FTDI. Si Microsoft Windows ne le détecte pas automatiquement il vous faudra peut être installer un pilote.

Une fois le pilote installé, donnez le nouveau port COMxx (xx étant un nombre) créé : `___________`. Pour consulter le nom des ports COM : touches ++left-windows+x++ puis choisissez *Gestionnaire de périphériques* puis la rubrique *Ports (COM et LPT)*.

Si le logiciel Putty n'est pas installé sur votre poste, téléchargez-le et installez-le. Vous utiliserez ce logiciel soit pour vous connecter par voie série, soit par SSH (connexion sécurisée) à travers le réseau ethernet.

!!! warning "Attention"
    Sur RPi3/4, le mode console disponible sur les broches TX/RX est désactivé, pour réactiver celui-ci, vous devez éditer le fichier **config.txt** sous Microsoft Windows présent sur la première partition de votre carte SD (un lecteur `boot` doit apparaître dans l'explorateur de Windows) à l’aide de notepad++ et rajouter à la fin de ce fichier  **enable_uart=1**.

Réalisez le câblage à l'aide des fils fournis puis tentez une connexion par voie série à l'aide du convertisseur vers le raspberrypi. La vitesse du port série doit être de 115200 bits/s. Le câblage nécessite trois fils :

- GND convertisseur USB → GND RPi,
- TX convertisseur USB → RX RPi,
- RX convertisseur USB → TX RPi

!!! tip "Rappel"
    Le brochage du raspberryPi est disponible de manière interactive sur le site <http://pinout.xyz/>.

Si vous n'arrivez pas à vous connecter, redémarrez le RPi et vérifiez votre câblage.

Configuration de base du Rpi avec la commande `raspi-config` : lire l'article sur innovelectronique et modifiez les paramètres suivants :

- faire toutes les modifications nécessaires dans le menu « Localisation options » pour passer votre raspberrypi en français (la langue, le clavier, le fuseau horaire (Timezone) Europe/Paris, le wifi),
- puis dans les menus :
    - Network options : Hostname : nom du Rpi, mettre *RPi3-votreNom*,
    - Interfacing options : activez le bus I2C , activez le serveur SSH.

!!! tldr "Résumé à faire valider par le professeur"
    ❑ Téléchargement de l'image RaspberryPi OS  
    ❑ Création de la carte SD  
    ❑ Connexion du convertisseur USB-Série et identification du port COM  
    ❑ Câblage des lignes GND, TX et RX  
    ❑ Installation de Putty et lancement d'une connexion vers le Rpi en mode série  
    ❑ Alimentation du RPi  
    ❑ Login sur la carte Rpi  
    ❑ Passage en français  
    ❑ changement du nom de votre Rpi (hostname)  
    ❑ activation du bus i2c et du serveur SSH  

<span style="color: #ff0000">**Validation professeur :**</span>

## Configuration réseau

Par défaut le RPi est en mode DHCP. Il cherche donc à obtenir son adresse IP depuis un serveur DHCP sur le réseau. Pour des raisons pratiques (prises ethernet en nombre limitées, adressage IP du lycée à respecter, câbles supplémentaires…), la connexion WiFi sera utilisée. Un routeur sans fil relié au réseau pédagogique en ethernet filaire sera votre point d’accès sans fil. Vous pourrez *remonter* vers votre Rpi en vous connectant depuis votre poste sous Windows sur l’adresse IP du routeur et sur un port spécifique. Pour cela nous fixerons l’adresse ip de l’interface WiFi du RPi pour le lycée.
D’autre part, il faut que votre Rpi s’insère facilement dans votre réseau personnel, nous allons donc faire en sorte qu’il se connecte automatiquement au lycée et chez vous.

### Schéma réseau du TP au lycée / à la maison

Vous trouverez ci-dessous les deux schémas réseaux possibles pour l'utilisation d'une carte RPi. Veuillez noter que le nom du point d’accès wifi du lycée ainsi que le mot de passe sont indiqués sur le schéma.

![Schéma réseau Introduction RPi](../img/schemaReseauIntroRPI.png)

### Modification de configuration spécifique à la section de BTS

Quelques dysfonctionnements ont été constatés avec les RPi3/4 et des routeurs sans fils (par exemple celui utilisez dans la section : un LinkSys WRT54G), pour y remédier, il faut réaliser la modification suivante en vous connectant par la voie série.

Modification pour le serveur SSH : éditez le fichier de configuration du serveur SSH (chemin dans l'arborescence : `/etc/ssh/sshd_config`) à l’aide de l’éditeur nano et rajoutez en fin de fichier la ligne 
`IPQoS cs0 cs0`. L'éditeur *nano* est un éditeur de texte très basique, pour sauvegarder votre modification faire ++ctrl+x++. La ligne de commande est donc :

```console
sudo nano /etc/ssh/sshd_config
```

!!! note "Exercice"
    Précisez ci-dessous le rôle de la commande `sudo` :

### Configuration du wifi

Avant d’aborder la configuration IP, il faut se connecter à un point d’accès WiFi. Cette connexion est maintenant toujours cryptée par un mot de passe. Pour gérer plusieurs configurations réseaux, il faut rajouter plusieurs sections `network` au fichier `/etc/wpa_supplicant/wpa_supplicant.conf` suivant le modèle suivant en fin de fichier (attention les doubles cotes sont à taper, pas d’espaces autour du signe = , la longueur du mot de passe minimum (psk) est de 8 caractères):

!!! example "Exemple de section `network` du fichier `wpa_supplicant.conf`"
    ```console
    network={
	  ssid="nom_de_la_borne_wifi"
	  psk="cle_de_connexion"
	  id_str="nom_du_reseau"
    }
    ```

!!! note "Exercice"
    === "Configuration réseau"
        A partir du schéma réseau précédent, proposez deux sections `network` pour le réseau de la section de BTS et pour votre réseau personnel
    === "Réseau BTS"
        A compléter...  
        .  
        .  
        .  
        .
    === "Réseau personnel"
        A compléter...  
        .  
        .  
        .  
        .

<span style="color: #ff0000">**Validation professeur :**</span>

Il faut ensuite modifier le fichier `/etc/network/interfaces`, et changer la fin du fichier qui concerne le réseau sans fil (wlan0 : wireless local area network). Un exemple de fichier peut être:

!!! example "Exemple de fichier de configuration `/etc/network/interfaces`"
    ```console
    auto wlan0
    allow-hotplug wlan0
    iface wlan0 inet manual
        wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf

    iface chezmoi inet dhcp
    iface aulycee inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
        dns-nameservers 192.168.1.1
    ```

L'adresse ip (champ `address`) vous sera donné par le professeur présent : `___________`.
Réalisez votre modification du fichier `/etc/network/interfaces` en tenant compte de ce que vous avez proposé précédemment puis faites validez par le professeur présent.  

Pour terminer, il faut modifier le fichier `/etc/dhcpcd.conf` et rajouter à la fin `denyinterfaces wlan0`. Cet ajout interdit au service DHCP de gérer la carte wifi et permet la prise en compte de notre configuration.

<span style="color: #ff0000">**Validation professeur :**</span>

### Configuration de l’ethernet Filaire en mode statique (facultatif)

La carte ethernet filaire sous GNU/Linux est identifiée par le nom `ethX`, X étant le numéro de la carte à partir de zéro. La Rpi disposant à la base d’un port ethernet, vous aurez donc une carte `eth0`.
La fondation RaspberryPi préconise de fixer les paramètres IP d’une carte en mode **statique** (à opposer au mode dynamique par DHCP) en modifiant le fichier `/etc/dhcpcd.conf` et en rajoutant les lignes suivantes à la fin du fichier:

!!! example "Exemple de fichier de configuration `/etc/dhcpcd.conf`"
    ```console
    interface eth0
    static ip_address=adresse_ip_fixe/masque en notation CIDR (ex : 192.168.1.2/24)
    static routers=adresse_ip_passerelle (ex : adresse de votre box)
    static domain_name_servers=adresse_ip_serveur_DNS (généralement votre BOX)
    ```

### Vérification de la configuration IP

Pour appliquer tous les changements que l’on vient d’effectuer, le plus simple est de redémarrer. La commande est simplement : `reboot`. Pour vérifier la configuration IP, il suffit de saisir la commande : `ip addr`. Faites un capture d’écran de cette commande, surlignez la ligne qui présente la configuration IP de la carte wlan0. Pour vérifier votre connexion Internet, tapez une commande simple vers un site Internet:`______________________________________________________________`

<span style="color: #ff0000">**Validation professeur :**</span>

### Configuration réseau alternative, activation du serveur ssh

Vous pouvez configurer le réseau sans fil et activer le serveur ssh de manière très simple en réalisant les manipulations suivantes :

- Personnalisez votre image au moment de la gravure sur carte SD avec l’utilitaire raspberryPi Imager (reportez-vous au début de ce document)  
<span style="color: #ff0000">**OU**</span>
- Activation du serveur ssh : créer un fichier vide avec le nom `ssh` sur la partition `/boot`.   Configuration sans fil (pour un seul réseau) : créer un fichier avec le nom `wpa_supplicant.conf` à la racine de la partition `/boot` avec le contenu suivant:

    ```console
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    country=FR

    network={
        ssid="nom de votre point d’accès"
        psk="mot de passe de votre point d’accès"
    }
    ```

### Connexion distante par SSH

Si les tests précédents sont concluants, vous pouvez éteindre la Rpi avec la commande: `sudo poweroff`.Vous pouvez alors déconnecter l’adaptateur USB-Série. Vous allez maintenant vous connecter à distance depuis votre poste Windows à l’aide du logiciel Putty. La connexion est cryptée ainsi personne ne peut intercepter vos commandes. Les connexions non cryptées sont à proscrire (exemple : commande telnet).

Comme nous avons une installation particulière pour le TP, vous n’allez pas directement vous connecter sur le port 22 du serveur SSH, mais sur un port précisé par le professeur qui sera redirigé vers le port 22 de votre carte Rpi.

Pour cela complétez le tableau ci-dessous avec le port que vous indiquera le professeur.

|Nom service|Adresse IP sur laquelle vous vous connectez = adresse IP du point d’accès sans fil|Numéro de port entrant (donné par le professeur)|Numéro de port sortant|Adresse IP destination = adresse ip de votre RPi|
|:--:|:--:|:--:|:--:|:--:|
|SSH|10.187.52.2||22||
|Apache|10.187.52.2||80||

Connectez-vous maintenant avec le logiciel Putty sur votre carte Rpi par le réseau ethernet en SSH.

<span style="color: #ff0000">**Validation professeur :**</span>

### Mise à l'heure automatique

Cette partie concerne uniquement la configuration d'une RPi au lycée Turgot. C'est inutile de faire cette modification chez vous ! Il faut éditez le fichier `/etc/systemd/timesyncd.conf` et rajouter sous la section `[Time]` la ligne `NTP=ntp.unice.fr`. Sauvez le fichier puis redémarrez. Vérifiez ensuite que votre RPi est bien à l'heure avec la commande `date`.

!!! tldr "Résumé de cette partie"
    ❑ Comprendre le schéma réseau du TP et de l’installation personnelle  
    ❑ Effectuer les modifications spécifiques au RPi3/4  
    ❑ Réaliser la configuration IP de l’ethernet sans fil (WiFi)  
    ❑ Réaliser la configuration IP de l’ethernet filaire (facultatif)  
    ❑ Tester la configuration réseau (commandes ip addr, ping)  
    ❑ Redémarrer le Rpi (reboot), l’éteindre de manière correcte (poweroff)  
    ❑ Se connecter à distance en mode sécurisée (connexion par SSH avec le logiciel putty sous Microsoft Windows)  
    ❑ Spécifique au lycée : mise à l’heure en modifiant un fichier de configuration, utilisation de l’éditeur nano  

## Commandes de gestion des logiciels et mise à jour

Vous travaillerez en ligne de commande donc vous ne pouvez pas utiliser un logiciel comme synaptics qui permet de gérer les paquets en mode graphique. La commande de gestion des paquets est `apt`.

Les commandes de gestion des logiciels doivent être exécuté en tant qu'utilisateur `root` (=administrateur) ou à l'aide de la commande 'sudo'.

- Pour mettre à jour la liste des logiciels présents sur les dépôts : `apt update`
- Pour mettre à jour tous les logiciels installés sur votre Raspian : `apt upgrade`
- Pour chercher un logiciel dans la liste des dépôts : `apt search` *`mot_clé`*
- Pour installer un logiciel depuis les dépôts d'internet : `apt install` *`nom_du_paquet`*
- Pour dé-installer un logiciel de la carte SD : `apt remove --purge` *`nom_du_paquet`*
- Pour libérer de la place sur la carte SD après une mise à jour : `apt clean` et `apt autoremove`

!!! note "Exercice commandes d'installation et test d'un serveur web"
    Actualisez la liste des paquets puis lancez une mise à jour: `________________`  
    Installez le paquet `build-essential` qui vous fournira entre autre le compilateur g++ (normalement il doit déjà être installé): `________________`  
    Testez celui-ci en lançant : `g++ -v` puis donnez la version installée : `_____________`

    Installez le serveur web `apache2`: `________________`  
    Testez que celui-ci est fonctionnel depuis un navigateur sous Microsoft Windows. Attention il faudra vous connecter sur le port indiqué par le professeur qui redirigera vers le port 80 du serveur Apache : Port : `_____` → complétez le tableau de la section précédente  
    Adresse http à taper dans le navigateur web qui permet de précisez le port: `_____________`  
    La page d'accueil par défaut affiche: `_____________`  
    Modifiez la page d'accueil de votre serveur Apache pour qu'il affiche votre nom et prénom : éditez le fichier `index.html` situé dans `/var/www/html`

<span style="color: #ff0000">**Validation professeur :**</span>

!!! note "Exercice installation du langage PHP et tests"
    Installez le langage PHP dans sa version la plus récente: `_____________`  
    Créez dans `/var/www/html` un fichier `index2.php` avec comme contenu `<?php phpinfo(); ?>`. Ouvrez ce fichier depuis votre navigateur web sous Windows. Que permet de vérifier ce test: `_____________`  

!!! note "Exercice : transférer un fichier html existant avec le logiciel WinSCP"
    Avant de transférer un fichier avec le logiciel winSCP, il faut modifier les droits du répertoire `/var/wwww/html' sinon vous n'avez pas les droits d'écriture. Pour cela, tapez la commande suivante :
    
    ```console
    sudo chmod 777 /var/www/html
    ```
    
    Transférez un fichier (par exemple un de ceux que vous avez produit lors des Tps HTML/Javascript/PHP) vers `/var/www/html` avec le logiciel **WinSCP** sous Windows, il faudra juste faire attention au port de connexion.

<span style="color: #ff0000">**Validation professeur :**</span>

Vous avez quasiment fini l'installation d'un serveur LAMP (Linux, Apache, MySQL=MariaDB, PHP). Il vous manque donc un logiciel de SGBDR (Système de Gestion de Base de Données Relationnel). Nous choisissons la base MariaDB car elle remplace avantageusement MySQL (Opensource, compatible au niveau programmation). Vous allez aussi installer un logiciel web de gestion de cette base de données : phpMyAdmin. 

!!! note "Exercice : installation d'un SGBDR"
    Pour réaliser le cahier des charges précédent il faut installer les paquets suivants : mariadb-server, mariadb-client, php-mysql, phpmyadmin.

    Lors de l’installation choisir le serveur web apache2 (touche espace : une étoile doit apparaître dans la case) et le mode de configuration « db-config ». Le mot de passe qui vous sera demandé lors de l’installation ne sert que pour l’utilisateur phpmyadmin qui n’a aucun droit de création de base.

!!! note "Exercice : création d'un nouvel utilisateur pour le SGBDR"
    Pour des raisons de sécurité, l’utilisateur `root` n’a pas le droit de se connecter sur l’interface de phpMyAdmin depuis un poste distant. Nous allons donc créer un utilisateur qui aura les mêmes droits que `root` mais depuis n’importe quel ordinateur. **Veuillez noter qu’il s’agit là d’une très mauvaise pratique qui ne devra en aucun cas être utilisé sur un serveur connecté à Internet.**

    Tapez la ligne suivante dans une console en mode administrateur ou préfixé par ‘sudo’ :
    ```console
    mysql -e "GRANT ALL ON *.* TO 'pi'@'%' IDENTIFIED BY 'raspberry' WITH GRANT OPTION"
    ```

Vous venez de créer un utilisateur nommé `pi` avec comme mot de passe `raspberry` qui aura tous les droits sur toutes les bases (`GRANT ALL ON *.*`) depuis n’importe quel ordinateur (`%`).

!!! note "Exercice : test de connexion à l'interface de gestion de la SGBDR avec phpMyAdmin"
    Pour vous connecter à phpMyAdmin : *http://adresse_ip_routeur:num_port/phpmyadmin* , l’utilisateur sera `pi` avec le mot de passe `raspberry`. Vous pouvez alors créer de nouvelles bases, de nouveaux utilisateurs, etc...

**Conclusion** : vous avez un serveur LAMP (Linux, Apache, MariaDB/MySQL, PHP) complet pour la maison afin de réaliser les Tps de première année HTML/PHP/MySQL !

<span style="color: #ff0000">**Validation professeur :**</span>

!!! tldr "Résumé de cette partie"
    ❑ Pour gérer l'installation/désinstallation des logiciels : en mode graphique utilisez le logiciel synaptics, en mode texte la commande apt  
    ❑ Pour maintenir à jour votre distribution Debian : sudo apt update suivi de sudo apt upgrade  
    ❑ Pour installer un compilateur C/C++, il suffit d'installer le pseudo paquet build-essential  
    ❑ Si vous créez des pages web, il suffit de les rajouter dans le répertoire /var/www/html après avoir installé Apache et éventuellement PHP  
    ❑ Vous pouvez utiliser la SGBDR MariaDB/MySQL pour vos projets. La gestion peut se faire avec le logiciel phpMyAdmin  

Vous venez de terminer la première partie de ce document. Vous aurez donc trés prochainement un examen de TP de 1H30 sur les concepts présentés ici. **Vous aurez droit à ce document pendant l'examen donc il est de votre responsabilité qu'il soit complété et annoté avec soin.**

La suite de ce document (logiquement la partie 2) présentera le développement en langage C++ sur carte RPi en utilisant le développement à distance depuis Microsoft Visual Studio et en utilisant une bibliothèque C/C++.