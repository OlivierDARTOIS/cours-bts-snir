# GNU/Linux embarqué sur carte RaspberryPi (Partie 2)

!!! tldr "Objectifs de ce document"

    - réaliser un exécutable en utilisant la chaîne de compilation GNU en ligne de commande,
    - comprendre l'intérêt du développement à distance sur RPi,
    - configurer l'EDI VisualStudio pour la compilation à distance,
    - installer et configurer dans l'EDI une bibliothèque C/C++,
    - réaliser quelques codes C++ simples pour piloter les Entrées/Sorties du RPi.
    ---
    Enseignant: Olivier DARTOIS - BTS SNIR - Lycée Turgot - Limoges

!!! info "Référentiel du BTS SNIR"

    - S4. Développement logiciel
        - S4.1 : Principes de bases (niveau 4)
        - S4.2 : Algorithmique (niveau 3)
    - S6. Système d'exploitation
        - S6.1 : Notions fondamentales (niveau 3)
        - S6.4 : Systèmes embarqués (niveau 2)
    - S7 :
        - S7.2 : Concepts fondamentaux des réseaux (niveau 3)

Dans le précédent document vous avez réalisé la configuration d'une RPi (clavier, langue, réseau, etc...). Puis vous avez installé différents services afin de réaliser un serveur LAMP. Dans ce document vous allez vous intéresser à la programmation C/C++ et aux outils qui permettent de vous faciliter le développement.

## Mise en place de l'environnement de développement

### Test de la chaîne de compilation : premier programme C/C++

Éditez avec le logiciel *nano* le fichier *hello.cpp* avec le contenu suivant :

```cpp linenums="1"
#include <iostream>
using namespace std;

int main()
{
   cout << "Compilation ARM sur Rpi OK" << endl;
   return 0;
}
```

Sauvegardez le fichier puis compilez-le :
```console
g++ -o hello hello.cpp -Wall
```

Exécution en tapant la commande :
```console
./hello
```

Résultat de l'exécution du programme, on appelle aussi cela une trace d'exécution :
`____________________`.

!!! tldr "Exercice : explications des paramètres de la ligne de commande"
    - `-o hello` :
    - `-Wall` :
    - `hello.cpp` :

!!! tldr "Exercice : explications du code ligne à ligne"
    |ligne|commentaire (une seule ligne, précis et concis)|
    |:---:|:---|
    |1||
    |2||
    |4||
    |5,8||
    |6||
    |7||

Modifiez le programme pour qu'il affiche votre nom.  
Donnez le résultat de la commande : `file hello`  
`_____________________`  
Donnez la taille du fichier hello en octets (commande `ls -l`) :  
Tapez la commande suivante : `strip hello` puis donnez à nouveau la taille du fichier : `_________`  
Rôle de la commande strip : `______________________`

<span style="color: #ff0000">**Validation professeur :**</span>

### Configuration de Microsoft Visual Studio 2019 pour une compilation sur cible distante

Développer avec nano en ligne de commande sous GNU/Linux reste possible mais relativement lourd. Nous allons donc utiliser un EDI (Environnement de Développement Intégré) sur le poste Microsoft Windows. Ici ce sera Microsoft Visual 2019/2022 mais il existe d'autres EDI (IDE en anglais) très répandu comme Eclipse, QtCreator, Code::Blocks, etc...

!!! tip "Compte pour l'utilisation de Microsoft Visual Studio Community Edition"
    Lorsque vous lancez Visual Studio, il vous demande de vous connecter avec une adresse de messagerie. Si vous ne voulez pas utiliser votre compte, vous utiliserez le compte suivant :  
    login : btssnir87@outlook.fr    mdp : TUX-BTS-SN-TP

Téléchargez depuis le site de Microsoft la version Visual Studio Community Edition 2019/2022 puis l'installez sur votre ordinateur personnel. Dans la section de BTS c’est inutile car il est déjà installé.
Nous allons mettre en place le développement sur cible distante (remote development en anglais). Coté Rpi, installez les logiciels : gdb, gdbserver, zip, rsync.

Lancez Visual Studio 2019/2022, connectez-vous s’il vous le demande avec le compte ci-dessus.
Cliquez sur *Créer un projet* puis dans la barre de recherche tapez *console c++ linux*. Cliquez alors sur le modèle *Application console pour LINUX* puis bouton Suivant. Vous pouvez lire les articles suivants <https://docs.microsoft.com/fr-fr/cpp/linux/download-install-and-setup-the-linux-development-workload?view=vs-2019> et <https://docs.microsoft.com/fr-fr/cpp/linux/create-a-new-linux-project?view=vs-2019> pour avoir plus de détails.

Donnez un nom à votre projet puis bouton Suivant. L’interface de Visual Studio se lance.
Double-cliquez sur le fichier *main.cpp* dans l’explorateur de solution, le code apparaît.
Modifiez votre code source, choisissez la **compilation sur architecture ARM** à coté de *Debug* (barre d’icône en haut de la fenêtre) puis lancez la génération de la solution : menu *Générer* puis *Générer la solution*.

Une fenêtre s’ouvre, vous devez saisir les paramètres de connexion à votre Rpi :

- nom d’hôte : l’adresse ip du routeur (pas l’adresse réelle de votre Rpi pour ce TP),
- port : le port donnée par le professeur pour la redirection du port SSH,
- nom d’utilisateur : à priori *pi* sauf si vous avez créé un autre utilisateur,
- Type d’authentification : mot de passe,
- Mot de passe : le mot de passe par défaut (raspberry) ou votre mot de passe si vous l’avez changé.

Connectez-vous sur la cible par ssh et exécutez le programme que vous venez de compiler. La commande `cd` permet de changer de répertoire. La commande `ls` affiche les fichiers contenus dans le répertoire.
L'exécutable se trouve dans le répertoire `home` de l’utilisateur *pi* généralement dans un répertoire `projects` puis un sous répertoire qui porte le nom de la *solution* (le nom de votre projet dans Visual Studio). L'exécutable se trouve dans le sous répertoire `bin`, `ARM` puis `Debug` ou `Release` en fonction du mode de compilation. Votre exécutable se termine par l’extension `.out` et s'exécute par `./nom_projet.out`.

Si le projet utilise des bibliothèques externes : par exemple pthread, serial, wiringPi, etc., il faut lire la page qui explique comment rajouter cela dans les options de compilation du projet:
<https://docs.microsoft.com/fr-fr/cpp/linux/configure-a-linux-project?view=vs-2019>

Solution (exemple pour la bibliothèque **pigpio**) : Il faut rajouter dans les paramètres du projet (clic droit sur le nom du projet dans l’explorateur de solution puis *propriétés*), cliquez sur l’item *Editeur de liens* puis *Entrées*. Rajouter dans le champ *Dépendances de bibliothèque* le mot **pigpio** puis valider ces changements en cliquant sur le bouton *OK*.

Si l’autocomplétion dans VisualStudio ne fonctionne pas sur votre projet Linux (touches ++ctrl+space++ pour compléter un mot clé lorsque vous tapez votre code), il faut suivre la procédure suivante tirée du site de Microsoft ([lien](https://docs.microsoft.com/fr-fr/cpp/linux/configure-a-linux-project?view=msvc-170)) :

> Pour gérer votre cache d’en-tête, accédez aux options Outils, Gestionnaire des connexions à distance d’en-têtes puis IntelliSense Manager. Pour mettre à jour le cache d’en-têtes après avoir effectué des changements sur votre ordinateur Linux, sélectionnez la connexion à distance, puis sélectionnez Mettre à jour. Sélectionnez Supprimer pour supprimer les en-têtes tout en conservant la connexion. Sélectionnez Explorer pour ouvrir le répertoire local dans l’Explorateur de fichiers.

<span style="color: #ff0000">**Validation professeur :**</span>

## Présentation, compilation et mise en œuvre de la bibliothèque piGpio

La bibliothèque [piGpio](http://abyz.co.uk/rpi/pigpio/) permet de piloter les broches d’entrées/sorties (General Purpose Input Output : GPIO) de la carte raspberryPi. Elle permet aussi d’utiliser les bus **i2c** et **spi** ainsi que la **voie série**. Le problème avec cette bibliothèque est que vos exécutables devront être lancés en mode *root* donc préfixé avec la commande sudo !

!!! tldr "Exercice"
    Connectez-vous avec putty sur votre RPi3/4 puis installez le paquet *pigpio*.

!!! faq "Que permet de faire la commande `raspi-gpio get` ?"
    Réponse:

La numérotation des GPIOs est celle du processeur Broadcom. C’est donc la même numérotation que sur le site <http://pinout.xyz>.

Saisissez le code suivant dans un nouveau projet Visual Studio, puis testez-le

```cpp linenums="1"
#include <iostream>
#include <pigpio.h>

using namespace std;

const int DEL = 18;

int main()
{
    cout << "Clignotement DEL" << endl;
    gpioInitialise();

    while (true) {
            gpioWrite(DEL, 1);
            gpioSleep(PI_TIME_RELATIVE, 1, 0);
            gpioWrite(DEL, 0);
            gpioSleep(PI_TIME_RELATIVE, 1, 0);
        }

    gpioTerminate();
    return 0;
}
```

!!! faq "Comment rajouter une bibliothèque dans un projet Visual Studio"
    Une erreur de compilation (en fait à l’étape de *liens*) survient. Que faut-il rajouter au niveau des options de compilation du projet Visual Studio (observez la ligne de compilation sur <https://abyz.me.uk/rpi/pigpio/cif.html>) et revenez en arrière dans ce support pour gérer la bibliothèque **pigpio** dans un projet Linux créé dans VisualStudio 2019.  
    Option de compilation à rajouter : `________________`

!!! tldr "Exercice"
    Câblez la DEL sur votre carte (fil noir : masse, fil rouge : gpio18). Exécutez le code précédent, qu’obtenez-vous : `_______________________________________`. A l’aide de la commande `raspi-gpio get`, déterminez si la GPIO18 est en entrée ou en sortie : `__________________`

    Corrigez le code ci-dessus en rajoutant la fonction qui permet de mettre la broche concernée en sortie. Pour cela, lisez <https://abyz.me.uk/rpi/pigpio/cif.html#gpioSetMode>. Recompilez puis relancez le programme, à nouveau qu’obtenez-vous ?: `________________`

!!! faq "Analyse de code C/C++"
    Pour répondre aux questions ci-dessous, reportez-vous à la documentation en ligne de la bibliothèque **pigpio** disponible sur <https://abyz.me.uk/rpi/pigpio/cif.html>.

    |Questions|Réponses : sur une ligne ou en fin de document|
    |:---|:---|
    |Quel est le rôle de la fonction `gpioInitialise()` ?||
    |Quel est le rôle de la fonction `gpioWrite()` ? Quels sont ces arguments ?||
    |Quelle est le prototype de la fonction `gpioSleep()` ? Précisez le type et le nom de ces arguments.||
    |A quelle fréquence clignote la DEL ? Justifiez votre réponse par rapport au code.||
    |Quel est le rôle de la fonction `gpioTerminate()` ?||
    |Pourquoi y a-t-il un `return 0` à la fin du programme ?||
    |Arrive-t-on à la ligne `return 0` ? si non pourquoi ?||
    |Comment quitte-t-on ce programme ?||

!!! tldr "Exercice"
    - Modifiez le code précédent pour qu'il fasse clignoter la del connecté sur la GPIO4
    - Modifiez votre code pour faire clignoter la del à une fréquence de 5Hz

<span style="color: #ff0000">**Validation professeur :**</span>

## Mise en œuvre de la MLI à l’aide de la bibliothèque pigpio : Application sur la variation lumineuse d’une DEL

La Modulation de Largeur d’Impulsion (MLI ou Pulse Width Modulation : PWM en anglais) est une technique couramment utilisé pour faire varier la vitesse d’un moteur à courant continu (moteur CC). Nous allons la mettre en œuvre sur une DEL afin de faire varier son intensité lumineuse. Vous constatez sur le chronogramme ci-dessous qu’il s’agit de signaux carrés.

![Chronogramme signaux MLI](../img/chronogrammeMLI.png)

!!! tldr "Exercice"

    === "Questions sur la MLI"

        Dessinez puis calculez la période et la fréquence du signal. 
        Calculez la valeur moyenne du signal dans chacun des cas ainsi que le rapport cyclique puis représentez la valeur moyenne en rouge dans chacun des cas.
        N'hésitez pas à lire cet [article](https://www.sonelec-musique.com/electronique_bases_modulation_largeur_impulsion.html).

    === "Vos réponses"
    
        - Période et Fréquence du signal :
        - Formule pour le calcul de la valeur moyenne :
        - Formule de calcul du rapport cyclique exprimé en % :

Dans la documentation technique de la bibliothèque **pigpio**, relevez les fonctions, dans le tableau ci-dessous, qui sont utilisées pour réaliser la MLI de manière matériel (ce n’est pas votre programme qui génère les signaux ci-dessus mais un module matériel à l’intérieur du circuit intégré). Vous consulterez ce [lien](https://abyz.me.uk/rpi/pigpio/cif.html#gpioPWM) de la documentation officielle.

!!! tldr "Exercice"
    |Partie *PWM*, Nom de la fonction|Rôle de la fonction|
    |---|---|
    |||
    |||
    |||
    |||

!!! tldr "Exercice"
    Proposez un programme qui fait varier l’intensité lumineuse de la DEL qui suit le *pseudo algorithme*  suivant :
    ```console
    del ← 18
    Initialise bibliothèque pigpio
    i ← 0
    pas ← 1

    tant que (vrai) faire
        gpioPWM(del, i)
        attendre 10ms
        i ← i + pas
        si (i = 255) alors pas ← -1 finsi
        si (i = 0) alors pas ← 1 finsi
    fin tant que

    ferme bibliothèque pigpio
    ```

    !!! faq "Pourquoi le test sur la variable i est égale à 255 et pas une autre valeur ?"
        Réponse:

!!! tldr "Exercice"
    Tracez ci-dessous l’évolution de la variable i dans le temps. Précisez les valeurs maximum et minimum de la variable i. Donnez la période et la fréquence de cette variable i.

    ![Evolution temporelle de la variable i](../img/mliVarITPRPi.svg)

<span style="color: #ff0000">**Validation professeur :**</span>

## Production d'une onde sinusoidale en MLI

Industriellement on ne commande pas une del de manière linéaire mais avec une sinusoide.
Proposez un programme qui fait varier l’intensité lumineuse de la DEL en parcourant un tableau de 1024 valeurs d’un sinus compris entre 0 et 254.

Un tableau se déclare en faisant : `type nom_du_tableau[nb de cases]` → `int sinus[1024]`
Les cases du tableau sont alors numérotées de 0 à 1023.

!!! info "Signal sinusoidale à produire"
    ![Signal sinusoidale](../img/signalSinusoidale.png)

Pour initialiser une case du tableau : `sinus[15] = 34;`. Lorsque vous arrivez à la fin du tableau vous repartez au début.

!!! tldr "Exercice"
    Proposez un code qui répond au cahier des charges précédent.

<span style="color: #ff0000">**Validation professeur :**</span>
