# Aide mémoire commandes Cisco iOS

!!! tldr "Objectifs de ce document"

    - vous trouverez dans ce document un résumé des commandes Cisco IOS pour les commutateurs et les routeurs,
    
    ---
    Enseignants: Dominique GLUCK & Olivier DARTOIS - BTS CIEL - TP AIS - Lycée Turgot - Limoges

Vous pouvez tester ces commandes sans avoir de matériel physique Cisco à l'aide du logiciel Cisco Packet Tracer. Dans la liste des commandes ci-dessous une paire d'accolades `{}` désigne un argument obligatoire, une paire de crochets `[]` désigne un argument facultatif, le caractère barre verticale `|` représente l'opérateur OU. Ce document se base en partie sur des informations du site [netwrix.com](https://www.netwrix.com/cisco_commands_cheat_sheet.html). Une autre source pour apprendre les commandes Cisco iOS [clemanet.com](https://www.clemanet.com/switch-cisco.php).

## Liste de commande Cisco iOS couramment utilisées

Les tableaux ci-dessous vous présentent un choix de commandes Cisco iOS parmi les plus courantes pour **configurer**, **sécuriser** et **dépanner** un matériel réseau. Ces tableaux incluent des commandes pour des commutateurs et des routeurs. Ci-dessous sont décrits les trois modes les plus courants pour gérer un matériel Cisco:

- **user exec mode** : Ce mode est le mode dans lequel vous arrivez lorsque vous vous connectez pour la première fois à un périphérique Cisco. Il offre un accès limité aux commandes et aux paramètres de configuration. Par exemple, ce mode vous permet d'afficher l'état à l'aide de certaines commandes *show*, mais ne vous permet pas d'afficher ou de modifier les configurations,
- **privileged exec mode** : Ce mode permet d'accéder à toutes les commandes, ce qui permet un examen et un contrôle plus détaillés du fonctionnement et de la configuration de l'appareil mais il ne permet pas de modifier la configuration,
- **global configuration mode** : Les commandes de configuration globale s'appliquent aux fonctionnalités qui affectent l'appareil dans son ensemble. Alors que les modes *exec* et *privileged exec* sont en lecture seule, le mode configuration globale donne à l'utilisateur un accès en écriture pour modifier le fichier de configuration actif. Pour utiliser le mode de configuration globale, **vous devez d'abord entrer dans le mode d'exécution privilégié**, puis exécuter la commande **configure terminal**. Le mode de configuration globale peut être divisé en modes de commande suivants, qui vous permettent de configurer différents composants :
    - Mode de configuration de l'interface 
    - Mode de configuration de la sous-interface 
    - Mode de configuration du routeur 
    - Mode de configuration de la ligne

Ci-dessous un schéma *Etats-Transitions* qui reprend les différents modes de fonctionnement d'un matériel Cisco :

```dot
digraph finite_state_machine {
	color=black;
	fontname="Helvetica,Arial,sans-serif";
	node [fontname="Helvetica,Arial,sans-serif"];
	edge [fontname="Helvetica,Arial,sans-serif"];
	node_DS [label="Démarrage\nSession\n", style="filled", fillcolor="#E6FFCC"];
	node_FS [label="Fin\nSession\n", style="filled", fillcolor="#FFB3B3"];
	rankdir=LR;
	node [shape = circle];
	node_DS -> "Mode\nUserExec\n" [label = "entrée"];
	node_DS -> "Identification\nUserExec\n" [label = "entrée", style=dashed];
	"Identification\nUserExec\n" -> "Mode\nUserExec\n" [label = "saisie\nlogin/mdp\n", style=dashed];
	"Mode\nUserExec\n" -> "Mode\nPrivilegedExec\n" [label = "enable"];
	"Mode\nUserExec\n" -> "Identification\nPrivilegedExec\n" [label = "enable", style=dashed];
	"Mode\nUserExec\n" -> node_FS [label = "exit"];
	"Identification\nPrivilegedExec\n" -> "Mode\nPrivilegedExec\n" [label = "saisie mdp\nenable\n", style=dashed];
	"Mode\nPrivilegedExec\n" -> "Mode\nUserExec\n" [label = "disable"];
	"Mode\nPrivilegedExec\n" -> "Mode\nGlobal\nConfiguration" [label = "configure\nterminal\n"];
	"Mode\nPrivilegedExec\n" -> node_FS [label = "exit"];
	
	subgraph cluster {
	    rank = same;
	    "Mode\nConfig.\ninterface\n";
	    "Mode\nConfig.\nline\n";
	    "Mode\nConfig.\nvlan\n";
	    "Mode\nConfig.\nrouteur\n";
	}
	
	"Mode\nGlobal\nConfiguration" -> "Mode\nPrivilegedExec\n" [label = "exit"];
	"Mode\nGlobal\nConfiguration" -> "Mode\nConfig.\ninterface\n" [label = "interface xxx\n"];
	"Mode\nGlobal\nConfiguration" -> "Mode\nConfig.\nline\n" [label = "line xxx\n"];
	"Mode\nGlobal\nConfiguration" -> "Mode\nConfig.\nvlan\n" [label = "vlan xxx\n"];
	"Mode\nGlobal\nConfiguration" -> "Mode\nConfig.\nrouteur\n" [label = "router xxx\n"];
	
	"Mode\nConfig.\ninterface\n" -> "Mode\nGlobal\nConfiguration" [label = "exit"];
	"Mode\nConfig.\ninterface\n" -> "Mode\nPrivilegedExec\n" [label = "end"];
	"Mode\nConfig.\nline\n" -> "Mode\nGlobal\nConfiguration" [label = "exit"];
	"Mode\nConfig.\nline\n" -> "Mode\nPrivilegedExec\n" [label = "end"];
	"Mode\nConfig.\nvlan\n" -> "Mode\nGlobal\nConfiguration" [label = "exit"];
	"Mode\nConfig.\nvlan\n" -> "Mode\nPrivilegedExec\n" [label = "end"];
	"Mode\nConfig.\nrouteur\n" -> "Mode\nGlobal\nConfiguration" [label = "exit"];
	"Mode\nConfig.\nrouteur\n" -> "Mode\nPrivilegedExec\n" [label = "end"];
	
}
```

Les noms des interfaces peuvent être :

- les interfaces 100Mbits/s sont nommées *fastethernet* ou *fa*,
- les interfaces 1Gbit/s sont nommées *gigabitEthernet* ou *gi*,
- les interfaces 10Gigabit/s sont nommées *TenGigabitEthernet*.

Les numéros des ports ont la syntaxe suivante: 0/1 ou 1/0/1. C'est à dire: **numéro du module/numéro du port** ou bien **numéro du switch dans le stack/numéro du module/numéro du port**.

### Liste des commandes de contrôle de mode

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**enable**|**en**|changer le niveau de privilège d'un utilisateur *classique* (user exec mode) vers le mode *privilégié* (privileged exec mode). Le mode privilégié se reconnait par l'usage du symbole `#` dans l'invite de commandes|
|**configure terminal**|**conf t**|passer dans le mode de configuration globale (global configuration mode)|
|**interface** *fastethernet/number*|**int**|passer dans le mode de configuration pour l'interface ethernet spécifiée|

### Liste des commandes de configuration basique

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**reload**||Redémarre le routeur ou le commutateur|
|**hostname** *name*||donne un nom au routeur ou au commutateur. Pour supprimer le nom: **no hostname**|
|**description** *string*||affecte une chaîne d'identification à une interface|
|**ip address** *ip-address* *mask*||fixe l'adresse IP et le masque à l'interface sélectionnée. Effacer le config ip: **no ip address**|
|**ip default-gateway** *ip_address*||fixe la passerelle par défaut du commutateur ou du routeur. Effacer la passerelle: **no ip default-gateway**|
|**ip name-server** *serverip-1* *serverip-2*||fixe la ou les adresses IP d'un serveur DNS utilisé par le matériel pour résoudre ces requêtes DNS|
|**ntp peer** *ip-address*||configure l'horloge logicielle pour synchroniser un homologue ou pour être synchronisée par un homologue ??|
|**shutdown**, **no shutdown**|shut|éteint (shutdown) ou active (no shutdown) l'interface sélectionnée|
|**show running-config**|ru|affiche la configuration actuelle du matériel|
|**show startup-config**||affiche la configuration sauvée dans la mémoire non volatile du matériel. Cette configuration est celle qui est chargée au démarrage du matériel|
|**show running-config interface** *interface slot/number*||affiche la configuration de l'interface spécifiée|
|**show ip interface** *[type number]||affiche le statut de l'interface sélectionnée ainsi que les détails de sa configuration IP|
|**copy running-config startup-config**|co ru st|remplace la configuration de démarrage (startup-config) par celle actuelle (running-config). Cette commande est équivalente à **write memory**|
|**exit**|ex|cette commande permet d'accéder au contexte précédent (par ex. de *config-if* à *config*)|
|**end**||permet de revenir à la *racine* du mode privilégié|
|**logout**||cette commande permet de vous déconnecter|

### Liste des commandes de diagnostique

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**ping** *{hostname &#124; system-address}* *[source source-address]*||test basique de connexion entre équipement sur la couche réseau|
|**speed** *{10 &#124; 100 &#124; 1000 &#124; auto}*||permet de fixer la vitesse de transmission d'une interface en mégabits/s ou d'activer la détection automatique|
|**duplex** *{auto &#124; full &#124; half}*||positionne le type de transmission sur half-duplex (à l'alternat), full-duplex (emission/réception simultanée) ou d'activer le mode automatique|
|**show mac address-table**||affiche la table des adresses MAC mémorisées par l'équipement|
|**show interfaces**||affiche des informations détaillées des interfaces (état, réglages, compteurs)|
|**show interface status**||affiche *interface line status*|
|**show interfaces switchport**||affiche de nombreux réglages et statut opérationnel, y compris les détails des agrégats de VLAN (*VLAN trunking*)|
|**show interfaces trunk**||Liste les informations des trunks actuellement opérationnels et des VLANs supportés par ces trunks|
|**show vlan**, **show vlan brief**||liste chaque VLAN et toutes les interfaces assignées à ce VLAN mais n'affiche pas les trunks|

### Liste des commandes de gestion des VLANs et de routage

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**show ip route**||affiche l'état de toutes les routes IP statiques ou dynamiques|
|**ip route** *network-number network-mask {ip-address &#124; interface}*||ajoute une route statique dans la table de routage|
|**router rip**||active le protocole RIP (Routing Information Protocol) ce qui vous conduit au mode de configuration du routeur|
|**network** *ip-address*||associe un réseau avec le processus de routage RIP|
|**version 2**||configure le logiciel embarqué de votre matériel pour émettre/recevoir seulement des paquets RIP version 2|
|**show ip rip database**||affiche le contenu de la base de données de routage RIP|
|**ip nat** *[inside &#124; outside]*||active le NAT (Network Address Translation)|
|**ip nat inside source** *{list{access-list-number &#124; access-list-name}} interface type number[overload]*||établit une translation dynamique de l'adresse IP source. L'utilisation du mot clé *list* vous permet d'utiliser une *ACL* (Access Control List) pour identifier le traffic qui sera soumis au NAT. L'option *overload* permet au routeur d'utiliser une adresse globale pour toute les adresses locales ??|
|**ip nat inside source static** *local-ip global-ip*||met en place une translation d'adresse statique entre une adresse locale et une adresse globale ??|
|**switchport access vlan**||associe le VLAN avec une interface|
|**switchport trunk encapsulation dot1q**||active l'encapsulation 802.1Q (tag des trames ethernet) sur l'agrégat de VLAN|
|**switchport access**||configure un port ethernet sur un commutateur pour qu'il fonctionne en mode d’accès afin qu'il se connecte à un ordinateur, un serveur ou une imprimante. Ce port doit être assigné à un seul VLAN|
|**vlan** *vlan-id* *[name vlan-name]*||crée un VLAN avec le numéro *vlan-id* et donne éventuellement un nom au VLAN (de 1 à 32 caractères). Pour effacer un VLAN: **no vlan** *vlan-id*|
|**switchport mode** *{ access &#124; trunk }*||configure le mode d'appartenance du port à un VLAN. Le mode *access port* est utilisé pour accéder de manière inconditionnelle et sans *trunk* à un seul VLAN pour envoyer et recevoir des trames non taggées (VLAN par port). Le mode *trunk* envoie et reçoit des trames taggées qui identifie le VLAN d'origine. Un *trunk* est un lien point à point entre deux commutateurs ou entre un commutateur et un routeur ??|
|**switchport trunk** *{encapsulation { dot1q }}*||positionne les caractéristiques du *trunk* quand une interface est en mode agrégat de VLAN. Dans ce mode, le commutateur supporte simultanément du traffic taggé ou non-taggé sur un port ??|
|**encapsulation dot1q vlan-id**||définit les critères de correspondance pour mapper l'entrée des trames 802.1Q sur une interface à l'instance de service appropriée ??|
|**show spanning-tree**||fourni les informations détaillées à propos du protocole *spanning-tree* pour tous les VLANs|

### Liste des commandes relative à la sécurité

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**password** *pass-value*||affiche le mot de passe requis si la commande login (sans autres paramètres) est configurée ??|
|**username** *name* **password** *pass-value*||définit l'un des noms d'utilisateur et mots de passe associés utilisés pour l'authentification de l'utilisateur. Il est utilisé lorsque la commande de configuration de la ligne locale login a été utilisée ??|
|**enable password** *pass-value*||fixe le mot de passe pour passer dans le mode privilégié|
|**enable secret** *pass-value*||fixe le mot de passe requis pour tous les utilisateurs pour passer en mode privilégié|
|**service** *password-encryption*||demande à iOS de chiffrer les mots de passe, les secrets *CHAP* et autres paramètres stockés dans la NVRAM de l'appareil|
|**ip domain-name** *name*||configure un nom de domaine DNS ??. Effacer le nom de domaine: **no ip domain-name**|
|**crypto key generate rsa**||crée et stocke dans la NVRAM les clés requises par le service SSH|
|**transport input** *{telnet &#124; ssh}*||défini si le protocole telnet ou le protocole SSH est utilisé dans le commutateur. Les deux protocoles peuvent être spécifiés en une seule commande. Préférez dés que c'est possible la connection sur le service SSH|
|**access-list** *access-list-number {deny &#124; permit} source [source-wildcard] [log]*||défini une liste d'accès d'adresses IP standards ??|
|**access-class**||restreint les connexions entrantes et sortantes entre un VTY particulier (dans un périphérique Cisco de base) et les adresses d'une liste d'accès ??|
|**ip access-list** *{standard &#124; extended}* *{access-list-name &#124; access-list-number}*||défini une liste d’accès d'adresses IP par un nom ou un nombre ??|
|**permit source** *[source-wildcard]*||permet à un paquet de passer par une liste de contrôle d'accès IP nommée. Pour supprimer une condition de permis d'une liste de contrôle d'accès, utilisez la forme « no » de cette commande ??|
|**deny source** *[source-wildcard]*||permet de définir des conditions dans une liste de contrôle d'accès IP nommée qui refusera les paquets. Pour supprimer une condition de refus d'une liste de contrôle d'accès, utilisez la forme « no » de cette commande ??|
|**switchport port-security**||activer la sécurité du port sur l'interface concernée ??|
|**switchport port-security maximum maximum**||positionne le nombre maximum d'adresses MAC sécurisées sur le port ??|
|**switchport port-security mac-address** *{mac-addr &#124; {sticky [mac-addr]}}*||ajoute une adresse MAC à la liste des adresses MAC sécurisées. L'option *sticky* rend l'adresse MAC associée avec l'interface|
|**switchport port-security violation** *{shutdown &#124; restrict &#124; protect}*||positionne l'action a faire lors de la détection d'une violation de sécurité|
|**show port security** *[interface interface-id]*||affiche les informations de sécurité configurées sur l'interface concerné|

### Liste des commandes de surveillance (monitoring) et de journalisation (logging)

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**logging** *ip address*||configure l'adresse ip de la machine qui va recevoir les messages de journalisation (généralement un démon syslog)|
|**logging trap level**||limiter les messages qui sont envoyés au démon syslog de la machine définie à la commande précédente en fonction d'un niveau de sévérité|
|**show logging**||affiche l'état du système de surveillance et les messages présents dans le tampon local de la machine|
|**terminal monitor**||envoyer une copie des messages de surveillance (inclus aussi les messages de déverminage) à l'utilisateur connecté en telnet ou en ssh|

### Liste des commandes DHCP (Dynamic Host Configuration Protocol)

|Commande|Raccourci|Description|
|:---:|:---:|:----|
|**ip address dhcp**||l'appareil effectue une requête DHCP pour obtenir une adresse IP sur une interface|
|**ip dhcp pool name**||permet de configurer un pool d'adresses DHCP sur un serveur DHCP et d'entrer en mode de configuration du pool DHCP ??|
|**domain-name** *domain*||précise le nom de domaine pour un client DHCP ??|
|**network** *network-number [mask]*||Configure le numéro de réseau et le masque d'un sous-réseau principal ou secondaire d'un pool d'adresses DHCP sur un serveur DHCP Cisco IOS ??|
|**ip dhcp excluded-address** *ip-address [last-ip-address]*||précise les adresses IP qu'un serveur DHCP ne doit jamais distribuer à ces clients|
|**ip helper-address** *address*||active la retransmission des messages UDP en mode diffusion, incluant le protocole BOOTP, reçu sur une interface|
|**default-router** *address[address2 ... address8]*||précise le ou les passerelles par défaut pour un client DHCP|	

## Cas d'usages

Ci-dessous vous trouverez des exemples de mises en oeuvre des commandes Cisco iOS dans des cas concrets

### Passer en *Mode privilégié*

```console
Switch> en
Switch#
```

### Passer en *Mode de configuration globale*

```console
Switch# configure terminal
Switch(config)#
```

### Nommer le switch

```console
Switch(config)# hostname S1
S1(config)#
```

### Adressage IPv4 d'une interface ethernet

Généralement le vlan1 est le VLAN par défaut du commutateur. Mais il est *conseillé* (donc non obligatoire) de créer un vlan d'administration (par ex: vlan 99) et de configurer l'adresse ip dans ce vlan.
Un [lien](https://www.cisco.com/c/fr_ca/support/docs/smb/switches/cisco-350-series-managed-switches/smb5557-configure-the-internet-protocol-ip-address-settings-on-a-swi.html) sur le forum de cisco qui présente cette mise en oeuvre.

```console
S1(config)#interface vlan1
S1(config-if)#ip address 192.168.100.1 255.255.255.0
S1(config-if)#end
S1#show ip interface
```

Vous pouvez compléter la configuration ip pour permettre au materiel de changer de réseau ip ou de faire des résolution DNS en remplaçant A.B.C.D par les adresses IP nécessaires :

```console
S1(config)#interface vlan1
S1(config-if)#ip default_gateway A.B.C.D
S1(config-if)#ip name_server A.B.C.D
S1(config-if)#end
S1#ping google.fr
```

### Adressage IPv4 d'une interface en mode DHCP

```console
S1(config)#interface vlan1
S1(config-if)#ip address dhcp
S1(config-if)#end
S1#show ip interface
S1#show ip dhcp client interface
```

### Adressage IPv6 d'une interface ethernet

```console
S1(config)#interface fastEthernet 0/0
S1(config-if)# ipv6 address FE80::1 link-local
S1(config-if)# ipv6 address 2001:DB8:CCCC:1::1/64
```

### Activation et désactivation d'une interface ethernet

```console
S1(config)# int fa0/0
S1(config-if)# no shutdown
S1(config)# int fa0/0
S1(config-if)# shutdown
```

### Afficher les différentes configuration de l'appareil

S1 est un commutateur. R1 est un routeur.

```console
S1#show run
S1#show running-config
S1#show startup-config
S1#show version
S1#show ip interface brief
S1#show ipv6 interface brief
S1#show interface fa0/6
S1#show vlan [brief]
S1#show interface vlan1
S1#show ip interface vlan1
R1#show ip route
R1#show ipv6 route
```

### Sauvegarder la configuration du matériel

```console
S1#copy running-config startup-config
```

### Ajouter des mots de passe pour l'authentification

La connexion au commutateur ou au routeur s'effectue par **le port console** en utilisant la ligne associée à ce port ou bien à distance en utilisant **les lignes virtuelles** (appelées VTY). Ces ports virtuels sont utilisés pour les connexions *telnet* ou *ssh*. Par défaut, il n'y a pas de compte créé pour l'authentification. Si un mot de passe n'est pas configuré les accès distants ne sont pas autorisés. Donc au départ, seul l'accès à la console est autorisé.

Il faut créer au minimum un mot de passe pour l'accès aux différents terminaux (console et virtuel) et un mot de passe pour l'accès au mode privilégié (enable).
Le mode d'administration par défaut est telnet. Par défaut, les mots de passe apparaissent en clair lors de l'affichage du fichier de configuration. Nous allons donc tout d'abord activer le service *encryption-password*, les mots de passe apparaîtront alors chiffrés lorsque les commandes d'affichage de la configuration sont entrées.

#### Activer le service *password-encryption*

```console
R1(config)#service password-encryption
```

#### Fixer la longueur du mot de passe minimum (ici 10)

```console
R1(config)#security passwords min-length 10
```

#### Créer le mote de passe *enable*

```console
R1(config)#enable secret m02p@55E
```

#### Créer un utilisateur avec mot de passe 

```console
R1(config)#username admin secret mot2passe
```

#### Créer des mots de passe et configurer de la console

```console
R1(config)#line con 0
R1(config-line)#password P@55w0rdcon5
R1(config-line)#login ou login local (utilisation d’un utilisateur local)
R1(config-line)#exec-timeout 2 (2 minutes)
R1(config-line)#exit
```

#### Créer des mots de passe et configurer des lignes virtuelles 

```console
R1(config)#line vty 0 4
R1(config-line)#password P@55w0rdcon5
R1(config-line)#login ou login local (utilisation d’un utilisateur local)
R1(config-line)#exec-timeout 2 (2 minutes)
R1(config-line)#end
```

#### Configurer le nom de domaine 

Le nom de domaine peut être fictif si vous n'en possédez pas un (par exemple turgot.lan). Attention à ne pas utiliser une extension en *.local*.

```console
R1(config)#ip domain-name turgot.lan
```

#### Générer une clé de chiffrement RSA 

```console
R1(config)#crypto key generate rsa modulus 1024
```

#### Activer la connexion ssh version 2

```console
R1(config)#ip ssh version2
```

### Commandes diverses

#### Régler l'horloge sur un routeur

Un lien pour tout savoir sur la gestion de la date et l'heure: [fastreroute](https://fastreroute.com/cisco-clock-timezone-configuration/).

Première version, manuellement:

```console
R1(config)#clock set hh:mm:ss 23 Nov 2023
R1(config)#show clock
```

Deuxième version, en utilisant un serveur NTP (Network Time Protocol). Il faut que la configuration du matériel soit complète (adresse ip/masque, passerelle, serveur DNS):

```console
R1(config)#ntp server ntp.unice.fr
R1(config)#show ntp associations
R1(config)#show clock
R1(config)#clock timezone CET +1
R1(config)#clock summer-time CEST recurring last Sun Mar 2:00 last Sun Oct 3:00
```

#### Ajouter une bannière à la connexion

```console
S1(config)#banner motd ʺAcces reserve aux personnes autoriseesʺ
```

#### Désactiver les messages systèmes sur la console

```console
S1>enable
S1#configure terminal
S1(config)#no logging console
```

#### Analyser le traffic réseau sur un ou plusieurs ports ou un VLAN

Sur un commutateur vous ne pouvez pas analyser le traffic entre deux ports ou dans un vlan à l'aide d'un ordinateur tiers. Il faut donc dupliquer le traffic à analyser sur un port libre puis connecter à celui-ci votre logiciel d'analyse (typiquement Wireshark). La commande sous Cisco iOS qui permet de dupliquer le traffic est `monitor`. La syntaxe de cette commande est donnée ci-dessous:

```
monitor session session_number
{destination
   {interface interface_id[,|-]
   [encapsulation {dot1q}]
   [ingress vlan vlan_id]
   } |
   remote vlan vlan_id reflector-port interface_id
} |
{source
  {interface interface_id[,|-][both|tx|rx]} |
  remote vlan vlan_id
}
```

Pour dupliquer le traffic d'une interface (ici fa0/1) sur une autre (ici fa0/8):

```console
S1>enable
S1#configure terminal
S1(config)#monitor session 1 source interface fa0/1 both
S1(config)#monitor session 1 destination interface fa0/8
S1(config)#end
S1#show monitor session 1
```

Pour dupliquer le traffic d'une plage d'interface, remplacez `fa0/1` par `fa0/1-4` dans la commande précédente.  
Vous pouvez aussi dupliquer tout le traffic d'un VLAN. Par exemple la commande ci-dessous capture dans la **session 2** tous le traffic du VLAN20 :

```console
S1>enable
S1#configure terminal
S1(config)#monitor session 2 source vlan20 both
S1(config)#monitor session 2 destination interface fa0/8
S1(config)#end
S1#show monitor session 2
```

Pour arrêter la duplication de traffic sur la session 1:

```console
S1>enable
S1#configure terminal
S1(config)#no monitor session 1
S1(config)#end
S1#show monitor session all
```

### Commandes liées à la gestion des VLANs

#### Créer un VLAN

```console
S1#conf t
S1(config)#
S1(config)#vlan 10  	 (crée le vlan 10 s’il n’existe pas)
S1(config-vlan)#name voip
S1(config-vlan)#exit
S1(config)#exit
S1#
```

#### Supprimer un VLAN

```console
S1(config)#no vlan 10
```

#### Visualiser les VLANs

```console
S1#show vlan
```

#### Affecter un port à un VLAN

```console
S1(config)#interface fa0/1
S1(config-if)#switchport mode access
S1(config-if)#switchport access vlan 10
S1(config-if)#no shutdown
S1(config-if)#exit
S1(config)#
```

#### Supprimer un port d'un VLAN

```console
S1(config)#interface fa0/1
S1(config-if)#no switchport access vlan 10
S1(config-if)#no switchport mode access
S1(config-if)#exit
S1(config)#
```

#### Affecter plusieurs ports à un VLAN

```console
S1(config)#interface range fa0/10-20
S1(config-if-range)#switchport mode access
S1(config-if-range)#switchport access vlan 10
S1(config-if-range)#exit
S1(config)#
```

#### Affecter une adresse IP au vlan de configuration

```console
S1(config)#int vlan 1
S1(config-if-range)# ip address 192.168.1.11 255.255.255.0
S1(config-if)#no shutdown
S1(config-if)#exit
```

#### Configurer un *port trunking*

```console
S1(config)#int range g0/1 – 2		(exemple sur interfaces G0/1 et G0/2)
S1(config-if-range)#switchport mode trunk
S1(config-if-range)#switchport trunk native vlan 99		(au lieu de vlan 1, facultatif)
S1(config-if-range)#switchport trunk allowed vlan add 10
S1(config-if-range)# show interface g0/1 switchport
S1(config-if-range)# exit

S1#show interfaces trunk		    (afficher les interfaces en mode trunk) 
S1#show interface g0/1 switchport   (montre que G0/1 est passé de port d’accès à port de jonction)
```

#### Supprimer un *trunk*

```console
S1(config)#int range g0/1 – 2		(exemple sur interfaces G0/1 et G0/2)
S1(config-if-range)#switchport mode access
S1(config-if-range)# exit

S1#show interfaces trunk		    (afficher les interfaces en mode trunk) 
```

### Retour à l'état par défaut d'un commutateur

Une [explication](https://www.cisco.com/c/en/us/support/docs/lan-switching/vlan/217969-reset-catalyst-switches-to-factory-defau.html) sur le forum de Cisco sur cette problématique.
Débranchez tous les câbles, à l'exception de la console et du câble d'alimentation. Ensuite, entrez les commandes ci-dessous :
```console
S1#erase startup-config
S1#delete vlan.dat
S1#reload
(à la question qui apparaît, répondre 'n')
```