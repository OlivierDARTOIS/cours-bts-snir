# Utilisation des capteurs sur PC (i2c et voie série) à l'aide d'un Pi Pico

!!! tldr "Objectifs de ce document"

    - programmer un Pi Pico avec un firmware permettant la liaison en USB avec un PC et l'accès à 6 voies séries et un bus i2c coté Pico,
    - configuration et vérification du fonctionnement en environnement Linux,
    - configuration et vérification du fonctionnement en environnement Microsoft Windows,
    - ajout de périphériques i2c (comme des capteurs) en environnement Linux et de périphériques séries en environnement Linux/Windows. 
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

## Récupération et programmation du firmware dans un Pi Pico

Le firmware est disponible sur le dépôt gitHub de 8086net, le firmware s'appelle [pico-sexa-uart-bridge](https://github.com/8086net/pico-sexa-uart-bridge). Il permet de se connecter en USB coté PC et propose 6 voies séries simples (TX, RX pas de broche de gestion de flux matériel) et un bus i2c suivant le protocole i2c-tiny-usb. Les ports séries ne nécessitent aucun pilote aussi bien sous Linux que sous Windows. Le bus i2c nécessite un pilote sous Linux mais qui est disponible dans la plupart des distributions. Cela permet donc d'avoir sur un PC l’accès à des voies séries supplémentaires et surtout un bus i2c sur lequel vous pourrez connecter toutes sortes de périphériques (du moment qu'il y ait un pilote pour votre périphérique dans le noyau Linux).

Vous avez deux possibilités pour récupérer le firmware:

- cloner le dépôt github puis lancer la génération d'un firmware avec un simple script : vous pouvez personnaliser le brochage sur lesquels sont disponibles les bus séries et le bus i2c en modifiant simplement un fichier (`uart-i2c-bridge.c`),
- récupérer directement le firmware au format *uf2* pour flasher le pico mais sans possibilité de personnaliser le brochage, vous aurez celui décrit sur le site.

### Récupération du firmware par défaut

Le firmware par défaut est disponible directement sur le site github en cliquant sur *Releases* dans la colonne de droite (essayer ce [lien](https://github.com/8086net/pico-sexa-uart-bridge/releases/tag/8086net-v4.1)). Téléchargez le fichier *uart_i2c_bridge.uf2* ainsi que le fichier *90-PicoUART6.rules*.

### Génération du firmware en local sur votre machine Linux

Commencez par cloner le dépôt gitHub puis lancez le script qui génère le firmware :

```console
$ git clone https://github.com/8086net/pico-sexa-uart-bridge.git
$ cd pico-sexa-uart-bridge/
$ ./build.sh
```

Soyez patient, le script va installer le SDK de la pico. Quand le script se termine, le firmware au format *uf2* est disponible dans le répertoire `build`. Le fichier *90-PicoUART6.rules* a été téléchargé lors du clonage du dépôt.

### Flasher le firmware dans le Pi Pico

Connectez votre pico sur un port USB de votre PC (Linux ou Windows) en appuyant sur le bouton *bootsel*. La pico se comporte alors comme un périphérique de stockage. Ouvrez ce périphérique et déposez-y le firmware au format *uf2*. Il sera automatiquement flashé et la pico redémarre ensuite automatiquement (sinon débrancher/rebrancher le cable USB).

## Configuration et premier test en environnement Linux

Sous Linux, vous pouvez observer les messages du noyau avec la commande `dmesg` :

```console
$ sudo dmesg
...
[28386.024896] usb 1-2: new full-speed USB device number 11 using xhci_hcd
[28386.183665] usb 1-2: New USB device found, idVendor=3171, idProduct=0060, bcdDevice= 1.00
[28386.183679] usb 1-2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[28386.183683] usb 1-2: Product: PicoUART6
[28386.183686] usb 1-2: Manufacturer: 8086 Consultancy
[28386.183688] usb 1-2: SerialNumber: E660583883449032
[28386.191289] cdc_acm 1-2:1.0: ttyACM0: USB ACM device
[28386.192354] cdc_acm 1-2:1.2: ttyACM1: USB ACM device
[28386.193283] cdc_acm 1-2:1.4: ttyACM2: USB ACM device
[28386.194160] cdc_acm 1-2:1.6: ttyACM3: USB ACM device
[28386.195020] cdc_acm 1-2:1.8: ttyACM4: USB ACM device
[28386.195732] cdc_acm 1-2:1.10: ttyACM5: USB ACM device
```

Les six ports séries sont apparus et sont disponibles à travers `/dev/ttyACMx`. Mais il manque le bus i2c. Pour cela il faut rajouter la règle udev, redémarrer le démon udev puis débrancher/rebrancher la pico. Le bus i2c doit apparaître dans les messages du noyau à condition que vous ayez le module *i2c-usb-tiny*.

```console
$ sudo cp 90-PicoUART6.rules /etc/udev/rules.d/ 
$ sudo udevadm control --reload
-> débrancher/rebrancher la pico
$ sudo dmesg
...
[28583.097219] i2c-tiny-usb 1-2:1.12: version 1.00 found at bus 001 address 012
[28583.097878] i2c i2c-13: connected i2c-tiny-usb device
...
```

Le bus i2c est apparu et il a le numéro 13 (ce chiffre peut bien sur changer sur votre PC). Pour accéder à ce bus i2c, il faut qu'il soit accessible à travers un périphérique. Pour cela vous devrez peut être chargé le module *i2c-dev* :

```console
$ sudo modprobe i2c-dev
$ ls /dev/i2c*
crw-rw---- root i2c 0 B Sun Jul 21 12:04:01 2024 /dev/i2c-0
crw-rw---- root i2c 0 B Sun Jul 21 12:04:01 2024 /dev/i2c-1
...
crw-rw---- root i2c 0 B Sun Jul 21 18:13:33 2024 /dev/i2c-13
```

Le nouveau périphérique i2c est apparu (ici i2c-13, le numéro peut changer). Le matériel à l'air fonctionnel et reconnu par le noyau. Prochaine étape, testez des périphériques séries et i2c.

## Configuration et premier test en environnement Microsoft Windows

Au branchement du pico, Windows 10/11 détecte bien les six ports séries et les numérote `COMxx`. Vous pouvez les voir dans le gestionnaire de périphériques. Les ports séries sont fonctionnelles. Par contre pour le pilote pour Windows de [i2c-tiny-usb](https://github.com/harbaum/I2C-Tiny-USB) récupéré sur le site officiel ne s'installe pas sous Windows 11 (je n'ai pas testé sous Windows 10).  
Conclusion : la pico permet de rajouter simplement six nouveaux ports séries mais pas de bus i2c.

## Mise en oeuvre de périphériques i2c et série sous Linux

Je connecte un capteur i2c de température/humidité SHT31 sur les broches du pico. L'adresse de ce périphérique est fixé à 0x44. Vous allez scanner ce bus i2c avec la commande `i2cdetect` (installer le paquet *i2c-tools* si la commande n'est pas disponible) :

```console
$ sudo i2cdetect -y 13
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:                         -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- 44 -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                       
```

Le SHT31 est bien reconnu. Un [module noyau](https://www.kernel.org/doc/html/latest/hwmon/sht3x.html) existe pour ce capteur, il s'appelle `sht3x`. Il faut charger ce module et ensuite informer le noyau que nous avons un périphérique de ce type à l'adresse 0x44. Une [méthode](https://www.kernel.org/doc/html/latest/i2c/instantiating-devices.html) est décrite dans la documentation du noyau, il faudra utiliser la méthode 4 : *Instanciate from user-space*.

```console
$ sudo modprobe sht3x
$ sudo su -
-> vous êtes passé root avec la commande précédente
# echo sht3x 0x44 > /sys/bus/i2c/devices/i2c-13/new_device
# dmesg
...
[30793.884838] i2c i2c-13: new_device: Instantiated device sht3x at 0x44
# i2cdetect -y 13        
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:                         -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- UU -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         
```

On voit que lors du scan du bus i2c le capteur est passé en *UU* ce qui indique qu'il est maintenant géré par le noyau. Cherchons à récupérer les données de température et humidité. Vous pouvez repasser simple utilisateur pour faire les commandes suivantes :

```console
$ cat /sys/bus/i2c/devices/13-0044/name 
sht3x
$ ls /sys/bus/i2c/devices/13-0044/hwmon/hwmon5/
device/             humidity1_min_hyst  temp1_max_hyst
heater_enable       name                temp1_min
humidity1_alarm     power/              temp1_min_hyst
humidity1_input     subsystem/          uevent
humidity1_max       temp1_alarm         update_interval
humidity1_max_hyst  temp1_input         
humidity1_min       temp1_max           
$ cat /sys/bus/i2c/devices/13-0044/hwmon/hwmon5/temp1_input 
21604
$ cat /sys/bus/i2c/devices/13-0044/hwmon/hwmon5/humidity1_input 
80754
```

On voit que ce capteur est gérer par le sous-système *hwmon*. la température et l'humidité sont à divisés par 1000 pour obtenir la valeur réelle.
Pour décharger le module proprement en tant qu'utilisateur root :

```console
# echo 0x44 > /sys/bus/i2c/devices/i2c-13/delete_device
```

Conclusion partielle : vous pouvez interroger un capteur sur bus i2c depuis un PC standard.

Testons maintenant un périphérique série. Je dispose d'un GPS qui envoie ces données au format 8N1 à 9600 bits/s. Je le connecte sur les broches du pico qui représente le port série ttyACM0 coté Linux. Pour cela nous allons utiliser le logiciel minicom (installer le paquet s'il n'est pas disponible) :

```console
$ minicom -b 9600 -D /dev/ttyACM0 -8
...
$GPGSV,3,3,10,24,14,250,,30,40,061,31,0*62
$BDGSV,2,1,05,14,29,140,08,24,50,059,23,25,68,197,26,33,56,146,33,0*7D
$BDGSV,2,2,05,41,67,298,34,0*41
$GNRMC,192706.000,A,4530.95959,N,00132.30496,E,0.00,8.11,220724,,,A,V*07
...
```

La réception des trames du GPS est correcte, le port série est donc validé. Chose à signaler sur ce récepteur GPS, le [*talker id*](https://fr.wikipedia.org/wiki/NMEA_0183) (les deux premières lettres après le $) change régulièrement : BD ou GB -> Beidou (Chine), GA -> Galiléo (Europe), GP -> GPS (EU), GL -> Glonass (Russie) et GN -> signaux mixés GPS+Glonass !

Conclusion : en environnement Linux cette solution apporte six ports séries supplémentaires et un bus i2c exploitable avec de nombreux périphériques sur **un PC standard**.

## Conclusion

Si vous le souhaitez vous pouvez acheter une carte fille sur [ebay](https://www.ebay.co.uk/itm/204561279568) réalisée par le site 8086.net. Il faudra soudé une pi pico sur cette carte pour la rendre fonctionnelle. Pour ma part j'utilise cette solution avec une carte [grove shield for pico](https://www.seeedstudio.com/Grove-Shield-for-Pi-Pico-v1-0-p-4846.html) et le [schema électronique](https://files.seeedstudio.com/wiki/Grove_Shield_for_Pi_Pico_V1.0/Grove_shield_for_PI_PICOv1.0SCH.pdf) de chez seeedstudio pour faciliter les connections/déconnections des périphériques.  
Pour information les tests et la compilation du firmware ont été réalisé sous Linux Mint Debian Edition 6 - Faye.