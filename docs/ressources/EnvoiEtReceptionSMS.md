# Envoyer et réceptionner des SMS avec une clé 4G sous GNU/Linux Debian

!!! tldr "Objectifs de ce document"

    - présenter la solution *smstools*,
    - choisir une clé 4G USB compatible,
    - installation et configuration de smstools,
    - envoyer et réceptionner des SMS. 
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Titre Pro A.I.S - Lycée Turgot - Limoges

## Présentation de la solution SMSTools

Nous avons comme prérequis l'utilisation d'une distribution Debian GNU Linux récente ou une de ces dérivées (Raspbian OS par exemple). Le paquet smstools propose l'installation d'un service de réception/émission de SMS robuste. Le site officiel est [https://smstools3.kekekasvi.com/](https://smstools3.kekekasvi.com/). A l'aide d'une clé USB 3G/4G par exemple, il permet d'envoyer et de recevoir des sms très facilement.

## Choix de la clé USB 3G/4G

Vous devez disposer d'une carte SIM physique et d'un abonnement à un opérateur de téléphonie mobile. La clé USB recommandée présentera lors de son insertion dans un port USB des interfaces séries sur bus USB (par ex ttyUSB0, ttyUSB1). C'est généralement le cas pour les clés un peu anciennes, les nouvelles seront vues comme une interface ethernet. Pour vérifier la présence ou l'absence de port série, affichez les messages du noyau juste après avoir connecté la clé avec la commande suivante: `dmesg`. Par exemple, vous pouvez obtenir:

```console
[  304.283977] usb 1-1.2: new high-speed USB device number 5 using dwc_otg
[  304.385158] usb 1-1.2: New USB device found, idVendor=12d1, idProduct=1506, bcdDevice= 1.02
[  304.385198] usb 1-1.2: New USB device strings: Mfr=3, Product=2, SerialNumber=0
[  304.385213] usb 1-1.2: Product: HUAWEI Mobile
[  304.385225] usb 1-1.2: Manufacturer: HUAWEI
[  304.388925] usb-storage 1-1.2:1.4: USB Mass Storage device detected
[  304.389708] scsi host0: usb-storage 1-1.2:1.4
[  304.390566] usb-storage 1-1.2:1.5: USB Mass Storage device detected
[  304.391208] scsi host1: usb-storage 1-1.2:1.5
[  305.416772] scsi 1:0:0:0: Direct-Access     HUAWEI   SD Storage       2.31 PQ: 0 ANSI: 2
[  305.417435] scsi 0:0:0:0: CD-ROM            HUAWEI   Mass Storage     2.31 PQ: 0 ANSI: 2
[  305.417862] sd 1:0:0:0: [sda] Media removed, stopped polling
[  305.418940] sd 1:0:0:0: [sda] Attached SCSI removable disk
[  305.758067] usbcore: registered new interface driver usbserial_generic
[  305.758130] usbserial: USB Serial support registered for generic
[  305.762049] usbcore: registered new interface driver cdc_ether
[  305.781189] usbcore: registered new interface driver cdc_ncm
[  305.789161] usbcore: registered new interface driver option
[  305.789244] usbserial: USB Serial support registered for GSM modem (1-port)
[  305.789489] option 1-1.2:1.0: GSM modem (1-port) converter detected
[  305.790510] usbcore: registered new interface driver cdc_wdm
[  305.791001] usb 1-1.2: GSM modem (1-port) converter now attached to ttyUSB0
[  305.791382] option 1-1.2:1.2: GSM modem (1-port) converter detected
[  305.793370] usb 1-1.2: GSM modem (1-port) converter now attached to ttyUSB1
[  305.793629] option 1-1.2:1.3: GSM modem (1-port) converter detected
[  305.795019] usb 1-1.2: GSM modem (1-port) converter now attached to ttyUSB2
[  305.816738] huawei_cdc_ncm 1-1.2:1.1: MAC-Address: 58:2c:80:13:92:63
[  305.816764] huawei_cdc_ncm 1-1.2:1.1: setting rx_max = 16384
[  305.816931] huawei_cdc_ncm 1-1.2:1.1: setting tx_max = 16384
[  305.817049] huawei_cdc_ncm 1-1.2:1.1: NDP will be placed at end of frame for this device.
[  305.817281] huawei_cdc_ncm 1-1.2:1.1: cdc-wdm0: USB WDM device
[  305.817907] huawei_cdc_ncm 1-1.2:1.1 wwan0: register 'huawei_cdc_ncm' at usb-3f980000.usb-1.2, Huawei CDC NCM device, 58:2c:80:13:92:63
[  305.818961] usbcore: registered new interface driver huawei_cdc_ncm
[  305.928435] sd 1:0:0:0: Attached scsi generic sg0 type 0
[  305.928575] scsi 0:0:0:0: Attached scsi generic sg1 type 5
[  305.966960] sr 0:0:0:0: [sr0] scsi-1 drive
[  305.967015] cdrom: Uniform CD-ROM driver Revision: 3.20
[  305.980943] sr 0:0:0:0: Attached scsi CD-ROM sr0
```

Attention il n'y a pas que la visualisation des ports série mais tout ce que le noyau linux a reconnu...

Il faut tester le port qui reçoit les commandes **AT**. Avec un logiciel type *minicom*, connectez-vous sur le premier port puis tapez la commande `AT` puis la touches *Entrée*. Si vous recevez en retour `OK`, vous êtes certainement sur le bon port, sinon recommencez avec un autre port. 

## Installation et configuration des smstools

Pour installer : `apt update` puis `apt install smstools`. Éditez ensuite le fichier `/etc/smsd.conf`. Décommentez (enlever le #) sur la ligne `loglevel = 7` puis à la fin du fichier, dans la partie `[GSM1]`, changez la ligne `device =` avec le port série que vous avez identifié auparavant (par exemple dans le cas précédent c'était `/dev/ttyUSB0`). Sauvez le fichier. **Dans cet exemple, la SIM n'a pas de code PIN configurée !**. 

Éditez ensuite le fichier `/etc/default/smstools`, modifiez alors les lignes `USER=` et `GROUP=` pour mettre `root` les deux fois. Sauvez le fichier. 

Vous allez visualiser le lancement du service smsd, pour cela vous allez afficher dans un terminal son fichier de log: `tail -f /var/log/smstools/smsd.log`. Dans un autre terminal, relancez le démon smsd : `systemctl restart smstools.service` et observez le fichier de log pour voir si tout se passe correctement.

## Envoyer et recevoir des sms

Vous allez envoyer un sms depuis un téléphone portable vers le numéro de téléphone de la SIM introduite dans le clé. Surveillez le fichier de log, vous devriez observer l'arrivée du sms. Celui-ci sera alors disponible dans le répertoire `/var/spool/sms/incoming`. Vous pouvez le visualiser avec une commande `cat`. Son contenu peut ressembler à cela :

```console
pi@RPI-Test:~ $ cat /var/spool/sms/incoming/GSM1.u8XHOQ 
From: 336xxxxxxxx
From_TOA: 91 international, ISDN/telephone
From_SMSC: 33695000638
Sent: 25-02-12 17:55:41
Received: 25-02-12 17:56:08
Subject: GSM1
Modem: GSM1
IMSI: 20815011625xxxx
IMEI: 86273101000xxxx
Report: no
Alphabet: ISO
Length: 21

Test depuis tel perso
```

Ce n'est pas le propos ici, il faudra exploiter ce fichier à l'aide d'un programme pour vérifier le numéro de l'expéditeur et/ou récupérer le message.

Pour envoyer un sms, il faut déposer un fichier dans le répertoire `/var/spool/sms/outgoing` avec un format particulier. Vous pouvez *plutôt* utiliser un script qui est fourni lors de l'installation du paquet smstools. Copiez-le dans le répertoire de votre utilisateur: `cp /usr/share/doc/smstools/examples/scripts/sendsms ~/`. Vous pouvez alors tester l'envoie de sms avec une commande comme (en tant qu'utilisateur root ou avec sudo): `./sendsms +3306xxxxxxxx "votre message ici"`. Si le sms a bien été envoyé, il sera archivé dans le répertoire `/var/spool/sms/sent` sinon il sera dans le répertoire `/var/spool/sms/failed`.

## Conclusion

Ce cours article présente l'envoi et la réception de sms avec une clé usg 4G présentant des ports séries pour sa gestion. Vous noterez qu'il y a quelques mauvaises pratiques comme le fait de faire tourner le service smsd en tant qu'utilisateur root. Je vous encourage à lire les documentations dans `/usr/share/doc/smstools` pour exécuter ce service avec des droits réduits.
