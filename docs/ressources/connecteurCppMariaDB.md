# Utilisation d'un connecteur C++ avec une base de données MariaDB/MySQL

!!! tldr "Objectifs de ce document"

    - installer le connecteur C++ pour la base de données MariaDB/MySQL (aussi bien sous GNU/Linux que sous Microsoft Windows). 
    - utiliser quelques méthodes simples du connecteur C++ à travers quelques exemples d'applications en manipulant une base de test.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Prérequis

Pour commencer il faut se rendre sur le site officiel pour lire ce que propose MySQL concernant ce [connecteur](https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-introduction.html)

Une machine de test Linux/Debian possède un serveur MySQL/MariaDB ainsi que phpMyAdmin pour faciliter la gestion des bases. La base de test s'appelle `olivier_db` et la table qui subira quelques unes de nos requêtes `animal`. Les champs de cette table sont présentés ci-dessous:
![TableAnimal](../img/TableTestAnimal.png)

Un utilisateur `olivier` a été créé avec le mot de passe `olivier`. Tous les droits lui sont donnés sur cette base et il pourra se connecter depuis n'importe quelle adresse IP.  
Remarque : c'est le même TP que la fin du TP de découverte de la raspberryPi partie 1 !

Elle contient au départ deux enregistrements :
![ContenuDepartTableAnimal](../img/ContenuTableTestAnimalAuDepart.png)

## Installation et premier test sous GNU/Linux Debian

On suppose qu'un compilateur C++ est installé (sinon faire la commande suivante: `apt install build-essential`). Pour installer notre connecteur C++ pour MySQL/MariaDB il suffit d'installer les paquets suivants :

- `libmysqlcppconn-dev` - MySQL Connector for C++ (development files)
- `libmysqlcppconn7v5`  - MySQL Connector for C++ (library)

Cela entraîne l'installation automatique de `libboost-dev`.

```console
root@casimir:~# apt install libmysqlcppconn7v5 libmysqlcppconn-dev
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances
Lecture des informations d'état... Fait
Les paquets supplémentaires suivants seront installés :
libboost-dev libboost1.67-dev
```

Rien de spécial à dire. L'installation se passe rapidement. Testons alors un premier exemple en modifiant un code donné sur le site de MySQL :

```cpp
// Test connecteur C++ MySQL
// O. Dartois, Juin 2017

#include <iostream>

// Les includes strictement nécessaire pour cet exemple
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

int main()
{
    cout << endl;
    cout << "Premier test du connecteur C++ Mysql" << endl;

    // La gestion d'erreur se fait proprement avec les exceptions
    // sinon retirer le try...catch

    try {
        // Les variables nécessaires à notre programme
        sql::Driver* driver;
        sql::Connection* con;
        sql::Statement* stmt;
        sql::ResultSet* res;

        // Etape 1 : créer une connexion à la BDD
        driver = get_driver_instance();
        // on note les paramètres classiques: adresse ip du serveur et port, login, mot de passe
        con = driver->connect("tcp://127.0.0.1:3306", "olivier", "olivier");

        // Etape 2 : connexion à la base choisie, ici olivier_db
        con->setSchema("olivier_db");

        // Etape 3 : création d'un objet qui permet d'effectuer des requêtes sur la base
        stmt = con->createStatement();

        // Etape 4 : exécution d'une requete : ici on sélectionne tous les enregistrements
        // de la table Animal
        res = stmt->executeQuery("SELECT * FROM Animal");
  
        // Etape 5 : exploitation du résultat de la requête
        while (res->next()) {
            cout << "\t... MySQL a repondu: ";
            // Acces par non du champ de la table : ici le champ 'id' que l'on recupère au format string
            cout << res->getString("id") << endl;
            cout << "\t... MySQL la suite : ";
            // Acces à la donnée par son numéro de colonne, 1 étant le premier (ici 'id'), 5 le nom de l'animal
            cout << res->getString(5) << endl;
        }

    // On nettoie tout avant de sortir : effacement des pointeurs
    // le pointeur sur le Driver sera effacé tout seul
    delete res;
    delete stmt;
    delete con;

    } catch (sql::SQLException &e) {
        // Gestion des execeptions pour déboggage
        cout << "# ERR: " << e.what();
        cout << " (code erreur MySQL: " << e.getErrorCode();
        cout << ", EtatSQL: " << e.getSQLState() << " )" << endl;
    }

    cout << endl;

    // on sort en indiquant que tout c'est bien passé
    return EXIT_SUCCESS;
}
```

Le code précédent se compile avec la ligne ci-dessous, on exécute ensuite le programme:

```console
olivier@casimir:~/dev/TestConnectionMySQLC++# g++ -o connexMySQL testconnectionmysqlcpp.cpp -Wall -lmysqlcppconn
olivier@casimir:~/dev/TestConnectionMySQLC++# ./connexMySQL 

Premier test du connecteur C++ Mysql
	... MySQL a repondu: 1
	... MySQL la suite : Diva
	... MySQL a repondu: 2
	... MySQL la suite : mirza
```

L'exécution correspond bien a ce que l'on attend : deux enregistrements, l'affichage de la valeur de l'id et du nom pour chaque résultat.

Si on met une valeur erronée pour le mot de passe, on obtient à l'exécution le message ci-dessous. Les exceptions fonctionnent correctement.

```console
olivier@casimir:~/dev/TestConnectionMySQLC++# ./connexMySQL 
Premier test du connecteur C++ Mysql
# ERR: Access denied for user 'olivier'@'localhost' (using password: YES) (code erreur MySQL: 1045, EtatSQL: 28000 )
```

Réalisons une insertion d'un enregistrement. Ci-dessous la partie de code modifié par rapport au code précédent. Attention il n'y a plus d'étape 5 :

```cpp
...
  // Etape 4 : exécution d'une requete : ici on insere un enregistrement
  // dans la table Animal
  string req = "INSERT INTO Animal VALUES(NULL,'chien','M', NOW(), 'medor', 'chien rare')";  
  int result = stmt->executeUpdate(req);
  cout << "Resultat INSERT: " << result << endl;
...
```

Cette fois-ci la requête est dans une variable de type *string*.

L'entier de retour (*result*) de la méthode `executeUpdate()` indique le nombre de lignes modifiées. Ci-dessous le résultat de trois exécution du code précédent:

![ContenuTableTestAnimalApresTroisInsertions](../img/ContenuTableTestAnimalApresTroisInsertions.png)

!!! warning "ATTENTION"
    Pour réaliser une commande `INSERT`, `UPDATE` ou `DELETE`, il faut utiliser la méthode `executeUpdate()`.  
    Pour la commande `SELECT`, la méthode `executeQuery()` car elle renvoie un type `ResultSet*`.


## Installation et premier test sous Microsoft Windows

Il faut tout d'abord lire les recommandations d'Oracle sur l'installation sous Microsoft Windows ici. On en déduit que l'on va télécharger deux archives:

- l'archive zip du connecteur c++ pour Microsoft Windows ( [lien](https://dev.mysql.com/downloads/connector/cpp/) ), choisir comme système d'exploitation Microsoft Windows puis l'archive en version 32 bits,
- la bibliothèque boost en version 7zip ( [lien](http://www.boost.org/users/history/version_1_64_0.html) ).

Je vous conseille très vivement d'installer [7Zip](https://www.7-zip.org/download.html) pour Microsoft Windows (il doit déjà être installé sur les postes) sinon vous allez passer quelques heures à décompresser l'archive de boost...  
Une fois ces deux archives décompressées sur votre disque dur (notez les chemins), il faut créer votre projet avec Microsoft Visual Studio.

- Créer un nouveau projet Visual C++, Choisissez Win32 puis Application console Win32. Faire un projet vide (décochez en-tête pré-compilé puis cochez Projet vide),
- **TRÈS IMPORTANT**: choisissez le mode *Realease* avant de faire les deux points suivants (les tests en mode Debug ont échoué...),
- Faites un clic droit sur votre projet dans la fenêtre explorateur de solutions. Sélectionnez *Ajouter* puis *Élément existant...*. Cherchez dans votre répertoire ou vous avez désarchivé le connecteur c++, le fichier `mysqlcppconn.lib` puis ajoutez-le,
- Faites un clic droit sur votre projet dans la fenêtre explorateur de solutions. Sélectionnez *Propriétés* puis *Répertoire VC++* puis l'item *Répertoires Include*. Rajoutez alors deux chemins : un vers la librairie Boost (par exemple: `c:\xxx\yyy\boost_1_64_0`) et un vers la librairie mysql-connector (par exemple `c:\xxx\zzz\mysql-connector-c++-noinstall-1.1.9-win32\include`).

Nous allons utiliser exactement le même code que sous GNU/Linux, donc copier-collez le dans un nouveau fichier cpp de votre projet (pour moi, ça sera *source.cpp*). La seule modification de code est l'adresse ip dans la méthode `connect()` :

```cpp
// Adresse ip du serveur de BDD : 192.168.1.107 : Debian en WiFi
con = driver->connect("tcp://192.168.1.107:3306", "olivier", "olivier");
```

Compilez et exécutez le code. Si tout se passe bien vous obtiendrez la même sortie que l'exécutable sous GNU/Linux.

Si vous n'arrivez pas à vous connecter, il est possible que le serveur MariaDB/MySQL n'accepte pas les connexions distantes. Sous Debian, éditez le fichier `/etc/mysql/my.cnf` puis commentez (mettre un `#` devant la ligne) la ligne `bind-address=` puis relancez le service mariaDB/MySQL.

## Conclusion

Dans la mesure du possible, il faudra désormais utiliser cette bibliothèque dans les futurs codes de projets ou de TPs. Pour aller plus loin dans l'exploration de cette bibliothèque, il faut télécharger les sources car elles contiennent un répertoire *examples*. La description des exemples est disponible [ici](https://docs.oracle.com/cd/E17952_01/connector-cpp-en/connector-cpp-getting-started-examples.html).
