# Utilisation des capteurs sur RaspberryPi

!!! tldr "Objectifs de ce document"

    - choisir son capteur en fonction de son support dans le **Device Tree**,
    - réaliser les modifications du fichier **config.txt**,
    - vérifier le fonctionnement du capteur avec la ligne de commande,
    - réaliser une classe C++ de gestion du capteur.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

## Présentation rapide des bus de communication

Il est préférable d'utiliser maintenant des capteurs *intelligents*. Ce sont des capteurs qui utilisent un bus de communication pour renvoyer leurs données.
Parmi les bus existants, nous allons en présenter trois, tous disponibles sur les cartes RPi (et la majeure partie des cartes *SBC* ainsi que sur de nombreux micro-contrôleurs) :

 - le bus série : en version TTL (0V / 3.3V) ou rs232 (+12V / -12V). Ce bus nécessite trois lignes au minimum pour communiquer : TXD (Transmit Data), RXD (Reception Data) et GND, liaison full-duplex de type asynchrone avec un débit généralement faible (le classique 115200 bits/s),
 - le bus SPI : c'est un bus série synchrone full-duplex, généralement haute vitesse (jusqu'à 40MHz). Ce bus utilise généralement une ligne d'horloge (SCLK), une ligne MOSI (Master Out Slave In), une ligne MISO (Master In Slave Out) et une ou plusieurs ligne de sélection de boîtiers CSx ou CEx (Chip Select ou Chip Enable). Ce bus sur la RPi est généralement utilisé avec des périphériques hautes vitesses comme des convertisseurs analogique/numériques (CAN) ou des écrans couleurs de faible dimension,
 - le bus I2C : c'est un bus série synchrone half-duplex utilisant le protocole de dialogue *maitre/esclave* et des adresses pour les esclaves. Les fréquences d'horloge couramment utilisées sont le 100KHz et le 400KHz. Ce bus utilise deux lignes bidirectionnelles (sans compter l'alimentation et la masse) : la ligne SCL (Signal CLock) et la ligne SDA (Signal Data). De très nombreux périphériques utilisent ce bus car il est facile d'utilisation, peu onéreux et les débits de données sont certes faibles mais suffisants pour la plupart des cas.

Vous pouvez activer chacun de ces bus en utilisant le logiciel **raspi-config** (il faut l'exécuter avec `sudo`) dans le menu **Interface Options**.

L'activation de ces bus conduira à la création de périphérique dans le répertoire `/dev` :

- `/dev/ttyS0` : voie série matérielle disponible sur les GPIO14 (TXD) et GPIO15 (RXD),
- `/dev/i2c-1` : bus i2c numéro 1 disponible sur les GPIO2 (SDA) et GPIO3 (SCL),
- `/dev/spidev0.x` : bus spi numero 0 disponible sur les GPIO10 (MOSI), GPIO9 (MISO), GPIO11 (SCLK) et deux lignes d'activation GPIO8 (CE0 -> `spidev0.0`) et GPIO7 (CE1 -> `spidev0.1`).

!!! tip "Brochage du raspberryPi"
    Un excellent site qui présente le brochage du raspberryPi de manière interactive est [pinout](https://pinout.xyz/). De plus il présente aussi les broches utilisées par de nombreux *hats* (cartes additionnelles qui se connectent sur les broches de la RPi). Vous pouvez aussi visualiser le brochage de la rpi sur laquelle vous travaillez en tapant la commande `pinout`.

!!! info "Remarque importante pour les raspberryPi4+"
    Vous pouvez obtenir plusieurs voies séries, plusieurs bus i2c et plusieurs bus spi sur une seule et même carte. Tous ne seront pas activables en même temps. Ces différents bus complémentaires s'activent en modifiant le contenu du fichier `/boot/config.txt`. La description et l'activation de ces différents bus est lisible sur [le dépôt GitHub](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README) de la fondation raspberrypi.

## Choisir son capteur et assurer sa gestion par le noyau Linux

Si vous le pouvez vous devez prendre un capteur qui est directement supporté par le noyau GNU/Linux et dont le chargement dans le noyau s'effectue par l'intermédiaire
d'un *Device Tree*. Pour cela il faut consulter le fichier **README** dans `/boot/overlays` (ou en ligne sur [GitHub](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README)). Par exemple si vous disposez d'un capteur de température/humidité HTU21D sur bus i2c, effectuez une recherche dans ce fichier avec le mot clé *htu*. Vous obtiendrez alors la réponse suivante:

```console
htu21                   Select the HTU21 temperature and humidity sensor
```

qui fait partie de la section suivante:

```console
[...]
Name:   i2c-sensor
Info:   Adds support for a number of I2C barometric pressure, temperature,
        light level and chemical sensors on i2c_arm
Load:   dtoverlay=i2c-sensor,<param>=<val>
Params: addr                    Set the address for the BH1750, BME280, BME680,
                                BMP280, BMP380, CCS811, DS1621, HDC100X, JC42,
                                LM75, MCP980x, MPU6050, MPU9250, MS5637, MS5803,
                                MS5805, MS5837, MS8607, SHT3x or TMP102
[...]
```

Donc le capteur htu21d est bien supporté dans le noyau linux et le *device tree*. Le capteur fait partie de la famille *i2c-sensor* et ne prend pas d'argument particulier. La ligne a ajouter à la fin du fichier `/boot/config.txt` sera donc `dtoverlay=i2c-sensor,htu21`.

**AVANT** de rajouter cette ligne, il faut connecter le capteur sur le bus i2c (à l'aide de fils ou avec une liaison de type *grove* qui simplifie beaucoup le câblage). Il faut activer le bus i2c si vous ne l'avez pas déjà fait. Installez le paquet `i2c-tools` pour avoir un ensemble de logiciels qui permettrons de dialoguer avec le bus i2c. Vous allez pouvoir parcourir le bus à la recherche des adresses des esclaves présent sur celui-ci. Ci-dessous l'exécution de la commande `i2cdetect -a -y 1` **SANS** capteur connecté (dans notre cas capteur htu21d non présent) :

```console
$ i2cdetect -a -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

Même commande en **connectant** un capteur (le htu21d donc) :

```console
$ i2cdetect -a -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --  
```

Vous pouvez donc savoir si le capteur est reconnu et à quelle adresse il répond (ici 0x40 car c'est de l'hexadécimal). Rajoutez maintenant la ligne vu précédemment dans le fichier *config.txt* avec la commande : `sudo nano /boot/config.txt`. Redémarrez la RPi. Puis refaites la commande `i2cdetect -a -y 1` :

```console
$ i2cdetect -a -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

Vous voyez que le capteur est maintenant géré par le noyau Linux (caractères UU dans la commande précédente). Vous êtes sur la bonne voie, voyons maintenant comment l'interroger en ligne de commande. 

Les capteurs vont *exposer* leurs données à travers des *pseudo-fichiers* dans une arborescence virtuelle gérée par le noyau (cette arborescence ne prend aucune place sur le disque dur puisqu'en fait elle est entièrement en mémoire, les fichiers n'en sont donc pas même si vous allez les utiliser comme si c'était de vrais fichiers). Les capteurs sont souvent accessibles par `/sys/bus/iio/devices/`, `/sys/bus/i2c/devices/` mais il y aura sans doute d'autres chemins possibles. Dans le cas du capteur htu21d, vous pouvez avoir ceci (le numéro de *device* peut changer) :

```console
$ ls /sys/bus/iio/devices/iio\:device0/
battery_low                   power/
heater_enable                 sampling_frequency
in_humidityrelative_input     sampling_frequency_available
in_temp_input                 subsystem/
```

Vous pouvez remarquer deux pseudo-fichiers : `in_humidityrelative_input` et `in_temp_input`. Visualisons le contenu de ces pseudo-fichiers avec la commande `cat` :

```console
$ cat /sys/bus/iio/devices/iio\:device0/in_temp_input 
19227
$ cat /sys/bus/iio/devices/iio\:device0/in_humidityrelative_input 
74370
```

Vous remarquez que la température et l'humidité sont à diviser par 1000 si vous voulez obtenir les valeurs réelles : 19,227°C et 74,370%HR. Vous venez d'interroger un capteur en ligne de commande. Vous pouvez donc utiliser des capteurs dans des scripts shell !

## Réaliser une classe C++ pour interroger le capteur

Il faut tout d'abord savoir utiliser la bibliothèque `fstream`. Pour cela je vous renvoie à l'[exemple de code](../snippets/index.html#lecture-dun-fichier-texte) présent sur l'article des *snippets*. Commencez par faire un programme simple en C++ (pas une classe) qui permet d'aller lire dans les pseudo-fichiers du capteur et afficher la valeur lue sur une console. Dans un deuxième temps, une fois que vous avez compris comment lire les données du capteur, vous allez faire une classe en réfléchissant sur quels attributs sont nécessaires, le constructeur doit-il avoir des arguments ou pas, les *getters* nécessaires. Réalisez un test unitaire de votre classe. Enfin, il serait préférable que l'interrogation du capteur se fasse dans un *thread* (en arrière plan) et que la demande d'une valeur du capteur par l'intermédiaire d'un *getter* retourne la valeur instantanément.
