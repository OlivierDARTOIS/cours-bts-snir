# Comment réaliser des documents avec MkDocs+Material

!!! tldr "Objectifs de ce document"

    - configurer une distribution Debian (ou dérivée) pour réaliser des documents en MkDocs+Material
    - réaliser le déploiement automatique sur un dépôt GitLab.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Installation en local

1. Installez une distribution GNU/Linux Debian (ici la version 12 Bookworm ou l'une de ces dérivées: ubuntu, linux mint),
2. Installez **pip** pour python : `sudo apt install python3-pip`
3. Je n'utilise pas d'environnement virtuel pour python donc je vais *casser* le système de gestion de packages avec la commande qui suit : `sudo pip3 install --upgrade pip --break-system-packages`
4. Installez les paquets suivants avec la commande pip3: `sudo pip3 install mkdocs-material mkdocs-material-extensions selenium mkdocs-page-pdf mkdocs-graphviz mkdocs-pdf-export-plugin mkdocs-kroki-plugin --break-system-packages`

## Synchronisation avec un dépôt GitLab

1. Installez le logiciel Git : `sudo apt install git`
2. Créez un répertoire pour héberger les documents et vous positionnez dans celui-ci,
3. Configurez votre commande git : `git config --global user.email "olivier.dartois@bidule.com"` et `git config --global user.name "Olivier DARTOIS"`
4. Créez une paire de clé ssh pour vous faire reconnaître sur le dépôt git : `ssh-keygen -t ed25519 -C "Cle pour site Gitlab cours BTS"`
5. Ajoutez cette paire de clés à votre trousseau : `ssh-add ~/.ssh/siteBTS_Gitlab`
6. Visualisez la clé publique : `nano ~/.ssh/siteBTS_Gitlab.pub` puis copiez-là,
7. Connectez-vous sur votre dépôt Gitlab et ajoutez la clé publique précédente dans celles autorisées à se connecter (menu *Preferences* puis *SSH Keys*),
8. Vérifiez que vous pouvez vous connecter sur Gitlab : `ssh -T git@gitlab.com`
9. Clonez le dépôt public actuel : `git clone git@gitlab.com:OlivierDARTOIS/cours-bts-snir.git` et déplacez-vous dans le répertoire nouvellement créé,
10. Pour envoyer vos modifications vers le dépôt, déroulez les trois commandes suivantes : `git add .` puis `git commit -m "Mon premier commit sur ce depot"` puis `git push`. Une fois la commande *push* effectuée, la création du site se réalise automatiquement (chaîne *Continuous Integration*),
11. Pour mettre à jour la copie locale du dépôt, positionnez-vous dans le répertoire local du dépôt puis tapez la commande `git fetch`
