# Utilisation et programmation du bus I2C sur RaspberryPi

!!! tldr "Objectifs de ce document"

    - savoir utiliser les utilitaires en ligne de commandes i2c pour dialoguer avec un périphérique,
    - utiliser la bibliothèque PiGpio en C/C++ pour dialoguer avec un périphérique,
    - utiliser les fonctions C du noyau Linux pour dialoguer avec un périphérique.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

## Présentation rapide du bus de communication i2c

Il est préférable d'utiliser maintenant des capteurs et/ou actionneurs *intelligents*. Ce sont des capteurs/actionneurs qui utilisent un bus de communication pour envoyer/recevoir leurs données. Parmi les bus existants, ce document va présenter le bus I2C (Inter Integrated Circuit). Je vous conseille de commencer par lire l'article [Wikipédia](https://fr.wikipedia.org/wiki/I2C) sur ce bus et une [documentation PDF](https://bootlin.com/pub/conferences/2022/elce/ceresoli-basics-of-i2c-on-linux/ceresoli-basics-of-i2c-on-linux.pdf) issue de la société [BootLin](https://bootlin.com/).

Le bus I2C : c'est un bus série synchrone half-duplex utilisant le protocole de dialogue *maitre/esclave* et des adresses pour les esclaves. Les fréquences d'horloge couramment utilisées sont le 100KHz et le 400KHz. Ce bus utilise deux lignes bidirectionnelles (sans compter l'alimentation et la masse) : la ligne SCL (Signal CLock) et la ligne SDA (Signal Data). De très nombreux périphériques utilisent ce bus car il est facile d'utilisation, peu onéreux et les débits de données sont certes faibles mais suffisants pour la plupart des cas.

Vous pouvez activer le bus i2c en utilisant le logiciel **raspi-config** (il faut l'exécuter avec `sudo`) dans le menu **Interface Options**.
L'activation de ces bus conduira à la création d'un périphérique dans le répertoire `/dev` : `/dev/i2c-1` : bus i2c numéro 1 disponible sur les GPIO2 (SDA) et GPIO3 (SCL).

!!! tip "Brochage du raspberryPi"
    Un excellent site qui présente le brochage du raspberryPi de manière interactive est [pinout](https://pinout.xyz/). De plus il présente aussi les broches utilisées par de nombreux *hats* (cartes additionnelles qui se connectent sur les broches de la RPi). Vous pouvez aussi visualiser le brochage de la rpi sur laquelle vous travaillez en tapant la commande `pinout`.

!!! info "Remarque importante pour les raspberryPi4+"
    Vous pouvez obtenir plusieurs bus i2c matériel sur une seule et même carte. Tous ne seront pas activables en même temps. Ces différents bus complémentaires s'activent en modifiant le contenu du fichier `/boot/config.txt`. La description et l'activation de ces différents bus est lisible sur [le dépôt GitHub](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README) de la fondation raspberrypi.

## Installation des outils i2c et gestion de périphériques i2c en ligne de commande

Installez le paquet `i2c-tools` pour avoir un ensemble de logiciels qui permettrons de dialoguer avec le bus i2c : `sudo apt install i2c-tools`  
Listez les contrôleurs de bus i2c disponibles : `i2cdetect -l`. Ci-dessous sur un RPi4 dans sa configuration par défaut :

```console
$ i2cdetect -l
i2c-1	i2c       	bcm2835 (i2c@7e804000)          	I2C adapter
i2c-20	i2c       	fef04500.i2c                    	I2C adapter
i2c-21	i2c       	fef09500.i2c                    	I2C adapter
```
Vous utiliserez généralement le bus `i2c-1`.  

Détectez les périphériques sur le bus i2c : `sudo i2cdetect -a -y 1`  
Vous pouvez obtenir une sortie comme celle-ci :

```console
$ i2cdetect -a -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00: -- -- -- 03 04 -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- 3e -- 
40: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- 62 -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: 70 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

Là ou un numéro apparaît, c'est qu'il existe un périphérique. Les lettres `UU` indique que cette adresse est utilisée par un périphérique géré par le noyau Linux.  
La commande `i2cget` permet de lire la valeur d'un registre 8 bits interne d'un périphérique. La commande `i2cset` permet d'écrire une valeur 8 bits dans un registre. Ces deux commandes sont très simples et peuvent dans certains cas suffire à dialoguer avec un périphérique. Cependant il est préférable d'utiliser la commande `i2ctransfer` beaucoup plus complète. Nous allons la détailler maintenant.

La forme générale d'utilisation de la commande `i2ctransfer` est la suivante:

```console
i2ctransfer <bus_number> <r|w><length>@<address> <data>
```

- bus_number : le numéro du bus i2c concerné, généralement 1,
- r|w : opération de lecture (r) **OU** d'écriture (w),
- length : le nombre d'octets à émettre ou recevoir,
- address : l'adresse du périphérique sur le bus i2c,
- data : les données à envoyer si on fait une opération d'écriture

Deux exemples sont donnés ci-dessous. Le premier est une lecture de 1 octet sur le bus numéro 1 avec l'adresse de périphérique 0x52. Le deuxième est un exemple d'écriture de 3 octet sur le bus numéro 1 avec l'adresse de périphérique 0x13, les valeur des octets a écrire sont 0xFE, 0xAA, 0x08.

```console
$ i2ctransfer 1 r1@0x52
$ i2ctransfer 1 w3@0x13 0xFE 0xAA 0x08
```

Vous pouvez aussi enchaîner des commandes `i2ctranfer` de deux manières différentes :

- une commande *write*, une pause (facultatif), une commande *read*. Par exemple dans le cas de la lecture des données d'un *nunchuck* : `$ i2ctransfer -v -y 1 w1@0x52 0x00 && sleep 0.5 && i2ctransfer -v -y 1 r6@0x52`. Le paramètre `-v` (verbose) permet d'avoir des informations complémentaires sur ce qu'effectue la commande. La pause est ici de 0.5s.
- une commande *write/read*. La même commande que ci-dessus en une fois (ATTENTION ! ne fonctionne pas sur la nunchuck, c'est juste pour l'exemple) : `$ i2ctransfer -v -y 1 w1@0x52 0x00 r6`.

Donc sans programmation et avec la documentation du périphérique vous devez pouvoir dialoguer avec un périphérique quelconque i2c. Vous pouvez alors ensuite passer à la partie programmation car vous savez que le coté *hardware* fonctionne ainsi que le *software* bas niveau : la commande `i2ctransfer`.

## Utilisation de la bibliothèque **piGpio**

La bibliothèque piGpio est disponible sur ce [site](http://abyz.me.uk/rpi/pigpio/). L'inconvénient de cette bibliothèque est qu'elle doit s'exécuter avec des droits *root* ou avec la commande *sudo*. La partie concernant le bus i2c débute en suivant ce [lien](http://abyz.me.uk/rpi/pigpio/cif.html#i2cOpen) par la fonction `i2cOpen`. Vous disposez de **20** fonctions de gestion du bus i2c. Généralement un échange sur le bus i2c se déroule de la manière suivante:

- **ouverture** du bus en lecture/écriture : fonction `i2cOpen()` : il faudra préciser le numéro du bus i2c et l'adresse de l'esclave. Pensez à stocker le code retour et tester celui-ci. S'il est négatif il y a eu un problème sinon il s'agit de l'identifiant de dialogue qu'il faudra utiliser dans les autres fonctions,
- **écriture** vers le périphérique : écrire un octet `i2cWriteByte()`, écrire un octet dans un *registre* `i2cWriteByteData()`,
- **lecture** depuis le périphérique : lecture d'un octet `i2cReadByte()`, lecture d'un *mot* (word en anglais) donc 2 octets `i2cReadWordData()`,
- **fermeture** du bus : fonction `i2cClose()`. Pensez à vérifier le code retour.

Afin de présenter un exemple de code nous allons reprendre la manette nunchuck dont l'adresse est 0x52. Quelques précisions sur la mise en oeuvre de cette manette. Il faut envoyer deux fois deux octets pour initialiser la manette : 0xF0, 0x55 puis une pause de 1 ms puis 0xFB, 0x00. Ensuite pour lire les données de la manette, il faut envoyer 0x00 puis lire les 6 octets de résultat. Le découpage de ces 6 octets se présente comme ci-dessous:

- octet 1 : Joystick – Axe X : un octet donc de 0 à 255
- octet 2 : Joystick – Axe Y : un octet donc de 0 à 255
- octet 3 : Accéléromètre – Axe X : un octet représentant les 8 bits de poids fort sur les 10 bits possibles, si on utilise que cet octet on aura des valeurs de 0 à 255
- octet 4 : Accéléromètre – Axe Y : 8 bits de poids fort sur les 10 bits possibles : de 0 à 255
- octet 5 : Accéléromètre – Axe Z : 8 bits de poids fort sur les 10 bits possibles : de 0 à 255
- octet 6 : du MSB au LSB : 2 bits pour l’axe Z de l’accéléromètre, concaténé avec les 8 bits précédents vous pouvez obtenir 10 bits de résolution soit des valeurs de 0 à 1023. 2 bits pour l’axe Y de l’accéléromètre, 2 bits pour l’axe X de l’accéléromètre. 1 bit qui représente l’état du bouton C : 0:enfoncé, 1:relâché.  1 bit qui représente l’état du bouton Z : 0:enfoncé, 1:relâché.

Dans le code ci-dessous, nous allons nous contenter d'afficher les valeurs des 6 octets mais pas de les analyser : c'est un exercice à faire individuellement !

```cpp linenums="1"
// Récupération des données d'une manette nunchuck connectée à un bus i2c
// O. DARTOIS, Nov. 2023
// Compilation : g++ -o testNunchuck testNunchuck.cpp -Wall -lpigpio

#include <iostream>
#include <pigpio.h>

const unsigned int ADDR_NUNCHUCK = 0x52;

int main()
{
    if (gpioInitialise() < 0) {
        std::cout << "Erreur initialisation piGpio" << std::endl;
        return -1;
    }

    int fdI2C;
    if ( (fdI2C = i2cOpen(1, ADDR_NUNCHUCK, 0)) < 0 ) {
        std::cout << "Erreur d'ouverture du bus i2c" << std::endl;
        return -2;
    }

    if (i2cWriteByteData(fdI2C, 0xF0, 0x55) < 0) {
        i2cClose(fdI2C);
        std::cout << "Erreur ecriture bus i2c" << std::endl;
        return -3;
    }

    gpioSleep(PI_TIME_RELATIVE, 0, 1000);  // Attente 1ms

    if (i2cWriteByteData(fdI2C, 0xFB, 0x00) < 0) {
        i2cClose(fdI2C);
        std::cout << "Erreur ecriture bus i2c" << std::endl;
        return -4;
    }

    // Attention pas de vérification des appels dans la boucle ci-dessous
    char dataNunchuck[6];  // le nunchuck retourne 6 octets de données
    for (unsigned int i=0; i<3000; i++) {
        i2cWriteByte(fdI2C, 0x00);  // Demande de lecture des données
        i2cReadDevice(fdI2C, dataNunchuck, 6);
        std::cout << std::hex <<
            "o1:" << static_cast<int>(dataNunchuck[0]) << " " <<
            "o2:" << static_cast<int>(dataNunchuck[1]) << " " <<
            "o3:" << static_cast<int>(dataNunchuck[2]) << " " <<
            "o4:" << static_cast<int>(dataNunchuck[3]) << " " <<
            "o5:" << static_cast<int>(dataNunchuck[4]) << " " <<
            "o6:" << static_cast<int>(dataNunchuck[5]) << " " <<
            "\r"  << std::flush;
        gpioSleep(PI_TIME_RELATIVE, 0, 20000);  // pause de 10ms
    }

    std::cout << std::dec << std::endl;
    i2cClose(fdI2C);
}
```

Ce code n'est pas volontairement très optimisé, à vous de l'améliorer et de le transformer en une classe de gestion de la manette nunchuck.

## Utilisation des fonctions i2c du noyau Linux

Vous pouvez aussi utiliser les fonctions natives de gestion du bus i2c fourni par le noyau. Pour l'instant cette partie n'est pas disponible...