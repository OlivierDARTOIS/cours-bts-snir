# Liste des logiciels utilisés dans la section de BTS SNIR/CIEL du lycée Turgot - Limoges

!!! tldr "Objectifs de ce document"

    - présenter une liste classé par ordre alphabétique des logiciels utilisés dans la section de BTS,
    - les logiciels peuvent être libre, opensource, gratuit, payant, etc... en environnement GNU/Linux ou Microsoft Windows. Consultez l'[article](https://fr.wikipedia.org/wiki/Licence_de_logiciel) sur Wikipédia qui présente différentes licences logicielles.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Liste des logiciels (Sept 2023)

1. [7Zip](https://www.7-zip.org/download.html) : **GNU LGPL**, compression/décompression d'archives de tout types (p7zip sous GNU/Linux),
1. [Audacity](https://www.audacityteam.org/) : **GNU GPL v2**, analyse, conversion, génération, traitement de tout type d'échantillon sonore en mode graphique,
1. [BalenaEtcher](https://etcher.balena.io/) : **Apache licence v2**, logiciel de transfert d'un fichier image vers une carte SD (par ex. création des cartes SD pour RaspberryPi),
1. [Chrome](https://www.google.com/intl/fr_fr/chrome/) : **gratuiciel, propriétaire**, Navigateur WEB de Google,
1. [com0com](https://com0com.sourceforge.net/) : **GNU GPL**, création de paires de port COM virtuels en environnement Microsoft Windows,
1. [Filezilla](https://filezilla-project.org/) : **GNU GPL v3**, transfert de fichiers, principalement par FTP,
1. [Google Earth Pro](https://www.google.com/intl/fr/earth/versions/#earth-pro) : **gratuiciel, propriétaire**, visualisation de fichiers KML sur le globe terrestre,
1. [Gpsfeed+](https://gpsfeed.sourceforge.io/) : **GNU GPL**, simulation d'un GPS avec envoie de trame GPRMC vers une voie série virtuelle,
1. [Herkules TCP Client/Serveur](https://www.hw-group.com/software/hercules-setup-utility) : **freeware**, gestion de client TCP/UDP et de serveur TCP/UDP,
1. [HxD](https://mh-nexus.de/en/hxd/) : **HxD licence**, éditeur hexadécimal gratuit pour une utilisation privé et commerciale,
1. [LibreOffice](https://fr.libreoffice.org/download/telecharger-libreoffice/) : **GNU LGPL**, suite de logiciels : traitement de textes, tableau, présentation et dessin,
1. [MagicDraw](https://www.3ds.com/products-services/catia/products/no-magic/magicdraw/) : **commerciale, CATIA, Dassault Systemes**, création de diagrammes SysML et UML,
1. [MindView](https://www.matchware.com/fr/logiciel-de-mind-mapping) : **commerciale, Matchware**, création de cartes mentales,
1. [Notepad++](https://notepad-plus-plus.org/) : **GNU GPL**, éditeur de texte avancé,
1. [Paint.net](https://www.getpaint.net/) : **Paint.net licence**, dessin 2D au format bitmap gratuit pour une utilisation privé et commerciale,
1. [Packet Tracer](https://www.netacad.com/fr/courses/packet-tracer) : **gratuiciel, propriétaire**, simulation de réseaux informatiques. La version 5 ne nécessite pas de compte NetAcad contrairement à la version 8. Ces deux versions sont disponibles sur toutes les machines,
1. [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/) : **MIT licence**, prise de main à distance d'un serveur en utilisant le protocole SSH,
1. [RaspberryPi Imager](https://www.raspberrypi.com/software/) : **Apache Licence v2**, création de cartes SD amorçables et personnalisables pour RaspberryPi,
1. [Realterm](https://sourceforge.net/projects/realterm/) : **BSD licence**, communication sur la voie série,
1. [Regressi](http://regressi.fr/WordPress/download/) : **GNU GPL**, logiciel de traitement des données expérimentales,
1. [Sumatra PDF Reader](https://www.sumatrapdfreader.org/free-pdf-reader) : **GNU GPL v3**, lecteur rapide et léger de fichiers au format PDF,
1. [Teraterm](http://ttssh2.osdn.jp/index.html.en) : **BSD licence**, communication sur la voie série, client SSH,
1. [Veyon](https://veyon.io/fr/) : **GNU GPL**, prise en main/visualisation à distance d'un ordinateur,
1. [VirtualBox](https://www.virtualbox.org/) + Extensions : **GNU GPL v3**, création et gestion de machines virtuels sur ordinateur personnel,
1. [Visual Studio](https://visualstudio.microsoft.com/fr/vs/community/) 2022 avec extension C++/CLI et C++/linux : **[OSI licence](https://visualstudio.microsoft.com/fr/license-terms/vs2022-ga-community/)**, environnement de développement intégré principalement utilisé pour développer en C/C++,
1. [VLC](https://www.videolan.org/vlc/index.fr.html) : **GNU GPL**, visualisation/conversion de tout type de fichiers audio/vidéo,
1. [VSCode](https://code.visualstudio.com/) avec extensions: **[VSCode licence](https://code.visualstudio.com/License/)**, éditeur de texte avancé avec de très nombreuses extensions :
    - C++ Intellisense : autocomplétion pour le langage C++,
    - Remote developpement : développement à distance à travers un tunnel SSH,
    - Markdown All in One : gestion du langage markdown pour les documentations,
    - PHP inteliphense : autocomplétion et gestion du langage PHP,
    - Plant UML : réaliser des diagrammes UML en langage textuel,
    - PlatformIO : EDI orienté pour la compilation et le déploiement sur micro contrôleurs :
        - Atmel AVR UNO,
        - ESP8266 et ESP32,
        - RP2040 (Pi Pico).
1. [Windirstat](https://windirstat.net/) : **GNU GPL v2**, visualisation de l'occupation des fichiers sur disques durs en mode graphique (KDirStat sous GNU/Linux),
1. [WinSCP](https://winscp.net/eng/index.php) : **GNU GPL**, transfert de fichiers entre machines supportant les protocoles suivants : FTP, FTPS, SCP, SFTP, WebDAV ou S3,
1. [WireShark](https://www.wireshark.org/) : **GNU GPL v2**, capture et analyse de protocoles réseaux.
