# Collection de "snippet" en langage C++ (version 11 minimum)

!!! tldr "Objectifs de ce document"

    - vous trouverez dans ce document des courts extraits de code à mettre en oeuvre dans vos codes,
    - la plupart du temps vous devrez les adapter à votre contexte,
    - ces extraits de code nécessite au moins C++11 (normalement géré par tous les compilateurs modernes)
    - **snippets** : définition anglaise : *a small and often interesting piece of news*. Appliqué au domaine de l'informatique : *des courts, et souvent utiles, extraits de code*.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

Pour tester ces extraits de codes rapidement, vous pouvez utiliser un compilateur C++ en ligne. Je conseille d'utiliser [cpp.sh](https://cpp.sh/) : simple d'utilisation et très rapide !

## Exemple basique : le *hello world*

Cet exemple met en oeuvre l'affichage d'un message sur une console texte. C'est l'exemple basique qui est utilisé pour tester une chaîne de compilation.

```cpp
// Exemple "HelloWorld" en C++
// O. Dartois, juillet 2020

// Les includes nécessaires à ce programme
#include <iostream>     // utilisation de std::cin et std::cout

int main() 
{
    std::cout << "Hello SNIR" << std::endl;
    return 0;
}
```

Sortie console :
>Hello SNIR

## Déclaration d'une variable et saisie utilisateur

Ce programme demande à l'utilisateur de saisir un nombre entier, ajoute 10 à celui-ci et affiche le résultat. Utilisation de `std::cout`, `std::cin` et déclaration d'une variable de type entier.

```cpp
// Demande à l'utilisateur de saisir un nombre entier
// puis affiche celui-ci plus 10
// O. Dartois, juillet 2020

// les "include" nécessaire au programme
#include <iostream>     // pour std::cin et std::cout

int main()
{
    // Affichage sur la console de la demande
    std::cout << "Saisissez un nombre entier : ";
    // déclaration de la variable de type entier nb
    // cette variable est initialisée à 0
    int nb = 0;
    // lecture au clavier de la valeur entière et affectation à la variable nb
    std::cin >> nb;
    // Ajout de 10 unités à la variable nb
    nb += 10;
    // Affichage du résultat
    std::cout << "Votre nombre plus 10 : " << nb << std::endl;
    return 0;
}
```

Sortie console :
>Saisissez un nombre entier : 34  
>Votre nombre plus 10 : 44

## Faire une pause dans un programme

Il existe sous Microsoft Windows une fonction pour faire une pause (inclure `windows.h` puis utiliser la fonction `Sleep(xx)` : attente de xx milli-secondes) ainsi que sous GNU/Linux (inclure `unistd.h` puis utiliser la fonction `sleep(xx)` : attente de xx seconde(s) ).

Il faut maintenant utiliser une méthode portable quelque soit les environnements. Vous trouverez ci-dessous une solution à reprendre dans vos codes.

```cpp
#include <iostream>
#include <chrono>
#include <thread>

int main()
{
    
    std::cout << "Pause de 3s" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::cout << "Fin pause de 3s" << std::endl;
    
    std::cout << "Pause de 30ms" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    std::cout << "Fin pause de 30ms" << std::endl;

    return 0;
}
```

Sortie console :

> Pause de 3s  
> Fin pause de 3s  
> Pause de 30ms  
> Fin pause de 30ms

## Tirage d'un nombre au hasard

Utilisation de la classe `random` pour générer des nombres entiers ou flottants au hasard. Notez que vous pouvez utiliser différentes distributions pour le tirage (uniforme, gaussienne, ...).

```cpp
// Tirage d'un nombre au hasard compris entre deux bornes.
// Tirage d'un entier entre 0 et 100
// Tirage d'un flottant entre -1.0 et +1.0
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <random>       // classe pour tirage hasard

// La documentation : https://en.cppreference.com/w/cpp/numeric/random

int main()
{
    // Tirage de 10 entiers au hasard compris entre 0 et 100 avec une distribution uniforme
    // => tous les chiffres entre 0 et 100 ont la même probabilité d'être tiré au hasard
    std::random_device rd;
    std::uniform_int_distribution<int> dist_int(0, 100);
    for (int i=0; i<10; i++)
        std::cout << "Tirage N°" << i << "  -> " << dist_int(rd) << std::endl;

    // Tirage de 10 flottants au hasard compris entre -1.0 et +1.0
    // => tous les chiffres entre -1.0 et +1.0 ont la même probabilité d'être tiré au hasard
    std::random_device rd1;
    // la partie "std::nextafter..." permet d'inclure la borne droite dans le tirage au hasard
    std::uniform_real_distribution<float> dist_float(-1.0, std::nextafter(1.0, std::numeric_limits<float>::max()));
    for (int i=0; i<10; i++)
        std::cout << "Tirage N°" << i << "  -> " << dist_float(rd1) << std::endl;
    return 0;
}
```

Sortie console :

> Tirage N°0  -> 74  
> Tirage N°1  -> 18  
> Tirage N°2  -> 74  
> Tirage N°3  -> 71  
> Tirage N°4  -> 61  
> Tirage N°5  -> 5  
> Tirage N°6  -> 7  
> Tirage N°7  -> 60  
> Tirage N°8  -> 82  
> Tirage N°9  -> 59  
> Tirage N°0  -> -0.425663  
> Tirage N°1  -> -0.896264  
> Tirage N°2  -> -0.630196  
> Tirage N°3  -> -0.896182  
> Tirage N°4  -> 0.555559  
> Tirage N°5  -> 0.769464  
> Tirage N°6  -> -0.0239952  
> Tirage N°7  -> 0.126055  
> Tirage N°8  -> -0.675316  
> Tirage N°9  -> 0.291472

## Utilisation basique de la classe **vector**

Les méthodes de bases pour utiliser le container **vector** de la STL.

```cpp
// Utilisation des méthodes de bases de la classe vector
// Initialisation, push_back, at, size, clear, opérateur []
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <vector>       // classe vector

// La documentation : https://en.cppreference.com/w/cpp/container/vector

int main()
{
    // Initialisation d'un vecteur
    std::vector<int> monVecteur {10, 8, -3, 45, -123};

    // Affichage du vecteur avec boucle for et utilisation méthodes size(), at()
    for (int i=0; i<monVecteur.size(); i++)
        std::cout << "Case N°" << i << "  Valeur : " << monVecteur.at(i) << std::endl;

    // Affichage du vecteur avec une boucle "for each" (à partir de C++11)
    // utilisation du mot clé 'auto' pour demander au compilateur de déterminer
    // automatiquement le type de contenu du vecteur
    for (auto n : monVecteur)
        std::cout << "Valeur : " << n << std::endl;

    // Ajout d'éléments en queue du vecteur, méthode push_back()
    std::cout << "Nb elements vecteur (avant push_back) : " << monVecteur.size() << std::endl;
    monVecteur.push_back(0);
    monVecteur.push_back(-1);
    std::cout << "Nb elements vecteur (apres push_back) : " << monVecteur.size() << std::endl;

    // Accés en ecriture à une case, operateur []
    // ATTENTION : cet opérateur est dangeureux, vous pouvez accéder à une case non allouée
    // RAPPEL : un containeur commence sa numérotation à 0
    monVecteur[1] = 10000;
    for (auto n : monVecteur)
        std::cout << "Valeur : " << n << std::endl;

    // Effacement du contenu du vecteur, méthode clear()
    monVecteur.clear();
    std::cout << "Nb elements vecteur : " << monVecteur.size() << std::endl;

    return 0;
}
```

Sortie console :

> Case N°0  Valeur : 10  
> Case N°1  Valeur : 8  
> Case N°2  Valeur : -3  
> Case N°3  Valeur : 45  
> Case N°4  Valeur : -123  
> Valeur : 10  
> Valeur : 8  
> Valeur : -3  
> Valeur : 45  
> Valeur : -123  
> Nb elements vecteur (avant push_back) : 5  
> Nb elements vecteur (apres push_back) : 7  
> Valeur : 10  
> Valeur : 10000  
> Valeur : -3  
> Valeur : 45  
> Valeur : -123  
> Valeur : 0  
> Valeur : -1  
> Nb elements vecteur : 0  

## Utilisation basique de la classe **string**

Les méthodes de bases pour utiliser le container **string** de la STL.

```cpp
// Utilisation des méthodes de bases de la classe string
// Initialisation, ajout (append), at, size, clear, substr, c_str, opérateurs [], += 
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string

// La documentation : https://en.cppreference.com/w/cpp/container/vector

int main() 
{
    // Initialisation d'une "string"
    std::string maChaine {"Hello SNIR"};

    // Affichage simple d'une chaine
    std::cout << maChaine << std::endl;

    // Affichage de la chaine lettre à lettre avec boucle for et utilisation méthodes size(), at()
    for (int i=0; i<maChaine.size(); i++)
        std::cout << maChaine.at(i) << " ";
    std::cout << std::endl;

    // Concaténation de chaines : méthode append, opérateur +=
    std::cout << "Nb de caractères (avant append) : " << maChaine.size() << std::endl;
    maChaine.append(" - Turgot");
    std::cout << "Nb de caractères (apres append) : " << maChaine.size() << std::endl;
    maChaine += " - Limoges";
    std::cout << "Nb de caractères (apres op. +=) : " << maChaine.size() << std::endl;
    std::cout << maChaine << std::endl;

    // Extraction d'une sous chaine, méthode substr
    // argument de la méthode : position de départ, nb de caractères à extraire
    std::string sousChaine = maChaine.substr(13, 6); // -> extrait de "Turgot"
    std::cout << "Sous chaine : " << sousChaine << std::endl;

    // Accés directe à un caractère de la chaine en écriture : opérateur []
    maChaine[0] = 'B';
    std::cout << maChaine << std::endl;

    // Transformation d'une "string" en chaine de caractère de type C
    // méthode c_str. Utile lorsque vous travaillez avec des API de type C
    const char* chaineTypeC = maChaine.c_str();
    std::cout << chaineTypeC << std::endl;

    // Effacement du contenu de la chaine, méthode clear
    maChaine.clear();
    std::cout << "Taille de la chaine : " << maChaine.size() << std::endl;

    return 0;
}
```

Sortie console :

> Hello SNIR  
> H e l l o   S N I R  
> Nb de caractères (avant append) : 10  
> Nb de caractères (apres append) : 19  
> Nb de caractères (apres op. +=) : 29  
> Hello SNIR - Turgot - Limoges  
> Sous chaine : Turgot  
> Bello SNIR - Turgot - Limoges  
> Bello SNIR - Turgot - Limoges  
> Taille de la chaine : 0  

## Conversion d'une chaine vers un nombre

Les méthodes utilisées ci-dessous sont présentes dans la classe **string**. Dans la mesure du possible, évitez les méthodes de conversion ascii to XX (`atoi`, `atof`, `atoll`) héritées du langage C dans un contexte d'utilisation du C++.

```cpp
// Conversion d'une chaine de caractères vers un nombre
// par ex: string "1.23" -> flottant 1.23
// par ex: string "-12789" -> entier négatif -12789
// méthodes string to XX -> stoXX : stoi, stol, stoll, stof, stod
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string, méthode stoXX

// La documentation : https://en.cppreference.com/w/cpp/string/basic_string/stol

int main()
{
    // Initialisation d'une "string"
    std::string nbChaine {"-123.56"};

    // Conversion en entier puis en flottant
    std::cout << std::stoi(nbChaine) << std::endl; // réalise la conversion en entier, ignore le '.'
    std::cout << std::stof(nbChaine) << std::endl; // réalise la conversion en flottant

    // Conversion vers un entier d'une chaine préfixée par 0X ou 0x (hexadécimal)
    std::string chaineHexa {"0xFFFF"};
    // variable nécessaire pour réaliser la conversion, reportez-vous à la doc.
    std::size_t tempInt;
    std::cout << std::stoi(chaineHexa, &tempInt, 16) << std::endl;

    return 0;
}
```

Sortie console:

> -123  
> -123.56  
> 65535  

## Conversion d'un nombre vers une chaine

La méthode utilisée ci-dessous (`to_string`) est présente dans la classe **string**. Vous remarquerez que la conversion vers une chaine peut se faire avec perte et donc ne pas représenter de manière fidèle le nombre d'origine.

```cpp
// Conversion d'un nombre vers une chaine de caractères
// par ex: flottant 1.23 -> string "1.23"
// par ex: entier négatif -12789 -> string "-12789" 
// Utilisation de la méthode to_string
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string, méthode to_string

// La documentation : https://en.cppreference.com/w/cpp/string/basic_string/to_string

int main()
{
    // Initialisation des variables "nombres" : entier, flottant, double
    int n1 = 123456789;
    int n2 = -0;
    float n3 = 1e-5;
    double n4 = 1e-15;
    double n5 = -1e8;

    // Affichage du nombre et de sa convertion en chaines de caractères
    // ATTENTION : il faut être critique face à la conversion qui peut être inexacte
    std::cout << "nombre n1: " << n1 << "  -> chaine n1: " << std::to_string(n1) << std::endl;
    std::cout << "nombre n2: " << n2 << "  -> chaine n2: " << std::to_string(n2) << std::endl;
    std::cout << "nombre n3: " << n3 << "  -> chaine n3: " << std::to_string(n3) << std::endl;
    std::cout << "nombre n4: " << n4 << "  -> chaine n4: " << std::to_string(n4) << std::endl;
    std::cout << "nombre n5: " << n5 << "  -> chaine n5: " << std::to_string(n5) << std::endl;

    return 0;
}
```

Sortie console :

> nombre n1: 123456789  -> chaine n1: 123456789  
> nombre n2: 0  -> chaine n2: 0  
> nombre n3: 1e-05  -> chaine n3: 0.000010  
> nombre n4: 1e-15  -> chaine n4: 0.000000  
> nombre n5: -1e+08  -> chaine n5: -100000000.000000

## Recherche dans une chaine ou un vecteur

Utilisation de la méthode `find` et introduction aux **itérateurs**. En un mot un itérateur peut être assimilé à un pointeur. Il existe de nombreuses méthodes de recherche, consultez la documentation.

```cpp
// Utilisation de la méthode find pour rechercher un motif dans une chaine ou 
// l'algorithme find pour un nombre dans un vecteur
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <vector>       // classe vector
#include <algorithm>    // algorithmes standards de la STL, ici algo. find pour le vecteur

// La documentation : https://en.cppreference.com/w/cpp/string/basic_string/find

int main()
{
    // Initialisation d'une "string" : ici le pangramme le plus utilisé en français
    // https://fr.wikipedia.org/wiki/Portez_ce_vieux_whisky_au_juge_blond_qui_fume
    std::string unPangramme {"Portez ce vieux whisky au juge blond qui fume"};

    // Affichage simple d'une chaine
    std::cout << unPangramme << std::endl;

    // Recherche d'une chaine dans une autre par la méthode find -> sous chaine existante
    std::string::size_type position;  // position dans la chaine de la sous chaine recherchée
    position = unPangramme.find("blond");
    std::cout << "Position du mot blond : " << position << std::endl;

    // Recherche d'une chaine dans une autre par la méthode find -> sous chaine non trouvée
    // la méthode renvoie la constante : std::string::npos
    // https://en.cppreference.com/w/cpp/string/basic_string/npos
    position = unPangramme.find("biere");
    if (position == std::string::npos)
        std::cout << "Recherche infructueuse" << std::endl;
    else
        std::cout << "Recherche OK -> position : " << position << std::endl;

    // Recherche dans un vecteur contenant des entiers
    std::vector<int> uneSuiteDeNombres {1, 2, 2, 4, 8, 32, 256};

    // Recherche du chiffre 32 dans le vecteur -> nombre existant
    // les méthodes begin() et end() renvoient des itérateurs sur l'objet considéré
    // la variable it est un itérateur, auto permet de ne pas spécifier son type 
    auto it = std::find(uneSuiteDeNombres.begin(), uneSuiteDeNombres.end(), 32);
    // recherche de la position dans le vecteur
    auto positionNb = std::distance(uneSuiteDeNombres.begin(), it);
    std::cout << "Nombre 32 trouvé ! Index dans le vecteur : " << positionNb << std::endl;

    // Recherche du chiffre 255 dans le vecteur -> nombre absent du vecteur
    auto it2 = std::find(uneSuiteDeNombres.begin(), uneSuiteDeNombres.end(), 255);
    if (it2 == uneSuiteDeNombres.end())
        std::cout << "Nombre 255 introuvable" << std::endl;

    return 0;
}
```

Sortie console :

> Portez ce vieux whisky au juge blond qui fume  
> Position du mot blond : 31  
> Recherche infructueuse  
> Nombre 32 trouvé ! Index dans le vecteur : 5  
> Nombre 255 introuvable

## Découper une chaine suivant un motif (split)

La fonction `split` n'existe pas encore dans la STL (mais dans la bibliothèque `boost` oui !). Vous allez donc utiliser une fonction *personnelle* pour réaliser le découpage d'une chaîne. Le but est de découper une chaîne en sous-chaînes suivant un motif (généralement un caractère). Par exemple les fichiers CSV (Comma Separated Value) peuvent être découpés facilement : la chaine "12.5,-6.8,7.0" sera découpé en de multiples chaines "12.5","-6" et "7.0" avec comme caractère de découpe ','.

```cpp
// Découpe d'une chaine suivant un motif
// Utilisation d'une fonction simple 'split' :
// en entrée : la chaine à découper, le caractère séparateur
// en sortie : un vecteur de chaines de caractère (vector<string>) qui contient les éléments
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <vector>       // classe vector
#include <sstream>      // classe stringStream

// Fonction "split" issu du site: 
// https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

int main()
{
    // Chaine à découper : une trame GPS GPRMC
    // Description sur : https://fr.wikipedia.org/wiki/NMEA_0183
    std::string trameRMC {"$GPRMC,053740.000,A,2503.6319,N,12136.0099,E,2.69,79.65,100106,,,A*53"};

    // un vecteur de chaines pour stocker les éléments de la trame RMC
    std::vector<std::string> morceauTrameRMC;

    // Découpage de la chaine d'origine en éléments, séparateur : le caractère ','
    morceauTrameRMC = split(trameRMC, ',');

    // Affichage du résultat : nombre d'éléments du vecteur, utilisation d'une boucle "for each"
    std::cout << "Nombre éléments : " << morceauTrameRMC.size() << std::endl;
    for (auto elt : morceauTrameRMC)
        std::cout << elt << std::endl;

    return 0;
}
```

Sortie console :

> Nombre éléments : 13  
> $GPRMC  
> 053740.000  
> A  
> 2503.6319  
> N  
> 12136.0099  
> E  
> 2.69  
> 79.65  
> 100106  
>  
>  
> A*53

## Obtenir la date et l'heure courante

Utilisation de la classe `chrono` pour obtenir la date et l'heure courante. Utilisation de la fonction `localtime` issue du langage C.

```cpp
// Obtention de la date et l'heure courante (de la machine)
// Utilisation de la classe chrono et d'anciennes fonctions C
// ATTENTION : ce code ne tient pas compte du fuseau horaire
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <chrono>       // classe chrono
#include <ctime>        // fonction localtime 

// La documentation : https://en.cppreference.com/w/cpp/chrono

int main()
{
    // Récupération de l'heure système et conversion en time_t pour être exploitée par localtime
    auto now = std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(now);
    // Description de la structure tm : https://en.cppreference.com/w/cpp/chrono/c/tm
    tm dateHeureLocal; // localtime_s remplira la structure tm
    localtime_s(&dateHeureLocal, &tt);
    // Affichage de la date au format JJ/MM/AA
    std::cout << dateHeureLocal.tm_mday << "/" << dateHeureLocal.tm_mon + 1 
        << "/" << dateHeureLocal.tm_year + 1900 << std::endl;

    // Affichage de l'heure au format hh:mm:ss
    std::cout << dateHeureLocal.tm_hour << ":" << dateHeureLocal.tm_min 
        << ":" << dateHeureLocal.tm_sec << std::endl;

    return 0;
}
```

Sortie console :

> 5/9/2022  
> 22:37:58  

## Les différents types de passages de paramètres à une fonction/méthode

Vous disposez de trois types de passages de paramètres à une fonction/méthode:

1. le passage par **copie**,
2. le passage par **adresse** (utilisation d'un pointeur),
3. le passage par **référence**.

Retenez que si vous produisez du code, la meilleure solution est sans doute le passage par référence.

```cpp
// Les différents types de passage de paramètres à une fonction/méthode
// Passage par copie ou valeur, par adresse (pointeur), par référence
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string

// Toutes les fonctions présentées ici ne renverront rien (void)
// Passage par copie
void foncCopie(int a, int b, std::string s)
{
    std::cout << "Dans foncCopie AVANT modification des variables" << std::endl;
    std::cout << "a:" << a << " b:" << b << " s:" << s << std::endl;
    a = a + b;
    s += "...ajout texte";
    std::cout << "Dans foncCopie APRES modification des variables" << std::endl;
    std::cout << "a:" << a << " b:" << b << " s:" << s << std::endl;
}

int main()
{
    // Les variables que l'on va passer aux différentes fonctions
    int nbA = 10, nbB = -5;
    std::string message {"Hello SNIR"};

    // Appel de la fonction foncCopie
    std::cout << "Dans main AVANT appel foncCopie" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;
    foncCopie(nbA, nbB, message);
    std::cout << "Dans main APRES appel foncCopie" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;

    return 0;
}
```

Sortie console :

> Dans main AVANT appel foncCopie  
> a:10 b:-5 s:Hello SNIR  
> Dans foncCopie AVANT modification des variables  
> a:10 b:-5 s:Hello SNIR  
> Dans foncCopie APRES modification des variables  
> a:5 b:-5 s:Hello SNIR...ajout texte  
> Dans main APRES appel foncCopie  
> a:10 b:-5 s:Hello SNIR  

```cpp
// Passage par adresse
void foncAdresse(int* a, int* b, std::string* s)
{
    std::cout << "Dans foncAdresse AVANT modification des variables" << std::endl;
    std::cout << "a:" << *a << " b:" << *b << " s:" << *s << std::endl;
    *a = *a + *b;
    *s += "...ajout texte";
    std::cout << "Dans foncAdresse APRES modification des variables" << std::endl;
    std::cout << "a:" << *a << " b:" << *b << " s:" << *s << std::endl;
}

int main()
{
    // Les variables que l'on va passer aux différentes fonctions
    int nbA = 10, nbB = -5;
    std::string message {"Hello SNIR"};

    // Appel de la fonction funcAdresse
    std::cout << "Dans main AVANT appel foncAdresse" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;
    foncAdresse(&nbA, &nbB, &message);
    std::cout << "Dans main APRES appel foncAdresse" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;

    return 0;
}
```

Sortie console :

> Dans main AVANT appel foncAdresse  
> a:10 b:-5 s:Hello SNIR  
> Dans foncAdresse AVANT modification des variables  
> a:10 b:-5 s:Hello SNIR  
> Dans foncAdresse APRES modification des variables  
> a:5 b:-5 s:Hello SNIR...ajout texte  
> Dans main APRES appel foncAdresse  
> a:5 b:-5 s:Hello SNIR...ajout texte

```cpp
// Passage par référence
void foncReference(int& a, int& b, std::string& s)
{
    std::cout << "Dans foncReference AVANT modification des variables" << std::endl;
    std::cout << "a:" << a << " b:" << b << " s:" << s << std::endl;
    a = a + b;
    s += "...ajout texte";
    std::cout << "Dans foncReference APRES modification des variables" << std::endl;
    std::cout << "a:" << a << " b:" << b << " s:" << s << std::endl;
}

int main()
{
    // Les variables que l'on va passer aux différentes fonctions
    int nbA = 10, nbB = -5;
    std::string message {"Hello SNIR"};

    // Appel de la fonction foncReference
    std::cout << "Dans main AVANT appel foncReference" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;
    foncReference(nbA, nbB, message);
    std::cout << "Dans main APRES appel foncReference" << std::endl;
    std::cout << "a:" << nbA << " b:" << nbB << " s:" << message << std::endl;

    return 0;
}
```

Sortie console :

> Dans main AVANT appel foncReference  
> a:10 b:-5 s:Hello SNIR  
> Dans foncReference AVANT modification des variables  
> a:10 b:-5 s:Hello SNIR  
> Dans foncReference APRES modification des variables  
> a:5 b:-5 s:Hello SNIR...ajout texte  
> Dans main APRES appel foncReference  
> a:5 b:-5 s:Hello SNIR...ajout texte

## Lecture  d'un fichier texte

Vous pouvez lire dans un fichier texte des chaines de caractères, des nombres mais pas des données binaires. Le fichier texte peut être stocké dans une `string` pour être traité ultérieurement. Si le fichier texte est correctement formaté, vous pouvez directement lire et affecter des valeurs à des variables. Dans les exemples ci-dessous, un fichier `data.txt` va être utilisé à des fins de démonstration.

Une lecture complète de l'article suivant est un plus :
<http://sdz.tdct.org/sdz/lecture-et-ecriture-dans-les-fichiers-en-c.html>

```console
$ls -lh data.txt
-rw-r--r-- 1 root root 50 août   1  2020 data.txt
$cat data.txt
SSD Samsung 1To   159.00
HDD WD      500Go  64.00
```

```cpp
// Lecture dans un fichier texte
// ATTENTION : il faut avoir des droits de lecture sur le fichier
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <fstream>      // Classe input file stream, fichier en lecture

// La documentation : https://en.cppreference.com/w/cpp/io/basic_ofstream

int main()
{
    // Le fichier texte s'appelle data.txt et contient des lignes formatées comme suit :
    // SSD Samsung 1To   159.00
    // HDD WD      500Go  64.00
    std::ifstream monFichier("data.txt");

    // Les variables qui vont accueillir les valeurs du fichier de données
    std::string typeHDD;
    std::string marque;
    std::string taille;
    double prix;

    // Le fichier est-il ouvert ?
    if (monFichier.is_open()) {
        std::cout << "Lecture dans un fichier élément par élément" << std::endl;
        // premiere lecture dans le fichier (obligatoire) et affectation aux variables
        monFichier >> typeHDD >> marque >> taille >> prix;
        // tant qu'on a pas atteint la fin du fichier on réalise un affichage puis on boucle
        // il faut bien respecter l'ordre des deux lignes ci-dessous
        while (!monFichier.eof()) {
            std::cout << typeHDD << "->" << marque << "->" << taille << "->" << prix << std::endl;
            monFichier >> typeHDD >> marque >> taille >> prix;
        };
        // on ferme le fichier qd on en a plus besoin
        monFichier.close();
    
        // lecture du fichier une ligne à la fois
        std::cout << "Lecture dans un fichier ligne par ligne" << std::endl;
        monFichier.open("data.txt");
        std::string uneLigne;
        // Documentation de getline : https://en.cppreference.com/w/cpp/string/basic_string/getline
        // par defaut le caractère pour détecter la fin d'une ligne est le retour chariot
        // ATTENTION : ici on lit une ligne et on stocke celle-ci dans une string, pas de différence
        // entre des chaines de caractères ou des nombres
        getline(monFichier, uneLigne);
        while (!monFichier.eof()) {
            std::cout << uneLigne << std::endl;
            getline(monFichier, uneLigne);
        };
        monFichier.close();
    }
    // sinon on affiche un message à l'utilisateur
    else
        std::cout << "Pb ouverture fichier..." << std::endl;
    return 0;
}
```

Sortie console :

> Lecture dans un fichier élément par élément  
> SSD->Samsung->1To->159  
> HDD->WD->500Go->64  
> Lecture dans un fichier ligne par ligne  
> SSD Samsung 1To   159.00  
> HDD WD      500Go  64.00

## Ecriture dans un fichier texte

Vous pouvez écrire dans un fichier texte des caractères, des chaines de caractères, des nombres (qui seront convertis en chaine de caractères) mais pas des données binaires. Pour créer un fichier inexistant il faut des droits d'écriture dans le répertoire concerné. Vous pouvez choisir différentes actions à la création du fichier :

- le créer,
- effacer son contenu s'il existe déjà,
- écrire à la fin du fichier s'il existe déjà.

Reportez-vous à la documentation de `std::ofstream` et en particulier les [méthodes d'ouverture d'un fichier](https://en.cppreference.com/w/cpp/io/ios_base/openmode).

```cpp
// Ecriture dans un fichier texte
// ATTENTION : il faut avoir des droits d'écriture dans le répertoire
// si le fichier existe déjà, il faut les droits d'écriture sur celui-ci
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <fstream>      // Classe output file stream, fichier en écriture

// La documentation : https://en.cppreference.com/w/cpp/io/basic_ofstream

int main()
{
    // On crée le fichier s'il n'existe pas, on l'écrase s'il existe
    // Documentation : https://en.cppreference.com/w/cpp/io/ios_base/openmode
    std::ofstream monFichierEnEcriture("mesDonnees.txt", std::ios_base::trunc);

    // Le fichier a-t-il été créé ou écrasé ?
    if (monFichierEnEcriture.is_open()) {
        // Variables qui seront écrites dans le fichier texte
        int unEntier {654321};
        double unDouble {-123.45678};
        std::string uneChaine {"Un texte a ecrire dans un fichier"};
    
        // Ecriture dans le fichier -> toutes les variables sont transformées en texte
        // le 'std::endl' permet de revenir à la ligne
        monFichierEnEcriture << unEntier << std::endl;
        monFichierEnEcriture << unDouble << " " << uneChaine << std::endl;
    
        // Fermeture du fichier
        monFichierEnEcriture.close();
        std::cout << "Fichier créé, données enregistrées..." << std::endl;
    }
    // sinon on affiche un message à l'utilisateur
    else
        std::cout << "Probleme de création ecriture du fichier" << std::endl;
    return 0;
}
```

Sortie console :
> Fichier créé, données enregistrées...

Visualisation des droits du fichier **mesDonnees.txt** ainsi que sa taille:

```console
$ls -lh mesDonnees.txt
-rw-r--r-- 1 snir snir 50 sept.  5 22:38 mesDonnees.txt
```

Visualisation du **contenu** du fichier **mesDonnees.txt** :

```console
$cat mesDonnees.txt
654321
-123.457 Un texte a ecrire dans un fichier
```

## Lecture/Ecriture dans un fichier binaire

Un fichier binaire permet de stocker n'importe quel type de données du moment que vous pouvez les écrire octet aprés octet et que vous connaissiez leur organisation au sein du fichier. Ce fichier ne sera pas lisible par un éditeur de texte. Son contenu peut être visualisé avec un éditeur hexadécimal, mais si vous ne connaissez pas l'organisation des données, il ne sera pas interprêtable.

Ce format est donc interessant pour enregistrer/lire des données que vous seul maitrisez.

```cpp
// Ecriture puis lecture dans un fichier en mode binaire
// ATTENTION : il faut avoir des droits de lecture/écriture dans le répertoire
// si le fichier existe déjà, il faut les droits d'écriture sur celui-ci
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <fstream>      // Classe file stream, fichier en lecture/écriture

// La documentation : http://www.cplusplus.com/reference/fstream/fstream/

int main()
{
    // On crée le fichier s'il n'existe pas, on l'écrase s'il existe
    // ce fichier sera ouvert en lecture/ecriture
    // Documentation : https://en.cppreference.com/w/cpp/io/ios_base/openmode
    std::fstream monFichierBinES("mesDonneesBinaires.txt", 
        std::ios::in | std::ios::out | std::ios::trunc | std::ios::binary);

    // Le fichier a-t-il été créé ou écrasé ?
    if (monFichierBinES.is_open()) {
        // Variables qui seront écrites dans le fichier binaire
        int unEntier {65535};  // Représentation en hexa : 0x0000FFFF
        float unFloat {-1.5};  // Représentation en hexa : 0xBFC00000 format IEEE754
        std::string uneChaine {"ABCDEF"};
    
        // Ecriture dans le fichier avec la méthode write()
        // cette méthode prend en entrée l'adresse de la première case mémoire
        // à écrire et le nombre d'octets à écrire
        // on transforme l'adresse d'un int en une adresse sur un char
        // par le reinterpret_cast<char*> pour travailler sur des octets
        // Ecriture d'un entier : 4 ou 8 octets, taille obtenue par sizeof()
        monFichierBinES.write(reinterpret_cast<char*>(&unEntier), sizeof(unEntier));
        // Idem pour le float
        monFichierBinES.write(reinterpret_cast<char*>(&unFloat), sizeof(unFloat));
        // Idem pour le string, on obtient un pointeur en lecture seule sur la chaine
        // avec la méthode c_str() et la taille par la méthode size()
        monFichierBinES.write(uneChaine.c_str(), uneChaine.size());
    
        std::cout << "Fichier créé, données enregistrées..." << std::endl;
        monFichierBinES.close();
    }
    // sinon on affiche un message à l'utilisateur
    else
        std::cout << "Probleme de création ecriture du fichier" << std::endl;
    return 0;
}
```

Sortie console :

> Fichier créé, données enregistrées...

Relecture du fichier précédemment créé avec le code ci-dessous :

```cpp
// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <string>       // classe string
#include <fstream>      // Classe file stream, fichier en lecture/écriture

int main()
{
    // On crée le fichier s'il n'existe pas, on l'écrase s'il existe
    // ce fichier sera ouvert en lecture/ecriture
    // Documentation : https://en.cppreference.com/w/cpp/io/ios_base/openmode
    std::fstream monFichierBinES("mesDonneesBinaires.txt", 
        std::ios::in | std::ios::out | std::ios::binary);

    // Si vous avez déjà ouvert un fichier et fait des opérations dessus,
    // il faut revenir au début du fichier, pour cela vous disposez des méthodes seekg (input stream)
    // ou seekp (output stream), la valeur '0' indique le début du fichier
    // monFichierBinES.seekp(0);

    // Variables pour la lecture du fichier, elles doivent être différentes de celles utilisées
    // pour l'écriture pour être certains que l'on lit bien les valeurs
    int lectureEntier;
    float lectureFloat;
    std::string lectureChaine;
    lectureChaine.resize(6);   // vous devez connaitre la taille à l'avance, ici 6 caractères

    // il faut utiliser la méthode read() pour relire le fichier binaire
    monFichierBinES.read(reinterpret_cast<char*>(&lectureEntier), sizeof(lectureEntier));
    monFichierBinES.read(reinterpret_cast<char*>(&lectureFloat), sizeof(lectureFloat));
    // on doit accéder en écriture par un pointeur sur la chaine -> méthode data()
    // la méthode c_str() ne fonctionne pas ici car elle renvoie un pointeur constant
    monFichierBinES.read(lectureChaine.data(), lectureChaine.size());

    // Affichage des données
    std::cout << "lecture Entier : " << lectureEntier << std::endl;
    std::cout << "lecture Float  : " << lectureFloat << std::endl;
    std::cout << "lecture Chaine : " << lectureChaine << std::endl;
                     
    // Fermeture du fichier
    monFichierBinES.close();

    return 0;
}
```

Sortie console :

> lecture Entier : 65535  
> lecture Float  : -1.5  
> lecture Chaine : ABCDEF

Vérification avec des dommandes Linux/Unix :

```console
$ls -lh mesDonneesBinaires.txt
-rw-r--r-- 1 snir snir 14 sept.  5 22:38 mesDonneesBinaires.txt
$hexdump -C mesDonneesBinaires.txt
00000000  ff ff 00 00 00 00 c0 bf  41 42 43 44 45 46        |........ABCDEF|
0000000e
```

Si vous êtes sur un système d'exploitation 32 bits, la taille d'un entier ou d'un flottant est de 4 octets. La chaine de caractères comporte 6 caractères donc 6 octets. Vous devez donc écrire 4+4+6 = 14 octets dans le fichier binaire. Cela est vérifié par la commande `ls` qui nous indique que le fichier a une taille de 14 octets.

Vous visualisez ensuite le fichier en hexadécimal avec la commande `hexdump`. Vous retrouvez la valeur de l'entier en commencant par les octets de poids faible (FF FF 00 00). Puis vous trouvez la valeur du flottant (00 00 C0 BF) et enfin les valeurs des caractères en ASCII (41 à 46).

Vous remarquez qu'il n'y a pas de retour à la ligne et que les données s'enchainent les unes apres les autres. Vous devez donc connaitre exactement la structure de votre fichier pour le relire.

## Capturer une exception

Les **exceptions** sont des mécanismes mis en place pour le C++ pour la gestion des erreurs. C'est la solution que vous devez employer pour rendre robuste votre code. Vous pouvez utiliser les exceptions de la STL ou créer les votres.

```cpp
// Gestion d'une exception levée par un objet de la STL
// O. Dartois, juillet 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <vector>       // classe vector
#include <exception>    // Classe exception

// La liste des exceptions : https://en.cppreference.com/w/cpp/error/exception
// La mise en oeuvre : https://en.cppreference.com/w/cpp/language/try_catch

int main()
{
    std::vector<int> listeNombre {12, 6, -5, 3};

    std::cout << "Taille du vecteur : " << listeNombre.size() << std::endl;
    // Accés à une case impossible avec la méthode at() qui lève alors une exception
    // voir la doc.: https://en.cppreference.com/w/cpp/container/vector/at
    try {
        std::cout << "Valeur case impossible : " << listeNombre.at(4) << std::endl;
        // ATTENTION : l'instruction suivante ne sera jamais exécutée
        listeNombre.at(0) = 0;
    } 
    // on capture ici l'exception 'out_of_range' que l'on affectera à l'objet err
    // par référence constante (pas de copie, pas de modification possible)
    catch (std::out_of_range const & err) {
        // la méthode what() permet d'afficher un message d'erreur
        std::cout << err.what() << std::endl;
    }

    // le programme continue ici
    for (auto & elt : listeNombre)
        std::cout << elt << std::endl;

    return 0;
}
```

Sortie console :

> Taille du vecteur : 4  
> Valeur case impossible : vector::_M_range_check: __n (which is 4) >= this->size() (which is 4)  
> 12  
> 6  
> -5  
> 3

## Traiter les arguments de la ligne de commande

lorsque vous exécutez un programme en ligne de commande (dans une console par exemple), vous pouvez passer des arguments par cette même ligne de commande. Il suffit juste de les rajouter après le nom de l'exécutable et de les séparer par un espace.

>Exemple: monProg.exe toto 10 titi 0.05

Dans le cas précédent vous avez 5 arguments (on compte le nom du programme) et on peut les récupérer de manière individuels. La fonction qui gère ces arguments est la fonction `main()`. En fait celle-ci peut posséder deux arguments :

- `argc` : *argument count* -> nombre d'arguments de la ligne de commande, c'est un entier,
- `argv` : *argument value* -> les chaines de caractères de chacun des arguments, c'est un tableau de tableau de caractères (noté `char** argv` ou `char* argv[]`).

L'exemple ci-dessous détaille l'utilisation des ces arguments.

**ATTENTION** : il n'y a aucune vérification dans le code ci-dessous sur la validité des arguments. Il faudrait faire une gestion des exceptions (méthode `std::stoi()` employée ici).

```cpp
// Gestion des arguments de la ligne de commande
// O. Dartois, aout 2020

// Les "includes" nécessaires au programme
#include <iostream>

int main (int argc, char* argv[])
{
    std::cout << "Analyse de la ligne de commande :" << std::endl;

    // Nombre d'arguments de la ligne de commande y compris le nom de l'exécutable
    // il y a au minimum un argument : le nom de l'exécutable
    std::cout << "Nb arguments ligne de commande : " << argc << std::endl;

    // Lister les arguments de la ligne de commande
    for (int i=0; i<argc; i++)
        std::cout << "Argument Num. : " << i << "  Valeur : " << argv[i] << std::endl;

    // Dans cet exemple on suppose que le nombre d'arguments doit etre égale à 2
    // pour être traité sinon on affiche un message pour l'utilisateur
    if (argc == 2) {
    	// Vous pouvez transformer les argument de la ligne de commande en "string"
    	std::string nomExecutable { argv[0] };  // ici conversion du nom de l'executable en string

    	// On admet ici que le deuxième arument de la ligne de commande est un entier
    	int nbEntierLigneCommande = std::stoi(std::string(argv[1]));
	    std::cout << "Nom executable : " << nomExecutable << std::endl;
	    std::cout << "nbEntierLigneCommande + 10 : " << nbEntierLigneCommande + 10 << std::endl;
    } else {
	    std::cout << "Syntaxe : " << argv[0] << " unEntier" << std::endl;
	    std::cout << "Exemple : " << argv[0] << " -123456"  << std::endl;
    }

    return 0;
}
```

Le résultat de l'exécution de ce programme est le suivant (sans et avec passage des paramètres) :

```console
$ ./analyseLigneDeCommande 
Analyse de la ligne de commande :
Nb arguments ligne de commande : 1
Argument Num. : 0  Valeur : ./analyseLigneDeCommande
Syntaxe : ./analyseLigneDeCommande unEntier
Exemple : ./analyseLigneDeCommande -123456

$ ./analyseLigneDeCommande -10
Analyse de la ligne de commande :
Nb arguments ligne de commande : 2
Argument Num. : 0  Valeur : ./analyseLigneDeCommande
Argument Num. : 1  Valeur : -10
Nom executable : ./analyseLigneDeCommande
nbEntierLigneCommande + 10 : 0
```

## Trier un vecteur

Le code ci-dessous montre comment trier un vecteur d'entiers ou un vecteur de chaines de caractères. Pour cela on utilise les algorithmes de la STL et en particulier la méthode `sort()`. Le trie s'effectue par ordre croissant ou alphabéthique. Si vous voulez trier différement (ordre décroissant par exemple), soit des méthodes de la STL existent, soit vous pouvez coder votre fonction de comparaison. Reportez-vous à la documentation de la méthode `sort()`.

```cpp
// Trier avec les algorithmes de la STL, application sur le container vector
// O. Dartois, aout 2020

// Les "includes" nécessaires au programme
#include <iostream>     // std::cout
#include <vector>       // classe vector
#include <algorithm>    // méthode sort()
#include <functional>   // fonction greater()

// La documentation : https://en.cppreference.com/w/cpp/container/vector
// et la méthode sort : https://en.cppreference.com/w/cpp/algorithm/sort

int main()
{
    // Initialisation d'un vecteur
    std::vector<int> monVecteurATrier {10, 8, -3, 45, -123};

    // Affichage du vecteur avec une boucle "for each" (à partir de C++11)
    // utilisation du mot clé 'auto' pour demander au compilateur de déterminer
    // automatiquement le type de contenu du vecteur
    for (auto n : monVecteurATrier)
        std::cout << n << " ";
    std::cout << std::endl;

    // Utilisation de la méthode sort(), elle nécesite des itérateurs de début et de fin
    // Par défaut la méthode sort() trie par ordre croissant : utilisation de l'operateur '<'
    std::sort(monVecteurATrier.begin(), monVecteurATrier.end());
    for (auto n : monVecteurATrier)
        std::cout << n << " ";
    std::cout << std::endl;

    // Trie par ordre décroissant, on spécifie la fonction de trie greater() : opérateur '>'
    // Documentation de std::greater : https://en.cppreference.com/w/cpp/utility/functional/greater
    std::sort(monVecteurATrier.begin(), monVecteurATrier.end(), std::greater<int>());
    for (auto n : monVecteurATrier)
        std::cout << n << " ";
    std::cout << std::endl;

    // Trie d'un ensemble de mots par ordre alphabétique (majuscules puis minuscules)
    std::vector<std::string> motsATrier {"Je", "maitrise", "le", "C++."};
    std::sort(motsATrier.begin(), motsATrier.end());
    for (auto n : motsATrier)
        std::cout << n << " ";
    std::cout << std::endl;

    return 0;
}
```

Sortie console :

> 10 8 -3 45 -123  
> -123 -3 8 10 45  
> 45 10 8 -3 -123  
> C++. Je le maitrise

## Utiliser la voie série (sous GNU/Linux ou Microsoft Windows)

### Identifier une voie série sous Linux ou Windows

Une voie série peut être directement intégré à la carte mère. Vous trouverez dans ce cas un connecteur DB9 à l'arrière de votre PC. On parle alors de voie série à la norme RS232 avec des niveaux de tensions compris entre +12V et -12V. Ces ports séries portent souvent le nom de `ttyS0` sous Linux ou `COM1` sous Windows.

Cette voie série ne sera donc pas directement utilisable avec des voies séries qui utilisent d'autres niveaux de tensions : par exemple 0-5V sur Arduino ou 0-3,3V sur RaspberryPi. Il faudra alors utiliser des convertisseurs de niveaux de tensions.

Dans le cas précedent il est préférable d'utiliser des convertisseurs USB/Série qui permettent généralement de choisir la tension de sortie (3,3V ou 5V) sur les lignes. Ces ports séries portent souvent le nom de `ttyUSBx` sous Linux ou `COMx` sous Windows avec `x` qui est un nombre entier.

### Choisir une bibliothèque C++ de gestion de la voie série

Idéalement cette bibliothèque doit être portable sur les deux environnements (Linux, Windows) et fournir un ensemble de méthodes permettant une gestion aisée des échanges. Le choix s'est porté sur la bibliothèque **Serialib** (site: <https://lucidar.me/fr/serialib/cross-plateform-rs232-serial-library/> ) disponible sur le dépot GitHub suivant: <https://github.com/imabot2/serialib>.

Cette bibliothèque est limitée dans ces fonctionnalités, en particulier dans le choix du format des données ou seul le format 8N1 (1 bit de start, 8 bits de données, pas de parité, 1 bit de stop) sera disponible, mais cela devrait satisfaire la plupart de nos besoins.

### Mettre en oeuvre des ports séries virtuels

Vous ne disposez pas forcement d'un appareil qui envoie des données séries ou d'une autre machine pour faire des tests. Pour pouvoir tester le dialogue entre deux codes C++ nous allons mettre en oeuvre des ports séries virtuels aussi bien sous Linux que sous Windows.

#### Ports séries virtuels sous GNU/Linux

La création de port série virtuel sous Linux s'effectue en chargeant un module noyau spécifique. Ce module noyau s'appelle `tty0tty`et est disponible sur le site GitHub suivant: <https://github.com/freemed/tty0tty/>. Il n'est donc pas directement disponible dans les paquets de votre distribution, il faut donc le recompiler. Les commandes suivantes présentent les différentes étapes nécessaires sur une distribution compatible Debian (ubuntu, raspberrypiOS, etc.. ):

1. Installation de la chaine de compilation : `sudo apt install build-essential`
2. Installation des entêtes du noyau : `sudo apt install linux-headers-\$(uname -r)`
3. installation de git pour le clonage du dépot : `sudo apt install git`
4. clonage du dépot : `git clone https://github.com/freemed/tty0tty.git`
5. déplacement dans le bon répertoire pour créer le module : `cd tty0tty/module/`
6. Création du module (un fichier Makefile est disponible) : `make`
7. Copie du module dans les modules disponibles pour le noyau : `sudo cp tty0tty.ko /lib/modules/\$(uname -r)/kernel/drivers/misc`
8. Mise à jour des dépendances des modules : `sudo depmod`
9. Chargement du module : `sudo modprobe tty0tty`
10. Vérification de la création des paires de voies séries virtuelles : `ls /dev/tnt*`
11. Modification des droits pour une utilisation par un utilisateur quelconque : `sudo chmod 666 /dev/tnt*`

A partir de ce moment vous pouvez utiliser les paires de voies séries virtuelles (`tnt0 <-> tnt1`, `tnt2 <-> tnt3`, etc...). Un test peut être réalisé avec **picocom** (`sudo apt install picocom`) en lancant deux terminaux et en tapant dans le premier : `picocom /dev/tnt0` et sur le deuxieme : `picocom /dev/tnt1`. Tout ce que vous taperez alors d'un des deux terminaux sera envoyé à l'autre par la voie série virtuelle.

#### Ports séries virtuels sous Microsoft Windows

Il existe plusieurs solution gratuite pour rajouter des paires de voies séries virtuelles sous Windows (tapez *virtual com port windows 10* pour vous en persuader). La solution retenue ici est une solution *open source* nommée **Com0Com**. Le site est disponible sur SourceForge : <https://sourceforge.net/projects/com0com/>. Installez ce logiciel et vous pouvez créer autant de paires de ports que désirez.
Vous pouvez tester une paires de ports séries virtuels en lancant deux fois le logiciel *teraterm* ou *putty* en mode série et en choisissant bien sur les deux ports séries corrects sur chacune des applications.

### Envoi d'un caractère sur une voie série

Le code suivant permet d'envoyer l'alphabet sur une voie série. Le code de réception est donné au paragraphe suivant.

```cpp
// Exemple d'utilisation de la voie série avec serialib
// Ecriture sur un une voie série
// O. Dartois - Janvier 2021

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include "serialib.h"

int main()
{

    // Instanciation d'un objet de la classe serialib
    serialib serial;

    // Nom du port COM à ouvrir, ici sous Microsoft Windows
    const std::string portCOM("COM3");

    // Ouverture du port série à la vitesse spécifié
    char erreur = serial.Open(portCOM.c_str(), 115200);

    // Si pb à l'ouverture du port série, on sort avec un message
    if (erreur != 1) {
        std::cout << "Ouverture du port serie KO !" << std::endl;
        return 0;
    }
    
    std::cout << "Ouverture du port serie OK" << std::endl;

    // Envoie des caractères ASCII de 32 à 128
    for (int c = 32; c < 128; c++)
    {
        // Ecriture d'un caractère sur le port série
        serial.WriteChar(c);
        // Pause de 1 seconde
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    // Fermeture du port série
    serial.Close();

    return 0;
}
```

### Réception d'un caractère sur une voie série

Le code suivant permet de recevoir et d'afficher l'alphabet reçue par une voie série avec le code précédent.

```cpp
// Exemple d'utilisation de la voie série avec serialib
// Reception d'un caractere sur une une voie série
// O. Dartois - Janvier 2021

#include <iostream>
#include <string>
#include <thread>

#include "serialib.h"

int main()
{

    // Instanciation d'un objet de la classe serialib
    serialib serial;

    // Nom du port COM à ouvrir
    const std::string portCOM("COM4");

    // Ouverture du port série à la vitesse spécifié
    char erreur = serial.Open(portCOM.c_str(), 115200);

    // Si pb à l'ouverture du port série, on sort avec un message
    if (erreur != 1) {
        std::cout << "Ouverture du port serie KO !" << std::endl;
        return 0;
    }
    
    std::cout << "Ouverture du port serie OK" << std::endl;

    // Réception des caractères sur la voie série
    erreur = 0;
    do {
            char caracRecu;
            // Lecture d'un caractère sur le port série avec un "timeout"  de 2s
            erreur = serial.ReadChar(&caracRecu, 2000);
            // Erreur de réception sur le port série
            if (erreur < 0) {
                std::cout << "Erreur réception" << std::endl;
                break;
            }
            // Réception d'un caractère
            else if (erreur == 1)
                std::cout << caracRecu << std::endl;
            // Fin du temps imparti pour la réception d'un caractère
            else if (erreur == 0)
                std::cout << "Reception timeout" << std::endl;
    } while (erreur != 0);

    // Fermeture du port série
    serial.Close();

    return 0;
}
```

### Traitement d'une trame issue d'une voie série

Le code suivant présente le traitement d'une trame en utilisant les méthodes non vues précédemment. Ci-dessous il s'agit d'extraire l'heure d'une trame GPRMC issue d'un récepteur GPS connecté par voie série.

```cpp
// Exemple d'utilisation de la voie série avec serialib
// Réception et analyse d'une trame GPS RMC : récupération de l'heure
// O. Dartois - Janvier 2021

#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <sstream>

#include "serialib.h"

// Fonction de découpage d'une chaine en sous chaine à partir d'un délimiteur
std::vector<std::string> split(const std::string& s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim))
		elems.push_back(item);
	return elems;
}

int main()
{
	// Instanciation d'un objet de la classe serialib
	serialib serial;

	// Nom du port COM à ouvrir sous Microsoft Windows
	const std::string portCOM("COM4");

	// Ouverture du port série à la vitesse spécifié
	char erreur = serial.Open(portCOM.c_str(), 115200);

	// Si pb à l'ouverture du port série, on sort avec un message
	if (erreur != 1) {
		std::cout << "Ouverture du port serie KO !" << std::endl;
		return 0;
	}

	std::cout << "Ouverture du port serie OK" << std::endl;

	// Réception des caractères sur la voie série
	erreur = 0;
	char trameGPS[200]; // Stockage de la trame GPS recue par la voie serie
	bool debutTrame = false; // pour être certain d'etre au debut d'une trame
	// cette boucle vérifie que l'on est bien au début d'une trame GPS
	// elle commence toute par le caractère '$'
	do {
		char caracRecu;
		erreur = serial.ReadChar(&caracRecu);
		// Erreur de réception sur le port série
		if (erreur < 0) {
			std::cout << "Erreur réception" << std::endl;
			serial.Close();
			return 0;
		}
		if (caracRecu == '$') debutTrame = true;
	} while (debutTrame == false);

	do {
		// Lecture d'une chaine se terminant par un retour chariot '\n' (fin de ligne d'une trame GPS)
		// sur le port série avec un "timeout"  de 5s
		erreur = serial.ReadString(trameGPS, '\n', 200, 5000);
		// Erreur de réception sur le port série
		if (erreur < 0) {
			std::cout << "Erreur réception" << std::endl;
			serial.Close();
			return 0;
		}
		// Fin du temps imparti pour la réception d'un caractère
		else if (erreur == 0)
			std::cout << "Reception timeout" << std::endl;
		else {
			// Analyse de la trame pour en extraire l'heure
			std::string trameGPSAAnalyser(trameGPS);
			if (trameGPSAAnalyser.substr(1, 5) == "GPRMC") {
				// découpage de la trame en morceaux suivant le caractère ','
				std::vector<std::string> partiesTrameGPS = split(trameGPSAAnalyser, ',');
				// la partie 1 contient l'heure au format HHMMSS
				std::cout << partiesTrameGPS.at(1).substr(0, 2) << ":"; // affichage heures
				std::cout << partiesTrameGPS.at(1).substr(2, 2) << ":"; // affichage minutes
				std::cout << partiesTrameGPS.at(1).substr(4, 2) << std::endl; // affichage secondes
			}
		}
	} while (erreur != 0);

	// Fermeture du port série
	serial.Close();

	return 0;
}
```

## Mesurer le temps d'exécution d'une fonction ou d'une partie d'un programme

L'exemple ci-dessous mesure le temps d'exécution du calcul de la suite de fibonnaci. Vous pouvez bien sur l'appliquer à n'importe quelle partie d'un programme en l'adaptant légèrement.

```cpp
#include <iostream>
#include <chrono>  // classe de gestion du temps de la STL

unsigned int fonctionAMesurer(unsigned int n)
{
    // Suite de Fibonacci : 1,1,2,3,5,8,13 ... (chaque terme égale la somme des deux précédents)
    // https://www.nationalgeographic.fr/sciences/mathematiques-la-fascinante-suite-de-fibonacci-et-le-nombre-dor
    int u = 0;
    int v = 1;
    int i, t;

    for (i = 2; i <= n; i++) {
        t = u + v;
        u = v;
        v = t;
    }
    return v;
}

int main()
{
    std::cout << "Prog mesure du temps d'execution" << std::endl;
    // récupération de l'heure du point de départ en haute résolution
    auto startTime = std::chrono::high_resolution_clock::now();
    // exécution de la fonction à mesurer
    unsigned int res = fonctionAMesurer(1000000);
    // récupération de l'heure du point de fin en haute résolution
    auto endTime = std::chrono::high_resolution_clock::now();
    // affichage du résultat de la suite fibonnaci (facultatif)
    std::cout << "resultat : " << res << std::endl;
    // Calcul du temps d'exécution en microsecondes, vous pouvez changez l'unité
    // en milliseconds ou seconds voire nanoseconds
    std::cout << "Temps execution : " <<
        std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count() << 
        " us" << std::endl;

    return 0;
}
```

Sortie console :

> Prog mesure du temps d'execution  
> resultat : 1884755131  
> Temps execution : 920 us

## Trouver si une valeur existe dans un vecteur

Il faut utiliser le plus possible les méthodes de la STL. En voici un exemple extremement court mais efficace.

```cpp
#include <iostream>
#include <vector>

int main()
{
    std::vector<int> nombres {23, 54, 87, 53, 99, 72, 34};
    int valCherche = -1;
    auto pos = std::find(std::begin(nombres), std::end(nombres), valCherche);

    if (pos != std::end(nombres))
        std::cout << *pos << " existe dans le vecteur" << std::endl;
    else
        std::cout << valCherche << " est absent du vecteur" << std::endl;

    return 0;
}
```

Sortie console :

> -1 est absent du vecteur

## Vérifier une saisie utilisateur à l'aide d'une expression régulière (regex)

Les expressions régulières permettent de réaliser des vérifications d'une saisie utilisateur de manière aisée mais écrire ces expressions n'est vraiment pas simple. Pour cela vous allez vous rendre sur le site [rgx.tools](https://rgx.tools) qui permet, à l'aide de chatGPT, d'obtenir votre expression régulière à partir d'une phrase simple. Des exemples sont donnés dans le code ci-dessous. Veuillez noter tout de même qu'il faudra rajouter un `\` devant les `\` de l'expression régulière pour les échapper.

```cpp
#include <iostream>
#include <regex>
#include <string>

// Les expressions régulières sont générées sur ce site : https://rgx.tools
// Il faut rajouter le caractère '\' dans l'expression régulière à chaque fois
// que vous rencontrer '\' pour le protéger (l'échapper). C'est pour cette
// raison que vous voyez '\\' dans les expressions ci-dessous.

int main()
{
    // Validation d'un @IP version 4
    const std::regex ip_regex("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    std::smatch base_match;
    std::string ip("10.187.52.33");
    // la variable resultat sera vrai si on a une @IP valide, faux sinon
    bool resultat = std::regex_search(ip, base_match, ip_regex);
    std::cout << "resultat IP:" << std::boolalpha << resultat << std::noboolalpha << std::endl;
    
    // Validation d'une adresse email
    const std::regex mail_regex("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
    std::string mail("not.me@site.fr");
    bool result2 = std::regex_search(mail, base_match, mail_regex);
    std::cout << "resultat mail:" << std::boolalpha << result2 << std::noboolalpha << std::endl;
    
    // Donnez en anglais votre cahier des charges au site rgx.tools:
    // Validate a password with at least 8 characters, one uppercase, one lowercase, and one number
    const std::regex pass_regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$");
    std::string pass("notgoodpass"); // val1dPassW0rd
    bool result3 = std::regex_search(pass, base_match, pass_regex);
    std::cout << "resultat pass:" << std::boolalpha << result3 << std::noboolalpha << std::endl;
    
    return 0;
}
```

