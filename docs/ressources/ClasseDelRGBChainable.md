# Classe C++ pour piloter des DELs RGB chainables

!!! tldr "Objectifs de ce document"

    - présenter une classe C++ pour piloter des dels RGB chainables utilisant la puce P9813,
    - présenter un exemple simple d'utilisation de cette classe.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Prérequis

Dans certains projets industriels il est nécessaire d'avoir des témoins lumineux facilement pilotables (utilisant des entrées/sorties tout ou rien classiques), permettant d'avoir un grand choix de couleurs (donc des dels RGB), chainables (pour minimiser le câblage) utilisant de la connectique simple (dans notre cas des connecteurs grove).

Un choix possible est d'utiliser des dels rgb chainables du constructeur seeedStudio. La documentation de ce module est disponible [ici](https://wiki.seeedstudio.com/Grove-Chainable_RGB_LED/). La documentation de la puce P9813 employée sur ces modules grove est disponible [ici](https://files.seeedstudio.com/wiki/Grove-Chainable_RGB_LED/res/P9813_datasheet.pdf), celle-ci décrit le protocole de communication avec cette puce.

Pour acheter ce module grove, vous pouvez vous rendre sur le site de [gotronic](https://www.gotronic.fr/art-led-8-mm-rgb-grove-v2-0-104020048-27067.htm), ou sur celui de [lextronic](https://www.lextronic.fr/module-grove-led-rvb-v20-chainable-104020048-39434.html). Vous pouvez bien sur le trouver chez d'autres revendeurs.

Le code C++ ci-dessous est basé sur le code original de seedstudio pour arduino disponible sur le site [github de seeedStudio](https://github.com/Seeed-Studio/Grove_Chainable_RGB_LED).

Pour piloter les entrées/sorties tout ou rien de la raspberryPi, j'ai choisi d'utiliser la bibliothèque [piGpio](https://abyz.me.uk/rpi/pigpio/cif.html). Celle-ci s'installe avec la commande suivante: `sudo apt install pigpio`. A noter : cette bibliothèque nécessite des droits **root** pour piloter les entrées/sorties. L'exécutable que vous allez produire avec cette bibliothèque devra donc être préfixé par la commande `sudo`.

Lors de la compilation il faut penser à rajouter cette bibliothèque à votre ligne de compilation (-lpigpio sous GNU/Linux ou rajoutez cette bibliothèque à votre projet si vous êtes sous Microsoft Visual Studio).

## Le code C++ de la classe DelRGBChainable

Le fichier de déclaration (DelRGBChainable.h) est disponible ci-dessous:

```c++
// Olivier DARTOIS, Mars 2023

#pragma once
#include <pigpio.h>

class DelRGBChainable
{

    public:
        DelRGBChainable(unsigned char clk_pin, unsigned char  data_pin, unsigned char  number_of_leds);
        ~DelRGBChainable();

        void setColorRGB(unsigned char led, unsigned char  red, unsigned char  green, unsigned char  blue);

    private:
        const unsigned int _CLK_PULSE_DELAY = 1;
        unsigned char _clk_pin;
        unsigned char _data_pin;
        unsigned char _num_leds;

        unsigned char* _led_state;

        void clk(void);
        void sendByte(unsigned char b);
        void sendColor(unsigned char red, unsigned char green, unsigned char blue);
};
```

Le fichier de définition (DelRGBChainable.cpp) est disponible ci-dessous:

```c++
#include "DelRGBChainable.h"

DelRGBChainable::DelRGBChainable(unsigned char clk_pin, 
    unsigned char data_pin, unsigned char number_of_leds) :
    _clk_pin(clk_pin), _data_pin(data_pin), _num_leds(number_of_leds)
{
    gpioInitialise();
    gpioSetMode(_clk_pin, PI_OUTPUT);
    gpioSetMode(_data_pin, PI_OUTPUT);

    _led_state = new unsigned char[_num_leds * 3];

    for (unsigned char i = 0; i < _num_leds; i++)
        setColorRGB(i, 0, 0, 0);
}

DelRGBChainable::~DelRGBChainable() {
    delete[] _led_state;
    gpioTerminate();
}

void DelRGBChainable::clk(void) {
    gpioWrite(_clk_pin, 0);
    gpioDelay(_CLK_PULSE_DELAY);
    gpioWrite(_clk_pin, 1);
    gpioDelay(_CLK_PULSE_DELAY);
}

void DelRGBChainable::sendByte(unsigned char b) {
    // Send one bit at a time, starting with the MSB
    for (unsigned char i = 0; i < 8; i++) {
        // If MSB is 1, write one and clock it, else write 0 and clock
        if ((b & 0x80) != 0) {
            gpioWrite(_data_pin, 1);
        }
        else {
            gpioWrite(_data_pin, 0);
        }
        clk();

        // Advance to the next bit to send
        b <<= 1;
    }
}

void DelRGBChainable::sendColor(unsigned char red, unsigned char green, unsigned char blue) {
    // Start by sending a byte with the format "1 1 /B7 /B6 /G7 /G6 /R7 /R6"
    unsigned char prefix = 0xC0; // B11000000;
    if ((blue & 0x80) == 0) {
        prefix |= 0x20; // B00100000;
    }
    if ((blue & 0x40) == 0) {
        prefix |= 0x10; // B00010000;
    }
    if ((green & 0x80) == 0) {
        prefix |= 0x08; // B00001000;
    }
    if ((green & 0x40) == 0) {
        prefix |= 0x04; // B00000100;
    }
    if ((red & 0x80) == 0) {
        prefix |= 0x02; // B00000010;
    }
    if ((red & 0x40) == 0) {
        prefix |= 0x01; // B00000001;
    }
    sendByte(prefix);

    // Now must send the 3 colors
    sendByte(blue);
    sendByte(green);
    sendByte(red);
}

void DelRGBChainable::setColorRGB(unsigned char led, unsigned char red, unsigned char green, unsigned char blue) {
    // Send data frame prefix (32x "0")
    sendByte(0x00);
    sendByte(0x00);
    sendByte(0x00);
    sendByte(0x00);

    // Send color data for each one of the leds
    for (unsigned char i = 0; i < _num_leds; i++) {
        if (i == led) {
            _led_state[i * 3] = red;
            _led_state[i * 3 + 1] = green;
            _led_state[i * 3 + 2] = blue;
        }

        sendColor(_led_state[i * 3],
            _led_state[i * 3 + 1],
            _led_state[i * 3 + 2]);
    }
}
```

## Exemple d'utilisation de la classe

Le programme ci-dessous commande un ensemble de 5 dels chainables RGB connectées sur le port grove (12,13) d'un shield grove pour raspberryPi.
La ligne de compilation sera sous GNU/Linux: `g++ -o testDelRGB main.cpp DelRGBChainable.cpp -lpigpio -Wall` et l'exécution se fera par la commande suivante: `sudo ./testDelRGB`. Les dels RGB doivent avoir des couleurs différentes (rouge, vert, bleu, etc...) et leur intensité doit augmenter puis diminuer progressivement.

```c++
#include <iostream>
#include <thread>
#include <chrono>

#include "DelRGBChainable.h"

int main()
{
	std::cout << "Debut prog..." << std::endl;

    // Broche 12 : signal clock, Broche 13 : signal data
    // le 5 représente le nb de dels chainées
	DelRGBChainable d(12, 13, 5);

	for (int j = 0; j < 10; j++) {
		for (unsigned char i = 0; i < 255; i++) {
			d.setColorRGB(0, i, 0, 0);
			d.setColorRGB(1, 0, i, 0);
			d.setColorRGB(2, 0, 0, i);
			d.setColorRGB(3, i, i, 0);
			d.setColorRGB(4, i, 0, i);
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		for (unsigned char i = 255; i > 0; i--) {
			d.setColorRGB(0, i, 0, 0);
			d.setColorRGB(1, 0, i, 0);
			d.setColorRGB(2, 0, 0, i);
			d.setColorRGB(3, i, i, 0);
			d.setColorRGB(4, i, 0, i);
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	}

    // extinction des dels
	for (unsigned char i = 0; i < 5; i++)
		d.setColorRGB(i, 0, 0, 0);
	for (unsigned char i = 0; i < 5; i++)
		d.setColorRGB(i, 0, 0, 0);

	std::cout << "Fin prog..." << std::endl;

	return 0;
}
```