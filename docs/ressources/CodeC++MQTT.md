# Exemples de code C++ pour réaliser des envois/réceptions de données avec MQTT

!!! tldr "Objectifs de ce document"

    - installer la bibliothèque de gestion du protocole MQTT **paho**,
    - proposer un code pour envoyer des données à un broker MQTT,
    - proposer un code pour recevoir des données d'un broker MQTT.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Lycée Turgot - Limoges

## Présentation rapide du protocole MQTT

De nombreux sites parlent du protocole de l'Internet des Objets MQTT. Je vous renvoie vers cet [article](https://www.paessler.com/fr/it-explained/mqtt).

Vous devez savoir définir les termes ci-dessous:

- protocole MQTT : Message Queuing Telemetry Transport est un protocole de messagerie utilisant le principe *publish/subscribe* (pub/sub) au dessus du protocole TCP/IP.
- un *broker* : un *courtier*, il centralise les données qui lui sont envoyées par les *publisher* et les renvoie vers les *subscrider*,
- un *publisher* : un *éditeur*, c'est un client du *broker* vers lequel il publie des données dans un *topic*,
- un *subscriber* : un *abonné*, c'est un client du *broker* qui reçoit toutes les données du *topic* auquel il s'est abonné,
- un *topic* : une *rubrique*, elles sont hiérarchisées avec le caractère `\` comme délimiteur,
- la *QoS* : Quality of Service : 3 niveaux sont possibles (QoS 0 : envoie une seule fois sans acquittement donc pas d'assurance que la donnée soit bien délivrée, QoS 1 : messages arrivent *au moins* une fois donc attention aux doublons et QoS 2 : messages arrivent *exactement une fois*)

Le protocole MQTT utilise par défaut le port TCP 1883 pour communiquer de manière non chiffrée et le port 8883 avec un chiffrement SSL/TLS.
Il existe de nombreuses bibliothèques MQTT, celle retenue dans cette article est la bibliothèque [Eclipse Paho](https://eclipse.dev/paho/). Le logiciel qui fera office de *broker* sera le serveur [Mosquitto](https://mosquitto.org/).

## Installer la bibliothèque de développement Paho

Sur la distribution Debian et ces dérivées (raspberryPi OS), il suffit de taper la commande suivante pour installer les bibliothèques nécessaires ainsi que les fichiers de développement:

```console
olivier@Casimir:~$ sudo apt install libpaho-mqttpp-dev libpaho-mqtt-dev
```
Pour tester les codes ci-dessous, j'ai utilisé le serveur Mosquitto installé sur un serveur HomeAssistant. Un utilisateur `testmqtt` avec le mot de passe `testmqtt` a été créé depuis l'interface web de HomeAssistant. Pour visualiser de manière concrete la réception de messages, un afficheur à dels RGB ULanzi reflashé avec le firmware AWtrix a été utilisé. Cet afficheur a été configuré pour ce connecter au serveur MQTT du serveur Home Assistant. Pour en apprendre plus sur cet afficheur et son utilisation, consultez cet [article](https://domopi.eu/test-afficheur-led-ulanza-tc001/).

## Exemple de code pour publier des données vers un serveur MQTT

```cpp
// Test client MQTT avec la lib paho
// Installation sur Debian et dérivées:
// sudo apt install libpaho-mqttpp-dev libpaho-mqtt-dev
// ligne de compilation:
/*
olivier@Casimir:~/Dev/cpp$ g++ -o testClientMQTT -Wall testClientMQTT.cpp -lpaho-mqttpp3 -lpaho-mqtt3a
olivier@Casimir:~/Dev/cpp$ file testClientMQTT
testClientMQTT: ELF 64-bit LSB pie executable, x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=15d500af2854b8ec9413c3ae391efc25653ba07f, for GNU/Linux 3.2.0, not stripped
olivier@Casimir:~/Dev/cpp$ ldd testClientMQTT
        linux-vdso.so.1 (0x00007ffffac98000)
        libpaho-mqttpp3.so.1 => /lib/x86_64-linux-gnu/libpaho-mqttpp3.so.1 (0x00007f114d0d9000)
        libpaho-mqtt3a.so.1 => /lib/x86_64-linux-gnu/libpaho-mqtt3a.so.1 (0x00007f114d013000)
        libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f114cc00000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f114cff3000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f114ca1f000)
        libpaho-mqtt3as.so.1 => /lib/x86_64-linux-gnu/libpaho-mqtt3as.so.1 (0x00007f114cf26000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f114d14f000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f114cf21000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f114ce42000)
        libssl.so.3 => /lib/x86_64-linux-gnu/libssl.so.3 (0x00007f114c976000)
        libcrypto.so.3 => /lib/x86_64-linux-gnu/libcrypto.so.3 (0x00007f114c400000)
*/
#include <iostream>
#include <mqtt/async_client.h>

// Protocole TCP, @IP du serveur mosqitto, Port d'écoute de mosquitto
const std::string SERVER_ADDRESS{"tcp://192.168.10.41:1883"};
// Un identifiant pour votre client MQTT
const std::string CLIENT_ID{"cppClientPaho"};
// Vers quel rubrique (topic), vous allez publier votre message
const std::string TOPIC{"awtrix/notify"};
// Le contenu du message (payload) que vous voulez envoyer : ici c'est du JSON
// adapté pour que l'afficheur avec le firmware awtrix l'interprete
const std::string PAYLOAD{"{\"text\":\"hello\",\"color\":\"00FF00\",\"icon\":230,\"progress\": 50}"};

int main() {
    // déclaration d'un objet client asynchrone (async_client) avec comme nom client
    // le constructeur prend en paramètre l'@IP du serveur et l'identifiant du client
    mqtt::async_client client(SERVER_ADDRESS, CLIENT_ID);

    // Les options de connexion au serveur, en particulier le login/mot de passe
    // si le serveur MQTT le nécessite
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);
    connOpts.set_user_name("testmqtt");
    connOpts.set_password("testmqtt");

    // La gestion des erreurs se fait avec des exceptions, d'ou le *try* ci-dessous
    try {
        // Connexion au *broker*
        std::cout << "Connection au broker MQTT..." << std::endl;
        mqtt::token_ptr conntok = client.connect(connOpts);
        conntok->wait();

        // Publication d'un message
        std::cout << "Publication du message..." << std::endl;
        // on crée un objet pubmsg avec la rubrique et le message à envoyer
        mqtt::message_ptr pubmsg = mqtt::make_message(TOPIC, PAYLOAD);
        // on publie le message
        client.publish(pubmsg)->wait();

        // Deconnection du *broker*
        std::cout << "Deconnection du broker MQTT..." << std::endl;
        client.disconnect()->wait();
    }
    catch (const mqtt::exception& exc) {
        std::cerr << "Erreur: " << exc.what() << std::endl;
        return 1; // on sort avec une valeur differente de 0
    }

    return 0; // tout c'est bien passé, on sort avec une valeur nulle
}
```

## Exemple de code pour réceptionner des données depuis un serveur MQTT

```cpp
// Test client (subscrider=abonné) MQTT avec la lib paho
// Installation sur Debian et dérivées:
// sudo apt install libpaho-mqttpp-dev libpaho-mqtt-dev
// ligne de compilation:
// olivier@Casimir:~/Dev/cpp$ g++ -o testSubscriderMQTT -Wall testSubscriderMQTT.cpp -lpaho-mqttpp3 -lpaho-mqtt3a

#include <iostream>
#include <mqtt/async_client.h>

// Protocole TCP, @IP du serveur mosqitto, Port d'écoute de mosquitto
const std::string SERVER_ADDRESS{"tcp://192.168.10.41:1883"};
// Un identifiant pour votre abonné MQTT. ATTENTION: doit être différent de celui qui publie
const std::string CLIENT_ID{"cppSubscriderPaho"};
// A quelle rubrique (topic), vous allez vous abonner
const std::string TOPIC{"awtrix/notify"};

// Le *Callback* qui sera appelé lorsqu'un message arrive sur le topic ou l'on est abonné
class callback : public virtual mqtt::callback {
public:
    // Méthode appellée quand un message arrive
    void message_arrived(mqtt::const_message_ptr msg) override {
        std::cout << "ARRIVEE d'un message!" << std::endl;
        std::cout << "Topic: " << msg->get_topic() << std::endl;
        std::cout << "Message (Payload): " << msg->to_string() << std::endl;
    }

    // Methode appellée quand la connection au serveur est coupée
    void connection_lost(const std::string& cause) override {
        std::cout << "\nConnection perdue..." << std::endl;
        if (!cause.empty())
            std::cout << "\tcause: " << cause << std::endl;
    }
};

int main() {
    mqtt::async_client client(SERVER_ADDRESS, CLIENT_ID);

    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);
    connOpts.set_user_name("testmqtt");
    connOpts.set_password("testmqtt");

    // cet objet cb de la classe callback définie plus haut sera appellé
    // quand un nouveau message apparaitra sur le *topic* auquel on est abonné
    callback cb;
    client.set_callback(cb);

    try {
        // Connexion au *broker*
        std::cout << "Connection au broker MQTT..." << std::endl;
        mqtt::token_ptr conntok = client.connect(connOpts);
        conntok->wait();

        // Abonnement au *topic* souhaité
        std::cout << "Abonnement au *topic*..." << std::endl;
        mqtt::token_ptr subtok = client.subscribe(TOPIC, 1);
        subtok->wait();
        std::cout << "Abonnement actif au *topic* : " << TOPIC << std::endl;

        // Le client attend des messages en provenance du broker sur le topic souhaité
        std::cout << "Appuyez sur la touche *Entrée* pour quitter..." << std::endl;
        std::cin.get();

        // On sort proprement du programme en demandant à se déconnecter du broker
        if (client.is_connected()) {
            std::cout << "Deconnection du broker MQTT..." << std::endl;
            client.disconnect()->wait();
        } else {
            std::cout << "Le client est déjà déconnecté." << std::endl;
        }
    }
    catch (const mqtt::exception& exc) {
        std::cerr << "Erreur: " << exc.what() << std::endl;
        return 1;
    }

    return 0;
}
```