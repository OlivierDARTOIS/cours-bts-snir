# Découverte d'un serveur DELL en lame : le R650xs

!!! tldr "Objectifs de ce document"

    - découvrir un serveur physique en lame (rackable) de marque DELL,
    - identifier tous les composants physiques ainsi que la connectique,
    - configurer le firmware et les périphériques du serveur (RAID) et découvrir l'[IPMI](https://fr.wikipedia.org/wiki/Intelligent_Platform_Management_Interface) de chez DELL : iDRAC,
    - installer une distribution GNU/Linux Debian sur ce matériel.
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel Administrateur d'Infrastructures Sécurisées (AIS) - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser le réseau d’entreprise
        - Administrer et sécuriser un environnement système hétérogène
        - Administrer et sécuriser une infrastructure de serveurs virtualisée
        - Appliquer les bonnes pratiques et participer à la qualité de service
        - Proposer une solution informatique répondant à des besoins nouveaux

## Introduction
Que vous soyez dans une TPE ou un grand groupe industriel, vous utiliserez toujours des serveurs physiques pour votre infrastructure informatique. Ces serveurs peuvent se présenter en format *tour* (comme un ordinateur fixe grand public) ou en format *lame* (blade en anglais) pour être installé dans des baies informatiques. L'intérêt de ce format est qu'il prend moins de place et donc que vous pouvez *empiler* les serveurs. Ce qui permet de rationaliser les coûts en permettant une mutualisation des matériels (par exemple l'onduleur).

Pour le TP AIS, nous avons choisi de vous faire travailler sur un serveur rackable du constructeur DELL EMC (mais il existe des modèles équivalents chez d'autres constructeurs HPE par exemple). La référence de ce modèle est [**R650xs**](https://www.dell.com/fr-fr/shop/ipovw/poweredge-r650xs). L'intégralité des documentations constructeurs est disponible à cette [adresse](https://www.dell.com/support/home/fr-fr/product-support/product/poweredge-r650xs/docs).

## Constitution matériel du serveur

Pour aborder la découverte du matériel de ce serveur, vous pouvez commencer à lire la [documentation officiel](https://www.dell.com/support/manuals/fr-fr/poweredge-r650xs/per650xs_ism_pub/%C3%A0-propos-du-pr%C3%A9sent-document?guid=guid-488b2355-1124-4f1b-8f48-29cf0290937b&lang=fr-fr) de celui-ci. Les illustrations qui suivent sont issues de celle-ci. Vous vous reportez aussi sur votre serveur physique dans votre baie.

Vue avant du système:
![Vue avant du système](../img/ServeurR650xs/FaceAvant.png)

!!! note "Complétez le tableau ci-dessous à l'aide de la documentation constructeur"
    |Num.|Description (en une phrase)|
    |:---:|:---|
    |1||
    |2||
    |3||
    |4||
    |5||
    |6||

Vue arrière du système (à adapter à votre machine):
![Vue avant du système](../img/ServeurR650xs/FaceArriere.png)

!!! note "Complétez le tableau ci-dessous à l'aide de la documentation constructeur"
    |Num.|Description (en une phrase)|
    |:---:|:---|
    |1||
    |2||
    |3||
    |4||
    |5||
    |6||
    |7||
    |8||
    |9||
    |10||
    |11||
    |12||

Vue intérieur du système (à faire avec l'enseignant sur un serveur *ouvert*) :
![Vue intérieure du système](../img/ServeurR650xs/Interieur.png)

!!! note "Complétez le tableau ci-dessous à l'aide de la documentation constructeur"
    |Num.|Description (en une phrase)|
    |:---:|:---|
    |1||
    |2||
    |3||
    |4||
    |5||
    |6||
    |7||
    |8||
    |9||
    |10||
    |11||
    |12||
    |13||
    |14||
    |15||
    |16||

Vous constatez que le serveur est équipé de 4 disques [HDD SAS](https://fr.wikipedia.org/wiki/Serial_Attached_SCSI) au format 2.5" de 600Go. Ces 4 disques sont contrôlés par une carte RAID matériel DELL PERC (**P**ower**E**dge **R**AID **C**ontroller) H755. Un module carte contrôleur [BOSS](https://www.dell.com/support/manuals/fr-fr/boss-s-1/boss_s1_ug_publication/pr%C3%A9sentation) (**B**oot **O**ptimized **S**torage **S**olution) avec deux disques SSD M2 de 240Go, une carte Broadcom 4 ports réseaux 1Gb/s complètent l'équipement de base.

### Qu'est ce que le **RAID** et son utilité ?

Le mot **RAID** est un acronyme qui signifie **R**edundant **A**rray of **I**ndependant **D**isks.  C'est le regroupement de plusieurs disques indépendants pour créer un ou plusieurs disques virtuels avec différents niveau RAID. L'[article](https://www.dell.com/support/kbdoc/fr-fr/000128635/serveurs-dell-quels-sont-les-niveaux-de-raid-et-leurs-sp%C3%A9cifications) présentant les niveaux de RAID chez DELL. Nous allons décrire cinq niveaux de RAID, sachant qu'il en existe d'autres, à l'aide du tableau ci-dessous extrait de cet [article](https://www.ionos.fr/digitalguide/serveur/securite/comparaison-des-niveaux-raid/):

||RAID 0|RAID 1|RAID 5|RAID 6|RAID 10 (1+0)|
|:---|:---:|:---:|:---:|:---:|:---:|
|Nombre minimum de disques durs|2|2|3|4|4|
|Processus utilisé|*Striping*|Mise en miroir (*mirroring*)|*Striping* et parité|*Striping* et double parité|*Striping* de données mises en miroir|
|Résilience|Faible|Très élevée, un lecteur peut tomber en panne|Moyenne, un lecteur peut tomber en panne|Élevée, deux lecteurs peuvent tomber en panne|Très élevée, un lecteur par sous-réseau peut tomber en panne|
|Capacité de stockage pour les données utilisateur|100 %|50 %|67 % (augmente avec chaque disque supplémentaire)|50 % (augmente avec chaque disque supplémentaire)|50 %|
|Vitesse d’écriture|Très élevée|Faible|Moyenne|Faible|Moyenne|
|Vitesse de lecture|Très élevée|Moyenne|Élevée|Élevée|Très élevée|
|Coût|Faible|Très élevé|Moyen|Élevé|Très élevé|

![Cas d'utilisation du RAID](../img/ServeurR650xs/CasUtilisationRAID.svg)

!!! warning "Remarque importante"
    Un système RAID ne vous affranchit pas de faire de la sauvegarde de données chiffrée (par exemple sur un disque externe ou sur un espace de stockage en ligne). En cas de défaillance catastrophique (typiquement le feu), vous devez pouvoir redémarrer rapidement à partir de vos sauvegardes. Cet aspect sera développé tout au long de la formation dans les autres TPs.

!!! note "Pourquoi avoir deux sous-système disques sur le serveur ?"
    Généralement vous allez installer le système d'exploitation sur un système disque et les données sur un autre système disque. Choisissez les niveaux RAID et le sous système disque afin de limiter les impacts sur le fonctionnement de votre entreprise en cas de défaillance matériel d'un ou plusieurs disques:  
    Système d'exploitation : `______________________________________________________________`  
    Données : `_____________________________________________________________________________`

Vous allez maintenant configurer la machine et les sous systèmes disques.

### Configuration du matériel par le BIOS du serveur

La documentation officielle sur le bios est disponible à cette [adresse](https://www.dell.com/support/manuals/fr-fr/poweredge-r650xs/per650xs_bios_ism_pub/applications-de-gestion-pr%C3%A9-syst%C3%A8me-dexploitation?guid=guid-fe82d037-c848-473f-8c57-fc9dfde6e027&lang=fr-fr). Dans un premier temps, vous allez connecter un écran VGA et un clavier USB (connecté sur un port USB à l'arrière du serveur) pour réaliser cette configuration. Nous verrons, dans un second temps, comment se passer de ce matériel pour prendre à distance le contrôle du serveur. Connectez l'écran VGA et le clavier USB, démarrez le serveur puis accédez au BIOS en appuyant sur la touche **F2 : System Setup** lorsque le système le propose à l'écran.

Le menu qui apparaît propose alors trois options:

- **System BIOS** : accès aux réglages du BIOS,
- **iDRAC Settings** : paramétrage du logiciel iDRAC de prise à distance du serveur,
- **Device Settings** : menu de configuration de toutes les cartes additionnelles du serveur (carte BOSS, PERC, Réseau, ...)

Nous allons limiter nos actions pour l'instant à la configuration de la carte RAID PERC dans le but d'installer un SE Debian. Comme défini précédemment, un système RAID1 sera défini sur le matériel BOSS, un système RAID5 sur 3 disques avec un disque de secours sur le matériel PERC.

!!! note "Mise en place des systèmes RAID"
    Mettre le disque HDD SAS numéro 3 en face avant en mode secours (hotspare) : F2:System setup -> Device Settings -> RAID Controller Dell PERC H755 Front -> View server profile -> Physical Disk Management -> Physical Disk 01:03 -> Select operation -> Assign Global Hot Spare -> Go. Le status du disque passe de **Ready** à **Hot Spare**. Echap -> Echap pour revenir au menu Device Settings  
    Configuration des trois disques restants en mode RAID5: F2:System setup -> Device Settings -> RAID Controller Dell PERC H755 Front -> Configure -> Clear Configuration -> Create virtual disk -> RAID Level : 5 -> Select Physical Disks -> Sélectionner vos disques (ici vous pouvez prendre Check All) -> Apply changes -> OK -> descendre au menu Create virtual disk -> Confirmer puis Yes -> OK -> Echap -> Echap pour revenir au menu Device Settings

Rentrez dans le menu *System BIOS* puis complétez le tableau ci-dessous en parcourant les différents menus:

|Question|Réponse|
|:---|---:|
|Version BIOS||
|Taille et type de la RAM||
|Marque et type du processeur||
|Nombre de coeurs matériels et logiciels||

Vous allez en profiter pour indiquer au BIOS que nous souhaitons démarrer en premier sur un périphérique USB externe (pour installer notre future SE Debian) : System BIOS -> Boot Settings, activez (enabled) l'option Generic USB Boot puis choisissez le menu UEFI Boot Settings -> UEFI Boot Sequence mettre l'option Generic USB Boot en premier puis bouton OK -> bouton Back -> Back -> Finish. Répondez Yes pour sauvegarder vos changements. Redémarrez.

Au démarrage appuyez sur la touche **F11** : Boot Manager. Laissez reposer le serveur !

## Installer la distribution GNU/Linux Debian sur le serveur DELL

Vous allez créer une clé USB amorçable à l'aide du logiciel Rufus depuis votre machine Microsoft Windows. Vous téléchargerez l'image ISO de la dernière version de la [Debian pour AMD64 en version NetInst](https://www.debian.org/CD/netinst/) depuis le site officiel **OU** pour économiser de la bande passante internet, vous utilisez l'image ISO de la Debian qui se trouve dans `C:\ImagesISO` de votre machine Microsoft Windows.

!!! note "Créer votre clé USB"
    Créez votre clé USB amorçable avec [Rufus](https://rufus.ie/fr/) et l'image ISO indiquée précédemment. Connectez votre clé USB sur le port USB en facade du serveur DELL.

Vous avez laisser en repos votre serveur. Une fois la clé introduite choisissez l'option *One shot UEFI Boot Menu* puis *Generic USB Boot* ou *Disk connected to front USB1*.

!!! note "Installation de Debian sur le serveur DELL"
    Si vous n'avez jamais installé de système Debian, il est conseillé de lire cet [article](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_01/#cahier-des-charges-pour-le-serveur-gnulinux-debian) en l'adaptant bien sur à votre contexte. Connectez le câble réseau WAN de votre baie à une prise réseau de la salle.  
    Pour cette installation vous affecterez les paramètres IP suivants: @IP/Masque: 10.187.52.`___`/24, Passerelle: 10.187.52.245, DNS: 10.187.52.5. Lors de la configuration du réseau, ne pas connecter de cable réseau sur le bandeau (port ETH0). Faire la configuration réseau à la  main sur la carte réseau **eno8303** puis connectez le câble réseau entre le port WAN et ETH0.
    Mettre la distribution avec les paramètres en français. Installez sur le *RAID1* (BOSS) le système (la *racine* -> `/`) et sur le *RAID5* (PERC) les données (le répertoire `/home`). Le système de fichier employé sera EXT4. Pour le partitionnement du système BOSS, choisir **assisté - utiliser un disque entier** puis **tout dans une seule partition** enfin vérifiez que vous avez une partition de type EFS (pour le boot UEFI), une partition EXT4 monté sur `/` et une partition d'échange (swap). <span style="color: #ff0000">**Validation professeur**</span> de cette étape : `___`    
    Pour économiser la bande passante vers Internet, lors du choix du *Pays du miroir*, prendre l'option en haut de liste *Saisie manuelle* puis saisir l'@IP de notre miroir local **10.187.52.10** et comme répertoire `/debian/`. Réalisez l'installation sans mettre d'interface graphique. Installez le chargeur de démarrage GRUB sur le RAID1 si l'installeur vous le demande. Redémarrez en enlevant la clé USB quand cela vous est demandé.

Une fois la machine redémarrée, connectez-vous en tant qu'utilisateur *root* avec le mot de passe que vous avez saisi. Puis testez les commandes ci-dessous:

|Commande|Rôle et résultat de la commande|
|:---|:---|
|`free -h`||
|`df -h`||
|`lsblk`||
|`lscpu`||
|`lsusb`||
|`lspci`||
|`lsmem`||
|`htop`|(à installer)|
|`inxi -Fxz`|(à installer)|
|`hwinfo --short`|(à installer)|
|`hdparm -Tt /dev/sdX`|(à installer et changer le X par le numéro du disque dur à tester)|

Vous venez d'installer votre premier SE sur un serveur DELL. Mais il y a un gros problème ! Vous étiez physiquement devant le serveur ce qui n'est généralement pas le cas quand vous installez votre serveur dans un *datacenter*. Nous allons remédier à ce problème en configurant l’accès à iDRAC !

## Configuration et découverte de iDRAC

iDRAC signifie *integrated Dell Remote Access Controller*. C'est un système Linux durci qui est embarqué dans une mémoire de stockage dans le serveur. Ce système est fonctionnel à partir du moment où le serveur est alimenté (donc même si le serveur n'est pas *démarré*). Ce système est disponible à travers une interface réseau dédié afin de séparer le réseau d'administration du reste des autres réseaux (voir le cours sur [les fondamentaux de la sécurité réseau](https://olivierdartois.gitlab.io/cours-bts-snir/cours/FondamentauxSecuReseau/)). Il faut donc affecter une adresse IP à cette carte ou avoir un serveur DHCP sur le réseau d'administration. Vous pouvez alors prendre la main sur le serveur à travers une interface web.

!!! info "Superviser un serveur avec l'IPMI"
    L'**IPMI** ou *Intelligent Platform Management Interface* est un ensemble de spécifications permettant de gérer et de superviser un système (souvent à distance) indépendamment de l'OS ou du firmware (BIOS/UEFI) de ce dernier. En gros cela permet de garder un accès sur une machine même si son SE est planté et cela même si elle est éteinte. Les seuls prérequis pour utiliser l'IPMI sont les suivant:

    - La machine doit être alimentée (pas forcément allumée),
    - La machine doit être connectée à un réseau et avoir/obtenir une adresse ip.

    Ce système s'appelle **iDRAC** chez Dell, **iLO** chez HP, **RSA** chez IBM...

Pour accéder à iDRAC, redémarrez le serveur (commande `reboot` sous Linux) et appuyez sur la touche **F2** lorsque le serveur vous le propose puis choisissez l'option *iDRAC Settings*. Pour accéder à distance à iDRAC, il faut configurer une adresse IP. De manière arbitraire, vous allez prendre une adresse réseau 10.10.10.0/24. L'iDRAC sera accessible sur la première adresse de ce réseau et vous allez configurer la carte réseau **filaire** sur le portable sous Microsoft Windows pour être sur la deuxième adresse.

!!! note "Configuration pile TCP/IP de l'iDRAC"
    Rubrique Network -> Noter l'@MAC de l'interface dédiée cela nous servira plus tard (@MAC: `_______________________`). Descendre à l'option IPV4 Settings -> Enable IPv4 : Enabled, Enable DHCP : Disabled. Fixer alors les paramètres IP de cette interface (IP Address, pas de Gateway, Subnet Mask). Si vous ne vous servez pas de IPv6 désactivez-le.  
    Enfin si votre interface d'administration est dans un VLAN, configurez la partie VLAN Settings -> Enabled VLAN ID : Enabled et VLAN ID au numéro de VLAN d'administration. **Câblez la liaison entre le portable et l'interface réseau dédié de l'IDRAC : port ADM du bandeau**.  
    Vous pouvez alors accéder à l'interface de gestion iDRAC depuis un navigateur en tapant: https://@IP-iDRAC/ (accepter l'exception de sécurité si besoin). Le login est *root*, le mdp est sur l'étiquette d'identification du serveur.

Vous pouvez désormais surveiller votre serveur à distance mais surtout **prendre la main sur celui-ci** à travers une interface KVM (Keyboard-Video-Mouse). Pour cela, il suffit de cliquer sur la **Console virtuelle** sur la page d'accueil. Vous pouvez aussi modifier tous les paramètres du BIOS depuis cette interface car cette console est disponible avant le démarrage physique de la machine. La documentation officielle de l'iDRAC est disponible sur cette [page](https://www.dell.com/support/manuals/fr-fr/poweredge-r650xs/idrac9_7.xx_ug/pr%C3%A9sentation-de-lidrac?guid=guid-a03c2558-4f39-40c8-88b8-38835d0e9003&lang=fr-fr).

!!! note "Testez iDRAC"
    Testez les différentes possibilités de l'iDRAC (y compris pour visualiser le démarrage de la machine). Visualisez par exemple l'état de vos systèmes RAID. Testez les possibilités de la console virtuelle... Bref détaillez toutes les possibilités de iDRAC est hors de propos de ce TP (RTFM !!)

## Éléments complémentaires

Ci-dessous vous trouverez quelques pistes pour compléter ce que vous venez de découvrir.

### Diminuer le niveau sonore du serveur

La ventilation forcée du serveur est bruyante ! Vous pouvez diminuer cette nuisance sonore en contrepartie d'une légère baisse de performance.

!!! tip "Diminuer le bruit de ventilation du serveur"
    Pour cela au démarrage du système accéder avec la touche F2 au System setup -> iDRAC settings -> Thermal -> Thermal Profile : le mettre à Sound Cap -> Sauvegardez **OU** faites-le depuis iDRAC.

### Tester différents niveau de RAID

Le serveur est livré avec quatre disques durs en face avant connectés sur le contrôleur PERC. Dell conseille dans ce cas de faire du RAID6 sur les 4 disques plutôt que du RAID5 avec un disque de spare.  
Vous pouvez aussi séparés en deux disques virtuels les 4 disques : par exemple 2 disques en RAID0 et deux disques en RAID1. Dans ce cas il est intéressant de comparer les vitesses avec la commande `hdparm`. Si vous voulez le débit maximum en lecture/écriture sur vos disques (cas d'usage: montage vidéo) vous pouvez tester le RAID0 sur tous les disques mais dans ce cas il n'y a aucune redondance de données et vous perdez tout si un disque meurt !
Vous pouvez aussi ne pas utiliser le contrôleur PERC et utiliser les disques en mode RAID logiciel à l'installation de la Debian.

### Administrer iDRAC en ligne de commande

Vous pouvez depuis un autre ordinateur piloter iDRAC à l'aide du logiciel [racadm](https://www.dell.com/support/contents/fr-fr/videos/videoplayer/tutorial-on-idrac-racadm-command-line/1706695616981987241), ce qui permet de réaliser des scripts d'automatisation quand vous avez de nombreux serveurs à gérer.

## Conclusion de ce TP

Si l'on fait le bilan de ce TP:

- découverte de la partie matériel dans un serveur lame de marque Dell,
- découverte et configuration d'un BIOS,
- configuration de cartes matérielles RAID (PERC et BOSS),
- découverte de l'IPMI avec iDRAC pour réaliser l'administration bas niveau à distance du serveur,
- installation d'un SE GNU/Linux Debian de base,
- découverte de quelques commandes Linux pour tester le matériel,
- séparation du réseau d'administration en utilisant l'interface dédié iDRAC du serveur.

Vous avez les principes de bases pour installer maintenant un hyperviseur de type Proxmox ou un SE Microsoft Server.


