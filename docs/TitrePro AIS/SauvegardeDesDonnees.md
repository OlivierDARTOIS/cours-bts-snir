# Réaliser la sauvegarde des données de votre entreprise

!!! tldr "Objectifs de ce document"

    - Présenter la théorie associée avec la sauvegarde des données,
    - Sauvegarder des données avec un script (linux: bash, windows: powershell) et une tâche planifiée (linux: cron, windows: planificateur de tâches) en utilisant des commandes disponibles dans les SE,
    - Utiliser un logiciel spécialisé pour la sauvegarde: BORG,
    - Utiliser une solution tout en un avec interface web : Proxmox Backup Server,
    - autres solutions (VEEAM, ...)
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel AIS - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser une infrastructure de serveurs virtualisée
        - Appliquer les bonnes pratiques et participer à la qualité de service
        - Participer à l’élaboration et à la mise en œuvre de la politique de sécurité

## Introduction

En mars 2021, l’incendie d’un datacenter OVH à Strasbourg a mis hors service un nombre très important de sites web (3,6 millions selon Netcraft), ce qui rappelle une fois encore l’importance des sauvegardes.

L’une des cyberattaques par ransomware les plus marquantes de ces dernières années a frappé le Centre hospitalier de Dax, 10 février 2021, lorsque l’ensemble de son système d’information hospitalier (SIH) a été mis hors service[...]Une sauvegarde préalable sur bandes a heureusement permis de récupérer les données informatisées de nombreux patients, bien que ces données ne puissent être consultées qu’en lecture seule sur un poste dédié, sans possibilité de mise à jour.[...] Après plus d’un an, les coûts totaux de cette attaque ont été estimés par l’établissement à plus 2,3 millions d’euros.

Deux exemples parmi tant d'autres qui soulignent l'importance de la sauvegarde du SI. L'ANSSI rappelle dans son document "Les essentiels - Sauvegarde des systèmes d'information" les bonnes pratiques à mettre en oeuvre. Ce [document](https://cyber.gouv.fr/sites/default/files/document/anssi_essentiels_sauvegarde-si_v1.1.pdf) est reproduit ci-dessous et il est complété par le document dédié à la [sauvegarde du SI](https://cyber.gouv.fr/sites/default/files/document/anssi-fondamentaux-sauvegarde_systemes_dinformation_v1-0.pdf).

![Les essentiels de l'ANSSI - Sauvegarde du SI](../img/SauvegardeSI/essentielsSauvegardeSI.png)

!!! note "Exercice"
    === "Question"
        Dans le document de l'ANSSI dédié à la sauvegarde du SI, vous pouvez trouver la règle **3-2-1**. Que signifie cette règle ? Proposez des solutions pour le **2** et le **1**.
        Qui doit être à l'initiative de la sauvegarde de données ?
    === "Votre réponse"
        Def. de la règle:  
        Solutions pour le **2**:  
        Solutions pour le **1**:  
        Initiative de la sauvegarde par:

Pour compléter cette partie théorique, je vous conseille de lire l'article de [linux pratique hors série n°51 de juin 2021](https://connect.ed-diamond.com/linux-pratique/lphs-051/faites-vous-de-bonnes-sauvegardes). Il vous permettra de vous poser les bonnes questions lorsque vous devrez choisir votre système de sauvegarde.

Enfin donnez la définition de quelques termes à connaître dans le domaine de la sauvegarde en consultant les sites suivants:

- [Sauvegarde sur AWS](https://aws.amazon.com/fr/compare/the-difference-between-incremental-differential-and-other-backups/#:~:text=Une%20strat%C3%A9gie%20de%20sauvegarde%20diff%C3%A9rentielle,donn%C3%A9es%20depuis%20la%20derni%C3%A8re%20sauvegarde.)
- Définition de la [déduplication](https://fr.wikipedia.org/wiki/D%C3%A9duplication) sur wikipédia

!!! note "Exercice"
    - Sauvegarde complète:
    - Sauvegarde incrémentale:
    - Sauvegarde différentielle:
    - Déduplication:

A votre charge de définir ce qui doit être sauvegardé, sa fréquence (tous les jours, toutes les semaines, etc...) ainsi que la durée de conservation des données (15 jours glissants, sur un mois, un an, dix ans !!!).

!!! warning "Remarque"
    Ce TP ne portera pas uniquement sur la mise en place de la sauvegarde des données mais sera aussi l'occasion de réviser ou d'apprendre des bases de l'administration système : création de groupes et d'utilisateurs, gestion des droits, chiffrement des données, envoie des sauvegardes vers un serveur, création de scripts simples, mise en oeuvre de tâches planifiées et ceci aussi bien sous Linux que sous Windows.

## Architecture du TP

Pour mettre en oeuvre ce TP, vous allez créer 3 machines virtuelles:

- une debian en mode console: cette MV va accueillir la sauvegarde à distance des autres postes, un partage Windows sera créé sur cette machine à l'aide du logiciel [Samba](https://www.samba.org/), le partage s'appellera **sauvegarde** et sera accéssible uniquement à l'utilisateur **userbackup** en lecture/écriture. Il sera localiser dans `/mnt/sauvegarde`. L'utilisateur **userbackup** aura comme *maison* `/mnt/sauvegarde`, son shell de connexion sera `/bin/bash` et il appartiendra au groupe **backup**,
- une debian en mode graphique: rajouter des utilisateurs (3, commande `adduser` sous Debian) et quelques fichiers pour chacun de ces utilisateurs,
- une windows10: rajouter des utilisateurs (3) et quelques fichiers pour chacun de ces utilisateurs.

!!! note "Exercice"
    Pour réaliser le cahier des charges pour la VM qui va accueillir la sauvegarde vous **devez** vous reporter à ce [TP](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_02/). Cependant pour vous aidez à vous organiser, vous effectuerez les étapes suivantes **dans l'ordre** :  
    - Créez le groupe *backup*, (`__________________`). Comment obtient-on la liste des groupes ? `__________________________________`  
    - Créez le répertoire `sauvegarde` dans `/mnt` (`__________________`). Vous pouvez visualiser une arborescence avec la commande `tree` (`__________________`).  
    - Créez un utilisateur *userbackup* (attention à tous les paramètres du cahier des charges): `_________________________________________________________________________________________` N'oubliez pas de lui fixer un mot de passe (`________________________`).  
    - Modifiez les droits du répertoire `/mnt/sauvegarde` pour que seul l'utilisateur *userbackup* puisse lire, écrire et rentrer dans celui-ci : `_________________________________________________________________________________________`. Créez un fichier `.profile` dans `/mnt/sauvegarde` avec le contenu suivant `umask 0077`, cela permet de fixer les droits des futurs fichiers à `rw-------` et des répertoires à `rwx------`.  
    - Installez le logiciel *samba* (`__________________`). Renommez le fichier de configuration original `/etc/samba/smb.conf` en `/etc/samba/smb.conf.ori` et créez votre configuration dans `/etc/samba/smb.conf`. Pour le partage `[sauvegarde]` la gestion des droits sera assuré par les lignes suivantes:
        ```console
        create mask = 0000
        directory mask = 0000
        force create mode = 0600
        force directory mode = 0700
        ```
    Filtrez l'utilisateur sur ce partage uniquement pour *userbackup* (paramètre *valid users*). Testez votre fichier de configuration avec la commande `testparm`. Corrigez si besoin les erreurs.  
    - Rajoutez l'utilisateur dans la base d'utilisateurs samba avec la commande `pdbedit` (`__________________`).  
    - Testez le partage *sauvegarde* depuis un poste sous Microsoft Windows, créez un répertoire et un fichier pour vérifier que les droits sont bien appliqués.

!!! note "Exercice"
    Créez votre VM Debian en mode graphique et rajoutez trois utilisateurs simples.  
    Créez votre VM Windows et rajoutez trois utilisateurs simples.  
    Rajoutez des fichiers et répertoires pour chacun des utilisateurs Linux et Windows.

## Mise en oeuvre d'une sauvegarde simple

Pour commencer vous allez utiliser les outils disponibles sur les différents SE (Linux, Windows).

Sur la machine Debian graphique : la sauvegarde du répertoire **home** sera réalisée avec la commande **tar** avec une compression au format **bzip2** en préservant les droits des fichiers. Vous devez créer sur cette machine un nouveau compte(**operateursauv**) avec des droits **sudo** uniquement pour cet utilisateur sans avoir de mot de passe à taper car c'est cet utilisateur qui créera l'archive tar dans un script sans interaction avec la console. Cette sauvegarde sera chiffré avec la commande **openssl**. Vous calculerez le condensat de cette archive avec l'algorithme **sha256** avec la commande **sha256sum**. Cette archive et son condensat seront transmise par la commande **scp** vers le répertoire */mnt/sauvegarde* sans saisir de mot de passe (donc avec génération d'une clé) tous les jours sauf le samedi et le dimanche à 3H du matin. Un script en langage bash sera codé pour cette tâche.

Sur la machine Windows: la sauvegarde du répertoire **utilisateurs** sera réalisée avec le logiciel **7z** avec le taux de compression maximum et le mot de passe de votre choix. Cette archive sera transmise sur le partage windows *sauvegarde* tous les jours sauf le lundi et le mardi à 5H du matin. Un script en langage PowerShell sera codé pour cette tâche.

!!! note "Exercice : Script de sauvegarde sous GNU/Linux Debian"
    Pour organiser votre travail, proposer sur papier une liste numérotée des actions à mener les unes après les autres. Faites valider à l'enseignant.  
    Pour vous aider sur la création du script bash, reportez-vous à cet [article](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_02/#creation-dun-script-de-sauvegarde-du-repertoire-home). ATTENTION il faut l'adapter ! Pour l'automatisation de la tâche avec **cron**, reportez-vous à la fin de l'article cité précédemment. Pour mieux comprendre la commande **tar**, lire cet [article](https://linux.goffinet.org/administration/arborescence-de-fichiers/archivage-et-compression/). Pour chiffrer le fichier avec la commande **openssl** consultez ce [site](https://angristan.fr/chiffrer-fichier-openssl-linux/). Pour donner des droits **sudo** uniquement à un utilisateur, reportez à cet [échange sur askubuntu](https://askubuntu.com/questions/334318/sudoers-file-enable-nopasswd-for-user-all-commands).Pour tout le reste vous devez l'avoir fait dans les autres TPs. Pourquoi calcule-t-on un condensat sur l'archive ?

!!! note "Exercice : Script de sauvegarde sous Microsoft Windows"
    Il faut installer le logiciel 7z en ligne de commande, pour cela rendez-vous sur le site [https://www.7-zip.org/download.html](https://www.7-zip.org/download.html), prendre le type **7z** pour architecture Windows x86/x64. Pour connecter un partage réseau en ligne de commande vous pouvez consulter [https://makerhelp.fr/connecter-un-lecteur-reseau-en-ligne-de-commande-avec-windows-10/](https://makerhelp.fr/connecter-un-lecteur-reseau-en-ligne-de-commande-avec-windows-10/). Pour créer mettre en place l'automatisation vous utiliserez le planificateur de tâches, une présentation est faite dans ce lien: [https://www.pcastuces.com/pratique/astuces/5515.htm](https://www.pcastuces.com/pratique/astuces/5515.htm). Votre script powershell n'en est pas vraiment un, c'est un fichier batch (extension .bat) qui est un fichier texte dans lequel vous mettez les lignes de commandes les unes apres les autres.

!!! note "Quelques questions pour critiquer ce système de sauvegarde"
    - Dans cette configuration vous déclenchez la sauvegarde sur le poste utilisateur vers le serveur de sauvegarde. Cela ne respecte pas la règle de l'ANSSI : *le serveur de sauvegarde est à l'initiative de la sauvegarde*. Comment faire pour que le serveur de sauvegarde soit à l'initiative de la sauvegarde ?
    - Quel est le défaut principal concernant le chiffrement aussi bien sous Windows que sous Linux.
    - Quel type de sauvegarde avez-vous mis en oeuvre ici ? Quel est son principal défaut ?

!!! note "Exercice : restauration de votre sauvegarde"
    Effacez vous utilisateurs sous Linux et Windows.  
    Décrivez la procédure de restauration en environnement Linux dans un document électronique. Puis codez un script qui réalise votre procédure. Testez votre script, vérifiez que les fichiers sont correctement restaurés et que les droits ont été conservés.  
    Même question pour l'environnement Windows.
    Conclusion.

## Installer, configurer et utiliser un serveur PBS (Proxmox Backup Server)

Proxmox Backup Server est une solution professionnelle de sauvegarde sur le modèle client-serveur. Le logiciel client permet de sauvegarder et de restaurer des machines virtuelles sur des serveurs Proxmox VE, des serveurs de stockage réseau ou des serveurs dédiés. Le serveur stocke les données sauvegardées et les met à disposition via une API, accessible depuis le client ou depuis son interface web officielle. La documentation officielle est disponible sur [https://pbs.proxmox.com/docs/index.html](https://pbs.proxmox.com/docs/index.html).

### Présentation du logiciel

Les fonctionnalités offertes par Proxmox Backup Server sont nombreuses. Voici ses fonctionnalités, que l’on retrouve communément dans les autres solutions de sauvegarde :

- sauvegarde incrémentielle : réduction de l’espace de stockage requis en ne sauvegardant que les modifications depuis la dernière sauvegarde ;
- prise en charge des sauvegardes sur bande : répond aux besoins des organisations ayant des exigences de conservation des données à long terme ;
- compression et déduplication des données : optimisation de l’utilisation du stockage et accélération des processus de sauvegarde et de restauration ;
- planification des sauvegardes : automatisation des processus de sauvegarde et réduction des risques d’oublis ou d’erreurs humaines ;
- restauration granulaire : possibilité de restaurer des fichiers, des dossiers ou des systèmes entiers en fonction des besoins ;
- sécurité : chiffrement des données en transit entre le client et le serveur, et au repos sur le serveur.

Fonctionnalités uniques de Proxmox Backup Server :

- intégration native avec Proxmox VE : solution unifiée pour la gestion des environnements virtualisés et de leurs sauvegardes ;
- outil de ligne de commandes *pxar* : contrôle précis des opérations de sauvegarde et de restauration ;
- gestion des serveurs distants et synchronisation : répartition optimale des ressources et meilleure résilience grâce à la gestion de plusieurs serveurs distants et à la synchronisation des données entre eux ;
- calendrier des événements : format inspiré de la spécification systemd pour planifier des tâches récurrentes, offrant une plus grande flexibilité dans la gestion des tâches de maintenance ;

PBS peut faire différents types de sauvegarde :

- vm : pour les machines virtuelles, avec un fichier de configuration et une archive d’image pour chaque disque ;
- ct : pour les conteneurs, avec un fichier de configuration et une archive de fichiers pour le contenu du système de fichiers ;
- host : pour les sauvegardes de fichiers/répertoires sur une machine, qu’elle soit physique, virtuelle ou sous forme de conteneur ;

Proxmox Backup Server prend en charge plusieurs méthodes de sauvegarde, notamment :

- sauvegarde complète : consiste à sauvegarder toutes les données du système. Cela prend plus de temps et nécessite plus d’espace de stockage, mais facilite la restauration, car il n’est pas nécessaire de combiner plusieurs sauvegardes incrémentielles ;
- Sauvegarde incrémentielle : consiste à sauvegarder uniquement les données modifiées depuis la dernière sauvegarde, qu’elle soit complète ou incrémentielle. Les sauvegardes incrémentielles sont plus rapides et nécessitent moins d’espace de stockage, mais la restauration peut être plus complexe, car il faut combiner plusieurs sauvegardes incrémentielles pour restaurer l’état complet d’un système.

Chaque sauvegarde possède un identifiant unique (ID) et un groupe de sauvegarde (type/ID). Pour les sauvegardes spécifiques de type snapshot (instantané), on utilise un triplet type/ID/time. Plusieurs types d’archives sont utilisés pour les sauvegardes :

- archives d’images (.img) : pour les images de machines virtuelles et autres données binaires volumineuses, divisées en morceaux de taille fixe ;
- archives de fichiers (.pxar) : pour stocker un arbre de répertoires complet, divisés en morceaux de taille variable pour optimiser la déduplication ;
- données binaires (BLOBs) : pour stocker de petites données binaires (< 16 Mo), comme les fichiers de configuration. Les fichiers plus volumineux doivent être stockés dans des archives d’images.

Proxmox Backup Server prend en charge la découpe en morceaux de taille fixe et variable :

- la découpe en morceaux de taille fixe nécessite peu de puissance de calcul et est utilisée pour les images de machines virtuelles ;
- la découpe en morceaux de taille variable nécessite plus de puissance de calcul, mais est essentielle pour obtenir de bons taux de déduplication pour les archives de fichiers.

On stocke les sauvegardes dans un **entrepôt de données** (*datastore* en anglais) : un emplacement logique, local ou distant, où les *snapshots* de sauvegarde et leurs *chunks* sont stockés.

### Installation de PBS

Vous allez installer PBS sur votre serveur Proxmox. Notez qu'il s'agit d'une **très mauvaise pratique** ! Le serveur PBS doit être installé dans le réseau d'administration sur un matériel dédié avec soit des disques durs en RAID, soit un système de fichiers qui support le RAID (type ZFS Raid) ou dans l'idéal une baie [SAN](https://fr.wikipedia.org/wiki/R%C3%A9seau_de_stockage_SAN).

!!! note "Installation du serveur PBS"
    L'image ISO de PBS est disponible sur le portable HP. Il faut la transférer dans votre serveur proxmox. Créez une MV qui utilise l'image ISO de PBS pour l'installation, avec 4Go de RAM, 4 coeurs, une carte réseau connectée à vmbr0 mais surtout plusieurs disques: un pour le système (32Go), trois autres pour servir d'entrepôt de données. Lancez la MV, le processus d'installation de PBS se lance automatiquement, faites l'installation en mode graphique. Lorsque vous devez choisir le disque d'installation (Target Harddisk), vérifiez que vous avez bien vos 4 disques et choisir le premier pour la suite de l'installation (laissez les options par défaut). Choisir *Country* : France puis continuez l'installation. Choisissez le mot de passe pour l'utilisateur *root* et une adresse mail syntaxiquement correct. Pour le *Hostname* mettre *pbs.tpais.lan* et comme adresse IP celle donnée pour une MV Debian (@IP/masque: 10.187.52.XX/24, gw: 10.187.52.245, dns: 10.187.52.5). Terminez l'installation. Connectez-vous ensuite à l'adresse IP et au port indiqué (normalement: https://10.187.52.XX:8007).

!!! note "Première connexion et configuration de base de PBS"
    Lorsque vous vous connectez, vous retrouvez la même organisation que ProxmoxVE avec un menu sur la gauche pour choisir les différentes options et une zone centrale qui permet la configuration de l'option choisie. Dans le menu *Administration*, vous trouverez une console *root* pour taper vos commandes item *Shell* (l’accès par ssh est toujours possible). Cliquez sur l'item *Administration* pour visualiser l'état du serveur car l'item *Statut du serveur* est automatiquement sélectionné. Vous pouvez aussi redémarrer/éteindre votre serveur. Passez sur l'item *Dépôts* pour *désactiver* le dépôt **pbs-entreprise** et *ajouter* le dépôt **pbs-no-subscription**. Cliquez sur l'item *Mises à jour* et le bouton *Rafraîchir*. Cliquez sur le bouton *Mettre à niveau* si nécessaire.  
    Vous allez maintenant configurer le serveur NTP à utiliser sur votre serveur PBS. Contrairement à PVE qui utilise *systemd-timesyncd*, PBS utilise le logiciel **chrony** pour se synchroniser sur un serveur NTP externe. Editez le fichier `/etc/chrony/chrony.conf`, commentez la ligne (mettre un `#` au début de la ligne) qui commence par `pool 2.debian...` puis rajoutez la ligne suivante : `pool ntp.unice.fr iburst`. Le démon chrony est géré par systemd : `systemctl restart chrony` pour relancer chrony, `systemctl status chrony` pour vérifier qu'il s'est bien démarré.  
    Vous allez maintenant créer vos futurs *datastores*. Cliquez sur le menu *Administration* puis *Stockage et disques*. Vous devez retrouver vos 4 disques (un pour le système et les autres à configurer). Initialisez les disques *sdb*, *sdc* et *sdd* avec GPT. Cliquez sur l'item *Répertoire* puis le bouton *Créer : Directory*, sélectionnez le disque */dev/sdb*, le système de fichiers *ext4* et comme nom *datastore1*. Au bout de quelques instants votre premier entrepôt de données est disponible. Vous devez le retrouver sous le menu *Entrepôt de données*.

Vous avez votre premier entrepôt de données, vous allez maintenant utiliser cet entrepôt pour réaliser la sauvegarde de données d'une machine Linux avec l'agent *proxmox-backup-client*.

### Réaliser la sauvegarde/restauration de données d'un ordinateur avec un SE Linux

L'agent de sauvegarde Linux est disponible pour les distributions basées sur Debian (Ubuntu, Mint, etc...). **Vous disposez d'un portable sous Linux, vous allez réaliser l'installation sur celui-ci**.

!!! note "Installation de l'agent PBS sur une machine Linux"
    Il vous faudra des droits *root* pour faire les commandes : donc soit vous utilisez la commande `sudo`, soit vous passez *root* avec la commande `su -`. Vous commencez par récupérer et vérifier la clé gpg du dépôt proxmox (vous devez retrouver les mêmes signatures que ci-dessous avec les commandes `sha512sum` et `md5sum`):
    ```console
    # wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
    # sha512sum /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg 7da6fe34168adc6e479327ba517796d4702fa2f8b4f0a9833f5ea6e6b48f6507a6da403a274fe201595edc86a84463d50383d07f64bdde2e3658108db7d6dc87  /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
    # md5sum /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
    41558dc019ef90bd0f6067644a51cf5b /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
    ```
    Éditez le fichier `/etc/apt/sources.list.d/pbs-client.list` et rajoutez la ligne suivante: `deb http://download.proxmox.com/debian/pbs-client bookworm main`. Sauvegardez puis mettez à jour la liste des paquets avec la commande `apt update`. Enfin installez le paquet **proxmox-backup-client**.

!!! note "Première sauvegarde avec l'agent proxmox-backup-client"
    vous allez sauvegarder le répertoire `/home` vers l'entrepôt de données *datastore1* en utilisant le seul utilisateur créé sur le serveur PBS : *root*. Consultez le lien suivant pour faire votre proposition de commande : [https://pbs.proxmox.com/docs/backup-client.html#](https://pbs.proxmox.com/docs/backup-client.html#). Votre commande:  
    `_______________________________________________________________________________________________________`  
    Vérifiez sur votre serveur PBS que la sauvegarde s'est bien réalisé: Menu *Entrepôt de données* puis *datastore1* puis *Contenu*. Dépliez l'arborescence de votre sauvegarde jusqu'à pouvoir parcourir l'archive (icône en forme de dossier).

Sur votre ordinateur portable Linux supprimer les dossiers `Documents` et `Images`.

!!! note "Restaurer une sauvegarde"
    En consultant le lien suivant [https://pbs.proxmox.com/docs/backup-client.html#restoring-data](https://pbs.proxmox.com/docs/backup-client.html#restoring-data) et en utilisant l'aide de la commande (tapez `proxmox-backup-client restore`). Proposez une commande qui permet la restauration de votre sauvegarde précédente :  
    `_______________________________________________________________________________________________________`  
    Lancez votre commande et vérifiez que les deux dossiers effacés sont réapparus.

Vous pouvez réaliser la sauvegarde de machines Linux mais quels sont les défauts de ce système (trouvez-en au moins 2):

|N°|Défauts|
|:---|:---|
|1||
|2||

### Sécuriser votre sauvegarde

Il faut tout d'abord créer un autre utilisateur avec les droits juste nécessaire pour réaliser la sauvegarde (principe du moindre privilège).

!!! note "Création d'un utilisateur et gestion des droits"
    Cliquez sur *Configuration* puis *Contrôle d'accès*. Cliquez sur le bouton *Ajouter* pour créer un nouvel utilisateur : par exemple **userbackup**. De base, cet utilisateur n'a aucun droit. Cliquez ensuite sur l'onglet *Permissions* puis le bouton *Ajouter* et *Permissions de l'utilisateur*. Dans le premier champ *Chemin d'accès* choisissez uniquement l'accès à *datastore1*, dans le champ *Utilisateur* choisissez *userbackup* et dans le champ *Rôle* choisissez *DatastoreBackup*. Ainsi cet utilisateur pourra uniquement faire des sauvegardes vers le datastore choisi.  
    Effacez la sauvegarde par l'intermédiaire de l'interface web du PBS (*Entrepôt de données*, *datastore1*, cliquez sur la sauvegarde puis cliquez sur la corbeille). Sur votre ordinateur Linux, relancez une sauvegarde cette fois-ci avec l'utilisateur **userbackup**. Commande :  
    `_______________________________________________________________________________________________________`  
    Effacez des fichiers/répertoires sur votre ordinateur Linux. Puis lancez une restauration avec l'utilisateur **userbackup**. Commande :  
    `_______________________________________________________________________________________________________`  

!!! note "Vérification des droits"
    Créez un nouvel entrepôt de données avec le nom *datastore2* sur le disque `/dev/sdc`. Lancez une sauvegarde vers ce datastore de votre ordinateur Linux avec l'utilisateur **userbackup**. Commande:  
    `_______________________________________________________________________________________________________`  
    Que s'est-il passé ? `_________________________________________________________`

## Sauvegarder ces MVs vers un serveur PBS (Proxmox Backup Server)

Pour sauvegarder vos MVs, vous allez vous connecter au serveur PBS suivant @IP: 10.187.52.241 avec le login **backup@pbs** (bien mettre le *@pbs*) et le mot de passe *backup*. Votre *datastore* s'appelle *sauvegarde*.
Pour configurer votre sauvegarde il faut ajouter un nouveau stockage (*Centre de données* puis *Stockage* puis *Ajouter* et choisir *Proxmox Backup Server*). Complétez les champs connus avec les renseignements précédents. Pour l'ID je vous conseille de mettre PBS. L'empreinte SHA-256 du certificat du serveur vous sera communiqué par mail. Cliquez sur le bouton *Ajouter* quand vous avez fini. Un nouveau stockage apparaît (avec l'identifiant *pbs*) qui permet de sauvegarder des *VZDump* (page de manuel de la commande [vzdump](https://pve.proxmox.com/pve-docs/vzdump.1.html)).

Vous remarquez qu'il existe trois mode de sauvegarde d'une MV:

- Le mode **arrêt** pour une MV est celui qui offre le plus de cohérence parmi les trois, au prix d’une courte période d’indisponibilité. Comme son nom l’indique, la VM doit être arrêtée avant d’effectuer ce mode de sauvegarde. L’idée est la même pour les conteneurs, mais il existe un risque de temps d’arrêt prolongé.
- Le mode **suspend** pour une MV tente de réduire le temps d’arrêt potentiel en n’arrêtant pas complètement la MV, mais le temps d’arrêt est toujours relativement important et la cohérence des données peut en souffrir considérablement, c’est pourquoi cette option n’est généralement pas recommandée.
La situation est quelque peu différente pour les conteneurs, puisque ce processus crée une copie des données du conteneur en direct dans un emplacement temporaire, suspend ensuite le conteneur, puis remplace les fichiers de la première sauvegarde par la copie du conteneur suspendu. Le temps d’arrêt de cette opération est nettement inférieur à celui du mode arrêt, mais elle nécessite un espace libre supplémentaire pour contenir la copie temporaire du conteneur.

- Le mode **snapshot** est probablement l’option la plus intéressante des trois, car elle permet de créer des sauvegardes de VM avec peu ou pas de temps d’arrêt, mais avec un risque d’incohérence de la tâche de sauvegarde. Comme les données sont copiées à partir d’une MV active, il est possible de copier un fichier à mi-parcours. Le processus de création d’un instantané d’un conteneur est essentiellement le même puisque le logiciel de sauvegarde suspend également les opérations à l’intérieur d’un conteneur spécifique, puis crée un instantané temporaire de tout le contenu de ce conteneur. L’intégralité du contenu du conteneur est ensuite archivée et l’instantané lui-même est ensuite supprimé.

Si vous le pouvez, choisissez en priorité le mode **arrêt** pour réaliser la sauvegarde. S'il n'est pas possible d'avoir un court temps d'indisponibilité pour votre MV, choisissez **snapshot**. Le mode **suspend** sera privilégié avec les conteneurs.

Pour sauvegarder une MV, cliquez sur celle-ci (arrêtez là si elle en fonctionnement) puis dans le menu de la MV choisissez l'option *Sauvegarde* puis le bouton *Sauvegarder maintenant*. Dans la fenêtre qui s'ouvre, choisissez votre stockage (dans notre cas *pbs*), le mode *Stopper* puis lancez la sauvegarde en cliquant sur le bouton *Sauvegarde*. La sauvegarde se réalise, vous pouvez suivre à l'écran son déroulement.

Pour visualiser les sauvegardes de la MV sur le serveur de sauvegarde, sélectionnez le stockage approprié en haut à droite de la fenêtre à coté du mot *Stockage*.

!!! note "Exercice vers le serveur PBS du TP AIS"
    Sauvegardez les MVs que vous souhaitez conserver vers le serveur pbs (par exemple vos modèles Linux et/ou Windows).

!!! note "Exercice vers le serveur PBS qu vous avez créer dans ce TP"
    Sur votre serveur PBS, créez un nouvel entrepôt de données **datastore3** sur le dernier disque dur libre. Créez un utilisateur **agentsauv** qui pourra réaliser ces sauvegardes dans ce datastore. Rajoutez à votre serveur PVE un nouvel espace de stockage qui pointe vers votre serveur PBS (n'oubliez de mettre la signature de **VOTRE** serveur PBS). Testez une sauvegarde d'une de vos MVs vers votre PBS. 

## Restaurer ces MVs depuis un serveur PBS

La restauration d'une MV depuis un serveur pbs vers votre serveur proxmox est très simple. Cliquez sur votre l'item *Stockage* de votre *Centre de données* dans le menu de droite. Cliquez ensuite sur le stockage correspondant à votre serveur pbs. Dans le menu qui vient d’apparaître au centre, cliquez sur l'item *Sauvegardes*. Toutes les sauvegardes de MVs apparaissent. Cliquez sur celle souhaitée puis cliquez le bouton *Restaurer*.

## Conclusion

Vous avez mis en oeuvre une sauvegarde basée sur des scripts bash ou powershell simples vers un partage réseau géré par le logiciel Samba. Dans un second temps vous avez installé un serveur PBS et testez certaines de ces fonctionnalités avec l'agent PBS. Mais il reste beaucoup à faire : utiliser la commande rsync sous linux sans doute plus adapté pour faire de la sauvegarde, installer un agent PBS sous Microsoft Windows, utiliser un logiciel spécialisé dans la sauvegarde comme Borg ou VEEAM Backup. Nous n'avons pas traité des sauvegardes automatiques à mettre en place et comment effacer les sauvegardes obsolètes sur le serveur PBS. Bref beaucoup de sujets que vous pouvez aborder dans votre dossier professionnel... 