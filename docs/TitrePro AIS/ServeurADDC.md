# Installation, configuration et sécurisation d'un serveur Microsoft AD-DC

!!! tldr "Objectifs de ce document"

    - installer le système d'exploitation Microsoft Server 2022 dans une MV sur un serveur Dell,
    - installer le service **AD** (Advanved Directory),
    - promouvoir ce serveur AD en **controleur du domaine** (DC : Domain Controller),
    - réaliser une jonction au domaine d'une machine Microsoft Windows 10,
    - créer des utilisateurs sur le serveur AD et tester leur authentification sur l'AD,
    - sécuriser le serveur avec les outils *PingCastle* et *Hello-my-dir* 
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel AIS - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser une infrastructure de serveurs virtualisée
        - Appliquer les bonnes pratiques et participer à la qualité de service
        - Participer à l’élaboration et à la mise en œuvre de la politique de sécurité

## Introduction

Le serveur Microsoft *Advanced Directory* est trés utilisé en entreprise pour réaliser l'authentification des utilisateurs et machines. Du fait de son rôle central qu'il joue, c'est aussi la cible principal des pirates lorsqu'ils se sont introduits dans un réseau d'entreprise. Il doit donc être dans une zone du SI avec un haut niveau de sécurité, les mises à jour de sécurité doivent être appliquées régulièrement et la sécurisation de ces machines peut être complexes. Ce document n'explore pas en profondeur la mise en oeuvre de ce serveur mais permet d'avoir une brique supplémentaire qui va servir à réaliser l'authentification pour d'autres services au sein du réseau (par ex: l'authentification des utilisateurs dans GLPI, la réalisation d'un proxy authentifiant avec IPFire).

## Installation du système d'exploitation Microsoft Server 2022

L'image ISO doit être disponible sur votre PC portable. Il faudra donc la transférer sur le serveur Proxmox. Vous allez aussi optimiser son installation avec les pilotes *VirtIO* de Redhat comme vous l'avez fait avec l'installation de Windows 10. Ce TP a été testé avec la version 0.1.266 disponible sur [https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/). Les conseils d'installation de Windows Server 2022 sur le site de Proxmox : [https://pve.proxmox.com/wiki/Windows_2022_guest_best_practices](https://pve.proxmox.com/wiki/Windows_2022_guest_best_practices).

!!! note "Exercice : installation de base du serveur Microsoft Server 2022"
    Créer une nouvelle MV : Nom: AIS-WinServer2022, Type de SE: Windows 11/2022/2025, cochez la case *Ajouter les pilotes VirtIO* et choisir l'image ISO VirtIO, choisir l'image Windows Server 2022. A l'étape *Système*, laissez les paramètres par défaut mais choisissez le *stockage EFI* et le *stockage TPM*. Le disque dur sera un SCSI de 64Go. Mettre 4 coeurs. Mémoire de 8Go. Réseau vmbr0. Lancez la MV pour lancer l'installation de Microsoft Windows Server 2022.  
    Lors de l'installation, prendre **Standard Evaluation avec expérience de bureau**. Puis à l'étape *Type d'installation*, choisissez *Personnalisé*. Chargez le pilote pour avoir accés à votre disque dur puis poursuivez l'installation jusqu'à l'obtention de l'interface graphique et la saisie du mot de passe administrateur : vous devrez attendre relativement longtemps. Changez le nom de votre serveur en ADServer-*VosInitiales* puis redémarrez votre serveur. Rajoutez le pilote pour la carte réseau et celui de la gestion mémoire (ballooning). Configurez le réseau pour avoir une @IP statique en 10.187.52.XX/24, passerelle en 10.187.52.245 et DNS en 10.187.52.5 : Paramètres -> Réseau et Internet -> Ethernet -> Modifier les options de l'adaptateur -> clic gauche sur la carte réseau puis propriétés -> double cliquez sur protocole ipv4 -> saisissez vos paramètres IP. N'installez pas les mises à jour pour l'instant.  
    Votre machine Windows Server de base est installée. **Arrêtez cette machine et clonez-là**. Elle peut nous servir pour d'autres cas d'usage. A partir de maintenant travaillez sur le clone.

## installation du rôle AD-Domain Service et promotion en controleur de domaine (PDC)

Le service d'annuaire (AD) n'est pas installé par défaut, vous allez maintenant rajouter ce rôle sur le clone de la machine Windows Server. Dans la foulée vous allez promouvoir ce serveur en contrôleur de domaine principal (PDC : Primary Domain Controler) afin de réaliser votre premier domaine Windows.

!!! note "Exercice : Ajout du role AD-DS"
    Lancez le *Gestionnaire de serveur* (il doit se lancer automatiquement au démarrage). Cliquez sur le bouton *Gérer* puis *Ajouter des roles et des fonctionnalités*. Une nouvelle fenêtre s'ouvre qui présente les prérequis : nous y répondons sauf pour les mises à jour. Cliquez sur *Suivant*. Cochez *Installation basée sur un rôle* puis *Suivant*. Sélectionnez votre serveur dans la liste puis *Suivant*. Dans la liste des rôles possibles, choisissez **Services de domaine Active Directory (AD DS)** et toutes ces dépendances. Cliquez sur *Suivant*. Laissez les fonctionnalités par défaut puis *Suivant* deux fois. Cliquez alors sur le bouton *Installer*. Attendre la fin de l'installation puis fermer la fenêtre.

!!! note "Exercice : Promotion de notre serveur en serveur PDC"
    Lancez le *Gestionnaire de serveur*. En haut de la fenêtre une nouvelle notification est apparue (drapeau avec un panneau jaune). Cliquez sur cette notification puis sur *Promouvoir ce serveur en contrôleur de domaine*. Une nouvelle fenêtre s'ouvre. Cliquez sur *Ajouter une nouvelle forêt* puis dans la zone *Nom de domaine racine*, mettre *tpais-***vosInitiales***.local* puis bouton *Suivant*. Saisissez un mot de passe dans les champs *restauration des services d'annuaire* puis *Suivant*. Bouton *Suivant* pour ignorer l'erreur sur la délégation DNS. Tapez le nom *Netbios* pour votre serveur s'il n'arrive pas à le trouver lui-même : *tpais-***vosInitiales** puis *Suivant* trois fois ! Ignorez les avertissements puis lancez l'installation avec le bouton *Installer*. Attendre la fin de l'installation et le redémarrage du serveur. A l'invite pour vous identifiez, le changement est visible puisque **vous vous identifiez désormais sur le domaine que vous avez saisi**. Dans le *Gestionnaire de serveur*, cliquez sur le nouvel item *AD DS*, votre serveur doit apparaitre avec la mention **en ligne**. Eteignez votre serveur puis faites un **instantané** pour figer cette configuration (si vous êtes sur un système de fichiers qui supporte cette fonctionalité).

## Jonction au domaine d'une machine Microsoft Windows 10/11

Vous allez maintenant intégrer une machine Windows dans un domaine, ce qui permettra de réaliser l'authentification des utilisateurs sur le serveur AD.

!!! note "Exercice : Jonction au domaine d'une machine Windows 10"
    Clonez votre machine modèle Windows 10. Vérifiez qu'elle est bien connectée au vmbr0. Démarrez la machine pour appliquer la configuration IP suivante: @IP/Masque: 10.187.52.XX/24, Passerelle: 10.187.52.245, DNS: **@IP du serveur AD**. Pour joindre le domaine: *Paramètres Systèmes* -> *Système* -> *A Propos* -> *Renommer ce PC (avancé)* -> Onglet *Nom de l'ordinateur* -> bouton *Modifier*. Cochez la case *Membre d'un domaine* puis compléter la zone texte avec le nom de votre domaine AD (ex: *tpais-***vosInitiales***.local*). Pour vous authentifier, tapez comme identifiant *Administrateur* et comme mot de passe celui de votre serveur AD. Redemarrez la machine une fois que vous avez joint le domaine. Au moment de vous identifiez sur la page de login, cliquez sur *Autre utilisateur* le nom de votre domaine doit apparaitre. Connectez-vous sur le domaine avec l'identifiant *Administrateur* pour valider le fait que l'AD vous authentifie.

Vous allez maintenant ajouter un nouvel utilisateur sur le serveur AD et vous connectez sur la machine Windows10 avec ce nouvel utilisateur.

!!! note "Exercice : Ajout d'utilisateur sur le serveur AD"
    Sur le serveur AD, identifiez-vous en tant qu'administrateur puis dans le *Gestionnaire de serveurs*, cliquez sur l'item *Outils*. Sélectionnez *Utilisateurs et ordinateurs Active Directory*. Dans la nouvelle fenêtre qui s'ouvre, sélectionnez et dépliez l'item de votre serveur AD. Sélectionnez l'item *Users* puis clic droit sur celui-ci. Choisissez l'item *Nouveau* puis *Utilisateur*. Dans la nouvelle fenêtre qui s'ouvre complétez les différents champs pour créer votre utilisateur puis validez avec le bouton *Suivant*. Saisissez le mot de passe et les cases si nécessaires. Identifiez-vous sur la machine windows10 avec ce nouvel utilisateur. Si tout est OK vous savez rajouter des utilisateurs sur un domaine et vous identifiez sur une des machines connectées au domaine.

!!! warning "Remarque importante"
    Il n'est pas question ici de faire une découverte complète d'un serveur Microsoft Server mais juste d'avoir les fonctionnalités de base qui vous nous permettre de travailler avec un domaine windows et surtout un annuaire AD pour la suite de la formation.

## Sécurisation du serveur AD

Vous allez commencer par installer un logiciel d'audit de la sécurité des machines Windows Server : **PingCastle**. Le téléchargement est disponible [https://www.pingcastle.com/download/](https://www.pingcastle.com/download/). Ce logiciel va vous donner un score général sur la sécurité de votre serveur AD. Plus le score est bas, plus le serveur est sécurisé.

!!! note "Installation et utilisation du logiciel PingCastle"
    Téléchargez puis installez ce logiciel (dézippez-le) en tant qu'administrateur enfi exécutez-le. Vous choisirez l'option 1 (healthcheck) et votre serveur AD. Tapez une touche quand le programme est fini. Dans le répertoire ou vous avez lancé le logiciel PingCastle, ouvrez avec un navigateur le fichier html qui porte le nom de votre serveur. Notez alors le score de votre serveur : `_________`. Vous pouvez lire les différents conseils à mettre en oeuvre pour améliorer le score de votre serveur AD. Il n'est pas question ici d'évoquer les solutions à mettre en oeuvre. **Avant de continuer éteignez votre serveur AD et la machine Windows10**.

Visiblement il y a des possibilités d'améliorations de la sécurité du serveur AD. Pour cela vous allez utiliser le logiciel *Hello-my-dir* (site: [https://github.com/LoicVeirman/Hello-My-Dir](https://github.com/LoicVeirman/Hello-My-Dir)).

!!! note "Utilisation du logiciel Hello-My-Dir pour sécuriser un serveur AD"
    Vous allez repartir de la version de base du serveur Microsoft Server 2022 que vous avez créé auparavant. Créez donc un nouveau clone de cette version de base. Suivez alors le tutoriel disponible sur [https://www.it-connect.fr/comment-creer-un-domaine-active-directory-respectueux-des-bonnes-pratiques-de-securite/](https://www.it-connect.fr/comment-creer-un-domaine-active-directory-respectueux-des-bonnes-pratiques-de-securite/). A la fin du tutoriel, installez et exécutez *PingCastle* : nouveau score `___________`. Si tout se passe bien vous avez du améliorer la sécurité de votre serveur AD.

## Utilisation du serveur AD pour réaliser l'authentification d'utilisateurs

De nombreux logiciels permettent de réaliser l'authentification d'un utilisateur à l'aide d'un serveur AD. Vous allez réaliser des tests avec deux logiciels que vous avez déjà utilisé : GLPI et IPFire.

!!! note "Exercice en autonomie"
    Reprenez votre MV qui fait fonctionner le serveur GLPI. Faites en sorte que les deux MV (GLPI et serveurAD) soient dans le même réseau (elles doivent pouvoir se pinguer). Configurez alors l'authentification GLPI sur le serveur AD. Lire par exemple : [https://www.it-connect.fr/tuto-glpi-configurer-authentification-active-directory/](https://www.it-connect.fr/tuto-glpi-configurer-authentification-active-directory/) mais il existe de nombreux autres sites, faites vos recherches et croisez les informations pour trouver une solution !  
    Pour IPFire, deplacez votre MV serveurAD pour qu'elle soit dans la zone verte du parefeu. Les machines doivent pouvoir se pinguer (Attention au parefeu de Windows). Vous allez activer le proxy web pour la zone verte. De plus le proxy sera un proxy authentifiant : il faut que votre utilisateur tape un couple *login/password* avant de sortir sur Internet. L'authentification se fera sur le serveur AD. Lire par exemple : [https://www.ipfire.org/docs/configuration/network/proxy/wui_conf/auth/ldap](https://www.ipfire.org/docs/configuration/network/proxy/wui_conf/auth/ldap). Encore une fois il existe d'autres documentations...

??? note "Correction : Proxy authentifiant géré par le logiciel IPFire avec authentification sur serveur Microsoft AD"
    |Etape|Commentaire|
    |:---:|:---|
    |++1++| Pour réaliser l'authentification sur le serveur AD, il faut créer un utilisateur qui a le droit de parcourir l'ensemble des utilisateurs dans l'arbre LDAP. Sous Microsoft Windows, un tel utilisateur doit appartenir au groupe *Opérateurs de compte* (source: [https://general.sio57.info/wp/?p=689](https://general.sio57.info/wp/?p=689)). Ensuite il faut suivre le tutoriel du site IPFire.|
    |++2++| Connectez-vous sur le serveur AD. Suivez la procédure d'ajout d'un nouvel utilisateur présenté précedemment **mais** cet utilisateur ne doit pas changer de mot de passe à la prochaine session, le mot de passe ne doit pas expirer et il ne peut pas changer de mot de passe: créez l'utilisateur **ipfire**.|
    |++3++| Pour rajouter l'utilisateur ipfire au groupe *Opérateur de compte* : clic droit sur l'utilisateur *ipfire* puis *Ajouter un groupe*. Dans la fenetre qui s'ouvre, cliquez sur le bouton *Avancé* puis recherchez le mot *opérateur*. Parmi les réponses choisissez le groupe *Opérateurs de compte*.|
    |++4++| Dans IPFire, cliquez sur le menu *Réseau* puis *Proxy web*. En bas de page, choisissez l'authentification *LDAP* puis bouton *Sauvegarder et redémarrer*. De nouveaux champs apparaissent en bas de page. Dans *Paramètres communs LDAP*, mettre dans le champ *Nom absolu LDAP* : *dc=tpais-***vosinitiales***,dc=local*, champ *Type LDAP* choisir *Active Directory*, champ *Serveur LDAP* : l'@IP de votre serveur AD, champ *Port* : 389. Dans *Paramètres lien nom d'utilisateur et mot de passe*, champs *Nom d'utilisateur* : celui que vous venez de créer *ipfire* et son mot de passe dans le champ *Mot de passe*. Cliquez sur le bouton *Sauvegarder et redémarrer*.|
    |++5++| Ouvrez un nouvel onglet dans votre navigateur, vous devez saisir votre *login/password* pour obtenir la page web. Testez avec un utilisateur que vous avez créé sur le serveur AD (**pas** l'utilisateur ipfire ou l'administrateur)|

??? note "Correction : Authentification des utilisateurs GLPI sur un serveur Microsoft AD"
    |Etape|Commentaire|
    |:---:|:---|
    |++1++| Pour réaliser l'authentification sur le serveur AD, vous allez utiliser le même utilisateur que précédemment, à savoir *ipfire*|
    |++2++| Assurez-vous que le serveur GLPI est dans le même réseau que le serveur Microsoft AD (modification du fichier */etc/network/interfaces*, vérification avec une commade ping). Sur le serveur Linux qui héberge le GLPI, mettre comme serveur DNS, l'@IP du serveur Microsoft AD. Connectez-vous sur le serveur GLPI en tant qu'administrateur (rappel compte par défaut: glpi/glpi)|
    |++3++| Menu *Configuration*, *Authentification*, *Annuaire LDAP*. Dans la page qui apparait, cherchez le bouton *+ Ajouter*. Vous devez remplir les différents paramètres de connexion pour le serveur AD/LDAP: champ *Nom* : par exemple *Test authentification Serveur AD*, *Serveur par défaut* et *Actif* à **Oui**, champ *Serveur* : @IP du serveur AD, champ *Port* : valeur par défaut 389. Dans le filtre de connexion, vous pouvez mettre une expression pour filtrer des comptes sur l'AD, vous laisserez vide par simplification. Champ *Base DN* : vous allez parcourir tout l'arbre LDAP, il faudrait créer des nouvelles OU pour sécuriser l'accés. Dans votre cas mettre *dc=tpais-***vosInitiales***,dc=local*. Choix *Bind* : **Oui**. Champ *DN du compte* : *ipfire@tpais-***vosInitiales***.local*. Mettre le mot de passe que vous avez saisi dans l'AD pour l'utilisateur *ipfire*. Champ *Nom de l'identifiant* : *samaccountname*. Champ *Champ de synchronisation* : *objectguid*. Bouton *Sauvegarder* : un test de connexion est réalisé. S'il échoue, vérifiez votre saisie|
    |++4++| Déconnectez-vous pour revenir à la page d'accueil de GLPI. Vérifiez que la *source de connexion* est sur votre serveur AD. Puis saisissez le nom d'un utilisateur basique que vous avez créé lors de la connexion au domaine d'une machine Windows 10 (vérifiez le nom dans le gestionnaire d'utilisateurs du serveur AD). Vous devez vous identifier en tant que simple utilisateur qui peut déposer un ticket|
    |++5++| **Important** : veuillez noter qu'il ne s'agit pas d'une configuration optimale mais d'une première configuration qui demande à être amélioré. Pour complétez la configuration vous pouvez aussi vous reportez à [https://neptunet.fr/glpi-ad/](https://neptunet.fr/glpi-ad/)|

## Conclusion

Vous avez installé un serveur Microsoft Windows Server 2022 avec le service AD-DC et la promotion de celui-ci en controleur de domaine afin de réaliser un réseau microsoft simple. Cette brique de base sera utilisé par de nombreux autres logiciels pour réaliser l'authentification des utilisateurs. Si vous voulez vous passer des technologies Microsoft, vous pouvez monter un serveur LDAP sous GNU/Linux pour réaliser l'authentification : cela peut être encore une idée pour votre dossier professionnel...
