# Installation, configuration et utilisation d'un logiciel de supervision : **Zabbix**

!!! tldr "Objectifs de ce document"

    - installer le logiciel **Zabbix** dans une MV Linux sur un serveur Dell,
    - réaliser la configuration de base du logiciel de supervision,
    - superviser vos premières machines avec un l'agent Zabbix,
    - superviser des matériels (réseaux, imprimantes, onduleur) par SNMP,
    - utiliser des modèles (*templates*) pour simplifier la gestion des matériels.
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel AIS - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser une infrastructure de serveurs virtualisée
        - Appliquer les bonnes pratiques et participer à la qualité de service
        - Participer à l’élaboration et à la mise en œuvre de la politique de sécurité

## Introduction : Qu'est-ce que la supervision ?

Un logiciel de supervision, comme **Nagios** ou **Zabbix**, est un outil conçu pour surveiller les systèmes informatiques, les réseaux et les infrastructures afin de garantir leur bon fonctionnement. Ces logiciels permettent de détecter, d’alerter et parfois de résoudre les problèmes avant qu’ils n’affectent gravement les utilisateurs ou les services.

Principales fonctions d’un logiciel de supervision :

- surveillance des ressources matérielles et logicielles :
    - serveurs (CPU, RAM, disque, etc.),
    - réseaux (routeurs, switchs, bande passante),
    - applications (bases de données, sites web, services critiques).
- collecte des données : les outils collectent des métriques sur l’état et les performances des ressources surveillées.
- alertes en cas de problème : notifications en cas de panne ou de dépassement de seuils définis (CPU trop élevé, disque plein, etc.), via e-mail, SMS ou autres moyens.
- rapports et analyses : génération de rapports sur la disponibilité, la performance et les tendances pour anticiper les problèmes futurs.
- cartographie et visualisation : affichage graphique des infrastructures surveillées pour une meilleure compréhension.

Vous allez étudier dans ce document la mise en oeuvre de Zabbix.

## Présentation rapide de Zabbix

Zabbix (site officiel: [https://www.zabbix.com/](https://www.zabbix.com/)) offre nativement de nombreuses fonctionnalités avancées, par exemple :

- découverte automatique des hôtes (serveurs et éléments de réseau) et de certains indicateurs (services à superviser) ;
- supervision distribuée (répartition de charge et réseaux éloignés) ;
- interface web de configuration et administration centralisée ;
- agents natifs sur de nombreuses plateformes ;
- support de nombreux protocoles pour la supervision sans agent (ports TCP, SNMP, SSH, Telnet, SQL...) ;
- supervision d'applications web ;
- permissions utilisateurs ;
- paramètres de notification avancés ;
- gestion d'une vue « haut niveau » de la supervision (SLA).

Afin d'offrir les fonctionnalités annoncées, Zabbix s'appuie sur certains principes :

- la logique est entièrement dans le serveur, les agents ne font que remonter des données ;
- toutes les données sont stockées en base de données pour permettre une réutilisation ultérieure ;
- les états des indicateurs sont décidés à partir d'expressions logiques, définies de la même manière qu'elles soient simples ou complexes ;
- les changements d'états peuvent faire l'objet de notifications... ou pas.

La figure ci-dessous résume ce principe de fonctionnement :

```dot
digraph FoncZabbix {
  fontname="Helvetica,Arial,sans-serif"
  node [fontname="Helvetica,Arial,sans-serif"]
  edge [fontname="Helvetica,Arial,sans-serif"]
  node [shape=box];
  rankdir="LR";
  
  subgraph cluster_0 {
    style=filled;
    color=lightgrey;
  fontcolor=black;
    node [style=filled,color=white,fontcolor=black];
    "Collecte des métriques\nagent Zabbix, SSH, SNMP..." -> "Eléments (items)\nstockage BDD"
    "Eléments (items)\nstockage BDD" -> "Déclencheurs (triggers)\nexpressions logiques"
    "Déclencheurs (triggers)\nexpressions logiques" -> "Actions (actions)\nnotification mail, SMS ou autres"
    label = "Cheminement des métriques dans Zabbix";
  }
}
```

Les éléments mis en jeu dans Zabbix:

- le **serveur Zabbix**, programmé en C pour offrir un maximum de performances, est le cœur de l'infrastructure de supervision : il contient toute la logique du système, il gère la réception et le stockage des valeurs collectées, l'activation des déclencheurs, l'exécution des actions...
- l'**interface web**, programmée en PHP, permet de configurer l'ensemble des éléments de la supervision et de visualiser l'état des indicateurs et leur historique.
- la **base de données**, utilisant MySQL, PostgreSQL, Oracle, DB2 ou SQLite, stocke l'intégralité des données de Zabbix (configuration, éléments, états...).
- l'**agent Zabbix**, programmé en C, facultatif, récupère les données sur les serveurs supervisés et les remonte au serveur Zabbix. La compatibilité ascendante est assurée, permettant à tout serveur Zabbix d'exploiter les données d'un agent Zabbix, quelle que soit sa version.
- le **proxy Zabbix**, programmé en C, facultatif, sert de relais permettant d'alléger la charge du travail du serveur et de réduire la complexité de l'architecture de supervision.

## Schéma réseau du TP

Vous trouverez ci-dessous le schéma réseau du TP que vous allez mettre en place et tester :

![Schéma réseau du TP Zabbix](../img/SchemaTPZabbix.svg)

## Installation de Zabbix

Vous allez installer Zabbix dans une MV Linux en mode console. Cette MV disposera de 4 coeurs, 4Go de RAM, de 64Go de disque dur, de deux cartes réseaux virtuelles *vmbr0* et *vmbr50*. Vous allez tester la version 7.2 de Zabbix.

!!! note "Exercice : Installation de Zabbix"
    Commencez par créer votre MV Linux avec les critères ci-dessus, n'oubliez pas de configurer les @IP de chacun des réseaux. Puis rendez-vous sur le site de téléchargement de Zabbix ([https://www.zabbix.com/download](https://www.zabbix.com/download)) puis choisissez *Zabbix Packages*. Choix: version: 7.2, OS: Debian, Version:12, Composants Zabbix: serveur, interface Web (frontend), *agent 2*, Bdd: MySQL, Serveur WEB: Apache.  
    Suivez alors la procédure d'installation qui vous est proposé par le site web : toute la partie **Etape 2**. Lors de cette installation, il est demandé d'avoir votre serveur de base de données installé : il faudra faire l'installation du paquet *mariadb-server*. Une fois cette étape réalisée, connectez-vous sur le site web de Zabbix pour finaliser l'installation. Pour vous identifier sur le site web : Admin/zabbix. Il faut changer le mot de passe pour un autre plus sécurisé: Menu *Utilisateurs*, item *Utilisateurs*. Cliquez sur l'utilisateur *Admin* pour changer le mot depasse et certains de ces paramètres.

## Ajout d'un nouvel *hôte* dans Zabbix

Vous avez déjà un hôte qui est surveillé : le serveur Zabbix lui-même par un agent Zabbix. Visualisez les métriques de ce serveur : Menu *Surveillance*, *Hôtes* puis cliquez sur le nom *Zabbix server*. Un menu s'ouvre, vous pouvez explorer toutes les possibilités mais commencez par visualiser l'option *Tableaux de bord*. 

Vous allez rajouter des nouveaux hôtes Linux et Windows à superviser dans Zabbix par l'intermédiaire d'un agent.

!!! note "Installation d'un agent Zabbix Linux"
    Commencez par cloner une nouvelle machine Linux en mode console. Réalisez la configuration de la machine *debAgentZabbix* (@IP et nom). Testez le ping vers le serveur Zabbix. Activez l'accés vers Internet au niveau du serveur Zabbix (revoir la méthode dans le TP sur DNSMasq). Revenez sur le site de téléchargement de Zabbix puis choisissez *Zabbix Packages*. Choix: version: 7.2, OS: Debian, Version:12, Composants Zabbix: agent 2. Puis suivez la procédure d'installation comme auparavant. Regardez l'état de l'agent avec `# systemctl status zabbix-agent2.service` : actif/non actif. Corrigez l'erreur en éditant le fichier de configuration (`/etc/zabbix/zabbix_agent2.conf`). Votre correction : `_____________________________________________`. Relancez l'agent zabbix et vérifiez qu'il s'est démarré correctement.

!!! bug "ATTENTION : Bug dans l'agent 7.2"
    Le plugin *nvidia* fait planter l'agent zabbix 7.2. Editez le fichier `/etc/zabbix/zabbix_agent2.d/plugins.d/nvidia.conf` et commentez (mettre un `#`) devant la ligne `Plugins.NVIDIA...`.

!!! note "Ajout du nouvel hôte linux dans le serveur Zabbix"
    Dans l'interface web, sélectionnez le menu *Collecte de données*, *Hôtes*, bouton *Créer un hôte*. Dans la fenêtre qui s'ouvre, complétez le champ *Nom de l'hôte* (par exemple *testDeb12AgentZabbix*), cliquez sur le bouton *Sélectionner* du champ *Modèles*. Tapez le début de *Templates* puis chercher *Linux by Zabbix Agent*. Dans le champ *Groupes d'hôtes*, prenez les plus appropriés selon vous. Dans le champ *Interfaces*, cliquez sur le bouton *Ajouter* pour ajouter *Agent*. Remplissez alors l'adresse IP de l'agent. Rajoutez un commentaire, vérifiez que la case *Activé* est coché. Enfin terminez par le bouton *Ajouter*.  
    Visualisez quelques métriques de ce nouvel hôte : Menu *Surveillance*, *Hôtes*, clic gauche sur la machine Linux et choisir *Tableaux de bord*.

!!! note "Installation et configuration d'un agent Zabbix Windows"
    Rajoutez la MV Windows de manière qu'elle soit dans le réseau du TP (@IP et nom). Cette partie n'est pas détaillée, à vous d'installer l'agent Windows Zabbix, le configurer et rajouter ce hôte dans le serveur Zabbix. Quelques pistes : [https://www.zabbix.com/documentation/current/en/manual/concepts/agent](https://www.zabbix.com/documentation/current/en/manual/concepts/agent) et [https://www.zabbix.com/documentation/current/en/manual/appendix/install/windows_agent](https://www.zabbix.com/documentation/current/en/manual/appendix/install/windows_agent). Détaillez votre installation dans le tableau ci-dessous.

|Etape|Commentaire|
|:---:|---:|
|++1++||
|++2++||
|++3++||
|++4++||
|++5++||

## Créer un tableau de bord (dashboard)

Vous avez maintenant trois hôtes qui sont supervisés. Vous pouvez créer un tableau de bord en vous rendant sur l'item *Tableau de bord* puis le bouton *Créer un tableau de bord*. Donnez un nom à votre tableau de bord puis commencez à tester l'ajout de widget. La description de la création d'un tableau de bord est assez intuitive et ne sera pas décrit ici.

## Ajouter des alertes

Un logiciel de supervision est là pour réaliser la surveillance de votre SI et vous prévenir en cas de problème. C'est donc un aspect important qu'il faut tester. Il faut tout d'abord créer un *déclencheur* (*trigger* en anglais). Commencez par visualiser quelques déclencheurs sur un hôte existant: *Collecte de données*, *Hôtes*, choisissez un hôte et sur sa ligne cliquez sur *Déclencheurs*. Lisez les liens suivants dans l'ordre: [https://www.zabbix.com/documentation/current/en/manual/quickstart/item](https://www.zabbix.com/documentation/current/en/manual/quickstart/item) et [https://www.zabbix.com/documentation/current/en/manual/quickstart/trigger](https://www.zabbix.com/documentation/current/en/manual/quickstart/trigger).

## Exercices en autonomie

!!! note "Exercice 1 : Récupérer des informations d'une imprimante en SNMP"
    Une imprimante de marque Brother est disponible à l'adresse IP suivante: `_____________________`. Ajoutez-là en tant que nouvel *hôte*, il faut trouver les **OID** pertinents en cherchant sur Internet, en utilisant la commande `snmpwalk` sur la communauté public de l'imprimante ou encore en utilisant un modèle de la communauté zabbix (*Collecte de données*, *Modèles*, Bouton *Importer* en haut à droite). Faire un tableau de bord pour visualiser les différents paramètres que vous aurez identifier. Ensuite créez un déclencheur pour faire une remontée d'alerte.

!!! note "Exercice 2 : Superviser un commutateur de marque Cisco en SNMP"
    Superviser un des commutateurs cisco disponible dans votre baie et réaliser le tableau de bord de votre choix.


## Sources documentaires

- la documentation officielle sur le site de zabbix : [https://www.zabbix.com/documentation/current/en/manual](https://www.zabbix.com/documentation/current/en/manual),
- La suite d'articles **Supervision d'entreprise avec Zabbix** (GNU/Linux France magazine N°158),
- de nombreux tutoriels existent sur Internet, bien s'assurer qu'ils sont suffisament récent (version 7 de Zabbix)