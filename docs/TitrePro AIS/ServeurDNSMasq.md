# DNSMasq: un serveur DNS/DHCP pour votre entreprise

!!! tldr "Objectifs de ce document"

    - Présenter le logiciel DNSMasq,
    - Mettre en place un serveur DNS simple et DNS Cache,
    - Mettre en place un serveur DHCP simple pour un ou plusieurs réseaux,
    - Mettre en place un serveur TFTP pour réaliser la mise à jour de certains matériels réseaux,
    - Bonus : partager la connexion internet en réalisant un NAT avec la commande *nft*.
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel AIS - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser les infrastructures systèmes et virtualisées
        - Appliquer les bonnes pratiques et participer à la qualité de service
    - Fiche compétence professionnelle N°3 : SE Linux, DNS, DHCP

## Introduction

Le logiciel DNSMasq permet, en un seul programme, de gérer les services DNS, DHCP, TFTP et PXE. Le site officiel est : [https://thekelleys.org.uk/dnsmasq/doc.html](https://thekelleys.org.uk/dnsmasq/doc.html). La plupart des distributions Linux ont un paquet pour ce logiciel. Ce logiciel se configure avec des fichiers textes facilement compréhensibles. Le fichier de configuration principal est `/etc/dnsmasq.conf`.

Ce TP sera construit autour du schéma réseau suivant:

![Shema réseau TP DNSMasq](../img/SchemaTPDnsMasq.svg)

Tous les ordinateurs sont des MVs basées sur votre modèle Debian. La machine *deb12gw* possède deux interfaces réseaux virtuels: *vmbr0* (sans doute *ens18*) et *vmbr50* (sans doute *ens19*). Les autres machines virtuelles seront connectées à *vmbr50*, les cartes réseaux seront *ens18*.

!!! note "Mise en place de l'architecture du TP"
    Rajoutez un nouveau pont réseau (bridge **vmbr50**) dans Proxmox. Clonez vos trois machines virtuelles en leur donnant comme nom celui du schéma. Rajoutez une carte réseau à la MV deb12gw reliée à vmbr50. Changez le *vmbr* sur les deux autres MVs pour les mettre sur le vmbr50. Démarrez vos MVs, faire la configuration réseau de chacune des MVs en modifiant le fichier `/etc/network/interfaces`. N'oubliez pas de redémarrer le service de gestion du réseau. Faire des tests de *ping* entre les deux machines qui ont une adresse ip fixe.  
    Il faut changer le nom (*hostname* en anglais) des MVs pour être conforme au schéma du TP. Lisez cet article : [https://www.linuxjournal.com/content/how-change-hostname-debian-12-bookworm](https://www.linuxjournal.com/content/how-change-hostname-debian-12-bookworm) et mettez à jour le nom de chacune de vos MVs Debian. Il faudra aussi éditer le fichier `/etc/hosts` et remplacer l'ancien nom par le nouveau nom. Redémarrez la MV et vérifiez que le nom à changer.

## Installation et première configuration de DNSMasq

Cette partie sera dédié à l'installation et la configuration du logiciel DNSMasq et en particulier de la partie DNS. Suivez les instructions ci-dessous.

!!! note "Installation de DNSMasq et configuration simple du DNS"
    Mettre à jour la liste des paquets. Installez le paquet **dnsmasq**. Renommez le fichier de configuration d'origine en `/etc/dnsmasq.conf.ori`. Comme tous les paquets Debian, la documentation est installé dans `/usr/share/doc`. Vous consulterez les documentations suivantes pour répondre aux questions du tableau ci-dessous (la commande **zmore** permet d'afficher les fichiers textes compressés avec gzip (.gz)):
    
    - `/usr/share/doc/dnsmasq-base/README.Debian`
    - `/usr/share/doc/dnsmasq-base/FAQ.gz`
    - `/usr/share/doc/dnsmasq-base/examples/dnsmasq.conf.example`

    Créez un fichier `/etc/dnsmasq.conf` avec le contenu suivant puis sauvez-le :

    ```console
    # Partie DNS
    domain-needed
    bogus-priv
    resolv-file=/etc/dnsmasq-dns.conf
    strict-order
    addn-hosts=/etc/dnsmasq-hosts.conf
    expand-hosts
    domain=tpais.lan
    log-queries
    interface=ens19
    ```

    Modifiez le fichier `/etc/resolv.conf` et rajoutez en première ligne: `nameserver 192.168.50.254`.

Complétez le tableau ci-dessous en expliquant les options:

|Option|Fonction|
|:---|:---|
|domain-needed||
|bogus-priv||
|resolv-file = /etc/dnsmasq-dns.conf||
|strict-order||
|addn-hosts = /etc/dnsmasq-hosts.conf||
|expand-hosts||
|domain = tpais.lan||
|log-queries||
|interface = ens19||

!!! note "Création des fichiers /etc/dnsmasq-dns.conf et /etc/dnsmasq-hosts.conf"
    Le fichier `/etc/dnsmasq-dns.conf` contient les adresses ip des serveurs DNS qu'interrogera DNSMasq lorsqu'il sera en dehors du nom de domaine qu'il gère (dans notre exemple *tpais.lan*). Créez ce fichier avec le contenu suivant:

    ```console
    nameserver 10.187.52.5   # DNS de la section de BTS
    nameserver 8.8.8.8       # un des DNS public de Google
    ```

    Le fichier `/etc/dnsmasq-hosts.conf` contient les correspondances @IP/nom que le service DNSMasq sera en mesure de résoudre pour le domaine *tpais.lan*. Créez ce fichier avec le contenu suivant:

    ```console
    deb12gw 192.168.50.254
    deb12info 192.168.50.1
    ```

    Relancez le service DNSMasq: `systemctl restart dnsmasq` et regardez s'il y a des erreurs avec `systemctl status dnsmasq`. Corrigez vos erreurs (généralement de syntaxe) si besoin. Réalisez les tests suivants:

    - sur la machine deb12gw: `ping deb12gw.tpais.lan` -> OK / KO, `ping deb12info.tpais.lan` -> OK / KO, `ping deb12gw` -> OK / KO, `ping deb12info` -> OK / KO,
    - sur la machine deb12info: `ping deb12gw.tpais.lan` -> OK / KO, `ping deb12info.tpais.lan` -> OK / KO, `ping deb12gw` -> OK / KO, `ping deb12info` -> OK / KO,
    - Pourquoi faire ces tests: `____________________________________________________________________________________________________________________________`

## Configuration du service DHCP dans le logiciel DNSMasq

D'après le schéma réseau proposé en début de document, donnez :

- l'adresse de réseau/masque pour les machines reliées à vmbr50 : `_______________________________`.
- une plage d'adresses utilisables pour le serveur DHCP dans ce réseau : `_____________________________________________________`
- l'adresse de passerelle (gateway) pour ce réseau : `_________________________________`
- l'adresse du serveur de noms (DNS) pour ce réseau :  `_________________________________`

!!! tip "Rappel : échanges entre un client DHCP et un serveur DHCP"
    ```dot
    digraph AffectationIPParServeurDHCP {
        fontname="Helvetica,Arial,sans-serif"
        node [fontname="Helvetica,Arial,sans-serif"]
        edge [fontname="Helvetica,Arial,sans-serif"]
        node [shape=box];
        rankdir="LR";
  
        subgraph cluster_0 {
	        style=filled;
	        color=lightgrey;
            fontcolor=black;
	        node [style=filled,color=white,fontcolor=black];
            "Client DHCP\nrequête DHCP_Discover" -> "Serveur DHCP\nréponse DHCP_Offer"
	        "Serveur DHCP\nréponse DHCP_Offer" -> "Client DHCP\nrequête DHCP_Request"
            "Client DHCP\nrequête DHCP_Request" -> "Serveur DHCP\nréponse DHCP_Ack"
        }
    }
    ```

!!! note "Complétez le fichier /etc/dnsmasq.conf en rajoutant les lignes ci-dessous pour la partie DHCP"
    Complétez les champs manquants avec les renseignements ci-dessus. Pour la syntaxe, regardez la documentation.
    ```console
    # Partie DHCP
    dhcp-range=________________,________________,12h
    dhcp-option=option:netmask,______________________
    dhcp-option=option:router,_______________________
    dhcp-option=option:dns-server,____________________________
    dhcp-option=option:ntp-server,134.59.1.5  # @IP de ntp.unice.fr
    dhcp-option=option:domain-name,tpais.lan
    log-dhcp
    ```
    Sauvez ce fichier. Relancez le service *dnsmasq* et vérifiez qu'il n'y a pas d'erreur. Démarrez ensuite la MV Debian qui est en mode DHCP. Relevez ces caractéristiques IP: @IP/Masque:`_________________`, Passerelle: `_________________`, DNS: `_____________________`. Ces paramètres sont-ils en accord avec votre configuration ? OUI/NON. Justification: `___________________________________________________________________________________________________________`  
    Vérifiez que le ping vers cette machine (deb12compta) fonctionne depuis les autres machines du réseau : OUI/NON et aussi que cette machine ping les autres machines du réseau : OUI/NON.  
    Affichez et commentez les logs du service DNSmasq avec la commande : `journalctl -u dnsmasq --since="5 minutes ago"`  
    Expliquez la commande précédente en vous aidant de ce site ([https://www.linuxtricks.fr/wiki/systemd-utiliser-journalctl-les-logs-de-systemd](https://www.linuxtricks.fr/wiki/systemd-utiliser-journalctl-les-logs-de-systemd)): `_________________________________________________________________________________________________________________________________`.

Dans certains cas d'usage vous souhaitez qu'une machine en mode DHCP ai toujours la même adresse ip distribuée par le serveur DHCP. Dans ce cas il faut avoir l'adresse MAC de la machine et choisir une adresse ip qui n'est pas dans la plage d'attribution automatique du serveur DHCP.

!!! note "Affectation d'une adresse ip fixe à une machine connaissant son adresse MAC"
    Relevez à l'aide de la commande `ip a`, l'adresse MAC de la machine deb12compta : `________________________`. Éteignez les MVs deb12info et deb12compta. Réalisez la modification du fichier `/etc/dnsmasq.conf` pour que la machine **deb12compta** ai toujours l'adresse IP **192.168.50.20** à l'aide de la documentation. Écrivez ci-contre votre proposition: `____________________________________________________________`. Redémarrez le service dnsmasq, vérifiez qu'il n'y a pas d'erreur. Démarrez la MV deb12compta, vérifiez qu'elle obtient bien l'@IP prévue et qu'elle peut être *pinguée* par les autres MVs.

## Mise en oeuvre du serveur TFTP

Le terme **TFTP** signifie *Trivial File Transfert Protocol*. Il s'agit d'un serveur FTP dont les fonctionnalités sont réduites au strict minimum (en gros à transmettre un fichier sur demande), il écoute sur le port 69. Il est souvent utilisé pour démarrer un système d'exploitation par le réseau (*boot PXE*) ou pour mettre à jour les *firmwares* de certains matériels réseaux. C'est le cas par exemple des matériels CISCO. Pour mettre en oeuvre le TFTP avec le le logiciel DNSMasq, il suffit de rajouter les lignes suivantes :

```console
# Partie TFTP
enable-tftp  # activation de la fonctionnalité TFTP
tftp-root=/srv/tftp  # répertoire servi par TFTP
```

Il faut bien sur que le répertoire `/srv/tftp` existe et que les droits d’accès à ce répertoire soit `rwxr-xr-x`. Vous mettrez dans celui-ci vos fichiers à transférer vers vos appareils réseaux.

## Permettre l’accès à Internet à vos machines

Les machines qui appartiennent au réseau 192.168.50.0/24 n'ont pas accès à Internet même si vous avez indiqué une passerelle (dans notre cas : deb12gw 192.168.50.254). La seule machine qui a accès à Internet c'est deb12gw car elle a une seconde carte réseau qui est connectée à un réseau qui, lui, a accès à Internet. Il faut donc transformer cette machine en un routeur qui va faire du NAT avec l'utilisation de la commande **nft** (net filter tables). Si vous voulez vous faire mal à la tête, consultez la page [https://www.linuxembedded.fr/2022/06/introduction-a-nftables](https://www.linuxembedded.fr/2022/06/introduction-a-nftables).

!!! note "Activation de l'accès Internet à l'aide de la commande nft"
    Tapez les commandes ci-dessous sur la machine deb12gw en étant *root*. Adaptez la carte réseau de sortie *oif ensXX* et l'adresse ip *10.187.52.XX* de la dernière ligne à votre cas.
    ```console
    # echo 1 > /proc/sys/net/ipv4/ip_forward
    # nft add table accesInternet
    # nft add chain accesInternet prerouting { type nat hook prerouting priority 0 \; }
    # nft add chain accesInternet postrouting { type nat hook postrouting priority 0 \; }
    # nft add rule accesInternet postrouting masquerade
    # nft add rule accesInternet postrouting ip saddr 192.168.50.0/24 oif ens18 snat 10.187.52.XX
    ```
    Ces commandes sont une adaptation de l'article de IT-connect : [https://www.it-connect.fr/chapitres/configurer-le-nat-sous-nftables/](https://www.it-connect.fr/chapitres/configurer-le-nat-sous-nftables/).  
    Testez l’accès Internet depuis vos machines deb12info et deb12compta. Notez que ces commandes sont temporaires et que si vous redémarrez votre machine deb12gw, l’accès internet ne sera plus actif. Il vous reste à faire deux scripts (activerInternet.sh et desactiverInternet.sh) pour automatiser le tout.


## Conclusion

Si vos services DNS et DHCP sont fonctionnels, il peut être intéressant de désactiver les logs pour limiter les écritures disques. Pour cela il suffit juste d'enlever les lignes `log-queries` et `log-dhcp`. Nous n'avons pas aborder ici le *vrai* serveur DNS de référence, j'ai nommé **BIND** (consultez [https://doc.ubuntu-fr.org/bind9](https://doc.ubuntu-fr.org/bind9) ou [https://wiki.debian.org/fr/Bind9](https://wiki.debian.org/fr/Bind9)). Il est disponible dans toutes les distributions Linux et peut faire l'objet d'une étude pour votre dossier professionnel.

Sources pour la rédaction de ce document:

- Le site LinuxTricks : [https://www.linuxtricks.fr/wiki/dnsmasq-le-serveur-dns-et-dhcp-facile-sous-linux](https://www.linuxtricks.fr/wiki/dnsmasq-le-serveur-dns-et-dhcp-facile-sous-linux)
- Le site ArchLinux : [https://wiki.archlinux.org/title/Dnsmasq](https://wiki.archlinux.org/title/Dnsmasq)