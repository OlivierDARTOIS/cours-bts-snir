# Installation, configuration d'un serveur Proxmox sur serveur Dell

!!! tldr "Objectifs de ce document"

    - installer le logiciel Proxmox (base Debian) sur un serveur Dell,
    - identifier les systèmes de fichiers (LVM, Ext4) à utiliser sur les systèmes de stockage en fonction de leur utilisation (MVs, containers, autres données),
    - créer des modèles de MVs pour Linux/Debian et Microsoft Windows,
    - créer des MVs à partir de ces modèles et utiliser la création *d'instantané* ou *snapshot*,
    - gérer les groupes, les utilisateurs à partir des rôles définis dans Proxmox,
    - configurations complémentaires utiles pour sécuriser votre serveur Proxmox 
    ---
    Enseignant : Olivier DARTOIS - Titre Professionnel AIS - Lycée Turgot - Limoges

!!! info "Référentiel du TP AIS"

    - année : Titre Professionnel AIS - Début de formation
    - Compétences évaluées dans ce TP:
        - Administrer et sécuriser une infrastructure de serveurs virtualisée
        - Appliquer les bonnes pratiques et participer à la qualité de service
        - Participer à l’élaboration et à la mise en œuvre de la politique de sécurité

## Introduction

Ce document présente l'installation et la configuration de base d'une solution de virtualisation Proxmox sur un serveur Dell R650xs. Avant de rentrer dans le vif du sujet et si vous ne connaissez pas Proxmox, il est conseillé de faire le [TP de découverte de l'environnement Proxmox](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_01/) en tant qu'*utilisateur* de cette solution. Si vous connaissez déjà Proxmox et son interface web de gestion, vous allez installer et configurer Proxmox. Vous serez donc cette fois-ci du coté *administrateur*.

Vous devez avoir fait le TP de découverte du serveur Dell et l'installation d'une distribution Debian sur celui-ci à partir d'une clé USB. Vous allez donc télécharger la solution Proxmox directement sur le [site officiel de l'éditeur](https://www.proxmox.com/en/) (base Debian). Transférez l'[image ISO](https://www.proxmox.com/en/downloads) sur une clé USB à l'aide du logiciel [Rufus](https://rufus.ie/downloads/) sous Microsoft Windows. Avant de lancer l'installation à proprement parler, nous allons fixer le cahier des charges simple suivant:

- Le SE Debian/Proxmox sera installé en mode RAID1 sur le système BOSS du serveur Dell. Le système de fichier utilisé sur ce système disque sera [*ext4*](https://en.wikipedia.org/wiki/Ext4).
- Les machines virtuelles ainsi que les conteneurs seront stockés en mode RAID6 sur le système PERC du serveur Dell. Le système de fichiers utilisé sur ce système disque sera de type [*LVM-Thin*](https://linuxconfig.org/introduction-to-lvm-thin-provisioning) pour des raisons de performance.
- Vous réaliserez des MVs modèles:
    - Debian minimale : installation la plus simple possible en mode console (nom : Debian-Console, 2 cœurs, 2Go RAM, 32Go DD, réseau vmbrX),
    - Debian minimale en mode graphique : installation de l'environnement graphique XFCE4 (nom : Debian-Graphique, 4 cœurs, 4Go RAM, 128Go DD, réseau vmbrX),
    - Microsoft Windows 10 avec l'installation des pilotes optimisés pour l'utilisation avec Proxmox (4 cœurs, 4Go RAM, 128Go DD, réseau vmbrX),
    - Microsoft Windows 11 avec pilotes optimisés, puce TPM et *bios* UEFI (4 cœurs, 8Go RAM, 128Go DD, réseau vmbrX).
- Création d'utilisateurs *simples*, d'utilisateurs *avancés* et d'utilisateurs *administrateurs*.
- enfin si vous avez le temps, la mise en place d'une authentification double facteur utilisant [TOTP](https://fr.wikipedia.org/wiki/Mot_de_passe_%C3%A0_usage_unique_bas%C3%A9_sur_le_temps), l'authentification avec un serveur Microsoft AD, l'agrégation de liens réseaux (*bonding* en anglais) en liaison avec le matériel réseau Cisco.

Vous êtes prêt ? Le programme est ambitieux mais pas infaisable ! Mais avant toute chose et pour éviter les [*RTFM*](https://fr.wikipedia.org/wiki/RTFM_%28expression%29) dans les forums, sachez que la [documentation](https://pve.proxmox.com/pve-docs/) en ligne de Proxmox est très bien faite et que **VOUS DEVEZ VOUS Y REPORTER AVANT** de poser une question !!!

## Installation de Proxmox

Suivez la procédure décrite dans l'exercice ci-dessous.

!!! note "Exercice"
    Créez votre clé USB avec l'image ISO de Proxmox. Réalisez la **configuration des systèmes RAID** sur le serveur Dell. Lancez l'installation depuis la clé USB. Suivez les étapes suivantes:

    - Connectez-vous à iDRAC puis lancez la *console virtuelle*,
    - Choisissez l'installation en mode graphique,
    - Acceptez la licence,
    - Choisissez le disque destination : par exemple `/dev/sda` (système BOSS) ou `/dev/sdb` (Système PERC), respectez le cahier des charges,
    - Cliquez sur le bouton *Options* par paramétrer plus finement le partitionnement du disque qui va accueillir le SE :
        - système de fichiers : respectez le cahier des charges,
        - hdsize : la taille du disque complet,
        - swap size : 8Go,
        - maxroot : vous limitez la taille à 100G,
        - maxvz : 0Go car vos images de MVs et conteneurs seront sur le système PERC,
        - Validez le tout pour passer à l'étape suivante.
    - Choix du pays : France et vous laissez le reste des champs par défaut,
    - Fixez le mot de passe de votre utilisateur *root* (`_____________________`), pour le mail mettre `me@home.lan`,
    - Choix de l'interface réseaux de gestion (*Management interface*) : laissez celle par défaut si vous le souhaitez (sinon mettre celle qui serait dans votre réseau d'administration). *Hostname (FQDN)* si vous avez un DNS dans votre architecture mettre celui prévu sinon *promox.home.lan*. Mettre les paramètres IP souhaités (@IP/CIDR : `____________________`, Passerelle : `____________________`, DNS : `____________________`),
    - Résumé avant installation définitive : vérifiez, si OK lancez l'install,
    - Si l'installation est correcte, retirez la clé USB puis redémarrez,
    - L'invite de commande qui apparaît vous donne l'@IP et le port pour vous connecter depuis un navigateur web : `____________________`.

## Configuration simple de Proxmox

Connectez-vous avec un navigateur sur l'adresse IP et le port que vous avez noté précédemment. Si nécessaire acceptez l'exception de sécurité. Authentifiez-vous avec le compte *root* défini pendant l'installation (vérifiez bien que vous êtes sur le royaume (realm) PAM). Les menus qui nous intéresse sont présentés ci-dessous.

![Menu Centre de données](../img/ProxMoxInstall/MenuCentreDeDonnees.png){align=left} 

Le menu [**Centre de données**](https://pve.proxmox.com/pve-docs/chapter-pve-gui.html#_datacenter) (tous les items ne sont pas présentés ici, seuls ceux que vous allez utiliser dans un premier temps) :

- Résumé : présente l'état du centre de données dans son ensemble (éventuellement composé de plusieurs nœuds),
- Stockage : permet d'ajouter un stockage au centre de données à l'aide des trois boutons *Ajouter*, *Supprimer* et *Éditer*,
- Sauvegarde : permet de sauver vos MVs et conteneurs vers un périphérique de sauvegarde (par exemple *Proxmox Backup*),
- Permissions : permet de gérer les différents utilisateurs, groupes, rôles et droits associés,
- ACME (Automated Certificate Management Environment) : gestion des certificats,
- Serveur de métriques : supervision du centre de données avec des outils tels que InfluxDB ou Graphite.


![Sous menu Permissions du Centre de données](../img/ProxMoxInstall/MenuCentreDeDonneesPermissions.png){align=left}

Le sous menu [**Permissions**](https://pve.proxmox.com/pve-docs/chapter-pveum.html) du **Centre de données** :

- Utilisateurs : A l'aide des boutons, vous pourrez *Ajouter* un utilisateur, *Éditer* ces caractéristiques, le *Supprimer*, fixer son *Mot de passe* si l'utilisateur l'a oublié, fixer les *Permissions* et *Débloquer l'A2F* (l’authentification double facteur, mise en oeuvre en fin de document),
- Jetons d'API : création d'un jeton pour utiliser l'API de Proxmox,
- Double facteur : gérer l’authentification double facteur, mise en oeuvre avec un utilisateur et un code TOTP plus loin dans ce document,
- Groupes : création des groupes d'utilisateurs (gestion des droits conseillé à ce niveau par l'éditeur de Proxmox),
- Pools : création d'un ensemble de resources partageables entre un groupe d'utilisateurs ou un utilisateur,
- Rôles : les différents rôles possibles que vous pouvez affecter à un groupe ou un utilisateur (par ex. : PVEAdmin, PVEVMUser),
- Royaumes : les différentes méthodes d'authentification pour se connecter au serveur Proxmox. Par exemple, le type *PAM* pour s'authentifier en local avec le système Linux sous-jacent (c'est le cas pour l'utilisateur *root*), le type *pve* pour s'authentifier en utilisant la base d'utilisateurs gérés par Proxmox et les types *LDAP* et *Serveur Active Directory* pour l’authentification par un annuaire.
 
Le sous menu [**Nœuds**](https://pve.proxmox.com/pve-docs/chapter-pve-gui.html#_nodes) du **Centre de données** :

Vous trouverez ici tous les *Nœuds* qui forment le cluster du *Centre de données*. Dans notre étude, nous n'avons qu'un seul nœud. Par défaut il s'appelle *proxmox*. Cliquez sur celui-ci pour accéder à son menu présenté ci-dessous.

![Sous menu Nœuds du Centre de données](../img/ProxMoxInstall/MenuNoeuds.png){align=left}

- Résumé : présente un résumé de fonctionnement du nœud choisi (utilisation processeur, utilisation mémoire, espace disque, etc...),
- Shell : vous permet d'accéder directement à un interpréteur de commandes en tant qu'utilisateur *root*,
- Système : visualise tous les services fonctionnant sur le nœud. Cet item est détaillé ci-dessous,
- [Mises à jour](https://pve.proxmox.com/pve-docs/chapter-sysadmin.html#sysadmin_package_repositories) : pour maintenir votre nœud avec les dernières versions des logiciels,
- Disques : gestion des disques physiques et des différents systèmes de fichiers possibles (LVM, LVM-Thin, Répertoire, ZFS).

Item **Système** du sous menu **Nœuds** du **Centre de données** :

![item Système du Sous menu Nœuds du Centre de données](../img/ProxMoxInstall/MenuNoeudSysteme.png){align=left}

- [Réseau](https://pve.proxmox.com/pve-docs/chapter-sysadmin.html#sysadmin_network_configuration) : cet item visualise toutes les cartes réseaux du nœud (réelles ou virtuelles). C'est ici que vous affectez les @IP/masques aux cartes réseaux. Vous pouvez aussi créer des nouvelles cartes réseaux (bridge, bond ou VLAN) avec le bouton *Créer*,
- [Certificats](https://pve.proxmox.com/pve-docs/chapter-sysadmin.html#sysadmin_certificate_management) : cet item gère les certificats de votre nœud, c'est ici que se trouve le certificat auto-signé pour accéder au site web de votre serveur proxmox,
- DNS : cet item vous permet d'ajouter/modifier/supprimer les serveurs DNS du nœud,
- Heure : gestion du fuseau horaire (important si vous avez un cluster),
- System Log : suivez les traces de l'activité de votre nœud.

Item **Disques** du sous menu **Nœuds** du **Centre de données** :

![item Disques du Sous menu Nœuds du Centre de données](../img/ProxMoxInstall/MenuNoeudDisques.png){align=left}

- [LVM](https://pve.proxmox.com/pve-docs/chapter-pvesm.html#storage_lvm) : cet item vous permet de visualiser les volumes LVM existants, de les détruire (après les avoir sélectionnés) ou d'un créer de nouveau : bouton *Créer: Volume Group*,
- [LVM-Thin](https://pve.proxmox.com/pve-docs/chapter-pvesm.html#storage_lvmthin) : même chose avec les volumes LVM-Thin,
- [Répertoire](https://pve.proxmox.com/pve-docs/chapter-pvesm.html#storage_directory) : cet item vous permet de visualiser, créer ou détruire de nouveaux stockages qui sont en fait des répertoires (sur le nœud local ou des systèmes de fichiers montés à distance NFS par exemple).

!!! note "Exercice"
    Conformément au cahier des charges, vous devez créer un espace de stockage LVM-Thin sur le sous-système disque PERC. Rendez-vous dans la gestion des disques, trouvez le périphérique concerné (`___________`). Il faudra peut être nettoyer le disque et l'initialiser avec le partitionnement de type [GPT](https://fr.wikipedia.org/wiki/GUID_Partition_Table). Créez alors un espace de stockage *LVM-Thin* avec le bouton *Créer: Thinpool*, lui donnez comme nom `VMs` par exemple. Notez bien toutes vos étapes pour savoir les refaire !  
    Cliquez sur l'item *Stockage* dans votre *Centre de données* puis vérifiez que vous avez un nouvel item *VMs*, cliquez sur *Résumé* pour voir ces caractéristiques. Notez bien que ce type de stockage ne peut contenir que des disques de MVs ou des conteneurs.  
    Cliquez sur le *Stockage* appelé *local*. Précisez ce que peut stocker cet espace : `_______________________________________________________________________`. Vous allez téléverser des images ISO pour créer vos futurs VMs en cliquant sur l'item *Images ISO* puis le bouton *Téléverser*. Les images ISO de Microsoft Windows 10, 11 et server2022, Debian NetInst sont disponibles sur le pc portable dans `C:\ImagesISO`. Avant de téléverser les images il faudrait récupérer leurs signatures pour vérifier leur intégrité lors du téléversement.  
    Créez les deux MVs Debian (minimale mode console et minimale graphique XFCE4) suivant le cahier des charges. Vous utiliserez comme *miroir* d'installation le serveur local à l'@IP `___________________` et le répertoire `/debian/`. Vérifiez leur fonctionnement (login, accès internet, installation d'un logiciel par la commande apt).  
    Si les deux VMs sont corrects, convertissez-les en *modèles* : clic-droit sur votre MV puis item *Convertir en modèle*. Ainsi elles ne peuvent pas être modifiées et vos utilisateurs partiront d'une base que vous avez choisi voire personnalisé. L'icône de votre MV doit changer avec un *petit écran* pour indiquer qu'il s'agit d'un modèle.

## Création d'une MV optimisée pour Microsoft Windows 10/11
un [article](https://pve.proxmox.com/wiki/Windows_10_guest_best_practices) sur le wiki de proxmox décrit cette procédure mais elle est un peu obsolète sur certains points. Cette installation optimisée permet d'obtenir de meilleures performances et donc un confort d'utilisation plus important pour vos utilisateurs.

!!! note "Exercice : installation optimisée de Microsoft Windows 10"
    
    Pour obtenir de bonnes performances, vous allez installez les [pilotes *VirtIO*](https://pve.proxmox.com/wiki/Windows_VirtIO_Drivers) lors de l'installation de Microsoft Windows. Le [site officiel](https://github.com/virtio-win/kvm-guest-drivers-windows/wiki/Driver-installation) des pilotes *virtio-win* décrit aussi la mise en place de ces pilotes. C'est depuis ce site que vous téléchargez [l'image ISO officielle](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso) des pilotes *virt-io*.

    - Téléchargez l'image ISO de Windows 10 et des drivers virt-io (elles doivent se trouver sur le PC portable pour économiser de la bande passante),
    - Téléversez ces deux images ISO vers le stockage proxmox adapté au image ISO,
    - Créez une nouvelle MV : RAM: 4Go, disque dur de 32Go, 4 , réseau sur vmbrX (X=`__`) en suivant la procédure suivante:
        - Onglet Général : Nom: Win10-Modele,
        - Onglet Système d'exploitation : Utiliser une image de média, Stockage : celui ou vous avez déposé vos ISOs, Image ISO : celle de Windows10, Type : Microsoft Windows, Version : 10, cochez la case *Ajouter un périphérique contenant les pilotes VirtIO*, choisissez Image ISO : virtio-win-XXX,
        - Onglet Système : Vérifiez que *Contrôleur SCSI* est à *VirtIO SCSI single*, cochez la case *Agent QEMU*,
        - Onglet Disques : Vérifiez le *Stockage* est sur celui qui supporte des MVs, *Taille* à 32Go, *Cache* à *Write Back*, cocher la case *Abandonner*,
        - Onglet Processeur : mettre 4 cœurs,
        - Onglet Mémoire : mettre 4096Mo (4Go),
        - Onglet Réseau : choisir la bonne carte réseau dans le champ *Pont*, le champ *Modèle* doit être sur *VirtIO*,
        - Onglet Confirmation : cocher *Démarrer après création* pour démarrer automatiquement l’installation de Windows 10 (patientez pendant que le curseur clignote...),
    - Suivez les instructions de l'installateur Windows 10 : vous n'avez pas de clé produit, choisissez Windows 10 Professionnel, choisissez *Personnalisé : installer uniquement Windows*. Sur l'écran *Où souhaitez-vous installer Windows ?* aucun disque n’apparaît car il faut installer le pilote virt-io pour les disques. Cliquez sur *Charger un pilote*, bouton *Parcourir* choisissez le lecteur de CDRom *virt-io-win*, répertoire *vioscsi*, répertoire *w10* puis répertoire *amd64*. Validez avec le bouton *OK*. Le disque dur doit apparaître, continuez votre installation, 
    - Choisir *Je n'ai pas internet* car le pilote de la carte réseau n'est pas installé donc *Continuer avec l'installation limitée*. Comme compte choisissez *ais*, le mot de passe de votre choix puis répondez aux trois questions secretes... Répondez *Non* à toutes les questions de fin d'installation. Apres quelques minutes votre Windows doit être prêt (il manque les mises à jour),
    - Vous allez visualiser les périphériques qui n'ont pas de pilotes : vous accédez aux différents menus systèmes en faisant ++left-windows+"X"++. Comment faire la touche :material-microsoft-windows: dans votre MV : déplier le panel NoVNC (petit triangle sur le bord gauche de l'écran de votre MV, cliquez sur l'icône :a: puis l'icône :material-microsoft-windows: enfin la touche *X* au clavier). Dans le menu qui s'ouvre, choisissez *Gestionnaire de périphériques*. Les éléments qui n'ont pas de pilotes apparaissent en haut de la liste,
    - Commencez par le contrôleur Ethernet, double-cliquez sur celui-ci puis *Mettre à jour le pilote...*, *Parcourir mon poste de travail...*, bouton *Parcourir* pour trouver le CDRom *virtio-win*. Cherchez le répertoire *NetKVM*, *w10*, *amd64*. Terminez l'installation du pilote. La carte réseau est fonctionnelle,
    - Faire de même avec *Contrôleur PCI de communication* : pilote *vioserial* et *Périphérique PCI* : pilote *Balloon* (gestion de la mémoire RAM),
    - Redémarrez votre MV Windows 10, configurez le réseau, faites les mises à jour (si vous avez le temps), personnalisez l'environnement pour vos futurs utilisateurs...
    - Maintenant que votre MV Windows 10 est prête, vous allez la convertir en **Modèle** pour que vos utilisateurs puissent la cloner de manière efficace. Vous leur imposez donc un cadre standard et maîtrisé au sein de votre SI. Pour convertir cette MV en modèle, éteignez votre MV, clic droit sur celle-ci puis choisir dans le menu *Convertir en modèle*. L'icône de la MV se transforme avec un petit écran supplémentaire indiquant que cette MV est un modèle.

!!! note "Exercice : installation optimisée de Microsoft Windows 11"

    *Cet exercice est facultatif !* Reprenez les étapes de configuration d'une machine Windows 10 pour l'adapter à Windows 11. L'image ISO de Windows 11 est disponible sur le PC portable.

## Création de MVs à partir de modèles et utilisation des *instantanés* (snapshot)

Vous disposez maintenant de quatre modèles de MVs pour vos utilisateurs. Vous allez faire un test sur le modèle de MV *Debian-Console*. Pour cela il faut tout d'abord **cloner** le modèle, créer un **instantané**, ajouter/tester des logiciels/faire une mise à jour, revenir à la situation précédente en cas de problème ou supprimer l'instantané si tout est OK.

!!! note "Exercice"

    Faites un clic droit sur votre modèle de MV Debian-Console, choisissez l'item *Cloner*. Donnez un nouveau nom à votre MV (par ex: Debian-Console-Test). Laissez le mode à *Clone lié* (lien direct avec la MV modèle, consomme peu de place sur le disque dur) si le système de stockage le supporte (c'est le cas de LVM-Thin) sinon choisir *Clone Intégral* (indépendant de la MV modèle, prend plus de place sur le disque dur). Cliquez sur le bouton **Cloner** pour terminer.  
    Votre MV est toute *propre* ! Avant de faire une modification dans celle-ci, vous allez faire créer un **instantané** (c'est une bonne pratique à utiliser en entreprise) pour revenir en arrière s'il y a un problème. Cliquez sur le menu **Instantanés** de votre MV puis cliquez sur le bouton *Créer un instantané*. Donnez lui un nom et une description explicite. L'instantané doit apparaître avec la date/heure du jour.  
    Lancez votre nouvelle MV, connectez-vous en *root*. Installez le logiciel de surveillance *htop* puis le lancez : `htop` (++f10++ pour quitter). Vous venez de réaliser une modification sur votre MV. Sans éteindre la MV, retournez dans le menu *Instantanés* puis cliquez sur votre instantané et sur le bouton *Retour en arrière*, acceptez le fait que *l'état actuel sera perdu* ! La MV s'est éteinte pour revenir à l'état précédent, redémarrez-là et vérifiez que le logiciel *htop* n'est pas installé. Vous avez retrouvé l'état d'origine mais vous avez perdu tout votre historique de commande et ce que vous avez pu faire comme modification.  
    Vous pouvez aussi créer un instantané pendant que la MV est en fonctionnement, dans ce cas la mémoire (RAM) est aussi sauvegardée ce qui peut conduire à une utilisation importante de l'espace disque si vous avez beaucoup de mémoire !

La documentation officielle sur les [instantanés](https://pve.proxmox.com/wiki/Live_Snapshots) est relativement succincte, faites plutôt une recherche sur internet.

!!! warning "Pensez à utiliser les snapshots"

    C'est une bonne pratique à mettre en oeuvre à chaque fois que vous installez un logiciel, effectuez une mise à jour ou changez les paramètres d'un fichier de configuration. Une fois que vous êtes sûr que votre modification n'a pas engendré de bug, vous pouvez effacer l'instantané pour libérer de l'espace disque.

## Gestion des utilisateurs et des groupes d'utilisateurs

Pour l'instant vous avez travaillé en tant que *super-admin* avec Proxmox. Vous ne pouvez naturellement pas donné ce niveau de droit à vos utilisateurs. Pour respecter les recommandations de l'ANSSI, il faut toujours donner le minimum de droit à votre utilisateur pour qu'il exécute sa tache. Dans notre cas vous allez :

- créer un utilisateur simple qui n'aura accès qu'à une MV que vous lui aurez assigné,
- créer un groupe d'utilisateurs *PowerUsers* qui pourront à peu près faire ce qu'ils veulent avec des MVs,
- créer un groupe d'utilisateurs *Administrateurs* pour déléguer les taches d'administrations à certains utilisateurs identifiés pour ensuite désactiver l'utilisateur *root* qui à bien trop de pouvoir !

Les différents rôles prédéfinis dans Proxmox sont disponibles dans le *Centre de données* puis l'item *Permissions* et *Rôles*. La [documentation sur l'authentification de vos utilisateurs et la gestion des droits](https://pve.proxmox.com/pve-docs/chapter-pveum.html) est disponible en ligne et demande une lecture attentive !

!!! note "Exercice"

    Après la lecture de la documentation, vous allez répondre à la première ligne de notre cahier des charges : créer un utilisateur avec une MV. Cliquez sur *Centre de données* puis *Permissions* puis *Utilisateurs* enfin cliquez sur le bouton *Ajouter*. Complétez les différents champs : le *Nom d'utilisateur* (utilisateur1), le *Royaume* (realm) sera mis pour l'instant à *Proxmox VE authentication server*, *Mot de passe* à votre convenance, Pas de *Groupe*. Vous pouvez fixer une date d'expiration (pratique dans le cadre d'un stagiaire par ex). Complétez les autres champs si vous le souhaitez.  
    Créez une nouvelle MV en clonant votre MV Windows 10 modèle (nom: Win10-utilisateur1). Sélectionnez cette nouvelle MV, cliquez sur l'item *Permissions* puis le bouton *Ajouter*, *Permissions de l'utilisateur*. Sélectionnez votre utilisateur, puis comme *Rôle* : *PVEVMUser*.  
    Déconnectez-vous puis identifiez-vous en tant qu'*utilisateur1*. Vous remarquez que les menus sont limités pour cet utilisateur. Vérifiez que vous avez accès à la VM prévue et uniquement celle-ci, testez son fonctionnement. Pouvez-vous créer une nouvelle MV avec cet utilisateur ? `________`

!!! note "Exercice"
    === "Énoncé"
        Vous allez créer un groupe d'utilisateurs *PowerUsers* qui auront comme rôle *PVEAdmin*. Créez des utilisateurs (pu1, pu2) appartenant à ce groupe. Testez qu'ils peuvent créer/cloner/effacer des MVs. Si ce n'est pas le cas, gérer les permissions pour que ces trois actions soient possibles sans donner trop de droits à ce groupe.
    === "Votre réponse"
        `->`

!!! note "Exercice"
    === "Énoncé"
        Vous allez créer un groupe d'utilisateurs *Administrateurs* qui auront comme rôle *Administrator*. Créez des utilisateurs (admin1, admin2) appartenant à ce groupe. Testez qu'ils peuvent créer/cloner/effacer des MVs. Si ce n'est pas le cas, gérer les permissions pour que ces trois actions soient possibles sans donner trop de droits à ce groupe.
    === "Votre réponse"
        `->`

!!! warning "La gestion des droits peut être complexe"
    Les trois exemples ci-dessus restent très simples et couvrent une petite partie de la gestion des droits sous Proxmox. Par exemple, nous n'avons pas évoqué dans ce document la gestion des **pools de resources** qui permet de restreindre encore l'environnement de travail de vos utilisateurs. Je vous mets en lien mes notes brutes sur la réalisation d'un pool pour les étudiants de BTS CIEL. Attention cependant les MVs sont dans ce cas hébergées sur un système de fichiers EXT4 et pas LVM.  
    La gestion des droits pourrait être un sujet de présentation en fin de cursus...

## Quelques éléments de sécurisation à mettre en oeuvre

Ce qui suit ne se veut pas exhaustif. Ce sont quelques bonnes pratiques que vous pouvez mettre en oeuvre.

### Sécurisation du compte *root* et durcissement du service ssh
Le compte **root** étant utilisé en interne pour différentes tâches d’administration, comme les sauvegardes ou encore la gestion d’un cluster Proxmox, vous ne pouvez pas le désactiver purement et simplement comme on le ferait sur un système Linux classique. Vous allez commencer par créer un compte utilisateur auquel on va donner les droits d’administration via la commande **sudo**. 

!!! tip "Pourquoi utiliser la commande **sudo**"
    - Les administrateurs n'ont pas à se souvenir d'un mot de passe supplémentaire,
    - Cela évite le comportement du "Je peux faire n'importe quoi sur ma machine" : avant d'effectuer une action d'administration, le système vous demande votre mot de passe, ce qui devrait faire vous réfléchir sur les conséquences de vos actions,
    - La commande **sudo** conserve une trace de toutes les commandes exécutées. Si un problème apparaît, vous pourrez toujours consulter ce journal afin de retrouver la commande ayant causé le problème (Assurer la **Tracabilité** dans le système),
    - Tous les pirates tentant de pénétrer par la force brute votre système savent qu'il existe un compte appelé root et essaieront de pirater celui-ci d'abord. Ils ne connaissent pas les identifiants des autres utilisateurs de votre ordinateur,
    - Ceci permet un transfert rapide des droits d'administration, sans compromettre la sécurité de votre environnement informatique par le partage d'un mot de passe unique pour le compte root,
    - La commande **sudo** peut être configuré de manière fine dans le fichier `/etc/sudoers`. Lire l'[article sur IT-Connect](https://www.it-connect.fr/commande-sudo-comment-configurer-sudoers-sous-linux/).

Connectez-vous à distance en ssh depuis votre machine Microsoft Windows : lancez une interface de commande et tapez la commande : `ssh root@IP_serv_proxmox`. Tapez alors les commandes suivantes :

```console
root@proxmox# apt install -y sudo
root@proxmox# adduser localadmin
root@proxmox# adduser localadmin sudo
```

Changez le mot de passe du compte *root* pour un mot de passe sécurisé (par ex. sur le [site de la CNIL](https://www.cnil.fr/fr/generer-un-mot-de-passe-solide)) avec la commande : `root@proxmox# passwd`. Déconnectez-vous du compte root pour vous reconnectez avec votre utilisateur *localadmin* (qui bien sur à lui aussi un mot de passe complexe). Pour passer root, il faut alors tapez la commande `sudo -i`.

Pour renforcer la sécurité, vous allez créer une paire de clés SSH pour vous connecter avec le compte *localadmin*.

!!!note "Exercice"
    Sous Linux, vous pouvez utiliser les commandes suivantes (avec le portable HP sous Linux par exemple, il faudra penser à changer son adresse IP pour dialoguer avec le serveur Proxmox) :  
    ```console
    ais@linux$ ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_servproxmox
    ais@linux$ ssh-copy-id -i ~/.ssh/id_ed25519_servproxmox.pub localadmin@IP_serv_proxmox
    ais@linux$ ssh -i ~/.ssh/id_ed25519_servproxmox localadmin@IP_serv_proxmox
    localadmin@proxmox$ sudo -i
    root@proxmox#
    ```
    Faites de même sur votre machine Microsoft Windows en suivant le [tutoriel](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_02/#comment-se-connecter-sur-un-serveur-sans-taper-de-mot-de-passe) de ce même site.

Pour poursuivre la protection du compte root du serveur Proxmox, vous allez désactiver l'authentification par mot de passe et par clé sauf pour l'utilisateur localadmin et ceci uniquement sur l'interface réseau d'administration du serveur (pas les autres cartes réseaux, voir le cours *Architecture d'un réseau sécurisé*). **ATTENTION** : vous risquez de vous faire fermer dehors en cas de mauvaise manip...

!!!note "Exercice"
    Connectez-vous sur le serveur Proxmox en tant qu'utilisateur *localadmin*. Faites précéder vos commandes par *sudo* pour obtenir des droits *root*. Éditez le fichier `/etc/ssh/sshd_config.d/durcissement_proxmox.conf`. Dans ce fichier vide rajoutez les lignes suivantes en expliquant leur rôle :
    ```console
    ListenAddress @IP_serv_proxmox
    AllowUsers localadmin
    PasswordAuthentication no
    PubkeyAuthentication yes
    ```
    Puis redémarrez le service ssh avec la commande *systemctl* : `systemctl restart ssh`. S'il y a un problème de redémarrage, affichez le status du serveur ssh avec la commande `systemctl status ssh`. Vous devriez trouver le pourquoi de l'erreur. Si tout va bien déconnectez-vous puis tentez de vous connectez en tant que *root* puis en tant que *localadmin*. Conclusion... 

Si vous souhaitez quelques informations complémentaires sur le durcissement du serveur ssh consultez cet [article](https://www.it-connect.fr/durcissement-de-config-comment-securiser-son-serveur-ssh/) sur le site IT-connect.

### Désactiver le *popup* d'abonnement
Connectez-vous sur le serveur Proxmox puis créez le fichier suivant avec nano : `sudo nano /usr/local/bin/disable-popup-proxmoxve`. Copier-coller le contenu suivant, il s'agit d'un script *bash* :

```console
#!/bin/bash
dpkg -V proxmox-widget-toolkit | grep -q '/proxmoxlib\.js$'
if [ $? -eq 1 ]; then
sed -i '/data.status/{s/\!//;s/active/disable/}' /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js
fi
```

!!!note "Exercice"
    Expliquez le contenu de ce script en quelques lignes...  
    `-`  
    `-`  
    `-`  
    `-`
      
Il faut rendre ce script exécutable. Donnez la commande utilisant *chmod* pour cela : `__________________________________________________` puis exécutez ce script en le préfixant avec `sudo`. Il faudra relancer ce script à chaque mise à jour de Proxmox.

Déconnectez-vous puis reconnectez-vous sur l'interface web de proxmox, normalement le popup d'abonnement a du disparaître.

### Activer la double authentification

Pour renforcer la sécurité vous allez mettre une **authentification à double facteur** : un couple login/mdp et un code TOTP (Time based One Time Password ou en français mot de passe à usage unique basé sur le temps).

!!!warning "Attention à bien vérifier la date et l'heure sur le serveur !"
    Pour que l’authentification TOTP fonctionne il faut que le serveur soit à l'heure ! Vous pouvez vérifier la date et l'heure courante du serveur avec la commande `date`. Le serveur Proxmox utilise le démon **chrony** pour obtenir l'heure sur un serveur NTP (Network Time Protocol) sur internet. Dans une configuration *classique* la récupération de l'heure ne pose pas problème. Cependant au lycée Turgot, le protocole NTP est bloqué par le parefeu sauf vers le serveur NTP **ntp.unice.fr**. Il faut donc adapter la configuration de chrony: éditer son fichier de configuration `/etc/chrony/chrony.conf` puis remplacer le serveur ntp de la ligne `pool 2.debian.pool.ntp.org iburst` par celui indiqué précédemment. Redémarrez le démon chrony avec `systemctl restart chrony` et vérifiez qu'il s'est bien relancer avec `systemctl status chrony`. Vérifiez que la date et l'heure sont correctes.

!!!note "Exercice"
    Se connecter sur le serveur web du ProxMox avec l'utilisateur qui doit activer une authentification TOTP. Cliquez sur *Centre de données* puis *Double facteur*. Cliquez sur le bouton *Ajouter* puis *Code TOTP*. Une nouvelle fenêtre s'ouvre. Vérifiez bien que c'est le bon utilisateur dans la liste déroulante puis saisir une *Description*. Installez sur votre smartphone une application TOTP (google authenticator par exemple mais il en existe d'autre). Scannez le QRCode à l'écran avec votre application TOTP sur le smartphone. Complétez les deux derniers champs sous le QRCode comme indiqué enfin validez avec le bouton *Ajouter*. Déconnectez-vous puis reconnectez-vous avec ce même utilisateur. Conclusion...

### Utiliser la ligne de commande pour réaliser l'administration du Proxmox

L'interface web est certes pratique mais pas toujours très efficace pour réaliser les opérations de gestion du serveur Proxmox. L'interface en ligne de commandes est bien efficace pour cela mais demande un effort supplémentaire d'apprentissage. Vous pouvez visualiser ce [document](https://siocours.lycees.nouvelle-aquitaine.pro/doku.php/reseau/cloud/proxmox/qm?do=export_pdf) réalisé par un enseignant de BTS SIO. Ce [site](https://blog.stephane-robert.info/docs/virtualiser/type1/proxmox/#les-outils-en-ligne-de-commande-de-proxmox) présente aussi des exemples de commandes en ligne pour la gestion d'un Proxmox.

!!!tips "Partie à développer dans votre projet de fin d'études"
    Vous pouvez présenter en fin d'année la gestion d'un serveur Proxmox en utilisant des commandes d'administration !!!

## Conclusion

Vous devez avoir acquis les rudiments du fonctionnement d'un serveur Proxmox mais vous pouvez encore grandement enrichir vos connaissances sur ce logiciel. Par exemple nous ferons dans un autre TP la présentation de la gestion des **conteneurs LXC** avec Proxmox. Mais c'est aussi à vous de vous former sur ce logiciel, soit avec les documentations en ligne, soit avec les multiples sites qui parle de Proxmox...