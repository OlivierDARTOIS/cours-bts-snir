# Fondamentaux de la sécurité réseau

!!! tldr "Objectifs de ce document"

    - définir ce que sont les termes : disponibilité (D), intégrité (I), confidentialité (C) complétés par le couple traçabilité (T) et non-répudiation (N),
    - notions de chiffrement symétrique/asymétrique, fonctions de hachage, certificat
    - présenter le chiffrement TLS
    - s'auto évaluer avec des QCMs
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Titre Pro AIS - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A / Titre Professionnel AIS - Tous les semestres
    - Compétences évaluées dans ce TP
        - C04 - Analyser un système informatique (20%)
        - C05 - Concevoir un système informatique (20%)
        - C06 - Valider un système informatique (50%)
        - C09 - Installer un réseau informatique (10%)
    - Principales connaissances associées
        - Acteurs de l’écosystème réglementaire, référence des bonnes pratiques : ANSSI -> Niveau 2 : C04
        - Niveaux de sécurité attendus par la solution logicielle (chiffrage, protection des communication, politique de mot de passe) -> Niveau 2 : C05
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Sécurisation des réseaux (ACL, mots de passe, pare-feu) -> Niveau 3 : C06
        - Protocoles usuels IPv4, HTTP, HTTPS,TCP/IP, Ethernet, ipV6, DNS, DHCP, SSH -> Niveau 4 : C09
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09

## Introduction

Pour présenter les principes de la **cybersécurité**, nous allons prendre comme exemple le schéma ci-dessous ou deux individus (Alice et Bob) veulent communiquer par le biais d'un canal de communication (sécurisé ou non). Un attaquant (un hacker : Hector) souhaite pouvoir espionner les données échangées, peut être les modifier au passage voire se faire passer pour Alice. Si il n'atteint aucun des objectifs précédents, Hector empêchera la communication en tentant de saturer le canal de communication.

![Schema de principe sur un canal de communication](../img/SchemaAttaqueSimpleCommReseau.svg)

Les communications sans fils sont facilement interceptées et les communications sur Internet empruntent des chemins inconnus par l'intermédiaire de routeurs potentiellement écoutés. Le canal de communication est donc très souvent non sécurisé.

Nous allons introduire trois critères principaux pour sécuriser les échanges:

- La **confidentialité** est le fait que Hector ne puisse pas comprendre les échanges entre Alice et Bob,
- L'**intégrité** est le fait que Hector ne puisse pas modifier les données échangées entre Alice et Bob (ou sinon que l'on puisse détecter une modification),
- La **disponibilité** est le fait qu'Alice et Bob puissent communiquer en toute circonstance (même si Hector essaye de saturer le canal de communication).

On vient de définir ce qu'est le **D.I.C.** ou (**C.I.A.** en anglais : Confidentiality, Integrity, Availability). A ces trois critères de bases vous rajouterez les suivants:

- l'**authentification** (authentication) assure Bob que le message vient bien d'Alice et inversement,
- la **non-répudiation** (non-repudiation) empêche Alice ou Bob de nier avoir envoyé tel ou tel message,
- et éventuellement la **traçabilité** (accounting) qui permet de suivre les actions de Alice ou Bob.

## La confidentialité

La **confidentialité** permet de s'assurer que seules les personnes autorisées peuvent accéder à l'information transmise en empêchant un attaquant, faisant de l'écoute clandestine, de récolter des données (en anglais eavesdropping).

La technique pour garantir la confidentialité des échanges est l'utilisation de **clés** pour le **chiffrement** des données.

Le **chiffrement** des données consiste à transformer les données d'origine en d'autres données (pas forcement de même longueur) à l'aide d'un algorithme de chiffrement qui va utiliser la clé comme paramètre. Ce principe est décrit dans l'image ci-dessous:

![Schema de principe du chiffrement sur un canal de communication](../img/SchemaCleChiffrement.svg)

Le développement des algorithmes de chiffrement est la **cryptographie**. Le déchiffrement d'un message sans en connaître la clé est la **cryptanalyse**.

!!! warning "Remarque importante"
    On parle donc de *chiffrement d'un message* avec une clé et de *déchiffrement* avec une clé. Si on ne possède pas de clé, l'attaquant va tenter de *décrypter* un message sans en connaître la clé. Par contre, la phrase *crypter un message* est **incorrect** même si elle est employée très souvent dans le langage courant. 

### Le chiffrement symétrique

Le **chiffrement symétrique** est le plus simple : l'émetteur et le récepteur partagent le **même secret** : la clé de chiffrement. Cette clé de chiffrement commune peut être par exemple une langue peu employé (exemple des [code talker](https://fr.wikipedia.org/wiki/Code_talker) pendant la seconde guerre mondiale) ou par exemple le nombre de décalage des lettres de l'alphabet dans le code de cesar (voir l'article [chiffrement par substitution](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) ou l'exercice sur le code de cesar sur ce même site).

Dans le schéma précédent cela signifie que la clé de chiffrement KE et de déchiffrement KD sont identiques. Le problème de ce chiffrement est comment transmettre la clé à votre destinataire de manière sûr ! L'algorithme de chiffrement symétrique le plus utilisé (Wifi WPA2 et HTTPS par exemple) est [AES](https://www.utc.fr/~wschon/sr06/txPHP/histoire/histoire.php) (Advanced Encryption Standard) avec des clés symétriques de 256 bits.

!!! abstract "Résumé"
    Les algorithmes de chiffrement symétriques sont plus rapides que les algorithmes de chiffrement asymétriques. Leur sécurité repose sur le transfert de la clé secrète entre les deux appareils.

### Le chiffrement asymétrique

Le **chiffrement asymétrique** utilise deux clés dans le processus de chiffrement : une **clé publique** qui sera connue de tous, et une **clé privé**, gardée secrète le plus souvent par le récepteur des données. La confidentialité des échanges est assurée lorsqu'on chiffre les données avec la clé publique, seul le possesseur de la clé privé pourra déchiffrer le message. Ce principe est présenté dans le schéma ci-dessous:

![Schema de principe du chiffrement asymétrique sur un canal de communication](../img/SchemaCleChiffrementAsymetrique.svg)

L'algorithme de chiffrement asymétrique le plus courant (au moment d'écrire ces lignes) est [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA). Le protocole d'échange de clés [Diffie-Hellman](https://fr.wikipedia.org/wiki/%C3%89change_de_cl%C3%A9s_Diffie-Hellman) utilise un algorithme asymétrique pour générer et partager de manière sécurisée une clé symétrique commune aux deux participants.

!!! abstract "Résumé"
    Les algorithmes asymétriques sont les plus coûteux en ressources pour les calculs et en temps. Ils sont cependant plus sécurisés que les algorithmes symétriques car aucune clé secrète ne doit être transmise. On utilise le plus souvent ces algorithmes comme un moyen d'établir et de communiquer une clé symétrique à chaque nouvelle session (avec la méthode Diffie-Hellman par exemple).

!!! note "Exercice"
    Le chiffrement asymétrique est utilisé par la commande SSH pour réaliser la gestion à distance des machines à travers un canal sécurisé. La plupart du temps vous vous authentifiez avec un couple login/mdp. Mais ce mot de passe peut être découvert par une technique type *force brute*. En résumé il faut supprimer la saisie du mot de passe. Pour cela vous générez une paire de clés (privé et publique sur un ordinateur) puis vous transmettez la clé publique au serveur sur lequel vous voulez vous connecter. A partir de ce moment la connexion vers ce serveur ne vous demandera plus de mot de passe. Si vous n'avez jamais mis en oeuvre cette technique elle est décrite dans le TP d'administration système [partie 2](https://olivierdartois.gitlab.io/cours-bts-snir/TPs/IntroProxmox_02/#comment-se-connecter-sur-un-serveur-sans-taper-de-mot-de-passe) et à faire à titre d'exercice.

## L'intégrité

L’**intégrité** des données échangées désigne le fait qu’elles n’ont pas été modifiées durant le cycle de vie du message. Pour garantir l’intégrité d'un logiciel, d'une archive Zip ou d'une image ISO que vous téléchargez sur Internet, vous trouverez souvent un *code* issu de l’ensemble des données de ce que vous voulez télécharger passé par une fonction de **hachage cryptographique** (en anglais [HMAC](https://fr.wikipedia.org/wiki/HMAC) : Hash Message Authentication Code).

### Fonctions de hachage

Une fonction de **hachage** est une fonction qui, pour une donnée en entrée de longueur variable (qui peut être très longue comme une vidéo ou l'installateur d'un logiciel hors ligne), retourne une valeur de longueur fixe (quelques octets) nommée **condensat** (ou en anglais **digest**).

!!! warning "Attention"
    Vous avez déjà vu les codes [CRC](https://fr.wikipedia.org/wiki/Contr%C3%B4le_de_redondance_cyclique) (Cyclic redundancy Check) qui termine les trames ethernet. Ces CRC utilisent des fonctions de hachage mais **PAS** de hachage cryptographique.

Les [fonctions de hachages cryptographiques](https://fr.wikipedia.org/wiki/Fonction_de_hachage_cryptographique) les plus utilisées sont celles de la famille [SHA2](https://fr.wikipedia.org/wiki/SHA-2) (SHA-256, SHA-512). Les fonctions de hachage MD5, SHA1 sont aujourd'hui (Mai 2024) considérées comme obsolètes.

!!! note "Exercice"
    Complétez le tableau ci-dessous (colonne *Caractéristique*) avec les termes suivants: Déterminisme, Rapidité, Non-réversibilité, Résistance aux collisions, Effet d'avalanche.

|Caractéristique|Définition|
|:---|---:|
||le temps de calcul d'un condensat doit être le plus court possible|
||on ne peut pas reconstruire un message en ne connaissant que son condensat|
||une légère modification du message d'origine entraîne une importante modification du condensat|
||la même valeur de hachage (condensat) sera systématiquement généré pour un même message|
||on ne peut pas trouvé facilement un second message produisant le même condensat|

!!! note "Exercice"
    Calculez le condensat SHA-1 des trois phrases du tableau ci-dessous en utilisant un [site](https://lehollandaisvolant.net/tout/tools/checksum/) de calcul de différents hachés. L'effet d'avalanche est-il vérifié ? `______`.

|Exemple de phrase|Condensat SHA-1 (notez le début seulement)|
|:---|---:|
|J'adore ta montre||
|J'adore ma montre||
|J'adore sa montre||
|j'adore sa montre||

Les fonctions de hachage sont aussi utilisées pour stocker les **mots de passe**. En effet le système d'exploitation ne sauve jamais le vrai mot de passe sur un support de stockage mais le condensat de celui-ci.

Lorsqu'on veut se connecter on saisi le mot de passe, le condensat est calculé et s'il identique à celui stocké, vous avez accès au système.

La fonction de hachage cryptographique utilisé sous GNU/Linux est très souvent [Yescrypt](https://en.wikipedia.org/wiki/Yescrypt) et le fichier qui stocke les condensats est `/etc/shadow`.

### La signature numérique

Comment savoir qu'un document a bien été édité par une personne. Une méthode consiste à utiliser la réversibilité des clés asymétriques. Dans le schéma ci-dessous Alice chiffre un document avec sa clé privé qu'elle seule connaît. Toute personne ayant la clé publique d'Alice peut alors déchiffrer le message et être sûr qu'il provient d'Alice.

![Schema de principe de la signature numérique](../img/SchemaPrincipeSignatureNumerique.svg)

Cependant, chiffrer un gros fichier avec une clé asymétrique est gourmand en temps et en énergie. Il est donc plus économe de le hacher et de ne chiffrer avec la clé privé que le condensat. Bob a ainsi l'assurance que le document reçu n'a pas été modifié entre son émission par Alice et sa réception si le condensat calculé est égale au condensat déchiffré.

!!! note "Exercice"
    Complétez le schéma ci-dessous avec les différents éléments présentés dans la phrase précédente.

![Schema de principe de la signature numérique](../img/SchemaPrincipeSignatureNumeriqueAvecHachage.svg)

## La non-répudiation

La **non-répudiation** assure qu'une action réalisée par une entité ne peut être niée ou ignorée. Cela apporte une preuve irréfutable qui pourra être utilisée a posteriori. L'utilisation de **signatures numériques** peut assurer la non-répudiation. En chiffrant les messages avec une clé privé, l'entité ne peut pas nier avoir signé le message car elle est la seule à posséder cette clé privée. Certains mécanismes de signature numérique utilise l'horodatage dans les données générant la signature numérique. Ainsi même si la clé privé est volée, on peut s'assurer que la signature est bien valide car générée avant le vol.

## L'authentification

L'**authentification** est le processus de vérification de l'identité d'une entité (utilisateur, système, appareil, etc.). Elle permet de s'assurer que, dans la communication, chacun est réellement qui il prétend être. Les mécanismes d'authentification peuvent être séparé en deux familles :

- les authentifications d'utilisateurs pré-enregistrés,
- les authentifications par certificats.

!!! warning "Attention à ne pas confondre identification et authentification"
    L'**identification** consiste à identifier un utilisateur particulier, souvent par le biais d'un **nom d'utilisateur**. L'**authentification** consiste à vérifier l'identité de cet utilisateur, ce qui exige généralement la saisie d'un **mot de passe** par exemple.

### Les authentifications d'utilisateurs pré-enregistrés

Cette famille regroupe l'authentification par nom d'utilisateur et mot de passe ou encore l'authentification biométrique (empreinte digitale, reconnaissance faciale). Elle demande à l'un des partenaires de l'échange (humain ou machine) de connaître les identifiants de l'autre partenaire. Cette solution ne peut pas être utilisée par deux entités qui ne se connaissent pas.

!!! info "L'authentification à plusieurs facteurs (souvent appelée 2FA)"
    Pour renforcer l'authentification, il est fréquent de mettre en place une authentification à plusieurs facteurs (deux voire trois). L'authentification à deux facteurs peut par exemple demander, en plus du mot de passe, la saisie d'une valeur envoyée par SMS.

### L'authentification par certificat

La simple déclaration de son identité ne suffit pas à vous authentifier : Hector peut affirmer être Alice auprès de Bob. Alice, pour s'authentifier, peut envoyer un couple login/mdp chiffré à Bob. Cependant si Hector enregistre la communication entre Alice et Bob, celui-ci peut **rejouer** l'échange login/mdp en affirmant qu'il est Alice. C'est ce qu'on appelle une [Attaque par rejeu](https://en.wikipedia.org/wiki/Replay_attack).

On peut déjouer l'attaque par rejeu en introduisant l'échange d'un nombre aléatoire entre Alice et Bob comme présenté ci-dessous:

![Authentification par signature d'un nombre N](../img/SchemaAuthentificationSignatureNombre.svg)

Mais il reste un problème dans l'échange précédent: comment être sûr que KPUBA est bien la clé publique d'Alice ? En effet il existe une attaque qui permet à Hector de se faire passer pour Alice auprès de Bob et vice-versa. Il s'agit de l'**attaque de l'homme du milieu** (man in the middle, [lien wikipédia](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu)). Son principe est présenté dans le schéma ci-dessous.

!!! note "Complétez le schéma de l'attaque de l'homme du milieu"
    On rappelle que KPRIVA et KPUBA sont les clés privées et publiques d'Alice. KPRIVH et KPUBH celles de Hector.

![Authentification par signature d'un nombre N](../img/SchemaPrincipeAttaqueHommeMilieu.svg)

Pour certifier sa clé publique, il faut faire appel à un **tiers de confiance** qui permet de délivrer des **certificats**. **X509** est le protocole de gestion des certificats le plus utilisé. Un tiers de confiance nommé **autorité de certification** signe numériquement (c'est à dire chiffre avec sa clé privé un **condensat**) le certificat contenant notamment la clé publique et le nom du protagoniste. La clé publique de cette autorité de certification est connue (par exemple les navigateurs web connaissent les clés publiques des principales autorités de certification).

Ce principe est décrit dans les schémas ci-dessous:

![Obtention d'un certificat auprès d'une autorité de certification](../img/SchemaObtentionCertificatAutCert.svg)

!!! note "Complétez le schéma d'authentification avec utilisation d'un certificat"
    En relisant attentivement le paragraphe ci-dessus et le schéma d’obtention d'un certificat complétez le schéma ci-dessous d'authentification d'Alice par Bob avec l'utilisation d'un certificat.

![Authentification avec utilisation d'un certificat](../img/SchemaAuthentificationAvecCertificat.svg)

!!! note "Exercice : Analyse d'un certificat"
    === "Complétez les questions suivantes"
        Connectez-vous sur le site [it-connect.fr](https://www.it-connect.fr/) (site de tutoriels informatiques). Dans la barre d'adresse et suivant votre navigateur repérez comment accéder au certificat du site web (Chrome: item La connexion est sécurisée puis item Certificat). Donnez alors le nom de l’autorité de certification et la fonction de hachage utilisée pour calculer les condensats du certificat et de la clé publique de l’autorité de certification. Donnez l'adresse du site web de l’autorité de certification. Pourquoi ce site est-il intéressant ?
    === "Vos réponses"  
        - autorité de certification :
        - Date de début et de fin de vie du certificat :
        - Fonction de hachage réalisant l'intégrité des données :
        - Adresse du site de l’autorité de certification :
        - Rôle de ce site :

Il est possible que dans des TPs de mise en oeuvre de services (en particulier de serveur WEB) vous utilisiez le site [let's encrypt](https://letsencrypt.org/) comme autorité de certification de votre site web.

## La disponibilité

La **disponibilité** désigne la capacité d'un service, ou d'un système, à être accessible par un utilisateur autorisé quand il le souhaite.

!!! warning "Importance de la disponibilité"
    La disponibilité d'un système d'information est l'un des piliers de son bon fonctionnement. Une attaque rendant impossible l’accès à un service en ligne fourni par une société nuit grandement à l'image de cette dernière.

Il existe différents types d'attaques pour rendre non opérationnel un site web. La plus connue est certainement l'attaque par déni de service distribué (DDOS : Distributed Denial of Service) venant très souvent d'appareils infectés attaquant simultanément un serveur afin de le saturer. Par exemple une attaque à 2.54Tb/s (2540Gb/s) sur des serveurs de Google (voir l'article de [Cloudflare](https://www.cloudflare.com/fr-fr/learning/ddos/famous-ddos-attacks/)).

Les requêtes étant légitimes il est difficile de les refuser. Cependant pour éviter les coupures de service au vu des enjeux financiers il faut mettre en oeuvre (liste non exhaustive):

- la redondance des systèmes,
- la répartition de charges sur plusieurs serveurs,
- le filtrage en amont des requêtes avec des pare-feux avancés.

## La traçabilité

La **traçabilité** en informatique fait référence à la capacité de suivre et d'enregistrer l'**historique des activités** ou des événements sur un système informatique ou dans un processus. Cela signifie généralement enregistrer et suivre les actions effectuées par les utilisateurs ou les processus sur un système, souvent dans le but de garantir la sécurité, l'auditabilité, la conformité réglementaire ou la résolution des problèmes.

La traçabilité peut être mise en œuvre de différentes manières, notamment en enregistrant les journaux d'activité (les fichiers **log** d'un système), en conservant des enregistrements des transactions (suivre les requêtes d'une base de données), en utilisant des identifiants uniques pour suivre les éléments du système, ou en utilisant des techniques de cryptographie pour assurer l'intégrité des données.

Dans le contexte de la sécurité informatique, la traçabilité peut aider à identifier les activités malveillantes, à retracer l'origine des incidents de sécurité et à prendre des mesures correctives appropriées. En matière de conformité réglementaire, la traçabilité peut être nécessaire pour démontrer que des contrôles adéquats sont en place pour protéger les données sensibles et assurer la confidentialité.

La traçabilité sera abordée dans des TPs qui suivront les recommandations de L'ANSSI présentés dans le document suivant: [lien](https://cyber.gouv.fr/sites/default/files/2022/01/anssi-guide-recommandations_securite_architecture_systeme_journalisation.pdf).

## Exemple du protocole TLS

Le protocole **TLS** (Transport Layer Security, sécurité de la couche de transport) est un protocole cryptographique permettant de sécuriser les communications sur un réseau informatique (ici
*confidentialité* via une clé symétrique AES-128, *intégrité* via une fonction de hachage SHA-256 et
*authentification* via un certificat X509 et des échanges par clés asymétriques Diffie Hellman), au dessus de la couche transport TCP et en-dessous de la couche application. Son utilisation la plus
connue est le protocole HTTPS qui consiste en une version sécurisée du protocole HTTP.

!!! warning "Qu'est-ce que SSL/TLS ?"
    On utilise parfois le terme "SSL/TLS". SSL désigne l’ancêtre du protocole TLS et toutes ses versions sont obsolètes depuis 2015. Il convient donc d’utiliser uniquement le terme TLS dans la conception d’un système d’information et de ne pas utiliser abusivement le terme "SSL/TLS".

## QCM d'autopositionnement

!!! note "Quelle est la bonne définition du terme chiffrement ?"

    - [ ] Transformation à l'aide d'une clé d'un message clair en un message incompréhensible (dit message chiffré) pour celui qui ne dispose pas de la clé de déchiffrement
    - [ ] Action réalisée sur un texte lorsqu'on remplace chaque lettre par la lettre décalée de 3 positions (A devient D, etc.)

!!! note "Quelles sont les principales limites de la cryptographie symétrique ?"

    - [ ] La transmission de la clé doit être confidentielle
    - [ ] Elle est difficile à implémenter
    - [ ] La distribution des clés ne peut être utilisée à large échelle
    - [ ] Le chiffrement nécessite un nombre très important de calculs qui ralentit les opérations

!!! note "Remplissez le texte à trou en choisissant le bon mot"
    Dans le procédé de cryptographie `__________________`, l'expéditeur et le destinataire se partagent une clé qui va servir au chiffrement et au déchiffrement du message.

!!! note "Quel outil cryptographique utilise-t-on pour s'assurer que la bi-clé utilisée pour signer un document appartient bien à notre correspondant ?"

    - [ ] La signature numérique
    - [ ] Le chiffrement asymétrique
    - [ ] Le certificat électronique

!!! note "Comment se nomme l'attaque qui consiste à intercepter et remplacer une clé publique échangée entre deux interlocuteurs ?"

    - [ ] L'attaque par rebond
    - [ ] L'attaque par l'homme du milieu
    - [ ] L'attaque par force brute
    - [ ] L'attaque par détournement de session

!!! note "À quels objectifs les mécanismes de cryptographie permettent-ils de répondre ? (plusieurs réponses possibles)"

    - [ ] Non-répudiation
    - [ ] Authenticité
    - [ ] Confidentialité
    - [ ] Intégrité

!!! note "Quel(s) problème(s) la signature numérique et les certificats électroniques permettent-ils de résoudre ?"

    - [ ] La distribution des clés
    - [ ] La confidentialité des données
    - [ ] L'intégrité d'une donnée et la preuve de sa provenance

!!! note "Lorsqu'on parle de chiffrement, le message à cacher est par convention appelé *message en clair* et le résultat du chiffrement *message chiffré* ?"

    - [ ] Faux
    - [ ] Vrai

!!! note "Quel terme est utilisé pour décrire une action visant à retrouver un message en clair sans posséder la clé de déchiffrement ?"

    - [ ] Décoder
    - [ ] Déchiffrer
    - [ ] Décrypter

!!! note "De quel principe découlent l'*imputabilité* et la *traçabilité* ?"

    - [ ] Principe d'autorisation
    - [ ] Principe d'authentification

!!! note "Associez chaque terme à sa définition"

    |Terme|||Définition|
    |:---|:---:|:---:|:---|
    |Identification|[]|[]|Prouver son identité|
    |Authentification|[]|[]|Relier de manière certaine une action à son auteur|
    |Imputabilité|[]|[]|Conserver l'historique d'une action|
    |Traçabilité|[]|[]|Décliner son identité|

!!! note "Mon site internet a été modifié, quels impacts peut avoir cette situation sur mon entreprise ?"

    - [ ] Atteinte à la disponibilité
    - [ ] Atteinte à l'intégrité
    - [ ] Atteinte à la confidentialité

!!! note "Que signifie l'acronyme *DIC* en sécurité de l'information ?"

    - [ ] Donnée, Intégrité, Confidentialité
    - [ ] Disponibilité, Intégration, Confidentialité
    - [ ] Disponibilité, Intégrité, Confidentialité
    - [ ] Disponibilité, Intégrité, Confiance

!!! note "Qu'est-ce que la perte d'*intégrité* sur les données ?"

    - [ ] L'indisponibilité d'une donnée
    - [ ] La modification non-autorisée d'une donnée
    - [ ] La perte de confidentialité d'une donnée
    - [ ] La modification d'une donnée

!!! note "Quel est le risque pour une entreprise victime d'une *attaque de déni de service* (DDoS) ?"

    - [ ] La perte de la disponibilité des données
    - [ ] La perte de la confidentialité des données
    - [ ] La perte de l'intégrité des données

## QCM d'approfondissement

!!! question "Chiffrement symétrique vs asymétrique. Parmi les affirmations suivantes, laquelle est fausse ?"

    - [ ] L’opération de chiffrement asymétrique est beaucoup plus lente que pour le chiffrement symétrique, à un niveau de sécurité égal
    - [ ] Le chiffrement symétrique pose le problème de la distribution de clés symétriques qui peut être résolu par le partage de clés Diffie-Hellman
    - [ ] Pour un même niveau de sécurité, la taille des clés pour le chiffrement symétrique est plus petite que pour le chiffrement asymétrique
    - [ ] Le chiffrement asymétrique, avec la clé publique du destinataire, permet d’assurer la confidentialité du message transmis 
    - [ ] Le chiffrement symétrique d’un message avec la clé (symétrique) permet d’assurer la confidentialité et l’intégrité du message transmis
    - [ ] Le HMAC permet de garantir l’intégrité d’un message
    - [ ] L’algorithme de chiffrement RSA est de type chiffrement asymétrique

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est **fausse**. La réponse à cocher est donc la 5. Le chiffrement symétrique n’assure que la confidentialité du message. Pour assurer l’intégrité du message, il est nécessaire de faire appel au mécanisme HMAC qui assure en plus de l’intégrité, l’authenticité de l’origine du message.

!!! question "Signature électronique vs HMAC. Parmi les affirmations suivantes, laquelle est fausse ?"

    Un message est transmis au destinataire Bob couplé à une signature électronique ou un HMAC

    - [ ] Une signature électronique peut être considérée comme un contrôle d’intégrité cryptographique du fait que ces mécanismes utilisent des clés cryptographiques secrètes
    - [ ] Un HMAC peut être considéré comme un contrôle d’intégrité cryptographique du fait que ces mécanismes utilisent des clés cryptographiques secrètes
    - [ ] Une signature électronique, tout comme un HMAC, permet au destinataire de vérifier l’intégrité du message et l’authenticité de l’origine du message
    - [ ] Une signature électronique peut, en cas de litige, protéger le destinataire en lui permettant de prouver que le message a bien été émis par l’émetteur. Elle garantit la propriété de non répudiation
    - [ ] Un HMAC peut, en cas de litige, protéger le destinataire en lui permettant de prouver que le message a bien été émis par l’émetteur. Elle garantit la propriété de non répudiation
    - [ ] Une signature électronique ou un HMAC fait usage de la fonction de hachage pour réduire la taille des informations traitées

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est **fausse**. La réponse à cocher est donc la 5.  
    Un HMAC ou une signature permet de détecter une perte d’intégrité du message sur lequel porte la signature ou le HMAC. A ce titre, il s’agit d’un contrôle d’intégrité. Comme en plus il fait appel à un élément cryptographique – soit la clé symétrique, soit la clé privée – ces deux mécanismes peuvent donc être appelés *contrôle d’intégrité cryptographique*.  
    Une signature est générée à l’aide de la clé privée de l’émetteur. Cette signature est donc individuelle et ne peut pas être usurpée par une autre entité. Le destinataire peut donc se servir de la signature comme preuve d’émission du message. En général pour le service de non répudiation, le message est horodaté et l’ensemble – message et horodateur - sont signés.  
    Un HMAC est, lui, généré à l’aide de la clé symétrique. Cette clé est connue de l’émetteur et du récepteur. Il n’est donc pas possible a posteriori d’identifier qui des deux a émis le HMAC. Le service de non répudiation n’est donc pas assuré.

!!! question "Fonction de hachage. Trouvez la mauvaise réponse"

    Une fonction de hachage H, de type SHA 2, appliquée sur un message M :

    - [ ] fournit un résultat de taille constante, par exemple de 224 bits, appelé "hash" ou "empreinte numérique"
    - [ ] fournit une empreinte de M de telle sorte que la modification d’un seul bit de M impacte de façon très importante le résultat de l’empreinte
    - [ ] doit être résistante aux collisions, c’est-à-dire il doit être calculatoirement difficile de trouver un autre message M1 tel que H(M1)=H(M)
    - [ ] vient en soutien aux mécanismes de génération de signature électronique ou de HMAC, l’objectif étant de réduire la taille des éléments d’informations traités
    - [ ] fournit un résultat qui permet de détecter les erreurs produites sur le message et de les corriger si le nombre d’erreurs est limité

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est **fausse**. La réponse à cocher est donc la 5.  
    Dans le cas du calcul de deux Hash, l’un sur le message M, l’autre sur le message M modifié d’un bit, les valeurs de Hash résultantes doivent être très différentes ; si les valeurs de Hash étaient égales, cela viendrait contredire la propriété de bonne résistance aux collisions ; si la différence entre Hash porte sur au moins la moitié des bits, on dit que la fonction H satisfait la propriété *effet avalanche*.  
    Les mécanismes HMAC et signature électronique font usage des fonctions de hachage pour travailler sur un élément d’information de taille plus petite que le message lui-même et qui correspond fidèlement au contenu du message ; cela leur permet de détecter des problèmes d’intégrité du message et de garantir l’authenticité du message.  
    Par contre, les fonctions de hachage ne sont pas des codes correcteurs d’erreur. Elles ne permettent que de détecter la perte d’intégrité d’un message, et n’assurent pas la correction des erreurs, aussi minimes soient-elles.

!!! question "Certificat électronique. Parmi les affirmations suivantes, laquelle est fausse ?"

    - [ ] Le certificat électronique d’Alice a pour finalité de garantir qu’Alice possède la clé publique publiée dans le certificat électronique
    - [ ] Le certificat électronique d’Alice peut être prouvé fiable et authentique du fait que le certificat est signé par Alice (avec la clé privée d’Alice)
    - [ ] Un certificat électronique est propre à la cryptographie asymétrique et ne s’applique pas à la cryptographie symétrique
    - [ ] Les certificats électroniques reposent sur la confiance qu’on accorde à des autorités de certification. Les certificats électroniques émis par ces autorités héritent alors du niveau de confiance qu’on place dans ces autorités. Si j’ai un très haut niveau de confiance dans une autorité, je vais alors avoir un très haut niveau de confiance dans le fait que la clé publique publiée dans le certificat est associée à l’entité détentrice

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est fausse. La réponse à cocher est donc la 2.  
    Un certificat électronique permet de publier le lien entre une clé publique et le propriétaire de cette clé (Alice par exemple). Ce lien est d’autant plus fort que l’autorité qui a émis le certificat est réputée de confiance. Chaque personne dispose de ses propres listes d’autorités de confiance qui peuvent être différentes des autres personnes, suivant le pays de résidence, l’entreprise dans laquelle elle travaille, ses convictions personnelles...  
    L’autorité émettrice du certificat d’Alice prouve qu’elle l’a émis en apposant, dans le certificat, sa signature électronique, procédé qui exige qu’elle fasse usage de sa clé privée. L’ajout de sa signature est le seul moyen de garantir l’authenticité du certificat. En aucun cas, il s’agit de la signature d’Alice. Sinon, cela signifierait qu’Alice est déjà reconnue comme autorité de confiance.

!!! question "Parmi les affirmations suivantes, laquelle est vraie ?"

    - [ ] Les échanges Diffie-Hellman ont pour objectif de convenir du groupe Diffie-Hellman à utiliser
    - [ ] La génération d’une signature électronique repose sur une fonction de hachage HMAC
    - [ ] A niveau de sécurité égal, les clés symétriques sont de tailles plus importantes que les clés asymétriques
    - [ ] Une même fonction de hachage appliquée à des messages de contenus différents aboutit à des Hash de taille différente
    - [ ] La gestion des clés publiques d’un ensemble d’entités peut être déléguée à une autorité de certification de confiance pour peu que cette autorité soit considérée de confiance

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est **vraie**. La réponse à cocher est donc la 5.  
    Les échanges Diffie-Hellman ont pour objectif que les deux entités communicantes conviennent d’une clé DH secrète commune. Le groupe DH doit être convenu préalablement à ces échanges, sinon les clés publiques DH échangées ne seront pas homogènes et le calcul de la clé DH n’aboutira pas avec succès.  
    Un HMAC n’est pas une fonction de hachage mais un procédé faisant appel à une fonction de hachage et à une clé symétrique pour, couplé à un message, garantir que le message est intègre et authentique. Un HMAC est l’équivalent de la signature électronique qui fait elle appel à des clés asymétriques.  
    Les clés symétriques sont de taille plus faible que les clés asymétriques. En effet, excepté certaines combinaisons à proscrire, une clé symétrique peut prendre n’importe quelle valeur ; elle est donc générée de façon totalement aléatoire. Par contre, les clés privées/publiques (asymétriques) doivent répondre à certaines propriétés mathématiques ; ainsi de nombreuses combinaisons binaires ne sont pas éligibles ; si un attaquant voulait craquer une clé privée, il mettrait moins d’effort à craquer une clé privée qu’une clé symétrique de même taille car il éviterait de tester de nombreuses combinaisons de clés asymétriques. Ainsi, pour un même niveau de sécurité (un même effort fourni pour l’attaquant), les clés asymétriques sont de taille plus importante que les clés symétriques. Pour exemple, à une taille de clé symétrique de 128 bits, correspond une clé de type RSA est de taille 3072 bits pour un même niveau de sécurité.  
    Une fonction de hachage appliquée à n’importe quel contenu en entrée fournit toujours en sortie un Hash de même taille. Le contenu du Hash change, mais la taille reste la même.  
    Les certificats électroniques permettent de déléguer la gestion des clés publiques à un tiers de confiance appelée autorité de certification.

!!! question "Établissement de clés Diffie-Hellman. Parmi les affirmations suivantes, laquelle est fausse ?"

    - [ ] L’établissement de clés DH réalisé entre deux entités ne suppose aucune connaissance particulière d’une entité vis-à-vis de l’autre, mais seulement de se mettre d’accord sur le groupe DH
    - [ ] Les échanges DH sont vulnérables à l’attaque Man in the Middle ou homme au milieu, c’est-à-dire qu’en ayant connaissance des valeurs publiques DH des deux interlocuteurs, il est possible de découvrir la valeur du secret DH
    - [ ] La sécurité des échanges DH repose sur le problème du logarithme discret qui est réputé calculatoirement difficile pour certains groupes DH
    - [ ] L’établissement de clés DH, malgré ses vulnérabilités, est très utilisé dans les protocoles de sécurité nécessitant d’établir une clé symétrique partagée. Ces protocoles doivent prévoir de convenir du groupe DH à utiliser et de s’échanger des valeurs publiques DH

??? note "Réponse"

    Attention on vous demande d'identifier l'affirmation qui est **fausse**. La réponse à cocher est donc la 2.  
    Les entités communicantes Alice et Bob qui veulent établir une clé de session (symétrique) à la manière des échanges DH doivent convenir du groupe DH à utiliser dans leurs échanges, pour paramétrer leurs algorithmes avec les bonnes valeurs de P et G, et ainsi générer des valeurs publiques A = Ga mod P et B = Gb mod P sur des bases homogènes.  
    Ces échanges sont vulnérables à l’attaque Man in the Middle du fait que l’attaquant peut générer ses propres valeurs publiques et privées DH et les transmettre à Alice et Bob en usurpant respectivement leurs identités.  
    Mais sous l’hypothèse du problème du logarithme discret (ce qui suppose de choisir un groupe DH correctement), la connaissance des valeurs publiques d’Alice et Bob ne permettent pas de craquer le secret DH que ces deux entités vont calculer chacune de leur côté sur la base de leur secret DH qu’elles seules connaissent.  
    Il ne faut donc pas confondre l’attaque Man in the Middle et l’attaque visant à craquer la clé DH.

## Conclusion

Si vous avez répondu aux différents QCM avec un nombre de réponses justes proches des 80% vous avez alors acquis de bonnes connaissances sur les principes de sécurité mis en oeuvre dans les réseaux actuels. Cependant l'arrivée d'ordinateurs dit *quantique* pourrait changer la donne à très court terme et sans doute mener à des nouveaux algorithmes de chiffrement.

## Sources pour la construction de ce document

- Le texte de ce document vient en grande partie de l'article sur les *Fondamentaux de la sécurité réseau* paru dans la [revue 3EI n°111](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/pedagogiques/16723/16723-la-revue-3ei-n111-janvier-2024-ensps.pdf),
- le site [SecNumAcademie](https://secnumacademie.gouv.fr/) de l'ANSSI,
- le MOOC sur France Université Numérique ([FUN](https://www.fun-mooc.fr/fr/)) : Sécurité des réseaux informatiques pour la partie QCM d'approfondissement,
- le site [PIX](https://pix.fr/) et en particulier le module *Protection et sécurité*.
