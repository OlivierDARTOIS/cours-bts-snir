# Architecture d'un réseau sécurisé

!!! tldr "Objectifs de ce document"

    - identifier les zones de sécurité dans un système d'information,
    - comprendre la nécessité des pare-feux dans une architecture,
    - comprendre le besoin de traiter les flux entrants et sortants au travers de zones démilitarisées et l'intérêt d'utiliser des équipements dédiés,
    - s'auto évaluer avec des QCMs.
    ---
    Enseignant : Olivier DARTOIS - BTS CIEL option A - Informatique et Réseau - Titre Pro AIS - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - année : CIEL1 option A / Titre Professionnel AIS - Tous les semestres
    - Compétences évaluées dans ce TP
        - C04 - Analyser un système informatique (20%)
        - C05 - Concevoir un système informatique (20%)
        - C06 - Valider un système informatique (50%)
        - C09 - Installer un réseau informatique (10%)
    - Principales connaissances associées
        - Acteurs de l’écosystème réglementaire, référence des bonnes pratiques : ANSSI -> Niveau 2 : C04
        - Niveaux de sécurité attendus par la solution logicielle (chiffrage, protection des communication, politique de mot de passe) -> Niveau 2 : C05
        - Réseaux informatiques (protocoles, équipements et outils usuels et industriels) -> Niveau 4 : C06
        - Sécurisation des réseaux (ACL, mots de passe, pare-feu) -> Niveau 3 : C06
        - Protocoles usuels IPv4, HTTP, HTTPS,TCP/IP, Ethernet, ipV6, DNS, DHCP, SSH -> Niveau 4 : C09
        - Systèmes d’exploitations (Windows, UNIX, virtualisations) -> Niveau 3 : C09

## Introduction

Ce document présente la **conception d'une architecture de réseau sécurisée** et la méthodologie associée qui vous permettra d'adapté le procédé à votre cas d'utilisation (par exemple dans l'entreprise qui vous accueille). Il faudra tout d'abord identifier les **zones de sécurité** dans un S.I., puis l'utilité des **parefeux** (firewalls) dans une architecture, le besoin de traiter les flux entrants et sortants au travers de zones démilitarisées (DMZ: Demilitarized Zones) et l'intérêts d'utiliser des équipements dédiés. Cette analyse se fera sur un cas concret d'une enterprise dont le schéma réseau de base est présenté ci-dessous.

![Schema réseau d'une entreprise industrielle](../img/SchemaReseauEntrepriseAPlat.svg)

Le réseau de l'entreprise utilise dans un premier temps un adressage IP *à plat*. C'est à dire que tous les équipements réseaux sont dans un seul réseau de classe A par exemple (10.0.0.0/8). Dans ce réseau tous les serveurs et le routeur ont des adresses IP fixes, le reste des matériels est configuré par DHCP.

Le réseau de production industriel comprend des automates programmables industriels, des caméras positionnées en des points stratégiques de la production, un serveur de supervision de la chaîne de production qui contrôle les automates, et accède aux images des caméras et un PC de supervision depuis lequel un salarié peut contrôler, via le serveur, le bon fonctionnement de l’usine de fabrication.

Le réseau bureautique, quant à lui, comprend des postes internes à l'entreprise et des postes
externes de télétravail depuis lesquels les salariés peuvent accéder aux ressources de
l’entreprise: un serveur de fichiers, un serveur de messagerie et un serveur web qui permet entre autres à l’entreprise de communiquer sur ses activités auprès du grand public.

Les bureaux sont équipés de téléphones IP, ce qui nécessite un serveur de téléphonie
sur IP pour gérer les communications IP. Possibilité est fournie aux internautes de se
connecter au serveur web présentant l’activité générale de l’entreprise, à des partenaires
industriels d’accéder à des documents partagés et à des visiteurs dans l’entreprise d’accéder à
Internet via un point d’accès Wi-Fi.

Le système d'information s’appuie sur des services réseaux. Un annuaire Active Directory
(AD) qui permet l’identification et l’authentification du personnel et des équipements de
l’entreprise est rendu accessible sur le réseau interne. Un serveur DHCP permet de gérer
dynamiquement les configurations IP

L’objectif de l’entreprise est donc de concevoir une architecture de sécurité qui permette de
répondre aux besoins de l’entreprise qui sont exprimés sous la forme d’une politique de
sécurité. La politique définit les règles qui régissent l’accès des utilisateurs ou catégories
d’utilisateurs à des services ou équipements et les interactions entre machine et services. Elle
doit tenir compte des besoins opérationnels, des utilisateurs, des administrateurs et de la
direction, et des risques que la politique fait peser sur le système d’information. L’architecture
a donc pour objectif la bonne mise en application des principes énoncés dans la politique de
sécurité. La sécurité obtenue est une affaire de compromis entre les besoins de sécurité et les
besoins métier, les menaces et les risques associés à l'entreprise, ainsi que les coûts de mise en
œuvre.

!!! note "Exercice"
    Sur le schéma réseau de l'entreprise et en relisant le texte de présentation de celle-ci:

    - identifiez le réseau bureautique et le réseau de production,
    - dessinez les flux de données principaux en interne et en externe. On suppose que tous les matériels sont correctement connectés entre eux par des commutateurs.

## Identifier les zones de sécurité dans un SI

Vous allez analyser le réseau de l'entreprise suivant quatre critères définit ci-après.

### Définir les zones par **exposition aux risques d'intrusion**

Quand on veut définir une architecture sécurisée dans son entreprise, la première des choses est d’identifier les équipements, les applications et les services à regrouper afin d’en assurer une sécurité homogène et de réduire la surface d’attaques du système d'information. Pour regrouper ces éléments, il faut identifier dans un premier temps leur degré d’exposition au monde extérieur : plus l’exposition est grande, plus le risque de compromission par des attaques extérieures est important. 

!!! note "Exercice"
    === "Analyse des zones d'exposition"
        Entourez en **rouge** sur le schéma réseau donné ci-dessous les zones d'expositions aux risques d'intrusion comme définit dans le paragraphe précédent.
    === "Réponse"  
        Dans le cas de notre usine, le poste du télétravailleur, le serveur web, le routeur d’accès au réseau et le réseau Wi-Fi pour les visiteurs sont particulièrement à risque pour subir des attaques, être compromis et éventuellement servir de rebond pour réaliser une attaque encore plus intrusive dans le SI. C’est le cas du serveur web qui peut souffrir de défauts de programmation et servir de porte d’entrée vers des ressources plus sensibles de l’entreprise.

### Définir les zones par **niveau de criticité**

Il faut également prendre en compte le niveau de criticité des équipements, applications et
services vis-à-vis des **besoins opérationnels** de l’entreprise. Quels matériels sont critiques, une défaillance pouvant entraîner une perte d’activités notable pour l’entreprise et conduire à des pertes coûteuses.

!!! note "Exercice"
    === "Analyse des zones critiques"
        Entourez en **bleu** sur le schéma réseau donné ci-dessous les zones critiques comme définit dans le paragraphe précédent.
    === "Réponse"  
        Les serveurs de messagerie et serveur de fichiers sont critiques, une défaillance pouvant entraîner une perte d’activités notable pour l’entreprise. Le réseau de production est aussi critique du fait qu’une attaque pourrait stopper l’activité de production et donc conduire à des pertes financières importantes. 

### Définir les zones par **niveau de confidentialité**

Le **niveau de confidentialité** des données hébergées est aussi à prendre en compte. Déterminez dans cette entreprise le ou les serveur(s) qui doivent héberger des données confidentielles.

!!! note "Exercice"
    === "Analyse des zones à haut niveau de confidentialité"
        Entourez en **vert** sur le schéma réseau donné ci-dessous les zones qui doivent avoir un haut niveau de confidentialité.
    === "Réponse"
        L’usine peut héberger dans son serveur de fichiers des secrets industriels sur certains procédés de production par exemple. Leur divulgation à ses concurrents peut la rendre moins compétitive et la conduire à la faillite. La zone qui comprend le serveur de fichiers doit donc être extrêmement bien protégée.

### Définir les zones par **catégories d'utilisateurs**

Une analyse peut également être menée sur **le type d’utilisateurs** qui accèdent aux ressources
de l’entreprise et à qui on attribue une confiance plus ou moins élevée. Dans l'exemple vous pouvez identifier 5 zones.

!!! note "Exercice"
    === "Analyse des zones à haut niveau de confidentialité"
        Entourez en **noir** sur le schéma réseau donné ci-dessous les cinq zones qui présente les différents types d'utilisateurs.
    === "Réponse"
        3 zones de confiance élevée depuis lesquelles les employés (considérés de confiance) accèdent aux ressources - leurs postes internes, les postes externes de télétravail et le PC de supervision de la production, 1 zone de confiance intermédiaire pour les visiteurs dans l’entreprise et une zone Internet où la confiance est inexistante. L’isolation de ces différentes zones est importante afin d’éviter qu’un utilisateur ne puisse, par rebond, augmenter abusivement ses droits d’accès et bénéficier de services qui lui sont interdits. 

![Schema réseau d'une entreprise industrielle](../img/SchemaReseauEntrepriseAPlat.svg)

### Identification des zones de sécurité

La méthodologie que vous venez de mettre en oeuvre permet d'identifier des zones de sécurité différentes. Vous allez compléter le schéma réseau ci-dessous en identifiants les zones présentés ci-après (les couleurs données ci-dessous n'ont pas de rapport avec les couleurs des différentes zones définies précédemment) :

- Les zones rouges sont des zones à risques élevés pouvant conduire à des pertes financières très importantes pour l’entreprise en cas de compromissions,
- Les zones orange comprenant les services internet et le réseau Wi-Fi sont soumis à un niveau d’attaques élevé mais leurs compromissions n’engendrent pas de pertes importantes,
- Les zones vert foncé sont des zones accédées par des utilisateurs de confiance et dont la compromission engendre une perte d’activités certes, mais moins onéreuse qu’une perte de production ou de secrets industriels,
- La zone vert clair correspond aux postes des télétravailleurs dont la sécurité est relativement maîtrisée par l'entreprise.

![Schema réseau d'une entreprise industrielle](../img/SchemaReseauEntrepriseAPlat.svg)

La difficulté de définir ces zones de sécurité est de trouver le bon niveau de granularité. En effet, un nombre de zones trop important implique une complexité d’administration, car les flux inter zone à contrôler sont d’autant plus nombreux ; c’est pourquoi, dans le cas de l’usine, les serveurs de messagerie et de fichiers sont regroupés dans la même zone alors que le serveur de fichiers abrite des données plus sensibles que le serveur de messagerie.

A contrario, définir un petit nombre de zones permet à un attaquant ayant compromis un des équipements de la zone de contaminer toute la zone sans rencontrer de barrières de sécurité. Maintenant que ces zones de sécurité sont identifiées, il est nécessaire d’isoler ces zones les unes des autres, de créer des barrières de sécurité, afin de limiter les risques de propagation d’une attaque, de confiner un attaquant à la seule zone compromise, et de protéger ainsi le reste du système d’information. Ainsi chacune de ces zones peut interagir avec d’autres zones au travers de **parefeux** (firewalls) qui sont chargés de filtrer les flux.

## Ajouter les parefeux dans le S.I.

Il est préconisé l'utilisation de deux parefeux dans une architecture sécurisée :

- Un parefeu dit **externe** dont le rôle est principalement de filtrer le traffic internet entrant. Il doit laisser passer le traffic légitime: les télétravailleurs vers le réseau interne de l'entreprise, les clients vers le serveur web et bloquer le traffic illégitime: accés non autorisé, attaque type déni de service distribué (DDOS), scan de ports, etc...,
- Un parefeu dit **interne** dont le rôle est principalement d'activer/désactiver le routage entre les zones, de filtrer les flux internes et externes. Les objectifs de ce parefeu sont:
    - bloquer les accès illégitimes venant du réseau interne,
    - isoler les différentes zones si elles n'ont pas besoin de communiquer entre-elles,
    - de ralentir la progression d'une attaque en confinant le hacker dans une zone,
    - de réaliser une deuxième barrière contre les attaques externes en complément du parefeu externe.

!!! note "Exercice"
    Le schéma réseau ci-dessous reprend tous les éléments définis précédemment: les différentes zones de sécurité (zones rouge, orange, vert foncé et vert clair), les parefeux externe et interne. Vous devez rajouter sur celui-ci les différents flux entre zones.

![Schema réseau avec deux parefeux](../img/SchemaReseauEntrepriseAvecParefeux.svg)

!!! danger "Et si on met un seul parefeu..."
    Pour des raisons de coûts, en particulier pour de petites entreprises, il est fréquent que les fonctions de parefeu interne et externe soient fusionnées dans un seul et unique parefeu, ce qui a pour effet d’amoindrir la sécurité et d’augmenter les risques d’intrusion du fait que la protection vis-à-vis d’Internet ne repose plus que sur un seul firewall. 

!!! warning "Bonne pratique"
    Un des principes fondamentaux dans la conception d’une architecture de sécurité est de ne pas faire reposer la sécurité sur un seul équipement. L’objectif est d’éviter le risque qu’en cas de compromission de l’équipement, tout le SI soit compromis. C’est un des principes de la **défense en profondeur**. Les bonnes pratiques sont donc de différencier le firewall externe du firewall interne, de ne pas mutualiser des fonctions de sécurité différentes - VPN, proxy, filtrage, Système de Détection d’Intrusions (IDS) - sur un même équipement dans le but de réduire leur surface d’attaque et enfin d’utiliser **des technologies et des marques d’équipements différentes**. Il faut en effet éviter qu’une vulnérabilité connue d’un constructeur ne puisse être utilisée plusieurs fois dans un SI de sorte à déjouer par exemple le double filtrage effectué par les deux firewalls. S’il n’est pas possible, pour des raisons de coûts par exemple, de dédier un équipement physique à une fonction de sécurité, il est possible de **mutualiser** l’équipement en assurant de *l’étanchéité* entre les fonctions de sécurité par des **mécanismes de virtualisation** (encore une fois pas les parefeux selon l'ANSSI).

Une autre solution moins onéreuse mais moins performante en terme de filtrage est l'utilisation des réseaux virtuels (VLAN) afin de séparer les différentes zones de sécurité.

!!! note "Rappel: qu'est-ce qu'un VLAN"
    Un VLAN, pour Virtual LAN, ou réseau local virtuel, c’est un moyen de segmenter de manière logique un réseau Ethernet. Un commutateur peut créer virtuellement des sous-réseaux étanches, qui ne peuvent pas communiquer entre eux. Pour cela, il colle une étiquette (ou un tag) avec un numéro sur certains ports. Ainsi, si les interfaces 1 et 2 sont étiquetées "VLAN 21", et les interfaces 3 et 4 "VLAN 42", pas moyen de communiquer entre l’hôte du port 1 et celui du port 3, pas plus qu’entre celui du port 2 et celui du port 4 ! Cette technique permet d’empêcher que dans un LAN, des machines ne communiquent avec d’autres alors qu’elles ne devraient rien avoir à se dire. 

Il faudra alors correctement configurer le commutateur pour qu'il réalise le routage interVLANs et mettre en place les ACLs (Access Control List). Un exemple utilisant les VLANs est présenté dans le schéma ci-dessous:

![Schema réseau avec un parefeu et les VLANs](../img/SchemaReseauEntrepriseAvecUnParefeuEtVLAN.svg)

## Ajouter les zones démilitarisées

L'ANSSI donne la définition suivante d'une zone démilitarisée : en informatique, le concept militaire de zone démilitarisée (DeMilitarized Zone) est régulièrement réutilisé pour désigner un sous-réseau (concrètement, quelques équipements) séparant deux zones de confiance hétérogène notamment grâce à des pare-feux réalisant un filtrage périmétrique de part et d’autre.

Concrètement, la DMZ disposant d’un filtrage périmétrique, d’une part avec Internet et d’autre part avec le SI interne de l’entité, doit également intégrer, autant que nécessaire, des relais applicatifs implémentant des fonctions de sécurité (ex. : serveur mandataire – proxy – pour les accès Web, résolveur DNS pour les requêtes de noms DNS publics).

De plus, la DMZ est ici considérée comme une zone neutre et perdable. En effet, sa sensibilité n’est pas nulle (des données du SI de l’entité peuvent y être exposées ou au moins y transiter) mais
une attaque en intégrité ou en confidentialité sur ses composants ne doit pas remettre en cause de
manière irréversible et durable le bon fonctionnement du SI de l’entité.

Dans le cas de notre exemple, nous allons créer 3 DMZs:

- une DMZ pour le traffic entrant qui met en oeuvre un *reverse proxy* pour filtrer les demandes vers le site web de l'entreprise,
- une DMZ pour le traffic sortant mettant en oeuvre un *proxy web* pour filtrer les demandes d’accès à Internet par les employés internes de l'entreprise et le reseau Wifi visiteurs de l'entreprise,
- une DMZ VPN dédiés aux accès des télétravailleurs (expliqué plus loin dans ce document).

Vous remarquerez que les *proxy* réalisent ce qu'on appelle une **rupture protocolaire**, c'est-à-dire que les flux sont *déconstruits* en entrée et *reconstruits* en sortie afin de les filtrer. L'analyse des flux peut se faire à différents niveaux de la pile OSI.

Le but de ces DMZs est une fois de plus d'isoler les différents flux afin de limiter les attaques et isoler les différentes zones entre elles.

!!! note "Exercice"
    Complétez le schéma réseau ci-dessous en rajoutant les 3 zones DMZ ainsi que les flux qui leur sont associés.

![Schema réseau avec un parefeu interne, un parefeu externe et les DMZs](../img/SchemaReseauEntrepriseAvecParefeuxEtDMZ.svg)

## Créer un réseau d'administration

Pour assurer une administration robuste et fiable de ces équipements, il est nécessaire de distinguer l’administration des services usuels offerts sur le réseau, en séparant les flux pour l’administration et les flux pour les services. Chaque serveur ou équipement doit ainsi disposer d’une connexion réseau dédiée à l’administration et le réseau permettant à l’administrateur de se connecter depuis son poste d’administration doit être de préférence physiquement séparé du réseau opérationnel, ou sinon la séparation peut être assurée logiquement via un **vlan dédié**.

On peut donc arriver au schéma réseau ci-dessous:

![Schema réseau avec un parefeu interne, un parefeu externe, les DMZs et le réseau d'administration](../img/SchemaReseauEntrepriseAvecParefeuxEtReseauAdmin.svg)


!!! note "Exercice"
    Complétez le schéma réseau ci-dessus en rajoutant les flux de données classiques (flux *in band*) et les flux de gestion administratif (flux *out band*). Une définition de la technique OOB (Out Of Band) est disponible sur [Wikipédia](https://en.wikipedia.org/wiki/Out-of-band_management#Out-of-band_versus_in-band). Veuillez noter que vous mettrez en oeuvre ce principe avec le matériel DELL et sa carte [iDRAC](https://en.wikipedia.org/wiki/Dell_DRAC) (ILO chez HP, AMT chez Intel, etc...).

L’administrateur accède au réseau d'administration exclusivement depuis un poste d’admin et interconnecte tous les équipements administrés au travers d’un **firewall admin dédié**. En outre, il est important que les flux d'administration utilisent des **protocoles sécurisés**, disposant d'un chiffrement, d'un contrôle d'accès et d'une authentification à l'état de l'art (ssh plutôt que Telnet).

Il est très important de séparer les flux d’administration des flux opérationnels pour permettre,
même en cas de **défaillances du reste de l’architecture**, de toujours avoir la capacité
d’administrer les systèmes. Notez également que cette séparation empêche les attaquants, où qu’ils soient sur le réseau opérationnel, d’accéder en tant qu’administrateur aux serveurs puisque **l’administration n’est strictement possible que depuis le réseau d’administration**.

Il est préférable que le firewall admin soit de **marque différente des autres firewalls**. Par ailleurs, il est important que le réseau d’administration fonctionne, à la manière d’un bunker, en complète autarcie, et donc que l’authentification des administrateurs ne fasse appel, non pas à l’annuaire du réseau opérationnel, mais à **un annuaire dédié au réseau d’administration**. Enfin il est capital de veiller à la protection des comptes d’administration en exigeant des administrateurs une **authentification à double-facteurs**, comme la connaissance d’un mot de passe et la possession d’une clé physique.

Chaque action réalisée par l’administrateur doit être **loguée** après avoir été authentifiée, de sorte à garder une trace de toutes les actions réalisées. Pour éviter de contaminer le réseau d’administration avec des malwares, il est très important de limiter les opérations depuis le réseau d’administration aux **seules opérations d’administration**. La navigation sur Internet et l'accès à la messagerie interne sont, par exemple, à proscrire.

## Permettre l’accès distant par VPN

Comme vous l’avez vu en début de document, il est nécessaire de passer par un firewall qui donne accès à une **DMZ dédiée au service de VPN** pour les accès distants. Il est préférable que ce firewall soit séparé des firewalls externes et internes et dispose de sa propre adresse IP. Il est également préférable que le service VPN soit un équipement dédié, mais il est possible qu’il soit intégré directement dans le firewall. 

Le service VPN doit en premier lieu **authentifier l’employé de façon forte** à l’aide d’un annuaire extérieur au service VPN. L’authentification forte fait référence à une **authentification à double-facteurs**, ici par exemple la connaissance d’un mot de passe et la possession d’un appareil comme un smartphone ou une carte à puce.

L’utilisateur se voit donner accès à un sous-ensemble des applications internes et à la navigation Internet. Le service VPN ou le firewall associé peut effectuer un filtrage précis et peut de la sorte limiter les accès. Le poste utilisateur en télétravail **ne doit pas pouvoir accéder à Internet directement**. Tous les flux doivent impérativement transiter par le tunnel VPN vers le SI de l'entreprise, et bénéficier des protections offertes par les firewalls et le proxy web de l'entreprise avant d'accéder à Internet.

!!! note "Exercice"
    Complétez, sur le schéma réseau précédent, les flux de données d'un télétravailleur.

## Conclusion

Ce document est assez dense en terme de recommandations. La mise en oeuvre de ces recommandations sera réalisées à travers différents TPs. Il faut noter qu'il reste encore deux thèmes après cette étude :

- ou placer les **sondes de détection d'intrusion** (I.D.S. en anglais) dans le réseau ?
- mettre en oeuvre une **redondance** des systèmes et des services pour obtenir une **haute disponibilité** du S.I. (H.A. : [High Availibilty](https://fr.wikipedia.org/wiki/Haute_disponibilit%C3%A9#:~:text=La%20haute%20disponibilit%C3%A9%20ou%20high,un%20taux%20de%20disponibilit%C3%A9%20convenable.)) et donc identifier les points de défaillance unique du S.I. (SPOF : [Single Point Of Failure](https://en.wikipedia.org/wiki/Single_point_of_failure)).

Ci-dessous, vous trouverez une exemple de [passerelle internet sécurisée](https://cyber.gouv.fr/publications/recommandations-relatives-linterconnexion-dun-si-internet) recommandée par l'ANSSI. Vous devez pouvoir interpréter ce schéma compte tenu de ce que vous venez de lire.

![Schema passerelle internet sécurisée selon l'ANSSI](../img/SchemaPasserelleInternetSecuriseeANSSI.png)

## QCM d'autopositionnement

Vous venez d’être nommé [RSSI](https://fr.wikipedia.org/wiki/Responsable_de_la_s%C3%A9curit%C3%A9_des_syst%C3%A8mes_d%27information) d’une petite banque d’investissement. Celle-ci est composée de 100 utilisateurs, regroupés sur un seul site. L’essentiel de son activité se fait au sein de la salle de marché, un open-space de 25 personnes. Cette activité représente l’unique valeur métier de l’entreprise et elle est donc considérée comme critique.

Les besoins de sécurité pour cette salle de marché sont les suivants :

- Confidentialité : Les opérations d’investissement ne doivent pas fuiter de la salle.
- Intégrité : Personne ne doit être en mesure d’altérer les opérations.
- Disponibilité : Les opérations doivent pouvoir être réalisées sans délai, 24h/24. A noter que ces opérations se font sur un site Internet sécurisé.

Notez que les flux critiques de la salle des marchés sont des flux permettant de réaliser des opérations financières sur des sites Internet distants. Notez également le vocabulaire utilisé par la suite : un *opérateur* fait partie du personnel qui travaille au sein de la salle des marchés, contrairement à un *utilisateur simple* qui travaille en dehors de la salle des marchés.

Pour sécuriser la salle de marché, la banque a également mis en place un système de contrôle d’accès par badge qui est mis en œuvre par un serveur de contrôle d’accès. Il est primordial que seul le personnel autorisé ait accès à cette salle. Ce système est géré par les 2 administrateurs de la banque.

Il y a un accès Internet mutualisé pour tous les utilisateurs, protégé par un pare-feu externe. Tous les utilisateurs sont dans un sous-réseau unique : 192.168.0.0/24. Tous les utilisateurs doivent pouvoir accéder à la messagerie interne de l’entreprise ainsi qu’à Internet.

La messagerie ne transporte pas d’informations confidentielles mais elle a un besoin de disponibilité important, notamment en cas de crise.

Il y a seulement 2 administrateurs pour tout le SI. Ceux-ci administrent tous les équipements (serveurs, commutateurs réseaux, etc.) depuis leur poste bureautique.

L’authentification des utilisateurs sur les postes est gérée par un serveur annuaire unique.

![Schema réseau QCM](../img/SchemaReseauQCM_Q1.svg)

!!! note "Question"
    Complétez sur le schéma réseau ci-dessus les flux de données principaux à l'aide du texte d'introduction.

!!! note "Quels sont les flux critiques pour le business de l'entreprise ?"
    - [ ] Les flux d’opérations de la salle des marchés
    - [ ] Les flux de messagerie
    - [ ] Tous les flux en provenance de la salle de marché
    - [ ] Les flux de contrôle d’accès (accès par badge)

!!! note "Quel est le découpage en zones de sécurité qui vous semblent répondre le mieux aux besoins de sécurité évoqués ?"
    - [ ] Solution A: ![Schema réseau QCM2.1](../img/SchemaReseauQCM_Q2.1.svg)
    - [ ] Solution B: ![Schema réseau QCM2.2](../img/SchemaReseauQCM_Q2.2.svg)
    - [ ] Solution C: ![Schema réseau QCM2.3](../img/SchemaReseauQCM_Q2.3.svg)

!!! note "Quel est l’intérêt de mettre en place un pare-feu interne ?"
    - [ ] Pour centraliser les journaux syslog
    - [ ] Pour filtrer les flux internes au réseau de l’entreprise
    - [ ] Pour réaliser un routage entre les différents vlan définis
    - [ ] Pour intercepter, déchiffrer et analyser les flux https à destination d’Internet

!!! note "Quels sont les flux du schéma ci-dessous à autoriser entre la zone salle des marchés et la zone serveur ? Dessinez ces flux de données"
    - [ ] Annuaire, Mail, Web
    - [ ] Annuaire
    - [ ] Annuaire, Mail
    - [ ] Annuaire, FTP
    ![Schema réseau QCM4](../img/SchemaReseauQCM_Q4.svg)

!!! note "Les utilisateurs de la salle des marchés se plaignent de lenteurs pour accéder au site Internet leur permettant de réaliser les opérations financières. Un nouveau lien Internet est commandé. Quelle architecture vous semble la plus sécurisée pour répondre aux besoins de sécurité de ces flux, dont le besoin de disponibilité ?"
    - [ ] Solution A: ![Schema réseau QCM5.1](../img/SchemaReseauQCM_Q5.1.svg)
    - [ ] Solution B: ![Schema réseau QCM5.2](../img/SchemaReseauQCM_Q5.2.svg)
    - [ ] Solution C: ![Schema réseau QCM5.3](../img/SchemaReseauQCM_Q5.3.svg)
    - [ ] Solution D: ![Schema réseau QCM5.4](../img/SchemaReseauQCM_Q5.4.svg)

!!! note "Des soupçons d’écoute sur votre réseau interne en dehors de la salle de marché par des utilisateurs malveillants vous sont remontés. Comment protéger en confidentialité et en intégrité les flux critiques de la salle des marchés ?"
    - [ ] Par la mise en place d’une sonde de détection
    - [ ] Par la mise en place d’un tunnel VPN IPsec entre le réseau de la salle des marchés et le pare-feu externe en frontal d’Internet
    - [ ] En activant les fonctions d’anti-spoofing ARP sur les commutateurs réseaux
    - [ ] Par la mise en place d’une authentification 802.1x sur le réseau interne

!!! note "Vous devez mettre en place une infrastructure d’administration au sein de votre SI. Quelles sont les mesures techniques à proposer ?"
    - [ ] Mettre en place un bastion d’administration dans votre réseau bureautique pour protéger l’accès au réseau d’administration
    - [ ] Mettre en place un poste d’administration dédié, et déconnecté d’Internet
    - [ ] Configurer une interface dédiée à l’administration sur chaque serveur et équipement réseau
    - [ ] Désactiver les protocoles non sécurisés (telnet, rlogin, etc.)

!!! note "Un utilisateur de la salle de marché doit, pour des raisons de santé, travailler depuis chez lui pendant quelques mois. Quelle architecture vous semble la plus sécurisée ?"
    - [ ] Solution A: ![Schema réseau QCM8.1](../img/SchemaReseauQCM_Q8.1.svg)
    - [ ] Solution B: ![Schema réseau QCM8.2](../img/SchemaReseauQCM_Q8.2.svg)
    - [ ] Solution C: ![Schema réseau QCM8.3](../img/SchemaReseauQCM_Q8.3.svg)
    - [ ] Solution D: ![Schema réseau QCM8.4](../img/SchemaReseauQCM_Q8.4.svg)

!!! note "Votre réseau de contrôle d’accès (physique) était jusqu’à présent isolé de votre réseau bureautique, ce qui est une bonne pratique. Une nouvelle demande de la direction vous impose l’ajout de photos sur les nouveaux badges créés par l’application de gestion des contrôles d’accès. Ces photos ne peuvent être situées que sur un nouveau serveur de fichiers qui doit être accessible en lecture/écriture par les utilisateurs (contrainte organisationnelle). Quelle architecture vous semble protéger le mieux votre SI de contrôle d’accès ?"
    - [ ] Solution A: ![Schema réseau QCM9.1](../img/SchemaReseauQCM_Q9.1.svg)
    - [ ] Solution B: ![Schema réseau QCM9.2](../img/SchemaReseauQCM_Q9.2.svg)
    - [ ] Solution C: ![Schema réseau QCM9.3](../img/SchemaReseauQCM_Q9.3.svg)
    - [ ] Solution D: ![Schema réseau QCM9.4](../img/SchemaReseauQCM_Q9.4.svg)

!!! note "Complétez le schéma réseau ci-dessous pour rajouter un réseau d'administration du S.I."
    ![Schema réseau QCM10](../img/SchemaReseauQCM_Q10.svg)

## Sources pour la construction de ce document

- Le texte de ce document vient quasiment intégralement du MOOC sur France Université Numérique ([FUN](https://www.fun-mooc.fr/fr/)) sur la *Sécurité des réseaux informatiques*,
- le livre blanc de l'ANSSI sur la réalisation d'une passerelle internet sécurisée ([lien](https://cyber.gouv.fr/publications/recommandations-relatives-linterconnexion-dun-si-internet))
