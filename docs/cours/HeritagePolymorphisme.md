# Héritage et polymorphisme en langage C++
sans oublier le reste : **méthodes virtuelles, virtuelles pures et classes abstraites**

!!! tldr "Objectifs du document"
    - mettre en oeuvre la notion d'héritage simple et le vocabulaire associé : classe mère et fille, classe dérivée, spécialisation, généralisation,
    - qu'est-ce qu'une méthode virtuelle, redéfinition d'une méthode virtuelle dans les classes dérivées,
    - pointeur sur une classe mère puis mise en oeuvre du polymorphisme,
    - pourquoi utiliser des méthodes virtuelles pures et les conséquences sur la classe mère (classe abstraite).
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"
    - S4. Développement logiciel
        - S4.1 : Principes de base
            - Gestion mémoire : adresse/valeur, pointeurs, variables statiques, allocations automatique et dynamique (pile/tas), etc : niveau 4 (maitrise méthodologique)
            - Variables ; durée de vie, visibilité : niveau 4 (maîtrise méthodologique)
        - S4.6 : Programmation Orientée Objet (C++)
            - Définition de classes (encapsulation) et modèle canonique (dit de Coplien) : niveau 3
            - Instanciation d’objets (new, delete, etc.) : niveau 4
            - Mécanisme d’héritage : niveau 4
            - Classes abstraites, virtualité : niveau 3

Nous traiterons toutes ces notions à partir d'un exemple simple : une classe mère `Personne` et deux classes dérivées `Prof` et `Eleve`. Dans les exemples de codes ci-dessous les directives du préprocesseur pour empêcher les inclusions multiples (`#ifndef`, `#define`, `#endif`, `#pragma once`) sont omises. De plus, il n'y a pas la séparation fichier de déclaration (fichier d'en-têtes ou .h) et fichier de définition (fichier qui contient le code ou .cpp). Tout est codé dans le fichier d'en-têtes. Les exemples de codes complets sont donnés à la fin de ce cours.

## La classe `Personne`

Cette classe sera la base de notre découverte. Elle contient les éléments suivants :

- deux attributs protégés (`protected`) :
    - `nom` : `string`
    - `age` : `int`
- deux méthodes publiques (`public`):
    - un constructeur qui prendra comme paramètre une chaîne pour le nom et un entier pour l'age
    - une méthode `print()` qui affichera à l'aide de la fonction `cout` le nom et l'age et ne renvoie rien
  
!!! note "Exercice"
    === "Réalisez le codage de cette classe"
        Vous devez écrire le fichier de déclaration (fichier d'en-têtes = .h) et le fichier de définition (fichier du code source = .cpp) de la classe `Personne`

    === "Une solution possible"
        Le fichier ci-dessous contient à la fois le fichier d'en-têtes et le code. On qualifie parfois ce type de codage de *.hpp*.

        ```cpp
        #include <iostream>
        #include <string>

        class Personne 
        {
          protected:
            std::string nom;
            int age;
          public:
            Personne(std::string nom, int age) {
                this->nom = nom;
                this->age = age;
                // Pour la compréhension on affiche un message
                std::cout << "Dans constructeur Personne" << std::endl;
            };

            void print() const {
                std::cout << "Nom: " << nom << "  Age: " << age << std::endl;
            };
        };
        ```

!!! note "Exercice"
    === "Utilisation de la classe"
        *Instanciez* un objet de la classe `Personne` avec comme nom de variable `p1` et comme paramètre "Toto" et 14. Appelez la méthode `print()` sur cet objet `p1`. Si vous disposez d'un compilateur, testez votre proposition puis regardez la solution.

    === "Une solution possible"
        ```cpp
        // objet p0 de type personne, on passe au constructeur un nom (string) : Toto et un age (int) : 14
        // puis on appelle la méthode print() sur cet objet p0
        Personne p1("Toto", 14);
        p1.print();
        ```
        
        Sortie console :
        
        ```Console
        Dans constructeur Personne
        Nom: Toto  Age: 14
        ```

!!! note "Exercice"
    === "Analyse de code C++"

        1. Que signifie `protected` pour les attributs ?
        2. Un attribut `protected` est-il visible de l'extérieur de la classe ?
        3. Que signifie `this` dans le constructeur ? Pourquoi l'emploie-t-on ici ?
        4. La méthode `print()` est déclarée `const`, qu'est ce que cela signifie ? Pourquoi n'a-t-elle pas d'argument ?

    === "Réponses"
        1. Un attribut peut être :

            - soit public (`public`) : donc visible de l'extérieur de la classe -> jamais utilisé,
            - soit privé (`private`) : pas d’accès depuis l'extérieur de la classe -> c'est le comportement par défaut que devrait avoir tous les attributs,
            - soit protégé (`protected`) : pas d’accès depuis l'extérieur de la classe sauf pour les classes dérivées
        2. Visible depuis une classe dérivée sinon invisible
        3. Le pointeur `this` désigne l'objet courant. Ici on l'utilise pour accéder aux attributs de la classe et ainsi pouvoir lui affecter la variable qui porte le même nom sans ambiguïté (sinon on aurait quelque chose du genre age=age donc ne faisant rien)
        4. La méthode `print()` est déclaré `const` ce qui indique qu'elle ne modifiera pas les attributs de la classe, ainsi le compilateur peut vérifier qu'effectivement votre code ne modifie pas un attribut mais se contente de le lire. Cette fonction n'a pas d'argument car elle peut accéder directement à tous les attributs privés ou protégés de la classe.   

## Les classes dérivées de Personne : Prof et Eleve

Le principe de **l'héritage** est de se dire qu'on doit **mutualiser** le code (c'est à dire éviter de réécrire les mêmes chose). On va donc écrire une classe dite **mère** qui va contenir tous **les attributs et méthodes communes** d'un ensemble. Puis on **dérivera** cette classe pour la **spécialiser** : lui rajouter des **caractéristiques propres**. Cette nouvelle classe **hérite** donc de la classe mère : on l'appellera **classe fille** ou **classe dérivée**. Lorsqu'on part d'une classe fille est que l'on remonte vers une classe mère, on parle alors de **généralisation** de la classe.

![Schéma spécialisation généralisation](../img/SchemaSpecialisationGeneralisation.png)

### La classe `Eleve`

La classe élève dérive de la classe Personne. Ce qui veut dire qu'un "élève" est une "personne" donc il a toutes les caractéristiques d'une personne plus celles qui lui sont spécifiques. Dans notre exemple, la classe `Eleve` aura :

- un attribut privé (`private`) :
    - `moyenne` : `float`
- deux méthodes publics (`public`) :
    - un constructeur qui prendra en paramètres une chaîne pour le nom, un entier pour l'age et un nombre réel pour la moyenne
    - une méthode `print()` qui affichera le nom, l'age et la moyenne à l'aide de `cout` et ne renvoie rien
  
!!! note "Exercice"
    === "Réalisez le codage de cette classe"
        Vous devez écrire le fichier de déclaration (fichier d'en-têtes = .h) et le fichier de définition (fichier du code source = .cpp) de la classe `Eleve`

    === "Une solution possible"
        ```cpp
        class Eleve : public Personne 
        {
              private:
                  double moyenne;
              public:
                  Eleve(std::string nom, int age, double moyenne)
                    :Personne(nom, age)
                  {
                      this->moyenne = moyenne;
                      // Pour la compréhension on affiche un message
                      std::cout << "Dans constructeur Eleve" << std::endl;
                  };

                  void print() const {
                        Personne::print();
                        std::cout << "Moyenne: " << moyenne << std::endl;
                  };
        };
        ```
!!! note "Exercice"
    === "Utilisation de la classe"
        *Instanciez* un objet de la classe `Eleve` avec comme nom de variable `e1` et comme paramètre "Casimir", 20 ans, 15.6 de moyenne. Appelez la méthode `print()` sur cet objet `e1`. Testez votre proposition puis regardez la solution.

    === "Une solution possible"
        ```cpp
        Eleve e1("Casimir", 20, 15.6);
        e1.print();
        ```

        sortie console :

        ```console
        Dans constructeur Personne
        Dans constructeur Eleve
        Nom: Casimir  Age: 20
        Moyenne: 15.6
        ```
!!! tldr "Ce que vous devez retenir de cette partie"

    - pour qu'une classe hérite d'une autre, il suffit de rajouter **:public nomClasseMere** derriere **class nomClasseFille** (héritage simple),
    - si l'héritage est `public` alors la classe dérivée peut accéder aux attributs de la classe mère (sans passer par des getters mais en utilisant l'opérateur de résolution de portée `::`),
    - le constructeur de la classe fille doit appeler appeler le constructeur de la classe mère en lui passant tous les arguments nécessaires,
    - lorsque vous instanciez un objet d'une classe dérivée, le constructeur de la classe mère est appelé **avant** le constructeur de la classe fille,
    - lorsqu'une même méthode est déclarée dans la classe mère et les classes filles, on dit qu'elle est **redéfinie** (et non surchargée). 

### La classe `Prof`

La classe `Prof` dérive de la classe `Personne`. Ce qui veut dire qu'un "prof" est une "personne" donc il a toutes les caractéristiques d'une personne plus celles qui lui sont spécifiques. Dans notre exemple, la classe `Prof` aura :

- deux attributs privés (`private`) :
    - `salaire` : `float`
    - `matiere` : `string`
- deux méthodes publics (`public`) :
    - un constructeur qui prendra en paramètres une chaîne pour le nom, un entier pour l'age, un nombre réel pour le salaire et une chaîne de caractères pour la matière
    - une méthode `print()` qui affichera le nom, l'age, le salaire et la matière à l'aide de `cout` et ne renvoie rien

!!! note "Exercice"
    === "Réalisation d'une classe et test de celle-ci"
        Réalisez le codage de la classe `Prof` puis instanciez un objet de cette classe avec les paramètres suivants : Haddock, 58, 2876.60, Piraterie

    === "Une solution possible"
        ```cpp
        include <string>

        class Prof : public Personne
        {
            private:
                double salaire;
                std::string matiere;
            public:
                Prof(std::string nom, int age, double salaire, std::string matiere)
                    :Personne(nom, age)
                {
                    this->salaire = salaire;
                    this->matiere = matiere;
                    // Pour la compréhension on affiche un message
                    std::cout << "Dans constructeur Prof" << std::endl;
                };

                void print() const {
                    Personne::print();
                    std::cout << "Salaire : " << salaire << 
                        "  Matiere : " << matiere << std::endl;
                };
          };

        Prof p2("Haddock", 58, 2876.60, "Piraterie");
        p2.print();
        ```

        sortie console :

        ```console
        Dans constructeur Personne
        Dans constructeur Prof
        Nom: Haddock  Age: 58
        Salaire : 2876.6  Matiere : Piraterie
        ```

## Diagramme de classes

!!! note "Exercice"
    === "Dessiner le diagramme UML de classes"
        Dessinez le diagramme de classe de ces trois classes (`Personne`, `Eleve` et `Prof`) en précisant :

        - les types de liaisons entre-elles,
        - les cardinalités s'il y a lieu.

    === "Une solution possible"
        Il n'y a pas de cardinalité à indiquer entre des classes mères et des classes dérivées.

        ```dot
        digraph Couriers {
        
        rankdir = BT

        node [
            fontname="Helvetica,Arial,sans-serif"
            shape=record
            style=filled
            fillcolor=gray85
        ]

        edge [
            arrowhead = "empty"
        ]

        Personne [
            fontcolor="black"
            label = "{Personne|# nom : string\l# age : int\l|+ Personne( nom : string, age : int)\l+ print() : void\l}"
            fillcolor=lightskyblue
        ]

        Eleve [
            fontcolor="black"
            label = "{Eleve|- moyenne : float\l|+ Eleve( nom : string, age : int, moyenne : float)\l+ print() : void\l}"
        ]

        Prof [
            fontcolor="black"
            label = "{Prof|- salaire : float\l- matiere : string\l|+ Prof( nom : string, age : int, salaire : float, matiere : string)\l+ print() : void\l}"
        ]

        Eleve -> Personne
        Prof -> Personne
        }
        ```

## Conversions implicites et pointeurs

!!! note "Exercice"
    === "Analyse de code"
        Saisissez ce code, exécutez-le puis commentez la sortie console.

        ```cpp
        // Construction d'une personne à partir d'un Prof
        Personne pers1 = Prof("Dupond", 49, 1234.56, "Detective");
        pers1.print();

        // Construction d'une personne à partir d'un élève
        Personne pers2 = Eleve("Alladin", 17, 9.5);
        pers2.print();
        ```

    === "Correction : Sortie console"
        
        sortie console :

        ```console
        Dans constructeur Personne
        Dans constructeur Prof
        Nom: Dupond  Age: 49
        Dans constructeur Personne
        Dans constructeur Eleve
        Nom: Alladin  Age: 17
        ```

    === "Correction : Commentaires"
        Vous remarquez que lorsque vous appelez la méthode `print()`, vous affichez les informations issues de la classe `Personne` même si l'objet `Personne` concerné a été créé à partir d'un `Prof` ou d'un `Eleve`. Les arguments non utilisés sont perdues. Cette manière de faire s'appelle la **conversion implicite** : en opposition à la **conversion explicite** lorsque vous utilisez un opérateur de "cast" (`static_cast`, `const_cast`, `reinterpret_cast`).

Testons maintenant avec des pointeurs.

!!! note "Exercice"
    === "Analyse de code"
        Saisissez ce code, exécutez-le puis commentez la sortie console.

        ```cpp
        Personne* pPers = &pers1;
        pPers->print();

        Prof* pProf = &p2;
        pProf->print();

        pPers = &p2;
        pPers->print();
        ```

    === "Correction : Sortie console"
        
        sortie console :

        ```console
        Nom: Dupond  Age: 49
        Nom: Haddock  Age: 58
        Salaire : 2876.6  Matiere : Piraterie
        Nom: Haddock  Age: 58
        ```

    === "Correction : Commentaires"
        - Quand un pointeur de type `Personne` (`pPers`) pointe vers un objet `Personne`, la méthode `print()` est correctement invoquée. Idem pour le pointeur `pProf`.
        - Par contre si un pointeur de type `Personne` pointe vers un objet dérivé de la classe `Personne` (ici un objet `Prof`), l'appel de la méthode `print()` est celle de `Personne` au lieu d'appeler celle de `Prof`. 
        - <span style="color: #ff0000">**Ce comportement n'est pas souhaitable**</span>. **Un pointeur doit se comporter comme ce qu'il pointe pas comme ce qu'il est**.

## Méthodes virtuelles et polymorphisme

Le problème évoqué à la fin du paragraphe précédent s'appelle la **résolution dynamique** des liens. Votre programme doit s'adapter **à l'exécution** en fonction de ce vers quoi pointe le pointeur. Le seul moyen pour le compilateur de prévoir de telle situation est de rajouter un mot clé devant les **méthodes redéfinies** dans les classes mères et dérivées : il s'agit du mot `virtual` (virtuel en français).

Une méthode définie `virtual` dans la classe mère entraîne que les méthodes redéfinies dans les classes filles sont automatiquement virtuelles. Cependant, pour la lisibilité du code, il est préférable de rajouter le mot clé `virtual` dans les classes filles des méthodes redéfinies.

!!! note "Exercice"
    === "Modifier un code"
        Revenez sur la déclaration la classe `Personne`vet rajoutez le mot clé `virtual`devant la méthode `print()`. Puis ré-exécutez votre code. Comparez la nouvelle sortie écran par rapport à l'ancienne.

    === "Ancienne sortie console"
        
        sortie console (**sans** mot clé `virtual`) :

        ```console
        Nom: Dupond  Age: 49
        Nom: Haddock  Age: 58
        Salaire : 2876.6  Matiere : Piraterie
        Nom: Haddock  Age: 58
        ```
    === "Nouvelle sortie console"
        
        sortie console (**avec** mot clé `virtual`) :

        ```console
        Nom: Dupond  Age: 49
        Nom: Haddock  Age: 58
        Salaire : 2876.6  Matiere : Piraterie
        Nom: Haddock  Age: 58
        Salaire : 2876.6  Matiere : Piraterie     <- cette ligne apparaît !
        ```

    === "Correction : Commentaires"
        Cette fois-ci le programme appelle bien la bonne méthode `print()`pour la dernière ligne, celle de la classe `Prof`. 

!!! note "Exercice"
    === "Produire un code"

        Modifiez la classe `Eleve` pour indiquer au programmeur qu'une méthode est virtuelle. Instanciez un élève (Robert, 17, 9.5), affectez son adresse au pointeur `pPers` puis appelez la méthode `print()`. Que constatez-vous ?

    === "Une solution possible"
        
        ```cpp
        Eleve eleveRobert("Robert", 17, 9.5);
        pPers = &eleveRobert;
        pPers->print();
        ```
    === "Ancienne sortie console"
        
        sortie console (**sans** mot clé `virtual`) :

        ```console
        Dans constructeur Personne
        Dans constructeur Eleve
        Nom: Robert  Age: 17
        ```
    === "Nouvelle sortie console"
        
        sortie console (**avec** mot clé `virtual`) :

        ```console
        Dans constructeur Personne
        Dans constructeur Eleve
        Nom: Robert  Age: 17
        Moyenne : 9.5    <- cette ligne apparaît !
        ```
    === "Commentaires"
        
        Si vous avez correctement mis le mot clé `virtual` alors le programme doit appelé la méthode `print()` de ce que pointe le pointeur `pPers`. En l’occurrence le `print()` de `Eleve` et pas la méthode `print()`de la classe `Personne`.

!!! tldr "Ce que vous devez retenir..."
    En fonction de ce que pointe le pointeur `pPers`, la méthode `print()` correcte est appelée. Ce comportement s'appelle <span style="color: #ff0000">**le polymorphisme**</span> et il s'applique **uniquement sur des pointeurs ou des références sur des méthodes redéfinies dans le cadre d'un héritage**.

## Classe abstraite et méthode virtuelle pure

Changeons de sujet et plaçons-nous maintenant dans le monde du jeu de role en ligne. Dans ces jeux, il y a toujours des personnages non joueur qui vous accompagne. Ce *PNJ* peut être un humain avec une spécificité (guerrier, archer, clerc...) mais aussi un non-humain (orc, nain, elfe voire dragon). Cependant tous ces PNJs ont des caractéristiques communes que l'on mettra dans une classe mère éponyme.

Nous allons donc traiter un exemple avec les classes suivantes :

- une classe mère `PNJ` qui aura :
    - comme **attributs** : un nombre de points de vie (entier, pv) et un nombre de pièces (entier, po),
    - comme **méthodes** : un constructeur qui prendra les arguments nécessaires pour initialiser les attributs, une méthode `attaque()` qui renverra un entier et une méthode `paye()` qui ne renverra rien.
- une classe `Humain` dérivée de la classe `PNJ` qui aura :
    - comme **attributs** : une armure (booléen, armure) et une arme (entier, arme),
    - comme **méthodes** : un constructeur qui permettra d'initialiser tous les attributs et une méthode `attaque()` qui aura le même prototype que celle de la classe mère.
- une classe `Dragon` dérivée de la classe PNJ qui aura :
    - comme attributs : une armure (booléen, écailles) et une arme (entier, souffleFeu),
    - comme méthodes : idem que la classe Humain

### Qu'est ce qu'une méthode virtuelle pure ?

C'est une méthode de la classe mère qui a été redéfini dans les classes dérivées (d'où l'utilisation du qualificatif `virtual`) mais qui n'a **aucune implémentation** (c'est à dire aucun code) qui lui est rattachée. Aucun code ne lui est rattaché car on ne sait pas comment, dans la classe mère, traiter cette méthode. Si on prend l'exemple ci-dessus : un PNJ sait attaquer, mais son attaque va dépendre de ce qu'il est (un humain ou un dragon) et forcement l'attaque va être différente.

Pour indiquer que cette méthode virtuelle n'a pas de code on met `=0` à la suite de sa déclaration et on dit de cette méthode qu'elle est **pure**.

!!! tldr "Ce qu'il faut retenir..."
    En résumé une **méthode virtuelle pure** est décrite dans la classe mère (le prototype) mais elle n'a pas de code associé, cela obligera les classes dérivées à implémenter cette fonction. Vous êtes en train d'imposer un *modèle* à vos classes dérivées.

### Qu'est ce qu'une classe abstraite ?

C'est une classe qui possède **au moins une méthode virtuelle pure**. Cette classe est qualifiée d'**abstraite** car vous **ne pouvez pas l'instancier** (une de ces méthodes n'a pas de code donc elle n'est pas compilable).

Les **classes abstraites** sont des **modèles** desquelles seront dérivées des classes.
Une classe abstraite n'a pas forcement que des méthodes virtuelles pures mais elles possèdent toutes les méthodes communes à ce qu'elles modélisent. Dans notre exemple, la méthode `payer()` est bien commune à l'ensemble des PNJs.

Dans un diagramme UML, le nom de la classe est mis en italique pour indiquer que la classe est abstraite. Cependant comme c'est quelque fois difficile à voir sur des photocopies, vous pouvez trouver le mot **"abstract"** (avec les guillemets) sous le nom de la classe.

!!! tldr "Ce qu'il faut retenir..."
    Une **classe abstraite** est une classe qui possède au moins un méthode virtuelle pure. Une classe abstraite ne peut être instanciée, elle sert de modèle aux classes dérivées.

## Modélisation UML d'une classe abstraite

!!! note "Exercice"
    === "Réalisation d'un diagramme UML"
        Vous êtes maintenant en mesure de modéliser, dans un diagramme de classes UML, les relations entre les classes `PNJ`, `Humain` et `Dragon`.
    === "Une solution possible"

        ```dot
        digraph Couriers {
        rankdir = BT
        node [
            fontname="Helvetica,Arial,sans-serif"
            shape=record
            style=filled
            fillcolor=gray85
        ]

        edge [
            arrowhead = "empty"
        ]

        PNJ [
            fontcolor="black"
            label = "{PNJ\n\"abstract\"|# pv : int\l# po : int\l|+ PNJ( pv : int, po : int)\l+ payer() : void\l+ attaque() : int virtual =0}"
            fillcolor=lightskyblue
        ]

        Humain [
            fontcolor="black"
            label = "{Humain|# armure : bool\l# arme : int\l|+ Humain( pv : int, po : int, armure : bool, arme : int)\l+ attaque() : int virtual\l}"
        ]

        Dragon [
            fontcolor="black"
            label = "{Dragon|# ecailles : bool\l# souffleFeu : int\l|+ Dragon( pv : int, po : int, ecailles : bool, souffleFeu : int)\l+ attaque() : int virtual\l}"
        ]

        Dragon -> PNJ
        Humain -> PNJ
        }
        ```

## Codage des classes abstraites et classes dérivées

!!! note "Exercice"
    === "Produire un code"

        Codez la classe abstraite `PNJ` (fichier de déclaration et de définition).

    === "Une solution possible"
        
        ```cpp
        #include <iostream>

        class PNJ
        {
            protected:
                int pv;
                int po;

            public:
                PNJ(int pv, int po) {
                    this->pv = pv;
                    this->po = po;
                    std::cout << "Constructeur PNJ" << std::endl;
                }  
                void payer() {};
                virtual int attaque() = 0;
          }
        ```

!!! note "Exercice"
    === "Utilisation de la classe"

        Instanciez un objet de la classe PNJ dans un code (pnjZero, 100 po et 100pv) puis essayer de compiler ce code. Lisez attentivement le message du compilateur et commentez son message.

    === "Une solution possible"

        ```cpp
        PNJ pnjZero(100, 100);
        ```

        ```console
        input_line_20:2:6: error: variable type 'PNJ' is an abstract class
        PNJ pnjZero(100, 100);
         ^
        input_line_19:13:17: note: unimplemented pure virtual method 'attaque' in 'PNJ'
        virtual int attaque() = 0;
                    ^
        ```
    === "Commentaires"

        Le compilateur indique que la classe `PNJ` est une classe abstraite, la méthode virtuelle pure est la méthode `attaque()`. Le code ne peut donc pas compiler tant qu'une classe dérivée implémentant la méthode `attaque()`  ne sera pas codée !

!!! note "Exercice"
    === "Produire un code"

        Codez la classe `Humain` (fichier de déclaration et de définition).

    === "Une solution possible"

        ```cpp
        class Humain : public PNJ
        {
            protected:
                bool armure;
                int arme;
  
            public:
                Humain(int pv, int po, bool armure, int arme):PNJ(pv, po) {
                    this->armure = armure;
                    this->arme = arme;
                    std::cout << "Constructeur Humain" << std::endl;
                }

                virtual int attaque() {
                    std::cout << "Méthode attaque() Humain : " << arme << " pv" << std::endl; 
                    return arme;
                }
        }
        ```

!!! note "Exercice"
    === "Produire un code"

        Instanciez un objet `conan` de la classe `Humain` avec 100pv, 10po, pas d'armure et une arme à -17pv. Appelez la méthode `attaque` sur cet objet. Donnez la sortie console.

    === "Une solution possible"

        ```cpp
        Humain conan(100, 10, false, -17);
        conan.attaque();
        ```

    === "Sortie console"

        ```console
        Constructeur PNJ
        Constructeur Humain
        Méthode attaque() Humain : -17 pv
        ```

La classe `Dragon` est donnée ci-dessous:

```cpp
class Dragon : public PNJ
{
  private:
    bool ecailles;
    int souffleFeu;
    
  public:
    Dragon(int pv, int po, bool armure, int arme):PNJ(pv,po)
    {
      this->ecailles = armure;
      this->souffleFeu = arme;
      std::cout << "Constructeur Dragon" << std::endl;
    }
    virtual int attaque() {
      std::cout << "Méthode attaque() Dragon : " << souffleFeu << " pv" << std::endl; 
      return souffleFeu;
    }  
}
```

On instancie un objet de la classe `Dragon` : *smaug* par exemple...

```cpp
Dragon smaug(2000, 900000000, true, -130);
smaug.attaque();
```

La sortie console donne :

```console
Constructeur PNJ
Constructeur Dragon
Méthode attaque() Dragon : -130 pv
```

Nos trois classes sont testées avec des variables automatiques. Pour tester le polymorphisme, il faut travailler avec des pointeurs. Analyser le code ci-dessous avant de l'exécuter.

!!! note "Exercice"
    === "Analyse de code"

        Commentez le code ci-dessous :

        ```cpp linenums="1"
        #include <vector>

        std::vector<PNJ*> listePNJ;
        listePNJ.push_back(new Humain(100, 0, false, -5));
        listePNJ.push_back(new Humain(120, 267, true, -17));
        listePNJ.push_back(new Dragon(1000, 2431, true, -49));

        for (int i=0; i < listePNJ.size(); i++)
            listePNJ.at(i)->attaque();

        for (int i=0; i < listePNJ.size(); i++)
            delete(listePNJ.at(i));
        ```

    === "Commentaires du code"

        - ligne 1 : inclusion de la classe `vector` car on va stocker nos PNJs dans un seul et même vecteur.
        - ligne 3 : création du vecteur de pointeur sur des objets `PNJ`. Rappel : le polymorphisme fonctionne avec les pointeurs.
        - ligne 4 à 6 : création dynamique des objets dérivés de la classe `PNJ` : deux humains et un dragon. Les pointeurs `PNJ*` sont maintenant initialisés avec l'adresse d'un objet en mémoire. Ces trois objets sont ajoutés dans le vecteur `listePNJ` avec la méthode `push_back()`.
        - ligne 8 et 9 : on parcours tous les éléments du vecteur avec une boucle `for`. Pour chaque élément (qui est un pointeur) on appelle la méthode `attaque()` (d'ou l'opérateur `->` au lieu de l'opérateur `.`). Le polymorphisme doit jouer son rôle et la méthode `attaque()` appelée doit être celle que pointe le pointeur (donc soit `Humain` soit `Dragon`)
        - ligne 11 et 12 : libération de la mémoire allouée avec l'opérateur `new` avec l'opérateur `delete`. Il est obligatoire de faire cela sinon vous avez des *fuites mémoires*.

A la compilation du code précédent le compilateur nous renvoie le message suivant :

```console
input_line_26:9:5: warning: delete called on 'PNJ' that is abstract but has non-virtual destructor [-Wdelete-non-virtual-dtor]
    delete(listePNJ.at(i));
    ^
```

!!! note "Exercice"
    === "Analyse compilation"

        - Le message renvoyée est-il bloquant pour la compilation ?
        - Comment corriger ce message ?

    === "Commentaire du message"

        - Le message renvoyé par le compilateur est un "warning" donc un avertissement. Ce type de message n'est pas bloquant pour la compilation mais cela signale un problème.
        - Dans le message il est dit que la classe abstraite PNJ n'a pas de destructeur virtuel. En effet pour assurer le polymorphisme il faut que le destructeur de la classe mère soit déclaré `virtual`. Pour supprimer ce message, il faut modifier la classe `PNJ`et rajouter, après le constructeur, le destructeur : `virtual ~PNJ()`.

!!! note "Exercice"

    === "Sortie console"

        Donnez la sortie console du code précédent.

    === "Correction sortie console"

        ```console
        Constructeur PNJ
        Constructeur Humain
        Constructeur PNJ
        Constructeur Humain
        Constructeur PNJ
        Constructeur Dragon
        Méthode attaque() Humain : -5 pv
        Méthode attaque() Humain : -17 pv
        Méthode attaque() Dragon : -49 pv
        ```

## Conclusion

Vous avez découvert à travers ce document les concepts de **P.O.O.** (Programmation Orientée Objet) que vous devez maîtriser à la sortie du BTS. Il faut du temps pour assimiler ces connaissances alors n'hésitez pas à revenir en arrière et faire vos propres tests.

Pour prolonger ce cours, un TD/TP autour des pokemons vous est proposé...

## Codes sources des différentes classes

Vous trouverez ci-dessous le code source de chacune des classes `Personne`, `Prof` et `Eleve` séparés en deux fichiers : fichier d'en-têtes (.h) et fichier d'implémentation (.cpp). Ces codes sont utilisables dans un EDI (Microsoft Visual Studio par exemple).

**Fichier d'en-tête de la classe `Personne`**

```cpp
#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <string>

using namespace std;

// Modélisation d'une personne
class Personne
{
protected:
  string nom;
  int age;
public:
  Personne(string nom, int age);
  virtual void print() const;
};

#endif // PERSONNE_H
```

**Fichier d'implémentation de la classe `Personne`**
`
```cpp
#include "personne.h"

Personne::Personne(string nom, int age)
{
  this->nom = nom;
  this->age = age;
}

void Personne::print() const
{
  cout << "Nom : " << this->nom << " ; "
       << "Age : " << this->age << endl;
}
```

**Fichier d'en-tête de la classe `Eleve`**

```cpp
#ifndef ELEVE_H
#define ELEVE_H

#include <iostream>
#include <string>

#include "personne.h"

using namespace std;

// Modélisation d'un eleve
class Eleve : public Personne
{
private:
  double moyenne;
  string nom;

public:
  Eleve(string nom, int age, double moyenne);
  virtual void print() const;
};

#endif // ELEVE_H
```

**Fichier d'implémentation de la classe `Eleve`**

```cpp
#include "eleve.h"

Eleve::Eleve(string nom, int age, double moyenne) : Personne(nom, age)
{
  this->moyenne=moyenne;
}

void Eleve::print() const
{
  Personne::print();
  cout << "Moyenne : " << this->m_moyenne << endl;
}

```

**Fichier d'en-tête de la classe `Prof`**

```cpp
#ifndef PROF_H
#define PROF_H

#include <iostream>
#include <string>

#include "personne.h"

using namespace std;

// Modélisation d'une personne
class Prof : public Personne
{
private:
  double salaire;
  string matiere;
  
public:
  Prof(string nom, int age, double salaire, string matiere);
  virtual void print() const;
};
#endif // PROF_H
```

**Fichier d'implémentation de la classe `Prof`**

```cpp
#include "prof.h"

Prof::Prof(string nom, int age, double salaire, string matiere) : CPersonne(nom, age)
{
  this->salaire = salaire;
  this->matiere = matiere;
}

void Prof::print() const
{
  CPersonne::print();
  cout << " ; Salaire : " << this->salaire
       << " ; Matiere : " << this->matiere
       << endl;
}
```

**Fichier de test des classes précédentes (main.cpp)**

```cpp
#include <iostream>

#include "personne.h"
#include "eleve.h"
#include "prof.h"

using namespace std;

int main()
{
  Personne pers1("Tintin", 35);
  pers1.print();

  Eleve elev1("Haddock", 45, 17.2);
  elev1.print();

  Prof prof1("Tournesol", 58, 4300, "Info");
  prof1.print();

  // conversion implicite
  cout << "personne 2 avec constructeur prof" << endl;
  Personne pers2 = Prof("prof2", 33, 2100, "Maths");
  pers2.print();

  cout << "personne 3 avec constructeur eleve" << endl;
  Personne pers3 = Eleve("elev2", 20, 10.5);
  pers3.print();

  cout << "pointeur personne vers objet personne" << endl;
  Personne* pPers = &pers1;
  pPers->print();

  cout << "pointeur prof vers objet prof" << endl;
  Prof* pProf = &prof1;
  pProf->print();

  cout << "pointeur personne vers objet prof" << endl;
  pPers = &prof1;
  pPers->print();

  // Allocation dynamique d'un prof dans un pointeur personne
  // Allocation sur le 'tas' (en opposition à l'allocation sur la pile)
  Personne* pPersDyn = new Prof("profDyn", 28, 2200, "Info");
  pPersDyn->print();
  // Libération dynamique de l'objet instancié avec l'opérateur new
  delete pPersDyn;
  return 0;
}
```
