# Surcharge d'opérateurs
<span style="color: #ff0000">**Des opérations intelligentes entre vos objets**</span> 

!!! tldr "Objectifs du cours"

    - définir ce qu'est un *opérateur*,
    - codez une classe pour laquelle des opérateurs auront un sens,
    - savoir coder la surcharge des opérateurs,
    - réaliser des tests de ces nouveaux opérateurs.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"

    - S4. Développement logiciel
        - S4.1 : Principes de base
            - Surcharges d’opérateurs (injection, etc.) : niveau 2 (maitrise d'expression)
            - Variables ; durée de vie, visibilité : niveau 4 (maitrise méthodologique)
            - Flux d’entrée et de sortie de base : terminaux, fichiers, réseau, etc. (spécifications POSIX) : niveau 3 (maitrise d'outils)

## Introduction

Un exemple simple de surcharge d'un opérateur que vous utilisez très souvent : la concaténation de chaînes de caractères.

En effet plutôt que de faire :

```cpp
std::string s1("hello ");
std::string s2("world");
s1.append(s2);
std::cout << s1 << std::endl
// Affiche "Hello world" sur une ligne
```

Vous faites :

```cpp
std::cout << s1 + s2 << std::endl;
```

Ce qui signifie que la classe `string` a donné un **nouveau sens à l'opérateur `+`**.

## Quels opérateurs sont surchargeables ?

En C++, on ne peut pas inventer de nouveaux opérateurs : par exemple le `<>` : **différent de** qui existe en PHP, n'existe pas en C++ et ne peut pas être *créé*.

Vous ne pouvez pas non plus surcharger les opérateurs suivants :

- la fonction `sizeof()`,
- l'opérateur ternaire `?:`,
- l'opérateur de résolution de portée `::`,
- l'opérateur `.` -> sélection d'un membre,
- l'opérateur `.*` -> pointeur sur membre,
- la fonction `typeid`,
- tous les opérateurs de *cast* (`static_cast`, `dynamic_cast`, `reinterpret_cast` et `const_cast`).

Par contre tous les autres opérateurs peuvent être redéfinis. Parmi les plus classiques : `+`, `-`, `*`, `/`, `<<`, `>>`, `==`, `!=`, `<`, `>`, etc...

Il faut bien sur que ces opérateurs aient un sens avec le (ou les) objet(s) manipulé(s)...

## Principe de création d'une surcharge d'opérateur

Le principe est le suivant, il faut réaliser la déclaration suivante :

> **typeRenvoyé** `operator` **operateur`(`paramètres`)`**

Par exemple avec une classe `Duree` :

> `Duree operator+(const Duree& a, const Duree& b);` 

La surcharge de l'opérateur `+` permettra d'additionner deux durées entre elles.

> `bool operator==(const Duree& a, const Duree& b);` 

Cette surcharge de l'opérateur `==` permettra de comparer deux durées entre elles.

Mais définissons cette classe Duree...

## Classe exemple : classe `Duree`

La classe `Duree` permet de stocker un temps sous la forme heure, minute et seconde exprimée avec des chiffres entiers.

On souhaite pouvoir comparer des durées entre elles, ou encore les additionner ou les retrancher, voire les afficher...

!!! note "Exercice"

    === "Produire un code"
        Vous devez proposer un fichier de déclaration (fichier d'en-tête ou .h) et de définition (fichier source ou .cpp) pour cette classe qui contiendra un constructeur avec des paramètres initialisés par défaut à 0 pour les variables `heure`, `minute` et `seconde`. Vous rajouterez les accesseurs sur les attributs.

        Le constructeur devra gérer les cas suivants :

        - vous prendrez la valeur absolue des paramètres (choix du concepteur, peut être pas pertinent),
        - vous gérerez le dépassement de capacité -> par ex : 75s sera converti en 1min 15s.

    === "Une correction possible"
        Ci-dessous vous disposez d'un exemple de fichier de définition de la classe `Duree`. Ce fichier contient aussi le code source : on appelle ce type de fichier un fichier *.hpp*.  

        ```cpp
        #include <iostream>
        #include <string>

        class Duree 
        {
            private:
                int heure;
                int minute;
                int seconde;

            public:
                Duree(int h=0, int m=0, int s=0) {
                    h = abs(h);
                    m = abs(m);
                    s = abs(s);

                    while (s > 59) {
                        m++; 
                        s -= 60;
                      };

                    while (m > 59) {
                        h++;
                        m -= 60;
                    };

                    this->heure = h;
                    this->minute = m;
                    this->seconde = s;

                    std::cout << "Constructeur : heure=" << this->heure 
                              << " minute=" << this->minute 
                              << " seconde=" << this->seconde
                              << std::endl;
                }
    
                int getH() const {
                    return this->heure;
                }
    
                int getM() const {
                    return this->minute;
                }
    
                int getS() const {
                    return this->seconde;
                }
        }
        ```

!!! note "Exercice"
    === "Réaliser des tests unitaires"
        Pour tester votre classe, instanciez les objets suivantes dans votre IDE préféré :

        - d1 -> 2H, 10m, 57s,
        - d2 -> 0H, 77m, 130s,

        Pouvez-vous instancier :

        - d3 -> sans paramètres,
        - d4 -> 1H,
        - d5 -> 1H, 65m,
        - d6 -> 50m, 60s
        - d7 -> 78s,
        - d8 -> 2H,15s.

    === "Une solution possible"
        ```cpp
        Duree d1(2, 10, 57);
        Duree d2(0, 77, 130);
        Duree d3;
        Duree d4(1);
        Duree d5(1, 65);
        Duree d6(0, 50, 60);
        Duree d7(0, 0, 78);
        Duree d8(2, 0, 15);
        ```

        Sortie console :
        ```console
        Constructeur : heure=2 minute=10 seconde=57
        Constructeur : heure=1 minute=19 seconde=10
        Constructeur : heure=0 minute=0 seconde=0
        Constructeur : heure=1 minute=0 seconde=0
        Constructeur : heure=2 minute=5 seconde=0
        Constructeur : heure=0 minute=51 seconde=0
        Constructeur : heure=0 minute=1 seconde=18
        Constructeur : heure=2 minute=0 seconde=15
        ```

## Surcharge des opérateurs

Vous allez commencer à surcharger des opérateurs. Il faut bien sur que cette surcharge ait un sens par rapport à la classe avec laquelle vous travaillez. Dans notre cas par exemple, il peut être intéressant de comparer deux durées entre elles pour savoir si elles sont égales ou différentes. Vous allez donc surcharger l'opérateur `==` et l'opérateur `!=`.

### Surcharge de l'opérateur `==` pour la classe `Duree`

!!! note "Exercice"

    === "Produire un code"
        Donnez le prototype de cette fonction de surcharge et donnez son code. Avant de vous lancez dans le codage, réfléchissez comment vous comparez deux durées *à la main*...

    === "Une réponse possible"
        Il faut comparer les heures de `d1` avec les heures de `d2`. Idem pour les minutes et secondes. Si ces trois comparaisons sont vraies alors seulement la durée d1 est égale à la durée d2. Les accesseurs de la classe permettent de récupérer la valeur des heures, minutes et secondes.

        ```cpp
        bool operator==(const Duree& d1, const Duree& d2) {
        if ((d1.getH() == d2.getH()) &&
            (d1.getM() == d2.getM()) &&
            (d1.getS() == d2.getS()))
            return true;
        else
            return false;
        }
        ```

!!! note "Exercice"

    === "Réaliser un test unitaire"
        
        Instanciez deux durées (dd10 et dd20) avec les durées que vous voulez puis testez la surcharge de l'opérateur `==`.

    === "Une solution possible"
        
        Dans le code ci-dessous on réalise une comparaison qui échoue et une comparaison qui réussie.
        
        ```cpp
        Duree dd10(1,1,1);
        Duree dd20(2,2,2);
        Duree dd30(2,2,2);

        if (dd10 == dd20) 
            std::cout << "dd10 et dd20 sont egales" << std::endl;
        else 
            std::cout << "dd10 et dd20 sont différentes" << std::endl;

        if (dd30 == dd20) 
            std::cout << "dd30 et dd20 sont egales" << std::endl;
        else 
            std::cout << "dd30 et dd20 sont différentes" << std::endl;
        ```

        Sortie console :

        ```console
        Constructeur : heure=1 minute=1 seconde=1
        Constructeur : heure=2 minute=2 seconde=2
        Constructeur : heure=2 minute=2 seconde=2
        dd10 et dd20 sont différentes
        dd30 et dd20 sont egales
        ```

### Surcharge de l'opérateur `!=` pour la classe `Duree`

!!! note "Exercice"

    === "Produire du code"
        Donnez le protype de cette fonction de surcharge et donnez son code puis testez-le sur des durées `dd10` et `dd20`.

    === "Une correction possible"
        Le prototype de la surcharge de l'opérateur `!=` :
        
        ```cpp
        bool operator!=(const Duree& d1, const Duree& d2) {
        ...
        }
        ```

        L'implémentation de cet opérateur est finalement très simple :
        ```cpp
        bool operateur!=(const Duree& d1, const Duree& d2) {
        return !(d1 == d2);
        }
        ```

    === "Test unitaire"

        Test unitaire du nouvel opérateur `!=` :
        ```cpp
        if (dd10 != dd20) std::cout << "dd10 et dd20 sont différentes" << std::endl;
        else std::cout << "dd10 et dd20 sont égales" << std::endl;

        if (dd30 != dd20) std::cout << "dd30 et dd20 sont différentes" << std::endl;
        else std::cout << "dd30 et dd20 sont égales" << std::endl;
        ```

        Sortie console :
        ```console
        dd10 et dd20 sont différentes
        dd30 et dd20 sont égales
        ```

### Surcharge de l'opérateur `+` pour la classe `Duree`

!!! note "Exercice"

    === "Produire du code"
        L'opérateur `+` peut avoir un sens avec des durées. Vous allez donc l'implémenter puis réaliser un test unitaire sur cette opérateur. Pensez à utiliser ce que vous avez déjà codé...

    === "Une solution possible"

        Une Implémentation de l'opérateur `+` :
        ```cpp
        Duree operateur+(const Duree& d1, const Duree& d2) {
            return Duree( d1.getH() + d2.getH(),
                          d1.getM() + d2.getM(),
                          d1.getS() + d2.getS() );
        }
        ```

    === "Test unitaire"
        Test unitaire du nouvel opérateur `+` sur la classe `Duree` :
        ```cpp
        Duree dd50 = dd10 + dd20;
        std::cout << "dd50: " << dd50.getH() << "H " 
                  << dd50.getM() << "M " 
                  << dd50.getS() << "S"  
                  << std::endl;
        ```

        Sortie console :
        ```console
        Constructeur : heure=3 minute=3 seconde=3
        dd50: 3H 3M 3S
        ```

### Surcharge de l'opérateur `<<` pour la classe `Duree`

!!! note "Exercice"
    === "Produire du code"
        L'opérateur `<<` est tréès intéressant à surcharger car il permet d'employer une méthode de gestion de flux (comme `std::cout` par exemple). Le prototype d'une telle fonction est `std::ostream& operator<<(std::ostream& flux, const Duree& d)`. Implémentez et testez cette surcharge.

    === "Une solution possible"

        Une Implémentation de l'opérateur `<<` :
        ```cpp
        std::ostream& OPERATEUR_FLUX(std::ostream& flux, const Duree& d) {
            flux << "H:" << d.getH() << " M:" << d.getM() << " S:" << d.getS();
            return flux;
        }
        ```

    === "Test unitaire"
        Test unitaire du nouvel opérateur `+` sur la classe `Duree` :
        ```cpp
        std::cout << dd10 << std::endl;
        std::cout << dd20 << std::endl;
        ```

        Sortie console :
        ```console
        H:1 M:1 S:1
        H:2 M:2 S:2
        ```

## Conclusion

Ce document vous a présenté la mise en oeuvre de la surcharge d'opérateurs. Pour aller plus loin, vous pouvez surcharger l'opérateur `-` pour soustraire deux durées par exemple. Vous pouvez aussi lire le cours en ligne de C++ de Christian CASTEYDE : [Surcharge Opérateurs](http://casteyde.christian.free.fr/cpp/cours/online/x3244.html).
