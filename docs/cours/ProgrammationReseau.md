# Communication réseau multiplateforme

!!! tldr "Objectifs du document"
    - Ce document présente la programmation réseau bas niveau à l'aide des **sockets**.
    - C'est la base de la communication de tous les protocoles existants se basant sur **TCP/IP**.
    - Une fois assimilé la notion de **client-serveur** vous pourrez communiquer à travers Internet et réaliser des applications simples comme des jeux.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

!!! info "Référentiel du BTS CIEL"
    - S4. Développement logiciel
        - S4.1 : Principes de base
            - Gestion mémoire : adresse/valeur, pointeurs, variables statiques, allocations automatique et dynamique (pile/tas), etc : niveau 4 (maitrise méthodologique)
            - Variables ; durée de vie, visibilité : niveau 4 (maitrise méthodologique)
            - Flux d’entrée et de sortie de base : terminaux, fichiers, réseau, etc. (spécifications POSIX) : niveau 3 (maitrise d'outils)
        - S4.2 : Algoritmique
            - Modèle canonique de gestion d'E/S : ouvrir, lire, écrire, fermer : niveau 3 (maitrise d'outils)
            - Bibliothèque standard (ANSI C) : niveau 3 (maitrise d'outils)
    - S7. Réseaux, télécommunications et modes de transmission
        - S7.7 : Programmation réseau
            - Concept client/serveur : niveau 4 (maitrise méthodologique)
            - Sockets POSIX : niveau 4 (maitrise méthodologique)

## "Socket" ... quésaco ?

- Communication par socket : **échanger des données** entre plusieurs programmes appartenant à des **systèmes distants** (machines distantes) ou **locaux** (même machine)
- Le terme **socket** désigne :
  - l'API réseau (Application Programming Interface : bibliothèque de fonctions mis à la disposition du programmeur), sous Microsoft Windows cette API s'appelle *WinSock*,
  - le point de communication par lequel le programme émet et reçoit des données dans la pile TCP/IP.

!!! info "Quelques repères historiques..."
    - Les sockets sont apparus pour la 1ère fois en 1983 dans un système équivalent UNIX,
    - Les sockets ont été développés à l'université Berkeley ; on parle de **socket BSD** (Berkeley Software Distribution),
    - L'API socket a été porté sous Windows vers 1991, Winsock 1.1. La version actuelle est Winsock 2.2,
    - L'API Winsock est basé sur les sockets BSD, avec certaines particularités propres à Windows.

## La place des sockets dans le modèle OSI

![La place des sockets dans le modèle OSI](../img/socketPileOSI.png)

Les sockets séparent **les 3 couches supérieures** (qui gèrent les détails de l'application) **des 4 couches inférieures** (qui gèrent la communication).

## Notions de "Client-Serveur"

- Un **serveur** dispose des **ressources** (données issues d'un SGBD, espace de stockage, microservices)
- Le **serveur** fournit un **service** (en manipulant ces ressources) à un ou plusieurs clients.

L'illustration suivante présente les échanges de base entre un client et un serveur. Généralement c'est le client qui initie le dialogue.

![Echange entre un client et serveur](../img/relationClientServeur.png)

## Les caractéristiques principales d'un "socket"

- Le type d'un socket : il définit la fiabilité, le séquencement, la nécessité d'établir une connexion, etc...
    - valeurs possibles que nous utiliserons :
        - `SOCK_RAW` : socket bas niveau, utilisé pour lire tout type de trame réseau (Ex: logiciel WireShark)
        - `SOCK_DGRAM` : socket de type UDP (User Datagram Protocol), mode non connecté et non fiable (Ex: diffusion de flux audio/vidéo -> streaming)
        - **`SOCK_STREAM`** : socket de type TCP (Transfert Control Protocol), mode connecté et fiable (Ex: transfert de fichiers)

- Le domaine d'un socket : il définit l'ensemble des sockets avec lesquels le socket pourra communiquer
    - valeurs possibles pour le domaine du socket :
        - `AF_UNIX` : pour réaliser des communications entre des machines de type UNIX/Linux
        - `AF_APPLETALK` : pour réaliser des communications entre des machines de type Apple (obsolète aujourd'hui)
        - **`AF_INET`** et **`AF_INET6`** : pour réaliser des communications entre n'importe quel type de machine en réseau local et sur Internet

!!! tip "Souvenez-vous..."
    AF signifie Address Family, les textes en gras ci-dessus indique ce qui est employé le plus souvent.

## Connexions de type TCP/IP

Examinons un extrait de la commande 'netstat' qui liste les connexions réseaux actives d'une machine.

Sortie de la commande 'netstat' qui liste les connexions de type TCP avec la résolution de noms pour la première commande, en mode numérique pour la deuxième.

```Console
$ netstat -ap tcp
Active Internet connections (including servers)
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  192.168.10.105.53515   server-143-204-2.https ESTABLISHED
tcp4       0      0  192.168.10.105.53512   clienthosting.eu.ssh   ESTABLISHED
...
$ netstat -anp tcp
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  192.168.10.105.53515   216.58.198.202.443     ESTABLISHED
tcp4       0      0  192.168.10.105.53512   217.160.233.159.22     ESTABLISHED
```

Identifiez le type de connexion et le port associé ci-dessus:

|Adresse IP Src|Port|Adresse IP Dest|Port|Protocol|
|:---:|:----:|:---:|:----:|:---:|
|192.168.10.105|53515|216.58.198.202|443|https|
|192.168.10.105|53512|217.160.233.159|22|ssh|

Chaque connexion sur un ordinateur est identifié par une *paire de socket*. Le diagramme ci-dessous présente ce qui identifie de manière unique et sure une connection réseau :
![Socket entre un client et serveur](../img/socketClientServeur.png)

## Notion de ports

D’après ce que vous venez de voir, chaque connexion est identifiée par un couple adresse IP et port. Le port est une notion virtuelle, il n'a pas d'existence physique sur une machine (il n'y a qu'un seul connecteur ethernet). Le port est un entier non signé codé sur 16 bits ou 2 octets, ce qui se traduit en programmation par un `unsigned short int`. L'étendue des ports possibles est donc de 0 à 2^16 - 1 soit l'intervalle 0 - 65535. Vous ne pouvez pas prendre un port au hasard que vous soyez un client (port tiré au hasard par la pile TCP/IP) ou un serveur (port fixé par le programmeur).

La répartition des ports suit la logique suivante :

- de 0 à 1023 : ports connus (well know ports) assignés par l'[IANA](https://www.iana.org/) (Internet Assigned Numbers Authority). Ce sont les ports sur lesquels les services les plus courants sont en mode écoute (Ex: 80->http, 443->https, 22->ssh, 21->ftp, etc...),
- de 1024 à 49151 : ports enregistrés : ils sont utilisés pour des services qui ne sont pas fixés de manière internationale (Ex: port du serveur Minecraft: ),
- de 49152 à 65535 : ports attribués de manière dynamique par la pile TCP/IP pour un client ou port privé.

Voir le site [frameip.com](https://www.frameip.com/liste-des-ports-tcp-udp/) pour obtenir le nom du service associé à un port.

## Schéma des fonctions C utilisées pour un "Client-Serveur"

Les étapes principales de la mise en oeuvre des fonctions C pour réaliser un client/serveur sont décrites dans le schéma ci-dessous:

![Utilisation API Socket entre un client et serveur](../img/schemaAPIClientServeur.png)

Quelques remarques concernant ce schéma:

- il s'agit d'un échange entre un **client TCP** et un **serveur TCP**,
- dans le cas d'un échange entre un **client UDP** et un **serveur UDP**, les fonctions `connect()` et `accept()` disparaissent (mode non connecté et non fiable), les fonctions `send()` et `recv()` sont respectivement remplacées par `sendto()` et `recvfrom()`,
- dans un serveur il y a toujours deux sockets d'utilisés :
    - une **socket d'écoute** utilisée par les fonctions `bind()`, `listen()` et `accept()`,
    - une **socket de dialogue** créée par la fonction `accept()` et utilisée par les fonctions `send()` et `recv()`
    - c'est pour cette raison qu'il y a deux appels à la fonction `close()` à la fin d'un serveur : un pour la socket de dialogue et un pour la socket d'écoute
- sous **Microsoft Windows**, la fonction `close()` est remplacé par `closesocket()`

## Analyse d'un **client TCP** sous environnement GNU/Linux

Nous allons maintenant étudier le code d'un client simple. Le but de celui-ci est de se connecter à un serveur qui renvoie la date et l'heure. Ce service s'appelle **daytime** et est disponible sur le **port 13**.

Testons la connexion avec la commande **nc** (netcat) vers un serveur daytime hébergé par le [NIST](https://tf.nist.gov/tf-cgi/servers.cgi), cela nous donnera un aperçu de ce que l'on doit recevoir avec notre client:

```console
$ nc -vv 129.6.15.28 13
Connection to 129.6.15.28 13 port [tcp/daytime] succeeded!
59465 21-09-08 07:36:26 50 0 0 391.7 UTC(NIST) * 
```

Il faut commencer par les différents include nécessaire (ici en version unix/linux) au programme ainsi que les constantes :

```cpp
#include <sys/types.h>
#include <sys/socket.h> // socket, recv, send
#include <netinet/in.h> // sockaddr_in, htonl
#include <arpa/inet.h>  // inet_addr
#include <unistd.h>     // close
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

// Les constantes utilisées dans le programme
const unsigned short NUM_PORT = 13;         // Port de connexion vers le serveur
const unsigned short DIMMAX = 150;          // Taille maximum de réception de la réponse du serveur
const string IP_SERVEUR = "129.6.15.28";    // addr IP du serveur Daytime
```

On passe alors au programme *main* et à la premiere étape de notre client : la création du socket avec l'appel à la fonction `socket()`. La documentation de cette fonction est disponible [en ligne](http://man7.org/linux/man-pages/man2/socket.2.html). Le prototype de cette fonction est le suivant : `int socket(int domain, int type, int protocol)` avec domain = `AF_INET`, type = `SOCK_STREAM` ou `SOCK_DGRAM` et protocol = 0. Cette fonction retourne une valeur négative en cas d'erreur sinon la valeur entière qui identifiera le socket. Sous **Microsoft Windows** cette fonction peut retourner la constante `INVALID_SOCKET` en cas d'échec.

```cpp
int ids_client; // id du socket client                          

// tableau contenant les messages provenant du serveur  
char trame_lect[DIMMAX];            

cout << "--- DEBUT du prog" << endl << endl;

// Création du socket client
ids_client = socket(AF_INET, SOCK_STREAM, 0);
if ( ids_client < 0 ) {
  cout << "Echec creation Socket ! "<< endl;
  exit(1);
}
cout << "ids_client : " << ids_client << endl;
```

```console
--- DEBUT du prog
ids_client : 30
```

Deuxième étape de notre client : la connection au serveur daytime avec la fonction `connect()`. 
Mais auparavant il faut renseigner la structure `sockaddr_in`. Celle-ci se présente de la manière suivante:

```Cpp
#include <sys/socket.h>
...
struct sockaddr_in {
    short sin_family;         /* Famille de l’adresse = AF_INET*/
    u_short sin_port;         /* numéro de port */
    struct in_addr sin_addr;  /* Adresse Internet du serveur */
    char sin_zero[8];         /* Champ de 8 zéros */
};
```

La documentation de cette fonction est disponible [en ligne](http://man7.org/linux/man-pages/man2/connect.2.html). Le prototype de cette fonction est le suivant : `int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)` avec sockfd = le socket créé précédemment, une structure sockaddr_in que l'on "caste" en sockaddr et la taille de la structure sockaddr_in. Cette fonction retourne une valeur négative en cas d'erreur ou 0 en cas de succès.

```cpp
struct sockaddr_in adr_serveur; // @ internet du serveur

// Mise en place de l'@ inet et demande de connexion au serveur
adr_serveur.sin_family = AF_INET;       // Domaine d'@
adr_serveur.sin_port = htons(NUM_PORT); // N° du port
adr_serveur.sin_addr.s_addr = inet_addr(IP_SERVEUR.c_str()); // @IP du serveur

int res_connexion = connect(ids_client, // identifiant de notre socket
        (struct sockaddr *)&adr_serveur, // la structure qui contient les renseignements du serveur
        sizeof(adr_serveur)); // la taille de la structure sockaddr_in

if ( res_connexion < 0 ) {
  cout << "Echec connexion ! " << endl;
  exit(1);
}
cout << "res_connexion : " << res_connexion << endl; // '0' signifie que la connection est effective
```

```console
res_connexion : 0
```

Normalement dans un client classique, vous envoyez votre demande avec l'appel `send()`. Ici le serveur daytime renvoie directement la date et l'heure sans avoir besoin de faire une requête. Cette étape n'existe donc pas ici dans notre client très simple.

Cependant il faut consulter [sa documentation](http://man7.org/linux/man-pages/man2/send.2.html) pour mettre en oeuvre les TPs client/serveur. Le prototype de cette fonction est le suivant : `ssize_t send(int sockfd, const void *buf, size_t len, int flags)` avec sockfd = le socket créé précédemment, *buf = un pointeur vers un tableau de caractères généralement, len = le nombre d'octets à transmettre du tableau passé en paramètre, flags = 0 pour nous. Cette fonction retourne une valeur négative en cas d'erreur ou le nombre d'octets transmis.

Nous passons donc directement à l'étape suivante : la réception des données renvoyées par le serveur avec l'appel `recv()`. La documentation est disponible [en ligne](http://man7.org/linux/man-pages/man2/recv.2.html). Son prototype est le suivant : `ssize_t recv(int sockfd, void *buf, size_t len, int flags)` avec sockfd = le socket créé précédemment, *buf = l'adresse d'un buffer de réception, généralement un tableau de caractères, len = la taille maximum de stockage du tableau précédent, flags = 0. Cette fonction retourne une valeur négative en cas d'erreur ou le nombre d'octets reçus.

```cpp
// Reçoit l'heure du serveur
int noctets = recv(ids_client, trame_lect, DIMMAX, 0);
trame_lect[noctets]= '\0'; // on rajoute un '0' à la fin de notre chaine de type C (marqueur de fin de chaine)

// Affichage des données reçues
cout << "Longueur de la chaine recue = " << noctets << endl;
cout << "Trame recue du serveur = " << trame_lect << endl;
```

```Console
Longueur de la chaine recue = 51
Trame recue du serveur = 
59092 20-08-31 07:45:10 50 0 0 214.7 UTC(NIST)
```

Enfin vous arrivez à la dernière étape, la fin de l'échange avec le serveur. On termine notre connection avec l'appel `close()`. La documentation est [en ligne](http://man7.org/linux/man-pages/man2/close.2.html). Le prototype est le suivant et nécessite l'inclusion de `#include <unistd.h>` : `int close(int fd)` avec fd = le socket créé précédemment. Cette fonction retourne une valeur négative en cas d'erreur ou 0 le socket a été correctement fermé.

```cpp
// fermeture du socket ouvert
close(ids_client);
cout << ">>>>>> APRES fonction close <<<<<<" << endl;
cout << "--- FIN du prog" << endl;
```

```Console
>>>>>> APRES fonction close <<<<<<
--- FIN du prog
```

## Analyse d'un **serveur TCP** sous environnement GNU/Linux

Il faut reprendre le schéma "Client/Serveur" pour identifier les nouvelles fonctions à mettre en oeuvre:

- la fonction `bind()`,
- la fonction `listen()`,
- la fonction `accept()`.

Chacune des fonctions ci-dessus va être présentés dans les lignes qui suivent puis vous aurez un exemple complet de code d'un serveur fonctionnant sous GNU/Linux.

!!! warning "**Important**"
    La socket créée au début du programme serveur par l'appel `socket()` sera appelée *socket_ecoute*.

### La fonction bind()

Cette fonction vient immédiatement après la création du socket. Elle permet d'associer (bind = lier) un couple adresse IP/port avec l'identifiant de la socket. La documentation de cette fonction est disponible [en ligne](http://man7.org/linux/man-pages/man2/bind.2.html). Son prototype est le suivant : `int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)` avec sockfd = l'identifiant de la socket_ecoute créée à l'étape précédente, *addr = un pointeur vers une structure `sockaddr_in` que l'on "castera" en pointeur de type `sockaddr` et enfin addrlen qui est la taille de la structure `sockaddr_in`. Cette fonction retoune une valeur négative en cas d'erreur ou 0 si l'attachement a réussi. Sous **Microsoft Windows** cette fonction peut retourner la constante `SOCKET_ERROR` en cas d'échec.

### La fonction listen()

Cette fonction permet de passer la socket en mode "écoute", ce qui permettra à un client de se connecter. La documentation de cette fonction est disponible [en ligne](http://man7.org/linux/man-pages/man2/listen.2.html). Son prototype est le suivant : int listen(int sockfd, int backlog) avec sockfd = l'identifiant de la socket_ecoute et backlog = c'est un entier la longueur de la file d'attente de clients qui peuvent se connecter. Cette file d'attente est de FIFO (First In, First Out = file d'attente à une caisse de supermarché). **ATTENTION** mettre un chiffre supérieur à 1 sur ce paramètre ne permet pas de traiter plusieurs clients **simultanement** mais les uns aprés les autres. Cette fonction retoune une valeur négative en cas d'erreur ou 0 si elle a réussi. Sous **Microsoft Windows** cette fonction peut retourner la constante `SOCKET_ERROR` en cas d'échec.

### La fonction accept()

Cette fonction réalise la connection avec un client (qui faisait éventuellement la queue au niveau de la fonction `listen()`). Cette fonction est **bloquante**, c'est à dire que votre programme s'arrête et attend une connection. Dans le cas du serveur TCP c'est cette étape qui réalise l'échange en poignée de main en trois temps du mode connecté (voir cours réseau). La documentation de cette fonction est disponible [en ligne](http://man7.org/linux/man-pages/man2/accept.2.html). Son prototype est le suivant : `int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)` avec sockfd = l'identifiant de la socket_ecoute créée au début du programme, \*addr = un pointeur vers une structure `sockaddr_in`. Attention, il s'agit ici d'une nouvelle structure `sockaddr_in` qui permettra de stocker les caractéristiques du **client** qui vient de se connecter (adresse ip, port). \*addrlen est un pointeur vers la taille de la structure. Cette fonction retoune une valeur négative en cas d'erreur ou l'identifiant d'une **nouvelle socket**, appelée **socket_dialogue**, en cas de réussite.

Cette nouvelle **socket_dialogue** sera donc utilisée par les fonctions `recv()` et `send()` pour dialoguer avec le client. A la fin du dialogue avec le client, il faudra fermer cette **socket_dialogue**.

!!! tip "Notion de serveur *itératif* ou *concurent*"
    Une fois que le client est "accepté" par le serveur, celui-ci peut le traiter de deux manières différentes:

    - il gère lui-même les échanges avec le client. Un seul client est servi en même temps : On parle de ***serveur itératif***,
    - il peut créer un **processus fils** ou un **thread** et demander à ce dernier de gérer les échanges avec le client. Ainsi, le serveur (ou le thread principal) peut continuer à "écouter" d'autres clients : on parle de ***serveur concurrent***.

## Quelques remarques complémentaires

Il existe deux représentations en mémoire des variables :

- soit on commence par les octets de poids forts : on parle d'architecture **BigEndian**,
- soit on commence par les octets de poids faibles : on parle d'architecture **LittleEndian**.

La plupart des processeurs d'ordinateurs personnels (Intel, AMD) utilisent une architecture **LittleEndian**. Les téléphones portables, les tablettes, les "RaspberryPi" utilisent une architecture **BigEndian**. Tous ces matériels envoient des données vers Internet, il faut donc se mettre d'accord pour dire quels octets on envoie en premier.

Internet est de type **BigEndian**. Il faut donc utiliser des fonctions qui rendent transparentes ces manipulations d'octets. Pour cela il faut employer les fonctions suivantes:

- `htons()` : Host to Network Short -> transformation du format de la machine hôte vers le format réseau d'un entier court (2 octets),
- `htonl()` : Host to Network Long -> transformation du format de la machine hôte vers le format réseau d'un entier long (8 octets),
- `ntohs()` : Network to Host Short -> fonctions inverses des précédentes
- `ntohl()` : Network to Host Long

Les sockets ont été implémentés dans de nombreux langages. Vous pouvez donc avoir un client Java sur un téléphone portable qui communique avec un serveur qui fonctionne avec un processeur Intel et dont le programme est réalisé en langage C++.

Les sockets existent dans des framework de haut niveau comme .Net ou Qt. Généralement ces langages fournissent des classes de haut niveau qui permettent de réduire le temps de développement (classes TcpClient, TcpServer, FTPClient, FTPServer, etc...). 
Dans vos projets industrielles vous pourrez utiliser de tel objet si vous le souhaitez. Par contre, les sockets peuvent être demandés à l'écrit du BTS (avec de la documentation).

## Exemple de code **serveur** sous GNU/Linux

Vous trouverez ci-dessous le code d'un serveur TCP sous GNU/Linux. Basiquement il renvoie la date et l'heure lors de la connection d'un client sur son port 55555. Il vous faudra un client TCP (par exemple Herkule sous Microsoft Windows, la commande nc sous GNU/Linux ou encore le code du client précédent) pour vous connecter sur celui-ci. L'adresse IP sera donné par le professeur.

```cpp
#include <sys/types.h>
#include <sys/socket.h> // socket, recv, send
#include <netinet/in.h> // sockaddr_in, htonl
#include <arpa/inet.h>  // inet_addr
#include <unistd.h>     // close
#include <cstring>      // memset
#include <cstdlib>
#include <iostream>

using namespace std;

const unsigned short PORT_NUM = 55555; // Port sur lequel écoute le serveur

int main(void) {

    struct sockaddr_in adr_serveur; // @ internet du serveur
    struct sockaddr_in adr_client; // @ internet du client

    int ids_ecoute; // id du socket d'écoute du serveur
    int ids_dialogue; // id du socket de dialogue

    unsigned int addr_len; // taille de l'@ internet

    cout << "--- DEBUT du prog " << endl;

    // Création du socket d'écoute
    ids_ecoute = socket(AF_INET, SOCK_STREAM, 0);
    if (ids_ecoute < 0) {
        cout << "Echec creation Socket ! " << endl;
        exit(1);
    }
    cout << "--- Socket ecoute OK " << endl;

    // RAZ de la structure contenant @ inet serveur
    memset(&adr_serveur, 0, sizeof (adr_serveur));

    // Mise en forme de l'@ du socket d'écoute
    // et attachement du socket
    adr_serveur.sin_family = AF_INET; // Domain d'@
    adr_serveur.sin_port = htons(PORT_NUM); // N° du port
    adr_serveur.sin_addr.s_addr = htonl(INADDR_ANY); // n'importe quel interface réseau = INADDR_ANY

    int res_attachement = bind(ids_ecoute,
            (struct sockaddr *) &adr_serveur, sizeof (adr_serveur));
    if (res_attachement < 0) {
        cout << "Echec Attachement Socket ! " << endl;
        exit(1);
    }
    cout << "--- Attachement socket OK " << endl;

    // Passage du socket en mode écoute
    // (1: une connexion au max)
    int res_ecoute = listen(ids_ecoute, 1);
    if (res_ecoute < 0) {
        cout << "Echec Lecture ! " << endl;
        exit(1);
    }
    cout << "--- Passage en mode ecoute du socket OK " << endl;

    // Acceptation d'une connexion cliente,
    // création d'un nouveau socket qui
    // sera utilisé pour l'émission et la réception des caractères
    // @ inet du client est récupérée
    cout << "--- En attente de connection client " << endl;
    addr_len = sizeof (adr_client);
    ids_dialogue = accept(ids_ecoute, (struct sockaddr *) &adr_client, &addr_len);
    cout << "--- Connection nouveau client " << endl;

    // On fabrique la chaîne à transmettre contenant l'heure que l'on va envoyer à notre client
    time_t tempsCourant = time(0);
    string message = asctime(localtime(&tempsCourant));

    int nb_car_emis = send(ids_dialogue, message.c_str(), message.size(), 0);
    if (nb_car_emis < 0) {
        cout << "Echec transmission " << endl;
        exit(1);
    }
    cout << "nb car emis par ce serveur : " << nb_car_emis << endl;
    cout << "trame transmise par ce serveur: " << message << endl;

    // fermeture des sockets ouverts
    close(ids_dialogue); // d'abord la socket de dialogue
    close(ids_ecoute);   // puis la socket d'écoute
    cout << "--- Fermeture des socket OK " << endl;

    cout << "--- FIN du prog" << endl;
    return 0;
}
```

## Exemple de code **client** sous Microsoft Windows

Le code ci-dessus reprend les différentes étapes d'un client sous GNU/Linux. Des particularités apparaissent au niveau des "include" et quelques lignes spécifiques précisées dans le code.
Vous pouvez le copier/coller dans un nouveau projet console Visual Studio.
Il ne compilera pas directement, des erreurs vont apparaitre, à vous de lire les messages d'erreurs et de corriger.

```Cpp
#include <winsock2.h>

#include <iostream>
#include <string>
using namespace std;

typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned char uchar;

const ushort NUM_PORT = 13;
const ushort DIMMAX = 150; // Taille max des tableaux
const string IP_SERVEUR = "129.6.15.28";

int main()
{
   uint n = 0, noctets;

   // Particularités Windows
   WORD nVersion = MAKEWORD(2,2);       
   WSADATA donneeWS;                              

   struct sockaddr_in  adr_serveur; // @ internet du serveur

   uint  ids_client;   // id du socket client                  
   // tableau contenant les messages provenant du serveur  
   char trame_lect[DIMMAX + 1];        

   // initialisation du winsock
   WSAStartup(nVersion, &donneeWS);

   cout << "--- DEBUT du prog" << endl << endl;

   // Création du socket client
   if ( (ids_client = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
   {
       cout << "Echec creation Socket ! "<< endl;
       exit(1);
   }

   // Mise en place de l'@ inet et demande de connexion au serveur 
   adr_serveur.sin_family      = AF_INET;                  // Domaine d'@ 
   adr_serveur.sin_port        = htons(NUM_PORT);          // N° du port 
   adr_serveur.sin_addr.s_addr = inet_addr(IP_SERVEUR.c_str()); // @IP du serveur
   if ( connect(ids_client, (struct sockaddr *)&adr_serveur, 
                     sizeof(adr_serveur)) < 0)
   {
       cout << "Echec connexion ! " << endl;
       exit(1);
   }
   
   // Reçoit l'heure du serveur
   noctets = recv(ids_client, trame_lect, DIMMAX, 0);
   trame_lect[noctets]= '\0';

   // Affichage des données reçues
   cout << "Longueur de la chaine recue = " << noctets << endl;
   cout << "Trame recue du serveur = " <<  trame_lect << endl;

   // fermeture le socket ouvert
   closesocket(ids_client); // à la place de close() sous Linux

   // Fermeture de winsock
   WSACleanup();

   cout << "--- FIN du prog" << endl;

   cin.get();
   return 0;
}
```

## Exemple de code **serveur** sous Microsoft Windows

Le code ci-dessous reprend les différentes étapes d'un serveur sous GNU/Linux. Des particularités apparaissent au niveau des "include" et quelques lignes spécifiques précisées dans le code.
Vous pouvez le copier/coller dans un nouveau projet console Visual Studio.
Il ne compilera pas directement, des erreurs vont apparaitre, à vous de lire les messages d'erreurs et de corriger.

```Cpp
#include <winsock2.h>
#include <time.h>
#include <iostream>
#include <string>
using namespace std;

typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned char uchar;

const ushort PORT_NUM = 55555;

int main(void)
{
   WORD nVersion = MAKEWORD(2,2);       
   WSADATA donneeWS;                              

   struct sockaddr_in   adr_serveur;       // @ internet du serveur
   struct sockaddr_in   adr_client;        // @ internet du client

   uint ids_ecoute;        // id du socket d'écoute du serveur
   uint	ids_connect;       // id du socket de connection
   uint nb_car_emis;       // nb car émis par send

   int addr_len;          // taille de l'@ internet 

   // initialisation du winsock
   WSAStartup(nVersion, &donneeWS);

   cout << "--- DEBUT du prog " << endl;

   // Création du socket d'écoute
   if ( (ids_ecoute = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
   {
       cout << "Echec creation Socket ! "<< endl;
       exit(1);
   }

   // RAZ de la structure contenant @ inet serveur
   memset(&adr_serveur, 0, sizeof(adr_serveur));
   // Mise en forme de l'@ du socket d'écoute et attachement du socket
   adr_serveur.sin_family      = AF_INET;            // Domain d'@ 
   adr_serveur.sin_port        = htons(PORT_NUM);    // N° du port 
   adr_serveur.sin_addr.s_addr = htonl(INADDR_ANY);  // n'importe quel
                                                     // interface réseau
   
   if ( bind(ids_ecoute, (struct sockaddr *)&adr_serveur, 
                                sizeof(adr_serveur)) == SOCKET_ERROR)
   {
       cout << "Echec Attachement Socket ! "<< endl;
       exit(1);
   }

   // Mise à disposition du socket (service) (1: une connection au max)
   if ( listen(ids_ecoute, 1) == SOCKET_ERROR)
   {
       cout << "Echec Lecture ! "<< endl;
       exit(1);
   }

   // Acceptation d'une connexion cliente, création d'un nouveau socket qui 
   // sera utilisé pour l'émission et la réception des caractères 
   // @ inet du clietnt est récupérée
   addr_len = sizeof(adr_client);
   ids_connect = accept(ids_ecoute, (struct sockaddr *)&adr_client, &addr_len);

   // On fabrique la chaîne à transmettre contenant l'heure
   time_t tempsCourant = time(0);
   string message = asctime( localtime(&tempsCourant) );

   if( (nb_car_emis = send(ids_connect, message.c_str(), 
                               message.size()+1, 0)) == SOCKET_ERROR)
   {
       cout << "Echec transmission "<< endl;
       exit(1);
   }
   cout << "nb car emis par ce serveur : " << nb_car_emis << endl;
   cout << "trame transmise par ce serveur: "<< message << endl;

   // fermeture des sockets ouverts
   closesocket(ids_ecoute);
   closesocket(ids_connect);

   // Fermeture de winsock
   WSACleanup();

   cout << "--- FIN du prog" << endl;
   cin.get();
   return 0;
}
```
