# Les outils du développeur

!!! tldr "Objectifs du document"
    - Ce document présente un panel d'outils (non exhaustif, partial, orienté C/C++) qu'un développeur se doit de maîtriser pour prétendre accéder à un grade *avancé*.
    - Cette présentation va suivre le développement logique d'une application logicielle : de l'idée jusqu'à son utilisation.
    ---
    Enseignant: Olivier DARTOIS - BTS CIEL - Lycée Turgot - Limoges

## Formaliser son idée, rédiger un cahier des charges

Lorsque vous concevez une application vous devez expliciter ce qu'elle fera. Vous regrouperez vos idées dans une **carte mentale** (en anglais **mindmap**) : le logiciel que nous utilisons est [MindView](https://www.matchware.com/fr/logiciel-de-mind-mapping) mais il existe des équivalents gratuits ([XMind](https://www.xmind.net/) par exemple). 

Vous devrez aussi présenter vos idées avec des **schémas SySML/UML** : le logiciel retenu pour cette tâche est [MagicDraw](https://www.nomagic.com/products/magicdraw) mais vous pouvez aussi utiliser des logiciels plus simples tel que [draw.io](https://app.diagrams.net/). 

Vous pouvez avoir une **interface graphique** pour votre logiciel, il faudra donc réaliser des prototypes d'IHM (Interface Homme Machine) : un logiciel comme [Pencil](https://pencil.evolus.vn/) peut vous aider. 

## Saisir son code source

Quelque soit le langage choisi, vous commencez toujours par saisir un code source au format textuel. Pour faciliter cette saisie, vous ferez appel à des **éditeurs de texte**. Cependant la plupart de ces éditeurs sont incapables avec *leurs fonctionnalités de base* de compiler ou de gérer un projet:

- Editeurs basique : [nano](https://www.nano-editor.org/) sous GNU/Linux, [notepad](https://www.microsoft.com/fr-fr/p/windows-notepad/9msmlrh6lzf3#activetab=pivot:overviewtab) sous Microsoft Windows,
- Editeurs avancés : [geany](https://www.geany.org/) sous GNU/Linux, [notepad++](https://notepad-plus-plus.org/) sous Microsoft Windows,
- Editeurs professionnels : [vim](https://www.vim.org/) ou [emacs](https://www.gnu.org/software/emacs/) sous GNU/Linux, [Visual Studio Code](https://code.visualstudio.com/) sous Microsoft Windows.

Pour avoir une gestion avancée de projets, il faut utiliser des **environnements de développement intégré** ou **EDI** (**IDE** en anglais). Les plus courants sont:

- sous Microsoft Windows : [Visual Studio Community Edition](https://visualstudio.microsoft.com/fr/vs/community/), [QtCreator](https://www.qt.io/product/development-tools), [Netbeans](https://netbeans.apache.org/), [Eclipse](https://www.eclipse.org/downloads/),
- sous Apple Mac OS : identique avec en plus [XCode](https://apps.apple.com/fr/app/xcode/id497799835?mt=12),
- sous GNU/Linux : identique sauf Visual Studio et XCode.

!!! warning "Attention"
    la prise en main d'un EDI est généralement plus complexe qu'un éditeur.

Une remarque sur les polices de caractères à employer pour coder. Il ne faut pas se contenter de la police par défaut du logiciel car celle-ci n'est pas forcement prévue pour un programmeur (par exemple : 0 (chiffre 0) et O (lettre o majuscule), l (lettre l minuscule) et 1 (chiffre 1), etc...). Je vous conseille donc d'utiliser une police spécifique, par exemple [Mononoki](http://madmalik.github.io/mononoki/) mais il en existe d'autres (lire cet [article](https://sebsauvage.net/wiki/doku.php?id=polices-pour-developpeurs)).

## Compiler son code source

Il existe deux manières de compiler son projet:

1. Créer un projet dans un EDI : les différents fichiers du projets seront regroupés dans une arborescence gérée par l'EDI. Il faudra modifier les propriétés du projet pour qu'il trouve les bibliothèques externes, rajouter les fichiers `.lib` nécessaires, peut être préciser des options de compilation particulières. Tout cela est réalisé au travers d'une interface graphique et de multiples fenêtres. De prime abord cela peut sembler plus simple mais au final il faut maîtriser l'environnement de développement pour ne rien oublier. Une fois le projet configuré, il suffira alors de *cliquer sur un bouton* pour générer l'exécutable final.
2. Gérer son projet en ligne de commande :
    1. en tapant directement la commande de compilation : cela peut convenir pour des projets très simple de quelques fichiers, réalisable aussi bien sous Microsoft Windows que GNU/Linux,
    2. en utilisant des commandes spécialisés pour réaliser la compilation : *make* et son fichier *Makefile.txt* associé ou encore *cmake* et son fichier *CMakeList.txt* associé. Ces commandes sont disponibles avec tous les systèmes d'exploitation même si la mise en oeuvre est généralement plus facile sous GNU/Linux.

### Microsoft Visual Studio : création d'un projet simple en C++

!!! warning "Attention"
    **Niveau minimum à acquérir**

La procédure ci-après décrit la création d'un projet console vide en C++ avec **Microsoft Visual Studio 2019 ou supérieur** :

1. Lancez Visual Studio 2019,
2. Choisir "Créer un projet",
3. Chercher "Projet vide", "Console" et "C++" puis cliquez sur le bouton "Suivant",
4. Donnez un nom à votre projet, vérifiez l'emplacement de création du projet sur le disque dur puis cliquez sur le bouton "Créer",
5. L'interface de Visual Studio se lance, pour rajouter des fichiers à votre projet, cliquez avec le bouton droit de la souris sur "Fichiers sources" dans la fenêtre "Explorateur de solution". 
6. Sélectionnez "Ajouter" puis une des options proposées parmi "Nouvel élément", "Élément existant", "Classe"

### Commande *make*

!!! warning "Attention"
    **Niveau intermédiaire, c'est un vrai plus pour votre cursus.**

La commande [*make*](https://www.gnu.org/software/make/) permet d'automatiser les étapes de compilation de votre projet en ligne de commande. Elle est principalement utilisée en environnement type Unix/Linux. Un fichier *Makefile.txt* contient les différentes directives qui seront exploitées par la commande *make*. Généralement pour compiler un projet, on peut se contenter de deux commandes enchaînées :

- `make clean` : nettoie le projet
- `make` : compile le projet et génère un exécutable

Un exemple de fichier *Makefile.txt* est disponible en annexe et peut servir de base pour vos projets.

### Commande *cmake*

!!! warning "Attention"
    **Niveau expert, à mettre en avant dans votre CV.**

CMake est un outil qui permet de générer automatiquement des fichiers de configuration pour la commande *make* (un fichier *Makefile.txt*), pour *MSBuild* l'outil de construction de Microsoft voire même des projets pour des EDI (par exemple Visual Studio). Voila pourquoi CMake est très employé en milieu professionnel (très souvent avec l'outil ninja).

Cet outil permet aussi de construire "out of source", c'est à dire de ne pas polluer votre répertoire qui contient vos fichiers cpp, h par des résidus de compilation et autres fichiers intermédiaires. Pour cela il faut construire une arborescence standard pour votre projet comme indiqué en annexe.

Une fois cette arborescence réalisée et les fichiers sources et CMakeLists.txt à leur place. Il suffit de faire les commandes suivantes:

- `cd build` : déplacement dans le répertoire de construction,
- `cmake ..` : exécution de l'outil cmake sur le fichier CMakeList.txt à la base du projet,
- `make` : compilation du projet en utilisant la commande *make* et le fichier *Makefile.txt* généré par l'outil cmake.

Un exemple basique de fichiers CMakeLists.txt est disponibles en annexe et peut là aussi servir de base à vos projet.

## Utiliser les bibliothèques standards

En C++, la bibliothèque la plus utilisée s'appelle la **STL** pour Standard Template Library. Cette bibliothèque propose de très nombreuses classes génériques pour couvrir le maximum de cas d'usage standard rencontré couramment. Avant de réinventer la roue, regardez si votre problème n'est pas résolu par une classe existante. Pour parcourir la STL rendez-vous sur le site [c++reference](https://cppreference.com) ou [cplusplus.com](http://www.cplusplus.com/) (attention le contenu est non libre). Les liens des classes suivantes pointent vers la version anglaise (plus complète que le français) du site C++ Reference.

Parmi toutes les classes existantes, vous mettrez en oeuvre:

- la classe [`iostream`](https://en.cppreference.com/w/cpp/header/iostream) : saisie utilisateur, affichage dans une console,
- la classe [`string`](https://en.cppreference.com/w/cpp/string/basic_string) : gestion des chaines de caractères,
- la classe [`vector`](https://en.cppreference.com/w/cpp/container/vector) : gestion de collections de données, peut s’apparenter à des tableaux *élastiques*,
- la classe [`fstream`](https://en.cppreference.com/w/cpp/header/fstream) : gestion des fichiers en sortie [`ofstream`](https://en.cppreference.com/w/cpp/io/basic_ofstream) et en entrée [`ifstream`](https://en.cppreference.com/w/cpp/io/basic_ifstream).

Les autres classes utilisées de manière classiques :

- la classe [`chrono`](https://en.cppreference.com/w/cpp/header/chrono) : gestion du temps et des dates,
- la classe [`random`](https://en.cppreference.com/w/cpp/header/random) : tirage de nombre au hasard,
- la classe [`exception`](https://en.cppreference.com/w/cpp/error) : gestion des erreurs,
- la classe [`thread`](https://en.cppreference.com/w/cpp/header/thread) : gestion du multitâche,
- la classe [`algoritm`](https://en.cppreference.com/w/cpp/header/algorithm) : algorithmes standards : tris, recherche, remplacements, etc...

Une autre bibliothèque vient compléter la STL, il s'agit de [**Boost**](https://www.boost.org/). La plupart du temps ce qui est proposé dans boost sera inclus dans la STL au fur et à mesure des évolutions du langage C++.

## Gérer les versions de son code source

!!! warning "Attention"
    **A mettre dans votre CV.**

Pas d'hésitation : un logiciel incontournable sur toutes les plateformes : [git](https://git-scm.com/). Ce logiciel s'utilise en ligne de commande. Il a été programmé par Linus Torvalds (le créateur de Linux) et il s'adresse avant tout à des utilisateurs expérimentés. Vous trouverez en annexe la liste des commandes minimales à connaître pour utiliser git. Vous pouvez aussi suivre le tutoriel d'[openClassRooms sur git](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git).

Tous les environnements de développement ainsi que les éditeurs de code avancés sont interfaçables avec git. Cependant pour faciliter la vie du développeur, on peut aussi gérer son code (et bien plus) avec des plateformes en ligne. Les deux plus connues sont certainement :

- [GitHub](https://github.com) : site qui héberge énormément de projets (85 millions, open source ou non). Maintenant propriété de Microsoft depuis juin 2018, acheté pour 7.5 milliard de dollars,
- [GitLab](https://gitlab.com) : concurrent direct de GitHub, sa popularité a fortement augmenté ces derniers mois. Une version opensource existe et est installable sur un serveur personnel.

L'utilisation de GitLab fera l'objet d'une présentation individuelle aux étudiants car ils doivent l'utiliser durant leur scolarité.

## Documenter son code

Il existe de nombreuses solutions pour documenter son code mais une seule solution est exposée ici : [Doxygen](https://www.doxygen.nl/index.html). Il suffit de rajouter dans les commentaires de votre code des *balises doxygen* qui permettent de documenter une fonction, une classe, un fichier, etc... Vous n'avez donc pas à créer un fichier supplémentaire pour la documentation.

Vous pouvez aussi ne rien documenter dans votre code et utiliser Doxygen pour générer une documentation minimaliste mais exploitable. Les formats de sorties sont multiples mais principalement nous en utiliserons deux :

- le format PDF : destiné à être imprimé,
- le format HTML : le plus souvent employé, pour réaliser la mise en ligne de la documentation.

Doxygen est [téléchargeable](https://www.doxygen.nl/download.html) pour Microsoft Windows, MacOS ainsi que les distributions GNU/Linux.

Vous pouvez consulter le [tutoriel officiel](https://www.doxygen.nl/manual/starting.html) mais aussi consulter avec profit l'article sur Doxygen sur [Developpez.com](https://www.doxygen.nl/manual/starting.html).

## Réaliser des tests unitaires

Vous devez *normalement* réaliser des tests sur les différentes parties de votre programme. Cela permet de valider votre code en fonction du cahier des charges fourni. Cela permet aussi de vérifier qu'il n'y a pas de régression dans une nouvelle version d'un programme.

Ces tests doivent être le plus souvent automatisés pour:

- être validés le plus rapidement possible, donc que les modifications apportées au programme se retrouve dans la version d'exploitation rapidement : intégration continue (Continuous Integration dans GitLab/GitHub),
- pilotés le développement : on rédige les tests avant même d'écrire une seule ligne de code. En anglais : Test Driven Developpement.

Pour le langage C++, il existe deux solutions : [catch2](https://github.com/catchorg/Catch2) ou [google tests](https://github.com/google/googletest). Nous étudierons plus spécifiquement **catch2** lors d'un TP sur la réalisation d'une classe.

Vous pouvez aussi lire la [documentation officielle](https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top) de catch2 et un [article](https://medium.com/dsckiit/a-guide-to-using-catch2-for-unit-testing-in-c-f0f5450d05fb) en anglais sur celui-ci.

Vous pouvez aussi faire de l'[analyse statique de code](https://fr.wikipedia.org/wiki/Analyse_statique_de_programmes), pour cela il est conseillé d'utiliser un outil tel que [cppcheck](http://cppcheck.sourceforge.net/). Ce logiciel est téléchargeable sur toutes les plateformes et fera un compte rendu sur les parties qu'il estime dangereuses de votre code.

## Tester son exécutable

Vous testez en condition d'utilisation réelle votre programme. Si votre programme *plante* malgré les tests unitaires précédents, il va falloir certainement le **déverminer** (**debugger** en anglais). Pour cela, la plupart des EDI ont un mode d'exécution pas à pas de votre programme.

Le plus connu des *debuggeurs* est [GDB](https://www.gnu.org/software/gdb/). Il permet le déverminage en ligne de commande en local sur votre exécutable, à travers une liaison série ou ethernet pour l'embarqué. C'est aussi celui qui se cache derrière le mode pas à pas de certains EDI.

L'utilisation du **debuggeur** ne sera pas présenté en cours mais au cas par cas en TP.

Pour détecter les fuites mémoires de votre programme, il est conseillé d'utiliser un logiciel du type [valgrind](https://valgrind.org/). Celui-ci vous permettra d'identifier plus facilement quel morceau de code n'a pas restitué la mémoire qu'il a alloué.

## Les forums de discussions

Il faut apprendre à chercher l'information de manière efficace sur Internet. Il faut donc se concentrer sur des forums réputés en langage C++. En effet il est fort possible que votre problème de code ait été déjà abordé par d'autres. 

- en anglais : consultez l'excellent site [stackoverflow](https://stackoverflow.com/). Attention cependant, si vous désirez poster sur ce forum assurez-vous que votre question n'existe pas déjà sinon vous n'aurez aucune aide voire des commentaires désagréables,
- en français : consultez les forums de [developpez.net](https://www.developpez.net/forums/f19/c-cpp/cpp/) ou d'[openclassroom](https://openclassrooms.com/forum/categorie/langage-c-1).

## Annexes

Dans les annexes nous détaillons ce qui a été présenté auparavant.

### Commande *make* et exemple de fichier *Makefile.txt*

Un exemple de fichier *Makefile.txt* est disponible ci-dessous. Il est commenté et s'applique ici dans le cadre d'un projet qui met en oeuvre la bibliothèque multimédia SFML:

```make
#
# 'make depend' uses makedepend to automatically generate dependencies 
#               (dependencies are added to end of Makefile)
# 'make'        build executable file 'mycc'
# 'make clean'  removes all .o and executable files
#

# define the C compiler to use
CC = g++

# define any compile-time flags
CFLAGS = -Wall

# define any directories containing header files other than /usr/include
#
INCLUDES =  #-I/home/newhall/include  -I../include

# define library paths in addition to /usr/lib
#   if I wanted to include libraries not in /usr/lib I'd specify
#   their path using -Lpath, something like:
LFLAGS = #-L/home/newhall/lib  -L../lib

# define any libraries to link into executable:
#   if I want to link in libraries (libx.so or libx.a) I use the -llibname 
#   option, something like (this will link in libmylib.so and libm.so:
LIBS = -lsfml-system -lsfml-graphics -lsfml-window

# define the C source files
SRCS = varicelle.cpp

# define the C object files 
#
# This uses Suffix Replacement within a macro:
#   $(name:string1=string2)
#         For each word in 'name' replace 'string1' with 'string2'
# Below we are replacing the suffix .c of all words in the macro SRCS
# with the .o suffix
#
OBJS = $(SRCS:.c=.o)

# define the executable file 
MAIN = varicelle

#
# The following part of the makefile is generic; it can be used to 
# build any executable just by changing the definitions above and by
# deleting dependencies appended to the file from 'make depend'
#

.PHONY: depend clean

all:    $(MAIN)
		@echo $(MAIN) "compilé"

$(MAIN): $(OBJS) 
		$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file) 
# (see the gnu make manual section about automatic variables)
.c.o:
		$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
		$(RM) *.o *~ $(MAIN)
```

### Commande *cmake*, arborescence et exemple de fichier *CMakeLists.txt*

Un projet utilisant **cmake** peut avoir la structure arborescente suivante:

![Arborescence conseillée pour un projet CMake](../img/ArboCMake.png)

Il faut donc créer un répertoire pour votre projet (ici **TestCMakeSFML**), puis à l'intérieur de celui-ci créer deux répertoires : **src** et **build**. Le répertoire **src** contiendra tous vos codes sources (fichiers cpp et h) et un fichier CMakeLists.txt. Le répertoire racine du projet contiendra le fichier **maitre** CMakeLists.txt qui appellera les autres.

Pour construire votre projet, il suffit alors de se déplacer dans le répertoire **build** puis de lancer la commande `cmake ..` puis la commande `make`. L'exécutable se trouve alors dans le répertoire *src* du répertoire **build** et porte le nom du projet indiqué dans le CMakeLists.txt maitre.

Exemple de fichier CMakeLists.txt maitre pour un projet utilisant la bibliothèque SFML:

```CMake
cmake_minimum_required(VERSION 3.1)
project(CMAKEDEMO_SFML)

set(CMAKE_CXX_FLAGS "-g -Wall")
add_subdirectory(src)
```

Exemple de fichier CMakeLists.txt dans le répertoire **src**:

```CMake
find_package(SFML 2.5 COMPONENTS graphics window REQUIRED)
add_executable(Fondu2cercles Fondu2cercles.cpp)
target_link_libraries(Fondu2cercles sfml-graphics sfml-window) 
```
